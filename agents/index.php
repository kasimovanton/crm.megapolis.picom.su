<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Агенты");
?>

<?/*$APPLICATION->IncludeComponent("picom:forum.user.list", "", array(
	"SHOW_USER_STATUS" => "Y",
	"URL_TEMPLATES_MESSAGE_SEND" => "message_send.php?TYPE=#TYPE#&UID=#UID#",
	"URL_TEMPLATES_PM_EDIT" => "pm_edit.php?FID=#FID#&MID=#MID#&UID=#UID#&mode=#mode#",
	"URL_TEMPLATES_PROFILE_VIEW" => "profile_view.php?UID=#UID#",
	"URL_TEMPLATES_USER_POST" => "user_post.php?UID=#UID#&mode=#mode#",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "0",
	"USERS_PER_PAGE" => "3",
	"SET_NAVIGATION" => "Y",
	"DATE_FORMAT" => "d.m.Y",
	"DATE_TIME_FORMAT" => "d.m.Y H:i:s",
	"PAGE_NAVIGATION_TEMPLATE" => "",
	"SET_TITLE" => "Y",
	"SEO_USER" => "Y"
	),
	false
);*/?>

<?php
    if (isset($_SESSION['boss'])){
        $prop_boss = $_SESSION['boss'];
    } else {
        $prop_boss = "all";
    }    
    if (isset($_SESSION['agent_page'])){
        $agent_page = $_SESSION['agent_page'];
    } else {
        $agent_page = 15;
    }    
    if (isset($_SESSION['alf_filter'])){
        $alf_filter = $_SESSION['alf_filter'];
    } else {
        $alf_filter = false;
    }
?>
<?

$APPLICATION->IncludeComponent("picom:users", ".default", array(
	"USERS_COUNT" => $agent_page,
	"SEF_MODE" => "Y",
	"SEF_FOLDER" => "/agents/",
	"BOSS_CHANGE" => $prop_boss,
	"ALF_FILTER" => $alf_filter,
	"DETAIL_PAGER_TEMPLATE" => "",
	"SEF_URL_TEMPLATES" => array(
		"users" => "",
		"detail" => "#ELEMENT_ID#/",
	)
	),
	false
);


?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>