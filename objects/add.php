<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");

if (!isset($_REQUEST['CODE'])){
    $title = "Добавить объект";
} else {
    $title = "Редактировать объект";
}

$APPLICATION->SetTitle($title);

$userStatus = getUserStatus($USER->GetID());
if (!in_array(9, $userStatus) && !($USER->IsAdmin())){
    echo 'У вас нет прав доступа к этому разделу';
    exit();
}

?>
<?$APPLICATION->IncludeComponent("picom:obj.form", ".default", array(
	"SEND_EMAIL" => "N",
	"IBLOCK_TYPE" => "objects",
	"IBLOCK_ID" => "16",
	"STATUS_NEW" => "N",
	"LIST_URL" => "",
	"USE_CAPTCHA" => "N",
	"USER_MESSAGE_EDIT" => "",
	"USER_MESSAGE_ADD" => "",
	"DEFAULT_INPUT_SIZE" => "30",
	"RESIZE_IMAGES" => "N",
	"PROPERTY_CODES" => array(
		0 => "NAME",
		1 => "DETAIL_TEXT",
		2 => "FIRST_CONTACT",
		3 => "OPEN_DATA",
		4 => "Client",
		5 => "City",
		6 => "District",
		7 => "Street",
		8 => "HOUSE",
		9 => "APARTMENT",
		10 => "Rooms",
		11 => "Floor",
		12 => "Floors",
		13 => "PRICE",
		14 => "MarketType",
		15 => "Square",
		16 => "HouseType",
		17 => "STATUS",
		18 => "CURRENT_DATE_OPTION",
		19 => "CURRENT_COMMENT_OPTION",
		20 => "WATCHER",
	),
	"PROPERTY_CODES_REQUIRED" => array(
		0 => "FIRST_CONTACT",
		1 => "Client",
		2 => "City",
		3 => "Street",
		4 => "HOUSE",
		5 => "Rooms",
		6 => "Floor",
		7 => "PRICE",
		8 => "STATUS",
        9 => "DETAIL_TEXT",
        10 => "HouseType",
	),
    "PROPERTY_CODES_SOME_REQUIRED" => array(
        0 => "District",
        1 => "CURRENT_DATE_OPTION",
        2 => "CURRENT_COMMENT_OPTION",
    ),
	"GROUPS" => array(
		0 => "1",
		1 => "9",
	),
	"STATUS" => "ANY",
	"ELEMENT_ASSOC" => "PROPERTY_ID",
	"ELEMENT_ASSOC_PROPERTY" => "AUTHOR",
	"MAX_USER_ENTRIES" => "100000",
	"MAX_LEVELS" => "100000",
	"LEVEL_LAST" => "Y",
	"MAX_FILE_SIZE" => "0",
	"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
	"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
	"SEF_MODE" => "N",
	"SEF_FOLDER" => "/objects/",
	"CUSTOM_TITLE_NAME" => "",
	"CUSTOM_TITLE_TAGS" => "",
	"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
	"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
	"CUSTOM_TITLE_IBLOCK_SECTION" => "",
	"CUSTOM_TITLE_PREVIEW_TEXT" => "",
	"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
	"CUSTOM_TITLE_DETAIL_TEXT" => "",
	"CUSTOM_TITLE_DETAIL_PICTURE" => "",
	"FORM_CLASS" => "content",
	"ROW_CLASS" => "form_row",
	"SUBMIT_CLASS" => "button add_object_btn",
	"CUSTOM_CLASS_NAME" => "",
	"CUSTOM_CLASS_TAGS" => "",
	"CUSTOM_CLASS_DATE_ACTIVE_FROM" => "",
	"CUSTOM_CLASS_DATE_ACTIVE_TO" => "",
	"CUSTOM_CLASS_IBLOCK_SECTION" => "",
	"CUSTOM_CLASS_PREVIEW_TEXT" => "",
	"CUSTOM_CLASS_PREVIEW_PICTURE" => "",
	"CUSTOM_CLASS_DETAIL_TEXT" => "",
	"CUSTOM_CLASS_DETAIL_PICTURE" => "",
	"CUSTOM_CLASS_FIRST_CONTACT" => "",
	"CUSTOM_CLASS_OPEN_DATA" => "",
	"CUSTOM_CLASS_Client" => "",
	"CUSTOM_CLASS_District" => "",
	"CUSTOM_CLASS_Street" => "",
	"CUSTOM_CLASS_HOUSE" => "",
	"CUSTOM_CLASS_APARTMENT" => "",
	"CUSTOM_CLASS_Rooms" => "",
	"CUSTOM_CLASS_Floor" => "",
	"CUSTOM_CLASS_Floors" => "",
	"CUSTOM_CLASS_PRICE" => "",
	"CUSTOM_CLASS_MarketType" => "",
	"CUSTOM_CLASS_Square" => "",
	"CUSTOM_CLASS_HouseType" => "",
	"CUSTOM_CLASS_STATUS" => "",
	"CUSTOM_CLASS_CURRENT_DATE_OPTION" => "",
	"CUSTOM_CLASS_CURRENT_COMMENT_OPTION" => "",
	"CUSTOM_CLASS_WATCHER" => "",
	"CUSTOM_CLASS_IS_WATCH" => "",
	"CUSTOM_CLASS_AUTHOR" => ""
	),
	false
);?> 
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>