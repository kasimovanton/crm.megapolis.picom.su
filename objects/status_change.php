<?php
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');
    CModule::IncludeModule("iblock");

	function strip_data($text) {
        $text = strip_tags($text);
        $text = htmlspecialchars($text);
        $text = mysql_escape_string($text);
                
        return $text;
    }
	
	$newStatus = strip_data($_REQUEST['new_status']);
	$newDate = strip_data($_REQUEST['date_time']);
	$newComment = strip_data($_REQUEST['comment']);
	$objectId = strip_data($_REQUEST['object_id']);
	$error = false;
	if (empty($newStatus) || empty($objectId)){
		$error = true;
		$text_error = "Не заполнены все поля";
	}
	if (!$error){
		$arDateTime = explode(" ", $newDate);
		switch($newStatus){
			  case 8940:
			  case 8962:
				if(empty($arDateTime[1]) || $arDateTime[1] == "00:00:00"){
					$error = true;
					$text_error = "Не заполнено время";
				}
			  case 8963:
			  case 8964:
			  case 8965:
			  case 8966:
			  case 8967:
			  case 8968:
			  case 8971:
			  case 8972:
			  case 8973:
			  case 8975:
			  case 11107:
				if(empty($arDateTime[0])){
					$error = true;
					$text_error = "Не заполнена дата";
				}
				break;
			  case 8977:
			  case 8978:
			  case 8979:
              case 11106:
              case 11377:
				if(empty($newComment)){
					$error = true;
					$text_error = "Не заполнен комментарий";
				}
				break;
			  default:
				$error = true;
				$text_error = "Неизвесный статус";
		}
	}
	if (!$error){
		$IBLOCK_ID = 16; 
		
		$arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "ID" =>$objectId);
		$arOrder = Array("NAME"=>"ASC");
		$arSelect = Array("ID", "NAME", "IBLOCK_ID");

		$list = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
		if($ob = $list->GetNextElement()){
			$arFields = $ob->GetFields();
			CIBlockElement::SetPropertyValues($arFields['ID'], $IBLOCK_ID, $newStatus, "STATUS");
			if(!empty($newDate)){
				CIBlockElement::SetPropertyValues($arFields['ID'], $IBLOCK_ID, $newDate, "CURRENT_DATE_OPTION");
			}
			if(!empty($newComment)){
				CIBlockElement::SetPropertyValues($arFields['ID'], $IBLOCK_ID, $newComment, "CURRENT_COMMENT_OPTION");
			}
			
			echo json_encode(array('error' => $error, 'text' =>"Статус объекта изменен успешно!"), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
		} else {
			echo json_encode(array('error' => $error, 'text' => "Объект не найден!"), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
		}
		

	} else {
		echo json_encode(array('error' => $error, 'text' => $text_error), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
	}
	

require($bxRoot.'modules/main/include/epilog_after.php');
?>
