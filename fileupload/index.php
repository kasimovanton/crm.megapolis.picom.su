<?php
/*
 * jQuery File Upload Plugin PHP Example 5.14
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

error_reporting(E_ALL | E_STRICT);

$names = array_keys($_FILES);
$name = array_shift($names);
//echo $name;

require('UploadHandler.php');
$upload_handler = new UploadHandler(array('param_name' => $name));
