<?
$_SERVER["DOCUMENT_ROOT"] = "/home/bitrix/ext_www/crm.megapolis18.ru";
$_SERVER['SERVER_NAME'] = "crm.megapolis18.ru";
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(CModule::IncludeModule('iblock')){
    if (file_exists($_SERVER["DOCUMENT_ROOT"].'/advertisment.xml') && CModule::IncludeModule('iblock')){
        $xml=new SimpleXMLElement('https://autoload.avito.ru/format/New_developments.xml', NULL, TRUE);

        $arObjectsDev = array();
        foreach ($xml->children() as $arRegions) {
            if ($arRegions['name'] == '��������'){
                foreach($arRegions->children() as $arCity){
                
                    foreach($arCity->children() as $arObject){
                        
                        $haveHouse = false;
                        foreach($arObject->children() as $arHousing){
                            $haveHouse = true;
                            $arObjectsDev[] = array(
                                                    'city' => $arCity['name'].'',
                                                    'developer' => $arObject['developer'].'',
                                                    'name' => $arObject['name'].', '.$arHousing['name'],
                                                    'id' => $arHousing['id'].'',
                                            );
                        }
                        
                        if(!$haveHouse){
                            $arObjectsDev[] = array(
                                                    'city' => $arCity['name'].'',
                                                    'developer' => $arObject['developer'].'',
                                                    'name' => $arObject['name'].'',
                                                    'id' => $arObject['id'].'',
                                            );
                        }
                    }
                }
            }
        }
        
        $IBlock = 5;
        $arFilter = array(
            "city",
        );
        $additionalProperty = getEnumValues($IBlock,$arFilter);
        
        foreach($arObjectsDev as $devObject){
            $IBLOCK_ID = 20; 
            $arOrder = Array("ID"=>"ASC");
            $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_*");
            $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "NAME" => $devObject['id'], "CHECK_PERMISSIONS" => "N");
            $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
            if ($ob = $res->GetNextElement()) {
                $arFields = $ob->GetFields();
                $arFields['prop'] = $ob->GetProperties();
                if (!empty($arFields['ID'])){
                    if ($arFields['prop']['NAME']['VALUE'] != $devObject['name']){
                        $el = new CIBlockElement;
                        $arLoadProperties = array('NAME' => $devObject['name']);
                        CIBlockElement::SetPropertyValuesEx($arFields['ID'], $IBLOCK_ID, $arLoadProperties);
                    }
                    if ($arFields['prop']['CITY']['VALUE'] != array_search($devObject['city'], $additionalProperty['City'])){
                        $arLoadProperties = array('CITY' => array_search($devObject['city'], $additionalProperty['City']));
                        CIBlockElement::SetPropertyValuesEx($arFields['ID'], $IBLOCK_ID, $arLoadProperties);
                    }
                    if ($arFields['prop']['DEVELOPER']['VALUE'] != $devObject['developer']){
                        $arLoadProperties = array('DEVELOPER' => $devObject['developer']);
                        CIBlockElement::SetPropertyValuesEx($arFields['ID'], $IBLOCK_ID, $arLoadProperties);
                    }
                }
            } else {
                $el = new CIBlockElement;

                $PROP = array();
                $PROP["CITY"] = array_search($devObject['city'], $additionalProperty['City']);
                $PROP["NAME"] = $devObject['name'];
                $PROP["DEVELOPER"] = $devObject['developer'];

                $arLoadProductArray = Array(
                    "MODIFIED_BY"    => $USER->GetID(),
                    "IBLOCK_SECTION_ID" => false,
                    "IBLOCK_ID"      => $IBLOCK_ID,
                    "PROPERTY_VALUES"=> $PROP,
                    "NAME"           => $devObject['id'],
                    "ACTIVE"         => "Y",
                );

                if($PRODUCT_ID = $el->Add($arLoadProductArray)){
                    echo $PRODUCT_ID;
                } else {
                    echo "Error: ".$el->LAST_ERROR;
                }
            }
            
        }
    }
}