<?php
AddEventHandler('main', 'OnUserTypeBuildList', array('CUserTypeIBlockProperty', 'GetUserTypeDescription'), 5000);

if (!class_exists('CUserTypeIBlockProperty')) {
	class CUserTypeIBlockProperty
	{
		private static $PROP_TYPES = array(
			'PROPS'  => 'Свойства',
			'FIELDS' => 'Поля',
			'ALL'    => 'Свойства и поля'
		);

		function GetUserTypeDescription()
		{
			return array(
				'USER_TYPE_ID' => 'property',
				'CLASS_NAME'   => 'CUserTypeIBlockProperty',
				'DESCRIPTION'  => 'Привязка к свойствам инфоблоков',
				'BASE_TYPE'    => 'string',
			);
		}

		function GetDBColumnType($arUserField)
		{
			global $DB;
			switch(strtolower($DB->type))
			{
				case 'mysql':
					return 'varchar(50)';
				case 'oracle':
					return 'text';
				case 'mssql':
					return 'text';
			}
		}

		function PrepareSettings($arUserField)
		{
			$iBlockTypeID = trim($arUserField['SETTINGS']['IBLOCK_TYPE_ID']);
			$iBlockID     = intval($arUserField['SETTINGS']['IBLOCK_ID']);
			$propsType    = trim($arUserField['SETTINGS']['PROPERTIES_TYPE']);
			$listSize     = intval($arUserField['SETTINGS']['LIST_SIZE']);

			$arPropTypes = array_keys(self::$PROP_TYPES);

			return array(
				'IBLOCK_TYPE_ID'  => $iBlockTypeID,
				'IBLOCK_ID'       => max($iBlockID, 0),
				'PROPERTIES_TYPE' => in_array($propsType, $arPropTypes) ? $propsType : $arPropTypes[0],
				'LIST_SIZE'       => max($listSize, 1),
			);
		}

		function GetSettingsHTML($arUserField = false, $arHtmlControl, $bVarsFromForm)
		{
			$n = $arHtmlControl['NAME'];
			$arPropTypes = self::$PROP_TYPES;
			$arPropTypesIDs = array_keys($arPropTypes);

			if($bVarsFromForm)              $iBlockType = trim($GLOBALS[$arHtmlControl['NAME']]['IBLOCK_TYPE_ID']);
			elseif (is_array($arUserField)) $iBlockType = trim($arUserField['SETTINGS']['IBLOCK_TYPE_ID']);
			else                            $iBlockType = '';

			if($bVarsFromForm)              $iBlockID = intval($GLOBALS[$arHtmlControl['NAME']]['IBLOCK_ID']);
			elseif (is_array($arUserField)) $iBlockID = intval($arUserField['SETTINGS']['IBLOCK_ID']);
			else                            $iBlockID = 0;

			if($bVarsFromForm)              $propsType = trim($GLOBALS[$arHtmlControl['NAME']]['PROPERTIES_TYPE']);
			elseif (is_array($arUserField)) $propsType = trim($arUserField['SETTINGS']['PROPERTIES_TYPE']);
			else                            $propsType = $arPropTypesIDs[0];

			if($bVarsFromForm)              $listSize = intval($GLOBALS[$arHtmlControl['NAME']]['LIST_SIZE']);
			elseif (is_array($arUserField)) $listSize = intval($arUserField['SETTINGS']['LIST_SIZE']);
			else                            $listSize = 0;

			if (!CModule::IncludeModule('iblock')) {
				return '<tr><td colspan="2">Модуль инфоблоков не подключён.</td></td>';
			}

			$html = '';

			$arTypes = CIBlockParameters::GetIBlockTypes();
			$arIBlockTypesHTML        = array( '"":{"":"(выберите инфоблок)"}' );
			$arIBlockTypesOptionsHTML = array( '<option selected="" value="">(выберите тип)</option>' );
			$arIBlockOptionsHTML      = array();
			foreach ($arTypes as $id => $type) {
				$typeSelected = $iBlockType === $id ? ' selected="Y"' : '';
				$arIBlocksHTML = array( '"":"(выберите инфоблок)"' );

				$arOrder    = array( 'SORT' => 'ASC', 'NAME' => 'ASC' );
				$arFilter   = array( 'TYPE' => $id );
				$resIBlocks = CIBlock::GetList($arOrder, $arFilter);
				while ($arIBlock = $resIBlocks->GetNext()) {
					$arIBlocksHTML[] = '"'.$arIBlock['ID'].'":"'.$arIBlock['NAME'].' ['.$arIBlock['ID'].']"';

					if ($typeSelected !== '') {
						$iBlockSelected = $iBlockID === $arIBlock['ID'] ? ' selected="Y"' : '';
						$arIBlockOptionsHTML[] = 
							'<option value="'.$arIBlock['ID'].'"'.$iBlockSelected.'>'.
								$arIBlock['NAME'].' ['.$arIBlock['ID'].']'.
							'</option>';
					}
				}
				$arIBlockTypesHTML[]        = '"'.$id.'":{'.implode(',', $arIBlocksHTML).'}';
				$arIBlockTypesOptionsHTML[] = '<option value="'.$id.'"'.$typeSelected.'>'.$type.'</option>';
			}
			$arIBlockOptionsHTML[] = '<option value="">(выберите инфоблок)</option>';

			$arPropsTypeOptionsHTML = array();
			foreach ($arPropTypes as $id => $type) {
				$selected = $id === $propsType ? ' selected="Y"' : '';
				$arPropsTypeOptionsHTML[] = '<option value="'.$id.'"'.$selected.'>'.$type.'</option>';
			}

			$listSize = max($listSize, 1);

			$html .= 
				'<tr>'.
					'<td>'.
						'Инфоблок:'.
					'</td>'.
					'<td>'.
						'<script type="text/javascript">'.
							'function OnTypeChanged(typeSelect, iblockSelectID) {'.
								'var arIBlocks = {'.implode(',', $arIBlockTypesHTML).'};'.
								'var iblockSelect = document.getElementById(iblockSelectID);'.
								'if (iblockSelect) {'.
									'for (var i=iblockSelect.length-1; i >= 0; i--) {'.
										'iblockSelect.remove(i);'.
									'}'.
									'var n = 0;'.
									'for (var j in arIBlocks[typeSelect.value]) {'.
										'var newoption = new Option(arIBlocks[typeSelect.value][j], j, false, false);'.
										'iblockSelect.options[n]=newoption;'.
										'n++;'.
									'}'.
								'}'.
							'}'.
						'</script>'.
						'<select class="adm-detail-iblock-types" onchange="OnTypeChanged(this, \''.$n.'[IBLOCK_ID]\')" name="'.$n.'[IBLOCK_TYPE_ID]" id="'.$n.'[IBLOCK_TYPE_ID]">'.
							implode('', $arIBlockTypesOptionsHTML).
						'</select>'.
						'<select class="adm-detail-iblock-list" id="'.$n.'[IBLOCK_ID]" name="'.$n.'[IBLOCK_ID]">'.
							implode('', $arIBlockOptionsHTML).
						'</select>'.
					'</td>'.
				'</tr>'.
				'<tr>'.
					'<td>'.
						'Отображать:'.
					'</td>'.
					'<td>'.
						'<select id="'.$n.'[PROPERTIES_TYPE]" name="'.$n.'[PROPERTIES_TYPE]">'.
							implode('', $arPropsTypeOptionsHTML).
						'</select>'.
					'</td>'.
				'</tr>'.
				'<tr>'.
					'<td>'.
						'Высота списка:'.
					'</td>'.
					'<td>'.
						'<input '.
							'type="text" '.
							'id="'.$n.'[LIST_SIZE]" '.
							'name="'.$n.'[LIST_SIZE]" '.
							'value="'.$listSize.'" '.
							'size="5" '.
						'/>'.
					'</td>'.
				'</tr>';

			return $html;
		}

		function GetEditFormHTML($arUserField, $arHtmlControl) { 
            return ''; 
        }

		function GetEditFormHTMLMulty($arUserField, $arHtmlControl)
		{
			if (!CModule::IncludeModule('iblock')) {
				return Err('Модуль инфоблоков не подключён.');
			}

			$iBlockID  = $arUserField['SETTINGS']['IBLOCK_ID'];
			$propsType = $arUserField['SETTINGS']['PROPERTIES_TYPE'];
			$listSize  = $arUserField['SETTINGS']['LIST_SIZE'];
			$arValue   = $arUserField['VALUE'];

			if ($iBlockID <= 0) {
				return Err('Не задан инфоблок.');
			}

			$arPropsOptionsHTML = array();

			$arPropsOptionsHTML[] = '<option value="">(Не выбрано)</option>';

			if ($propsType === 'FIELDS' || $propsType === 'ALL') {
				$arFields = CIBlock::GetFields($iBlockID);
				foreach ($arFields as $code => $arField) {
					$selected = in_array($code, $arValue) ? ' selected="Y"' : '';
					$arPropsOptionsHTML[] = 
						'<option value="'.$code.'"'.$selected.'>'.
							$arField['NAME'].
						'</option>';
				}
			}

			if ($propsType === 'PROPS' || $propsType === 'ALL') {
				$arOrder  = array( 'SORT' => 'ASC' );
				$arFilter = array( 'IBLOCK_ID' => $iBlockID, 'ACTIVE' => 'Y' );
				$resProps = CIBlockProperty::GetList($arOrder, $arFilter);
				while ($arProp = $resProps->GetNext()) {
					$value    = 'PROPERTY_'.$arProp['CODE'];
					$selected = in_array($value, $arValue) ? ' selected="Y"' : '';
					$arPropsOptionsHTML[] = 
						'<option value="'.$value.'"'.$selected.'>'.
							$arProp['NAME'].' ['.$arProp['CODE'].']'.
						'</option>';
				}
			}

			$html = 
				'<select '.
					'name="'.$arHtmlControl['NAME'].'" '.
					'id="'.$arHtmlControl['NAME'].'" '.
					'multiple="Y" '.
					'size="'.$listSize.'" '.
				'>'.
					implode('', $arPropsOptionsHTML).
				'</select>'.
				'<!-- value: ['.implode(', ', $arValue).'] -->';

			return $html;
/*
			if($arUserField['ENTITY_VALUE_ID']<1)
				$arHtmlControl['VALUE'] = intval($arUserField['SETTINGS']['DEFAULT_VALUE']);
			switch($arUserField['SETTINGS']['DISPLAY'])
			{
				case 'DROPDOWN':
					$arHtmlControl['VALIGN'] = 'middle';
					return '
						<select name=''.$arHtmlControl['NAME'].''>
						<option value='1''.($arHtmlControl['VALUE']? ' selected': '').'>'.GetMessage('MAIN_YES').'</option>
						<option value='0''.(!$arHtmlControl['VALUE']? ' selected': '').'>'.GetMessage('MAIN_NO').'</option>
						</select>
					';
				case 'RADIO':
					return '
						<label><input type='radio' value='1' name=''.$arHtmlControl['NAME'].'''.($arHtmlControl['VALUE']? ' checked': '').'>'.GetMessage('MAIN_YES').'</label><br>
						<label><input type='radio' value='0' name=''.$arHtmlControl['NAME'].'''.(!$arHtmlControl['VALUE']? ' checked': '').'>'.GetMessage('MAIN_NO').'</label>
					';
				default:
					$arHtmlControl['VALIGN'] = 'middle';
					return '
						<input type='hidden' value='0' name=''.$arHtmlControl['NAME'].''>
						<input type='checkbox' value='1' name=''.$arHtmlControl['NAME'].'''.($arHtmlControl['VALUE']? ' checked': '').'>
					';
			}
*/
		}
/*
		function GetFilterHTML($arUserField, $arHtmlControl)
		{
			return '
				<select name=''.$arHtmlControl['NAME'].''>
				<option value='''.(strlen($arHtmlControl['VALUE'])<1? ' selected': '').'>'.GetMessage('MAIN_ALL').'</option>
				<option value='1''.($arHtmlControl['VALUE']? ' selected': '').'>'.GetMessage('MAIN_YES').'</option>
				<option value='0''.(strlen($arHtmlControl['VALUE'])>0 && !$arHtmlControl['VALUE']? ' selected': '').'>'.GetMessage('MAIN_NO').'</option>
				</select>
			';
		}

		function GetAdminListViewHTML($arUserField, $arHtmlControl)
		{
			if($arHtmlControl['VALUE'])
				return GetMessage('MAIN_YES');
			else
				return GetMessage('MAIN_NO');
		}

		function GetAdminListEditHTML($arUserField, $arHtmlControl)
		{
			return '
				<input type='hidden' value='0' name=''.$arHtmlControl['NAME'].''>
				<input type='checkbox' value='1' name=''.$arHtmlControl['NAME'].'''.($arHtmlControl['VALUE']? ' checked': '').'>
			';
		}

		function OnBeforeSave($arUserField, $value)
		{
			if($value)
				return 1;
			else
				return 0;
		}
*/
	}
}