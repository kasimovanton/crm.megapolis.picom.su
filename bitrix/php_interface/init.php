<?
function strip_data($text) {
    $text = strip_tags($text);
    $text = htmlspecialchars($text);
    $text = mysql_escape_string($text);
            
    return $text;
}

function getSectionProps($iblock, $sec_id, $prop_array){
    $arOrder = Array("SORT"=>"ASC");
    $arSelect = Array("ID", "NAME", "IBLOCK_SECTION_ID", "IBLOCK_ID", "CODE", "UF_*");
    if (isset($prop_array) && !empty($prop_array)){
        $arSelect = array_merge($arSelect, $prop_array);
    }
    $arFilter = Array("IBLOCK_ID"=>$iblock, "ACTIVE"=>"Y", "CHECK_PERMISSIONS" => "N", "ID" =>$sec_id);
    $res_sec = CIBlockSection::GetList($arOrder, $arFilter, true, $arSelect);
    $ob = $res_sec->GetNextElement();
    $ar_res = $ob->GetFields();
    return $ar_res;

}

//Трансформация даты в UNIX-формат
function timeExplode($strDate)
{
    $arDateTime = explode(" ", $strDate);
    if (!isset($arDateTime[1])){
        $arDateTime[1] = "00:00";
    }
    $arDate = explode(".", $arDateTime[0]);
    $arTime = explode(":", $arDateTime[1]);
                
    return mktime($arTime[0], $arTime[1], $arTime[2], $arDate[1], $arDate[0], $arDate[2]);
}

//Получает объект истории событий элемента elementID
function getHistoryObject($elementID){
    CModule::IncludeModule("iblock");
    $IBLOCK_ID = 21; 
    global $USER;

    $arOrder = Array("ID"=>"DESC");
    $arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_*");
    $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "PROPERTY_ELEMENT_ID"=>$elementID);
    
    
    $list = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
    
    return $list;
}

//Список клиентов с отсутствующими активными потребностями
function getClientIDListWithNeeds(){
    $response = array();

    $arOrder = Array("ID"=>"ASC");
    $arFilter = Array("IBLOCK_ID"=>10, "ACTIVE"=>"Y", "ID"=>CIBlockElement::SubQuery("PROPERTY_Client", array(
                                                "IBLOCK_ID" => 5,
                                                "ACTIVE"=>"Y",
                                                "!PROPERTY_IS_NEED"=>false
                                              )),);
    $arSelect = Array("ID", "NAME");
    $list = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
    while ($ob = $list->GetNextElement()) {
        $arFields = $ob->GetFields();
        $response[] = $arFields['ID'];
    }
    return $response;
}
//Возвращает количество активных потребностей клиента
function getActiveNeeds($clientID){
    $arOrder = Array("ID"=>"ASC");
    $arFilter = Array("IBLOCK_ID"=>5, "ACTIVE"=>"Y", "!PROPERTY_IS_NEED"=>false, "PROPERTY_Client"=>$clientID);
    $arSelect = Array("ID", "NAME");
    $list = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
    return $list->SelectedRowsCount();
}

function getVariatsList($needID, $isObject=false){
    $arResponse = array();
    $arFilter = Array("IBLOCK_ID"=>19, "ACTIVE"=>"Y", "PROPERTY_NEEDS"=>$needID);
    if(!$isObject){
        $arFilter['PROPERTY_OBJECT'] = false;
    } else {
        $arFilter['!PROPERTY_OBJECT'] = false;
    }
    $arOrder = Array("ID"=>"ASC");
    $arSelect = Array("ID", "NAME", "PROPERTY_ADVERT", "PROPERTY_OBJECT");
    $list = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
    while ($ob = $list->GetNextElement()) {
        $arFields = $ob->GetFields();
        if(!$isObject){
            $arResponse[] = $arFields['PROPERTY_ADVERT_VALUE'];
        } else {
            $arResponse[] = $arFields['PROPERTY_OBJECT_VALUE'];
        }
    }
    return $arResponse;
}

//проверка выбора закрепленного варианта
function checkNeedChoice($needID){
    $arCheckOrder = Array("ID"=>"DESC");
    $arCheckSelect = Array("ID", "NAME", "IBLOCK_ID", "ACTIVE_FROM", "DETAIL_TEXT", "CREATED_BY", "PROPERTY_*");
    $arCheckFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "PROPERTY_NEED_BY" => $needID);
    $arCheck = CIBlockElement::GetList($arCheckOrder, $arCheckFilter, false, false, $arCheckSelect);
    $needHasChoice = false;
    if ($obCheck = $arCheck->GetNextElement()) {
        $arFields = $obCheck->GetFields();
        $needHasChoice = $arFields['ID'];
    }
    return $needHasChoice;
}

//Получает телефон call-center
function checkClientUniqNumber($phoneNumber){
    CModule::IncludeModule("iblock");

    $IBLOCK_ID = 10; 
    $number = strip_data($phoneNumber);
    
    $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y");
    $arOrder = Array("NAME"=>"ASC");
    $arSelect = Array("ID", "NAME", "IBLOCK_ID",);
    $new_array = array();
    $new_array["LOGIC"] = "OR";
    $new_array[] = array('PROPERTY_PHONE'=>$number);
    $new_array[] = array('PROPERTY_DOP_PHONE'=>$number);
    $arFilter[] = $new_array;
    $list = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

    if($ob = $list->GetNextElement()){
        return false;
    } else {
        return true;
    }
}
//Получает телефон call-center
function getCallcenterNumber(){
    $callcenter = CIBlockElement::GetProperty(12, 2658, "sort", "asc", array());
    if ($ob_arr = $callcenter->GetNext()){
        return trim($ob_arr['VALUE']);
    } else {
        return false;
    }
    
}
//Выбор раздела, противоположного выбранному, для работы итерационного поиска
function getRevSect($iblock, $isect, $flag){
    $arOrder = Array("SORT"=>"ASC");
    $arSelect = Array("ID", "NAME", "IBLOCK_ID", "CODE", "UF_*");
    $arFilter = Array("IBLOCK_ID"=>$iblock, "ACTIVE"=>"Y", "ID" =>$isect);
    $res_sec = CIBlockSection::GetList($arOrder, $arFilter, true, $arSelect);
    $ob = $res_sec->GetNextElement();
    $ar_res = $ob->GetFields();

    $res = CIBlockSection::GetByID($ar_res['UF_REVERSE']);
    $ar_res = $res->GetNext();

    if (isset($flag) && $flag){
        return $ar_res['ID'];
    } else {
        return $ar_res['CODE'];
    }
    
}

// function infoVerification() {
// }

require 'user_types/UserTypeIBlockProperty.php';
require 'lib/UserFunc.php';
require 'lib/Handlers.php';


function pre($arr,$visible = true) {
    if ($visible){
        echo '<pre>';
    } else {
        echo '<pre style="display:none">';
    }
    print_r($arr);
    echo '</pre>';
}

function getUsers(
    $select = array('UF_HEAD'),
    $fields = array('ID'),
    $filter = array('GROUPS_ID'=>6,'ACTIVE'=>'Y'),
    $vBy = 'id',
    $vOrder = 'asc')
{
// pre($filter);
    $rsUsers = CUser::GetList(
        ($by=$vBy), ($order=$vOrder),
        $filter,
        array('SELECT'=>$select,'FIELDS' => $fields)
    );//array('ID','LOGIN','LAST_NAME','SECOND_NAME','NAME')
    while ($user = $rsUsers->GetNext()) {
        if (!$user['UF_HEAD']) $user['UF_HEAD']=0;
        $users[$user['ID']] = $user;
    }
    return $users;
}

//Если задан параметр $noRec, то функция возвращает только прямых потомков (подчиненных), без рекурссии
//Если задан параметр $withTree, то функция возвращает массив с учетом уровней потомков(подчиненных). Параметр рекурсии $noRec не влияет на результат.
// if($withTree) return array([USER_ID] => [lvlTree])
function getChildsId($parentId,$users = false, $noRec = false, $withTree = false, $lvl = 0) {
    if (!$users) $users = getUsers();
    $childs=array();
    if (!$withTree){
        foreach ($users as $user) {
            if ($user["UF_HEAD"]==$parentId) {
                //$childs[] = $user;
                $childs[] = $user['ID'];
                if (!$noRec){
                    $childs = array_merge($childs,getChildsId($user['ID'],$users));
                }
            }
        }
    } else {
        foreach ($users as $user) {
            if ($user["UF_HEAD"]==$parentId) {
            
                $childs[$user['ID']] = $lvl;
                $childs = $childs + getChildsId($user['ID'],$users, false, true, $lvl+1);
            }
        }
    }
    return $childs;
}

function getParentsId($childId,$users = false) {
    if (!$users) $users = getUsers();
    $parents=array();
    if ($users[$childId]['UF_HEAD']>0) {
        $parents[] = $users[$users[$childId]['UF_HEAD']]['ID'];
        if ($users[$users[$childId]['UF_HEAD']]['UF_HEAD']>0) {
            $parents = array_merge($parents,getParentsId($users[$childId]['UF_HEAD'],$users));
        }
    }
    return $parents;
}

//Функция выводит количество элементов инфоблока iblock с группированное по полю prop
//Возвращает справочник вида "значение свойства" => "количество элементов" с таким значением
function getArrayOfCntByProp($iblock, $prop, $addFilter=array()){

    $response = array();
    if(CModule::IncludeModule("iblock")) {
 
        $IBLOCK_ID = $iblock; 
     
        $arOrder = Array("SORT"=>"ASC");
        if (!empty($addFilter)){
            $arFilter = array_merge($addFilter, array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y"));
        } else {
            $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y");
        }
        $arGroup = Array($prop);

        $res = CIBlockElement::GetList($arOrder, $arFilter, $arGroup);
        
        while($ob = $res->GetNextElement()){
            $arFields = $ob->GetFields();
            if(!empty($arFields[$prop.'_VALUE'])){
                $response[$arFields[$prop.'_VALUE']] = $arFields['CNT'];
            }
        }
    }
    return $response;
}


//Функция возвращает значения свойств прописанных в массиве $arFilter инфоблока $iblock
//Возвращает справочник вида "код свойства" => "список элементов" свойства
//Если установлен "флаг", то у списка элементов свойства в качестве ключа будет выступать XML_ID (свойство должно при этом иметь тип список)
function getEnumValues($iblock, $arFilter, $flag){

    CModule::IncludeModule('iblock');
    
    foreach($arFilter as $code){
        if ($code == 'emptyblock'){
            $response['emptyblock'] = array();
        } else {
            $res = CIBlock::GetProperties($iblock, Array(), Array("CODE" => $code));
            $res_arr = $res->Fetch();
            
            if ($flag){
                $ar_enum = CIBlockProperty::GetPropertyEnum($res_arr['ID'],Array("SORT"=>"asc"),Array());
                while ($ar_enum_list = $ar_enum->GetNext()){
                    $response[$res_arr['CODE']][$ar_enum_list['XML_ID']] = $ar_enum_list['VALUE'];
                }
                break;
            }
            
            switch ($res_arr['PROPERTY_TYPE']) {
                case 'L':         
                    $ar_enum = CIBlockProperty::GetPropertyEnum($res_arr['ID'],Array("SORT"=>"asc"),Array());
                    while ($ar_enum_list = $ar_enum->GetNext()){
                        $response[$res_arr['CODE']][$ar_enum_list['ID']] = $ar_enum_list['VALUE'];
                    } 
                    break;                
                case 'S':         
                    $order = array('sort' => 'asc');
                    $tmp = 'sort'; // параметр проигнорируется методом, но обязан быть
                    $rsUsers = CUser::GetList($order, $tmp);

                    while ($user_enum_list = $rsUsers->GetNext()){
                        $response[$res_arr['CODE']][$user_enum_list['ID']] = mb_substr(trim($user_enum_list['NAME']), 0, 1, "UTF-8").'. '.$user_enum_list['LAST_NAME'];
                    }

                    break;        
                case 'E':
                    $IBLOCK_ID = $res_arr['LINK_IBLOCK_ID']; 
                 
                    $arOrder = Array("SORT"=>"ASC");
                    $arSelect = Array("ID", "CODE", "NAME", "IBLOCK_ID");
                    $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y");
                    $list = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

                    while ($ob = $list->GetNextElement()){
                        $arFields = $ob->GetFields();
                        $response[$res_arr['CODE']][$arFields['ID']] = $arFields['NAME'];
                    }
                    break;
            }
        }
    }
    return $response;
}

//Возвращает значение элемента списка (из XML_ID)
function getValueOfEnum($iblock, $property, $value){
    $list = getEnumValues($iblock, array($property));
    return $list[$property][$value];
}

//Формирует из входного списка свойств, поля фильтра в виде html
function setHtmlSelectOption($enum_list) {
    foreach ($enum_list as $prop_key => $list){
        if ($prop_key == 'emptyblock') {
            $response .= '<div class="form_select emptyblock">';
            $response .= '<select>';
            $response .= '</select>';
            $response .= '</div>';
        } else {
            $iblock_prop = CIBlockProperty::GetByID($prop_key, 5, false);
            $iblock_prop = $iblock_prop->GetNext();
            
            $response .= '<div class="form_select">';
            $response .= '<select id="'.strtolower($prop_key).'" name="'.$prop_key.'">';
            $response .= '<option class="hidden" disabled="" selected="">'.$iblock_prop['NAME'].'</option>';
            // foreach ($list as $prop_id => $prop){
                // $response .= '<option value="'.$prop_id.'">'.$prop.'</option>';
            // }        
            foreach ($list as $prop_id => $prop){
                $response .= '<option value="'.$prop_id.'"';
                if ($_REQUEST[$prop_key] == $prop_id){
                    $response .= ' selected="y"';
                }
                $response .= ' >'.$prop.'</option>';
            }
            $response .= '</select>';
            $response .= '</div>';
        }
    }

    return $response;
}

//Формирует из входного списка свойств, поля фильтра в виде html (мультиселект)
function setHtmlMultiSelectOption($enum_list) {
    foreach ($enum_list as $prop_key => $list){
    
        $iblock_prop = CIBlockProperty::GetByID($prop_key, 5, false);
        $iblock_prop = $iblock_prop->GetNext();
     
        foreach ($list as $prop_id => $prop){
            $response .= '<option value="'.$prop_id.'"';
            if (in_array($prop_id, $_REQUEST[$prop_key])){
                $response .= ' selected';
            }
            $response .= ' >'.$prop.'</option>';
        }
    }

    return $response;
}

//Возвращает имя элемента, являющегося элементом инфоблоков
function getElemName($elem_id){
    $element = GetIBlockElement($elem_id);

    return $element['NAME'];
}


//Возвращает тип свойства по его переданному коду
function getPropType($iblock, $code){

    $res = CIBlock::GetProperties($iblock, Array(), Array("CODE" => $code));
    $res_arr = $res->Fetch();
        
    return $res_arr['PROPERTY_TYPE'];
}


//Возвращает всех связанных с элементов пользователей
function getClientUsers($elementId){

    $response = array();

    CModule::IncludeModule('iblock');
    $IBLOCK_ID = 10; 
    $arOrder = Array("SORT"=>"asc");
    $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "ID" => $elementId);
    $res = CIBlockElement::GetList($arOrder, $arFilter, false, false);
    if ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arProperties = $ob->GetProperties();
        if ($arProperties['RIELTOR']['VALUE']){
            $rsUser = CUser::GetByID($arProperties['RIELTOR']['VALUE']);
            $arUser = $rsUser->Fetch();
            $arUser['FULL_NAME'] = $arUser['LAST_NAME'].' '.$arUser['NAME'].' '.$arUser['SECOND_NAME'];
            $response['rieltor'] = $arUser;
        }
        if ($arProperties['AGENT']['VALUE']){
            $rsUser = CUser::GetByID($arProperties['AGENT']['VALUE']);
            $arUser = $rsUser->Fetch();
            $arUser['FULL_NAME'] = $arUser['LAST_NAME'].' '.$arUser['NAME'].' '.$arUser['SECOND_NAME'];
            $response['agent'] = $arUser;
        }
        if ($arProperties['MANAGER']['VALUE']){
            $rsUser = CUser::GetByID($arProperties['MANAGER']['VALUE']);
            $arUser = $rsUser->Fetch();
            $arUser['FULL_NAME'] = $arUser['LAST_NAME'].' '.$arUser['NAME'].' '.$arUser['SECOND_NAME'];
            $response['manager'] = $arUser;
        }
        if ($arProperties['OPERATOR']['VALUE']){
            $rsUser = CUser::GetByID($arProperties['OPERATOR']['VALUE']);
            $arUser = $rsUser->Fetch();
            $arUser['FULL_NAME'] = $arUser['LAST_NAME'].' '.$arUser['NAME'].' '.$arUser['SECOND_NAME'];
            $response['operator'] = $arUser;
        }
    }
    return $response;
}

//Выбор свойств для подбора в итерационном поиске
/*Добавит в поиск свойства, которые в группе имеют свойства *, *From, *To, *List */
function iterArray($complex) {
    if ($complex){
        return array("Price", "Square", "LandArea", "Floor", "Floors", "Rooms");
    } else {
        return array("City", "District");
    }
}

//Функция возвращает количество фотографий объектов
function getObjectsPhotoCount($arObjectsId) {

    $response = array();
    CModule::IncludeModule('iblock');
    $IBLOCK_ID = 16; 
    $arOrder = Array("SORT"=>"asc");
    $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "ID" => $arObjectsId);
    $res = CIBlockElement::GetList($arOrder, $arFilter, false, false);
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arProperties = $ob->GetProperties();
        if (is_array($arProperties['FILES_OBJECTS']['VALUE'])){
            $response[$arFields['ID']] = count($arProperties['FILES_OBJECTS']['VALUE']);
        }
    }
    
    return $response;
    
}

//Функция возвращает сообщение оповещения, в случаи его наличия и активности
function getAlertBlock() {

    $response = "";
    CModule::IncludeModule('iblock');
    $IBLOCK_ID = 14; 
    $arOrder = Array("SORT"=>"asc");
    $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", ">=DATE_ACTIVE_TO" => date("d.m.Y H:i:s"));
    $res = CIBlockElement::GetList($arOrder, $arFilter, false, Array ("nTopCount" => 1));
    if ($ob = $res->GetNextElement()) {
        $response = $ob->GetFields();
    }
    
    return $response;
    
}

//Вовзращает доступы пользователя к разделам сайта
function getUserStatus($userId) {

    $response = array();
    $count = tempClientCount($userId);
    if ($count){
        $response[] = 'reassign';
    }
    $watchCount = watchCount($userId);
    if ($watchCount){
        $response[] = 'watch';
    }
    $showCount = showCount($userId);
    if ($showCount){
        $response[] = 'show';
    }
    $response = array_merge(CUser::GetUserGroup($userId), $response);
    
    return $response;
    
}
//Возвращает количество объявлений агента на показ
//В случае $countResponse=false - возвращает массив ID объявлений
function showCount($userId, $countResponse=true) {

    CModule::IncludeModule('iblock');
    
    $arOrder = Array("SORT"=>"ASC");
    $arFilter = Array("IBLOCK_ID"=>5, "PROPERTY_Author" => $userId, "ID" => CIBlockElement::SubQuery("PROPERTY_ADVERT", array(
                                                "IBLOCK_ID" => 19,
                                              )),);
    $list = CIBlockElement::GetList($arOrder, $arFilter, false, false);

    $count = $list->SelectedRowsCount();
    if ($count){
        if ($countResponse){
            return $count;
        } else {
            $arResponse = array();
            while ($ob = $list->GetNextElement()){
                $arFields = $ob->GetFields();
                $arResponse[] = $arFields['ID'];
            }
            return $arResponse;
        }
    } else {
        return false;
    }
}
//Возвращает количество объектов агента на осмотр
function watchCount($userId) {

    CModule::IncludeModule('iblock');
    
    $arOrder = Array("SORT"=>"ASC");
    $arFilter = Array("IBLOCK_ID"=>16, "ACTIVE"=>"Y", "PROPERTY_WATCHER" => $userId);
    $list = CIBlockElement::GetList($arOrder, $arFilter, false, false);

    $count = $list->SelectedRowsCount();

    if ($count){
        return $count;
    } else {
        return false;
    }
}
//Возвращает количество временных клиентов для переданного агента(юзера)
function tempClientCount($userId) {
    CModule::IncludeModule('iblock');
    
    $arOrder = Array("SORT"=>"ASC");
    $arFilter = Array("IBLOCK_ID"=>10, "ACTIVE"=>"Y", "PROPERTY_RIELTOR"=> $userId, "!PROPERTY_REASSIGN" => false);
    $list = CIBlockElement::GetList($arOrder, $arFilter, false, false);

    $count = $list->SelectedRowsCount();

    if ($count){
        return $count;
    } else {
        return false;
    }
}

//Возращает максимальную цену переданного раздела
function getMaxSectPrice($sub, $type) {
    $IBLOCK_ID = 5; 
    $arOrder = Array("PROPERTY_Price"=>"desc", "PROPERTY_PriceTo"=>"desc");
    $arSelect = Array("ID", "NAME", "PROPERTY_Price");
    $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "SECTION_CODE" => $sub."_".$type);
    $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

    if ($ob = $res->GetNextElement()){
        $arFields = $ob->GetFields();
        if (!empty($arFields['PROPERTY_PRICE_VALUE'])){
            return array($arFields['PROPERTY_PRICE_VALUE'], false);
        } else {
            return array($arFields['PROPERTY_PRICETO_VALUE'], true);
        }
    } else {
        return 0;
    }
}
//Возращает максимальную цену раздела объекты
function getMaxObjectsPrice($userID) {
    $IBLOCK_ID = 16; 
    $arOrder = Array("PROPERTY_Price"=>"desc");
    $arSelect = Array("ID", "NAME", "PROPERTY_Price");
    $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "PROPERTY_AUTHOR" => $userID);
    $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

    if ($ob = $res->GetNextElement()){
        $arFields = $ob->GetFields();
        if (!empty($arFields['PROPERTY_PRICE_VALUE'])){
            return $arFields['PROPERTY_PRICE_VALUE'];
        }
    } else {
        return 0;
    }
}

//Выгружаемые свойства для фильтра
function filterProp() {
    $property_array = array(//удалено Rooms из kvartiry.prodam,kvartiry.sdam,komnaty.prodam,komnaty.sdam,
        "kvartiry" => array(
                        "prodam" => array("emptyblock", "FloorList", "MarketType","HouseType","SquareListFrom", "SquareListTo"),
                        "sdam" => array("emptyblock", "FloorList", "LeaseType","HouseType","SquareListFrom", "SquareListTo"),
                        "kuplyu" => array("emptyblock"),
                        "snimu" => array("emptyblock", "LeaseType"),
                    ),
        "komnaty" => array(
                        "prodam" => array("emptyblock", "FloorList", "HouseType","SquareListFrom", "SquareListTo"),
                        "sdam" => array("emptyblock", "FloorList", "LeaseType", "HouseType","SquareListFrom", "SquareListTo"),
                        "kuplyu" => array("emptyblock"),
                        "snimu" => array("LeaseType"),
                    ),
        "doma_dachi_kottedzhi" => array(
                        "prodam" => array("ObjectTypeDoma", "WallsType","DistanceToCity","SquareListFrom", "SquareListTo"),
                        "sdam" => array("ObjectTypeDoma", "LeaseType", "WallsType","DistanceToCity","SquareListFrom", "SquareListTo"),
                        "kuplyu" => array("ObjectTypeDoma", "WallsType","DistanceToCity"),
                        "snimu" => array("ObjectTypeDoma", "LeaseType", "WallsType","DistanceToCity"),
                    ), 
        "zemelnye_uchastki" => array(
                        "prodam" => array("ObjectTypeZemlya","DistanceToCity","LandAreaListFrom" ,"LandAreaListTo"),
                        "sdam" => array("ObjectTypeZemlya","DistanceToCity","LandAreaListFrom" ,"LandAreaListTo"),
                        "kuplyu" => array("ObjectTypeZemlya","DistanceToCity"),
                        "snimu" => array("ObjectTypeZemlya","DistanceToCity"),
                    ), 
        "garazhi_i_mashinomesta" => array(
                        "prodam" => array("ObjectTypeGaraj","Secured"),
                        "sdam" => array("ObjectTypeGaraj", "Secured"),
                        "kuplyu" => array("ObjectTypeGaraj"),
                        "snimu" => array("ObjectTypeGaraj"),
                    ), 
        "kommercheskaya_nedvizhimost" => array(
                        "prodam" => array("ObjectTypeCommerce","BuildingClass"),
                        "sdam" => array("ObjectTypeCommerce","BuildingClass"),
                        "kuplyu" => array("ObjectTypeCommerce","BuildingClass"),
                        "snimu" => array("ObjectTypeCommerce","BuildingClass", "LeaseType"),
                    ), 
        "nedvizhimost_za_rubezhom" => array(
                        "prodam" => array("ObjectTypeZagran","Country"),
                        "sdam" => array("ObjectTypeZagran","Country", "LeaseType"),
                        "kuplyu" => array("ObjectTypeZagran","Country"),
                        "snimu" => array("ObjectTypeZagran","Country", "LeaseType"),
                    ), 
    );
    
    return $property_array;
}

//Выгружаемые свойства для интерактивного поиска
function filterSearchProp() {
    $property_array = array(
        "kvartiry" => array(
                        "prodam" => array(
                                        'sliders' => array(
                                                        'Floor' => array('min'=>0,'max'=>31, 'step'=>1, 'name'=>'Этаж'),
                                                        'Floors' => array('min'=>0,'max'=>31, 'step'=>1,'name'=>'Этажей в доме'),
                                                        'Square' => array('min'=>10,'max'=>130, 'step'=>3, 'name'=>'Площадь квартиры'),
                                                        'Price' => array('min'=>0, 'name'=> 'Цена'),
                                                    ),
                                        'multiselects' => array(
                                                        'HouseType' => array('name'=> 'Тип дома', 'caption'=>'Типов'),
                                                        'Rooms' => array('name'=> 'Количество комнат', 'caption'=>'Выбрано'),
                                                    ),
                                        'selects' => array(
                                                        'MarketType' => array('name'=> 'Вид объекта'),
                                                    ),
                                        'objects' => array(
                                            'status' => array(),
                                        ),
                                    ),
                        "sdam" => array(
                                        'sliders' => array(
                                                        'Floor' => array('min'=>0,'max'=>31, 'step'=>1, 'name'=>'Этаж'),
                                                        'Floors' => array('min'=>0,'max'=>31, 'step'=>1, 'name'=>'Этажей в доме'),
                                                        'Square' => array('min'=>10,'max'=>100, 'step'=>3,'name'=>'Площадь квартиры'),
                                                        'Price' => array('min'=>0, 'name'=> 'Цена'),
                                                    ),
                                        'multiselects' => array(
                                                        'HouseType' => array('name'=> 'Тип дома', 'caption'=>'Типов'),
                                                        'Rooms' => array('name'=> 'Количество комнат', 'caption'=>'Комнат'),
                                                    ),
                                        'selects' => array(
                                                        'LeaseType' => array('name'=> 'Срок аренды'),
                                                    ),
                                    ),
                        "kuplyu" => array(),
                        "snimu" => array(),
                    ),
                    
                    
                    
                    
        "komnaty" => array(
                        "prodam" => array(
                                        'sliders' => array(
                                                        'Floor' => array('min'=>0,'max'=>31, 'step'=>1, 'name'=>'Этаж'),
                                                        'Floors' => array('min'=>0,'max'=>31, 'step'=>1, 'name'=>'Этажей в доме'),
                                                        'Square' => array('min'=>5,'max'=>50, 'step'=>5, 'name'=>'Площадь комнаты'),
                                                        'Price' => array('min'=>0, 'name'=> 'Цена'),
                                                    ),
                                        'multiselects' => array(
                                                        'HouseType' => array('name'=> 'Тип дома', 'caption'=>'Типов'),
                                                        'Rooms' => array('name'=> 'Количество комнат', 'caption'=>'Комнат'),
                                        ),
                                    ),
                        "sdam" => array(
                                        'sliders' => array(
                                                        'Floor' => array('min'=>0,'max'=>31, 'step'=>1, 'name'=>'Этаж'),
                                                        'Floors' => array('min'=>0,'max'=>31, 'step'=>1, 'name'=>'Этажей в доме'),
                                                        'Square' => array('min'=>5,'max'=>50, 'step'=>5, 'name'=>'Площадь комнаты'),
                                                        'Price' => array('min'=>0, 'name'=> 'Цена'),
                                                    ),
                                        'multiselects' => array(
                                                        'Rooms' => array('name'=> 'Количество комнат', 'caption'=>'Комнат'),
                                        ),
                                        'selects' => array(
                                                        'LeaseType' => array('name'=> 'Срок аренды'),
                                        ),
                                    ),
                        "kuplyu" => array(),
                        "snimu" => array(),
                    ),
        "doma_dachi_kottedzhi" => array(
                        "prodam" => array(
                                        'sliders' => array(
                                                        'Floors' => array('min'=>0,'max'=>5, 'step'=>1, 'name'=>'Этажей в доме'),
                                                        'Square' => array('min'=>50,'max'=>500, 'step'=>25, 'name'=>'Площадь дома'),
                                                        'DistanceToCity' => array('min'=>0,'max'=>100, 'step'=>5, 'name'=>'Расстояние до города'),
                                                        'Price' => array('min'=>0, 'name'=> 'Цена'),
                                                    ),
                                        'multiselects' => array(
                                                        'ObjectTypeDoma' => array('name'=> 'Вид объекта', 'caption'=>'Видов'),
                                                        'WallsType' => array('name'=> 'Материал стен', 'caption'=>'Материалов'),
                                        ),
                                        
                                    ),
                        "sdam" => array(
                                        'sliders' => array(
                                                        'Floors' => array('min'=>0,'max'=>5, 'step'=>1, 'name'=>'Этажей в доме'),
                                                        'Square' => array('min'=>50,'max'=>500, 'step'=>25, 'name'=>'Площадь дома'),
                                                        'DistanceToCity' => array('min'=>0,'max'=>100, 'step'=>5, 'name'=>'Расстояние до города'),
                                                        'Price' => array('min'=>0, 'name'=> 'Цена'),
                                                    ),
                                        'multiselects' => array(
                                                        'ObjectTypeDoma' => array('name'=> 'Вид объекта', 'caption'=>'Видов'),
                                                        'WallsType' => array('name'=> 'Материал стен', 'caption'=>'Материалов'),
                                        ),
                                        'selects' => array(
                                                        'LeaseType' => array('name'=> 'Срок аренды'),
                                        ),
                                    ),
                        "kuplyu" => array(),
                        "snimu" => array(),
                    ),
        "zemelnye_uchastki" => array(
                        "prodam" => array(
                                        'sliders' => array(
                                                        'DistanceToCity' => array('min'=>0,'max'=>100, 'step'=>5, 'name'=>'Расстояние до города'),
                                                        'LandArea' => array('min'=>0,'max'=>500, 'step'=>10, 'name'=>'Площадь земель'),
                                                        'Price' => array('min'=>0, 'name'=> 'Цена'),
                                                    ),
                                        'multiselects' => array(
                                                        'ObjectTypeZemlya' => array('name'=> 'Категория земель', 'caption'=>'Категорий'),
                                        ),
                                    ),
                        "sdam" => array(
                                        'sliders' => array(
                                                        'DistanceToCity' => array('min'=>0,'max'=>100, 'step'=>5, 'name'=>'Расстояние до города'),
                                                        'LandArea' => array('min'=>0,'max'=>500, 'step'=>10, 'name'=>'Площадь земель'),
                                                        'Price' => array('min'=>0, 'name'=> 'Цена'),
                                                    ),
                                        'multiselects' => array(
                                                        'ObjectTypeZemlya' => array('name'=> 'Категория земель', 'caption'=>'Категорий'),
                                        ),
                                    ),
                        "kuplyu" => array(),
                        "snimu" => array(),
                    ),
        "garazhi_i_mashinomesta" => array(
                        "prodam" => array(
                                        'sliders' => array(
                                                        'Price' => array('min'=>0, 'name'=> 'Цена'),
                                                    ),
                                    ),
                        "sdam" => array(
                                        'sliders' => array(
                                                        'Price' => array('min'=>0, 'name'=> 'Цена'),
                                                    ),
                                    ),
                        "kuplyu" => array(),
                        "snimu" => array(),
                    ),
        "kommercheskaya_nedvizhimost" => array(
                        "prodam" => array(
                                        'sliders' => array(
                                                        'Price' => array('min'=>0, 'name'=> 'Цена'),
                                                    ),
                                    ),
                        "sdam" => array(
                                        'sliders' => array(
                                                        'Price' => array('min'=>0, 'name'=> 'Цена'),
                                                    ),
                                    ),
                        "kuplyu" => array(),
                        "snimu" => array(),
                    ),
        "nedvizhimost_za_rubezhom" => array(
                        "prodam" => array(
                                        'sliders' => array(
                                                        'Price' => array('min'=>0, 'name'=> 'Цена'),
                                                    ),
                                    ),
                        "sdam" => array(
                                        'sliders' => array(
                                                        'Price' => array('min'=>0, 'name'=> 'Цена'),
                                                    ),
                                    ),
                        "kuplyu" => array(),
                        "snimu" => array(),
                    ),
    );
    
    return $property_array;
}

function filterPropForRooms($type){
    //удалено Rooms из kvartiry.prodam,kvartiry.sdam,komnaty.prodam,komnaty.sdam в функции filterProp()
    // if ($type = 'rooms') {
        // $property_array = array(
            // "kvartiry" => array(
                            // "prodam" => array("Rooms"),
                            // "sdam" => array("Rooms"),
                            // "kuplyu" => array(),
                            // "snimu" => array(),
                        // ),
            // "komnaty" => array(
                            // "prodam" => array("Rooms"),
                            // "sdam" => array("Rooms"),
                            // "kuplyu" => array(),
                            // "snimu" => array(),
                        // ), 
        // );
    // } elseif($type = 'roomslist'){
        // $property_array = array(
            // "kvartiry" => array(
                            // "prodam" => array(),
                            // "sdam" => array(),
                            // "kuplyu" => array("RoomsList"),
                            // "snimu" => array("RoomsList"),
                        // ),
            // "komnaty" => array(
                            // "prodam" => array(),
                            // "sdam" => array(),
                            // "kuplyu" => array(),
                            // "snimu" => array(),
                        // ), 
        // );
    // }
    $property_array = array(
        "kvartiry" => array(
                        "prodam" => array("Rooms"),
                        "sdam" => array("Rooms"),
                        "kuplyu" => array("RoomsList"),
                        "snimu" => array("RoomsList"),
                    ),
        "komnaty" => array(
                        "prodam" => array("Rooms"),
                        "sdam" => array("Rooms"),
                        "kuplyu" => array(),
                        "snimu" => array(),
                    ), 
    );

    return $property_array;
}


function addLog($logType, $info, $userId, $systemObj="", $objectId=""){
    CModule::IncludeModule('iblock');
    
    $newElement = new CIBlockElement;

    $arrayProperty = array();
    
    //Событие
    $arrayProperty[97] = $logType;
    
    //Время
    $arrayProperty[98] = date("d.m.y H:i:s");
    
    //Информация
    $arrayProperty[99] = $info;

    //Тип объекта
    $arrayProperty[100] = $systemObj;
    
    //id объекта
    $arrayProperty[101] = $objectId;
    //Id пользователя
    $arrayProperty[102] = $userId;
    
    $arLoadProductArray = Array(
        "IBLOCK_SECTION_ID" => false,
        "IBLOCK_ID"      => 15,
        "PROPERTY_VALUES"=> $arrayProperty,
        "NAME"           => "Событие",
        "ACTIVE"         => "Y",
    );

    if($PRODUCT_ID = $newElement->Add($arLoadProductArray)){
        return "New ID: ".$PRODUCT_ID;
    }else{
        return "Error: ".$newElement->LAST_ERROR;
    }
      
}


//добавляет новому пользователю контакты в чатик. Таблица "b_sonet_user_relations"
function addNewContactsChat($newUserIdd){
	global $DB;
	$cUser = new CUser; 
	$sort_by = "ID";
	$sort_ord = "ASC";
	$arFilter = array();
	$dbUsers = $cUser->GetList($sort_by, $sort_ord, $arFilter);
	$arUsersIds = array();
	while ($arUser = $dbUsers->Fetch()) 
	{
	  $arUsersIds[$arUser["ID"]] = $arUser["ID"];
	}
	unset($arUsersIds[200]);
	unset($arUsersIds[1]);
	foreach($arUsersIds as $val){
			$arFields = array(
			'ID'=>NULL,
			'FIRST_USER_ID'=>"'".$newUserIdd."'",
			'SECOND_USER_ID'=>"'".$val."'",	
			'RELATION'=>"'".'F'."'",
			'DATE_CREATE'=>"''",
			'DATE_UPDATE'=>"''",
			'MESSAGE'=>NULL,
			'INITIATED_BY'=>"'".'S'."'",	
		);
		$DB->Insert("b_sonet_user_relations", $arFields, $err_mess.__LINE__);	
	}
}

//добавляет всем пользователям друзей в чатик(база должна быть чистая, просто так не врубать). Таблица "b_sonet_user_relations"
function importAllUsersChat(){
	/*$cUser = new CUser; 
	$sort_by = "ID";
	$sort_ord = "ASC";
	$arFilter = array();
	$dbUsers = $cUser->GetList($sort_by, $sort_ord, $arFilter);
	$arUsersIds = array();
	while ($arUser = $dbUsers->Fetch()) 
	{
	  $arUsersIds[$arUser["ID"]] = $arUser["ID"];
	}
	unset($arUsersIds[200]);
	unset($arUsersIds[1]);

	$arInvites = array();
	$arInvitesUpdate = array();
	foreach($arUsersIds as $key=>$val){
		
		foreach($arUsersIds as $key1=>$val1){
			if(in_array($val1,$arInvites)){
				$arInvitesUpdate[$val][] =  $val1;
			}
		}
		$arInvites[] = $val;
	}

	global $DB;

	foreach($arInvitesUpdate as $key=>$val){
		foreach($val as $val1){
			$arFields = array(
				'ID'=>NULL,
				'FIRST_USER_ID'=>"'".$key."'",
				'SECOND_USER_ID'=>"'".$val1."'",	
				'RELATION'=>"'".'F'."'",
				'DATE_CREATE'=>"''",
				'DATE_UPDATE'=>"''",
				'MESSAGE'=>NULL,
				'INITIATED_BY'=>"'".'S'."'",	
			);
			$DB->Insert("b_sonet_user_relations", $arFields, $err_mess.__LINE__);
		}
	}*/
}
 

