<?php
/**
 * Created by PhpStorm.
 * User: picom
 * Date: 03.12.14
 * Time: 13:27
 */
 
AddEventHandler("main", "OnAdminListDisplay", "MyOnAdminListDisplay");
AddEventHandler("main", "OnBeforeUserAdd", "OnBeforeUserAddUpdateHandler");
AddEventHandler("main", "OnAfterUserAdd", "OnAfterUserAddHandler");
AddEventHandler("main", "OnBeforeUserUpdate", "OnBeforeUserAddUpdateHandler");
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", "OnBeforeIBlockElementAddHandler");
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "OnBeforeIBlockElementUpdateHandler");
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "OnAfterIBlockElementAddUpdateHandler");
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "OnAfterIBlockElementAddUpdateHandler");
// AddEventHandler("iblock", "OnBeforeIBlockPropertyUpdate", "OnBeforeIBlockPropertyUpdateHandler");

AddEventHandler("main", "OnBeforeUserDelete", "OnBeforeUserDeleteHandler");


function OnBeforeUserDeleteHandler($user_id)
{
    global $APPLICATION;
    global $USER;
    
    $CLIENT_IBLOCK_ID = 10; //Инфоблок с клиентами
    $OBJECT_IBLOCK_ID = 16; //Инфоблок с объектами
    $ADS_IBLOCK_ID = 5; //Инфоблок с объявлениями
    CModule::IncludeModule("iblock");
    
    
    //Получаем данные удаляемого юзера
    $rsUser = CUser::GetByID($user_id);
    $currentUser = $rsUser->Fetch();
    $arUserGroup = CUser::GetUserGroup($user_id);
    
    // Запрет удаления офис-менеджеров
    if (in_array(9, $arUserGroup)){
        echo 'Удаление офис-менеджеров из системы временно недоступно.';
        die();
    }
    
    $userBoss = !empty($currentUser['UF_HEAD']) ? $currentUser['UF_HEAD'] : 1;
    
    $rsUserBoss = CUser::GetByID($userBoss);
    $userBossData = $rsUserBoss->Fetch();

    //Заносим информацию об удаляемом юзере в архив
    $el = new CIBlockElement;
    $arLoadProductArray = Array(
      "MODIFIED_BY"    => $USER->GetID(),
      "IBLOCK_SECTION_ID" => false,          
      "IBLOCK_ID"      => 13,
      "PROPERTY_VALUES"=> $currentUser,
      "NAME"           => $currentUser['LAST_NAME'].' '.$currentUser['NAME'].' '.$currentUser['SECOND_NAME'],
      "ACTIVE"         => "Y",            
      );
    
    if($archiveUser = $el->Add($arLoadProductArray)){
      // echo "New ID: ".$archiveUser;
    } else{
      echo "Error: ".$el->LAST_ERROR;
    }
    
    //Проверяем наличие подчиненных у удаляемого юзера
    $filter = Array("UF_HEAD" => $user_id);
    $rsUsers = CUser::GetList(($by="LAST_NAME"), ($order="asc"), $filter,array("SELECT"=>array("UF_*")));
    
    //Если есть подчиненные, переопределяем на вышестоящего начальника или на админа
    if (intval($rsUsers->SelectedRowsCount()) > 0) {
       while($subUser = $rsUsers->Fetch()) {
            $user = new CUser;
            $fields = Array(
                "UF_HEAD" => $userBoss,
            ); 
            $user->Update($subUser['ID'], $fields);
        }
    }
    
    //Получаем список клиентов удаляемого юзера
    $arOrder = Array("SORT"=>"ASC");
    $arSelect = Array("ID", "IBLOCK_ID", "ACTIVE", "NAME");
    $arFilter = Array("IBLOCK_ID"=>$CLIENT_IBLOCK_ID, "PROPERTY_RIELTOR"=>$currentUser['ID']);
    $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

    while ($ob = $res->GetNextElement()){
        $userClient = $ob->GetFields();
        
        CIBlockElement::SetPropertyValuesEx($userClient['ID'], false, array("LAST_OWNER" => $archiveUser)); // Новодобавленный в архив работник
        if ($userClient['ACTIVE'] == "Y"){
            CIBlockElement::SetPropertyValuesEx($userClient['ID'], false, array("REASSIGN" => 514)); //Да
            CIBlockElement::SetPropertyValuesEx($userClient['ID'], false, array("RIELTOR" => $userBoss));//Начальник удаляемого агента или админ
        } else {
            CIBlockElement::SetPropertyValuesEx($userClient['ID'], false, array("RIELTOR" => 1));//Удаленные (деактивированные объявления) переходят на админа
        }
    }
    
    //Получаем список объявлений удаляемого юзера
    $arSelectAds = Array("ID", "IBLOCK_ID", "ACTIVE", "NAME");
    $arFilterAds = Array("IBLOCK_ID"=>$ADS_IBLOCK_ID, "PROPERTY_Author"=>$currentUser['ID']);
    $resads = CIBlockElement::GetList($arOrder, $arFilterAds, false, false, $arSelectAds);
    while ($ob = $resads->GetNextElement()){
        $userAds = $ob->GetFields();
        $userAds['properties'] = $ob->GetProperties();
        $userObject = $userAds['properties']['OBJECT_ELEMENT']['VALUE'];
        if (!empty($userObject)){
            $db_props = CIBlockElement::GetProperty($OBJECT_IBLOCK_ID, $userObject, array("sort" => "asc"), Array("CODE"=>"Author"));
            if($ar_props = $db_props->Fetch()){
                $officeManagerId = IntVal($ar_props["VALUE"]);
                CIBlockElement::SetPropertyValuesEx($userAds['ID'], false, array("Author" => $officeManagerId));
                CIBlockElement::SetPropertyValuesEx($userObject, false, array("WATCHER" => $officeManagerId));
            }
        } else {
            if ($userAds['ACTIVE'] == "Y"){
                CIBlockElement::SetPropertyValuesEx($userAds['ID'], false, array("Author" => $userBoss));//Начальник удаляемого агента или админ
            } else {
                CIBlockElement::SetPropertyValuesEx($userAds['ID'], false, array("Author" => 1));//Удаленные (деактивированные объявления) переходят на админа
            }
        }
    }
    
    if (!empty($userBossData['EMAIL'])){
        $arMail = array(
            "EMAIL" => $userBossData['EMAIL'],
            "BOSS_NAME" => ''.$userBossData['NAME'].' '.$userBossData['SECOND_NAME'],
            "DATE_TIME" => date("d.m.Y"),
            "FIO" => $currentUser['LAST_NAME'].' '.$currentUser['NAME'].' '.$currentUser['SECOND_NAME'],
        );
        CEvent::Send("DELETE_USER", array("s1"), $arMail, "N", 27);
    }
    addLog("Удаление", "Пользователь \"{$currentUser['LAST_NAME']} {$currentUser['NAME']} {$currentUser['SECOND_NAME']}\" (id:{$currentUser['ID']}) был удален из системы", 1, "Архив",$archiveUser);
}

function MyOnAdminListDisplay(&$list)
{
    if($list->table_id != "tbl_user") return;
    if (!$list->aVisibleHeaders["UF_HEAD"]) return;
    $list->aHeaders["HEAD"] = array("id"=>"HEAD","content"=>"Начальник [login]", "sort"=>"","default"=>"");
    $list->aVisibleHeaders["HEAD"] = array("id"=>"HEAD","content"=>"Начальник [login]", "sort"=>"","default"=>"");
    foreach($list->aRows as $row) {
        if ($row->arRes['UF_HEAD']) {
            $rsUser = CUser::GetByID($row->arRes['UF_HEAD']);
            $arUser = $rsUser->Fetch();
            $row->AddField('HEAD',$arUser["LAST_NAME"].' '.$arUser["NAME"].' '.$arUser["SECOND_NAME"].' ['.$arUser["LOGIN"].']');
        }
    }
}

function OnBeforeUserAddUpdateHandler(&$arFields) {

    if ($_REQUEST['mode']=='frame') return;
    global $APPLICATION;
    $e = new CAdminException();
    $err = false;

    if(isset($_REQUEST['LAST_NAME']) && strlen(trim($arFields["LAST_NAME"])) <= 0) {
        $err = true;
        $e->AddMessage(array("text" => "Пожалуйста, введите фамилию.",));
    }
    if(isset($_REQUEST['NAME']) && strlen(trim($arFields["NAME"])) <= 0) {
        $err = true;
        $e->AddMessage(array("text" => "Пожалуйста, введите имя.",));
    }
    if(strlen(trim($arFields["PERSONAL_PHONE"])) <= 0 && strlen(trim($arFields["PERSONAL_MOBILE"])) <= 0) {
        $err = true;
        $e->AddMessage(array("text" => "Необходимо указать хотя бы один телефон",));
    }
    if ($arFields['ID']) {
        if ($arFields['UF_HEAD']) {
            $parents = getParentsId($arFields['UF_HEAD']);
            if (in_array($arFields['ID'],$parents)) {
                $err = true;
                $e->AddMessage(array("text" => "Неправильно указан начальник. Предполагаемый начальник находится в подчинении",));
            }
        }
    }
    if ($err) {
        $APPLICATION->throwException($e);
        return false;
    }
}

function OnBeforeIBlockElementAddHandler(&$arFields) {

    global $APPLICATION;
    global $USER;
    $text_error = new CAdminException();
    $error = false; 

    if ($arFields['IBLOCK_ID'] == 16){//Объекты
        // if ($USER->GetID() == 200){
            // pre($arFields);
            // die();
        // }
        if ($arFields['PROPERTY_VALUES']['City']['VALUE'] == 49 && empty($arFields['PROPERTY_VALUES']['District']['VALUE'])){
            $error = true;
            $text_error->AddMessage(   
                array ("text" => 'Заполните район города',)
            );
        }
        switch ($arFields['PROPERTY_VALUES']['STATUS']['VALUE']){
            case 8962://Презентация
                if (empty($arFields['PROPERTY_VALUES']['WATCHER']['VALUE'])){
                    $error = true;
                    $text_error->AddMessage(   
                        array ("text" => 'Для выбранного статуса необходимо заполнить поле Риэлтор',)
                    );
                }
            case 8940:
            case 8963:
            case 8964:
            case 8965:
            case 8966:
            case 8967:
            case 8968:
            case 8971:
            case 8972:
            case 8973:
            case 8975:
                if (empty($arFields['PROPERTY_VALUES']['CURRENT_DATE_OPTION']['VALUE'])){
                    $error = true;
                    $text_error->AddMessage(   
                        array ("text" => 'Для выбранного статуса необходимо заполнить поле Дата/Время',)
                    );
                }
                break;
            case 8977:
            case 8978:
            case 8979:
            case 11106:
            case 11377:
                if (empty($arFields['PROPERTY_VALUES']['CURRENT_COMMENT_OPTION'])){
                    $error = true;
                    $text_error->AddMessage(   
                        array ("text" => 'Для выбранного статуса необходимо заполнить поле Комментарий',)
                    );
                }
                break;
        }
        $arFields['IBLOCK_SECTION'][] = 55;
        $arFields['NAME'] = $arFields['PROPERTY_VALUES']['Street']." ".$arFields['PROPERTY_VALUES']['HOUSE'];
        if (!empty($arFields['PROPERTY_VALUES']['APARTMENT'])){
            $arFields['NAME'] .= ", кв. ".$arFields['PROPERTY_VALUES']['APARTMENT'];
        }
    }
    
    
    //Установка "номера колл центра"
    $rsUser = CUser::GetByID(CUser::GetID());
    $CurUser = $rsUser->Fetch();
    
    if ($CurUser['UF_CALLCENTER']) {
        $arFields['PROPERTY_VALUES']['CALLC'][0] = 513;
    }

    if ($arFields['IBLOCK_ID'] == 5){//Объявления
    $ar_res = getSectionProps(5,$arFields["IBLOCK_SECTION"][0]);
        //Индивидуальные ограничения свойства каждого раздела
        switch ($ar_res['IBLOCK_SECTION_ID']){
            case '6': //квартиры
                $check_array['floors'][] = 1;
                $check_array['floors'][] = 99;
                $check_array['square'] = 10;
                $check_array['max_img'] = 20;
                $check_array['objectType'] = 'MarketType';
                break;
            case '11': //комнаты
                $check_array['floors'][] = 1;
                $check_array['floors'][] = 99;
                $check_array['square'] = 6;
                $check_array['max_img'] = 10;
                break;
            case '12': //дома, дачи, коттеджи
                $check_array['floors'][] = 1;
                $check_array['floors'][] = 10;
                $check_array['square'] = 20;
                $check_array['landarea'] = 0;   
                $check_array['max_img'] = 20;
                $check_array['objectType'] = 'ObjectTypeDoma';
                break;            
            case '13': //земельные участки
                $check_array['landarea'] = 1;
                $check_array['max_img'] = 5;
                $check_array['objectType'] = 'ObjectTypeZemlya';
                break;            
            case '14': //гаражи
                $check_array['square'] = 10;
                $check_array['max_img'] = 5;
                $check_array['objectType'] = 'ObjectTypeGaraj';
                break;            
            case '15': //коммерческая недвижимость  
                $check_array['square'] = 5;
                $check_array['max_img'] = 10;
                $check_array['objectType'] = 'ObjectTypeCommerce';
                break;            
            case '16': //недвижимость за рубежом
                $check_array['square'] = 0;
                $check_array['max_img'] = 10;
                $check_array['objectType'] = 'ObjectTypeZagran';
                break;
        }
        
        //Поле "Этажность" обязательное у разделов "комнаты","квартиры" и "дома"
        $floors = $arFields['PROPERTY_VALUES']['Floors'];
        if (in_array('PROPERTY_Floors', $ar_res['UF_PROPERTY']) && empty($floors)){
            $error = true;
            $text_error->AddMessage(   
                    array ("text" => 'Заполните поле \'Этажность\'.',)
            );
        } else {
            //Floors, integer - свое у каждого типа. Значения сформированы в массиве check_array
            if (!empty($floors) && (!is_numeric($floors) || fmod($floors,1)!=0 || ($floors<$check_array['floors'][0] || $floors>$check_array['floors'][1]))){
                $error = true;  
                $text_error->AddMessage(   
                    array ("text" => 'Поле \'Этажей в доме\' должно быть целым числом от '.$check_array['floors'][0].' до '.$check_array['floors'][1].'.',)
                );
            }   
        }
        
        $floor = $arFields['PROPERTY_VALUES']['Floor'];
        //Поле "Этаж" обязательное у раздела "комнаты" и "квартиры"
        if (in_array('PROPERTY_Floor', $ar_res['UF_PROPERTY']) && empty($floor)){
            $error = true;
            $text_error->AddMessage(   
                    array ("text" => 'Заполните поле \'Этаж\'.',)
            );
        } else {
            //Floor, integer,от 1 до 99
            if (!empty($floor) && (!is_numeric($floor) || fmod($floor,1)!=0 || ($floor<1 || $floor>99))){
                $error = true;  
                   $text_error->AddMessage(   
                     array ("text" => 'Поле \'Этаж\' должно быть целым числом от 1 до 99.',)
                   );
            }  
        }

        //Square, decimal - свое у каждого типа. Значения сформированы в массиве check_array
        $square = $arFields['PROPERTY_VALUES']['Square'];
        if (!empty($square) && (!is_numeric($square) || $square<$check_array['square'])){
            $error = true;  
            $text_error->AddMessage(   
                array ("text" => 'Поле \'Площадь\' должно быть числом от '.$check_array['square'].'.',)
           );
        }
        
        //LandArea, decimal - свое у каждого типа. Значения сформированы в массиве check_array
        $landarea = $arFields['PROPERTY_VALUES']['LandArea'];
        if (!empty($landarea) && (!is_numeric($landarea) || $landarea<$check_array['landarea'])){
            $error = true;  
           $text_error->AddMessage(   
             array ("text" => 'Поле \'Площадь земли в сотках\' должно быть числом от '.$check_array['landarea'].'.',)
           );
        }      
        //LIVING_SQUARE, целое число
        $livSquare = $arFields['PROPERTY_VALUES']['LIVING_SQUARE'];
        if (!empty($livSquare) && !is_numeric($livSquare)){
            $error = true;  
            $text_error->AddMessage(   
                array ("text" => 'Поле \'Жилая площадь\' должно быть числом.',)
           );
        }          
        //KITCHEN_SQUARE, целое число
        $kitSquare = $arFields['PROPERTY_VALUES']['KITCHEN_SQUARE'];
        if (!empty($kitSquare) && !is_numeric($kitSquare)){
            $error = true;  
            $text_error->AddMessage(   
                array ("text" => 'Поле \'Кухня\' должно быть числом.',)
           );
        }        
        
        //Images, принимается максимум n фото - свое у каждого типа. Значения сформированы в массиве check_array
        if (count($arFields['PROPERTY_VALUES']['Images'])>$check_array['max_img']){
            $error = true;  
            $text_error->AddMessage(   
                array ("text" => 'Дополнительных изображений в этом разделе должно быть не более '.$check_array['max_img'].'.',)
            );
        }
        
        //Description, обязательное поле с версии 3.0
        if (empty($arFields['DETAIL_TEXT'])){
            $error = true;  
            $text_error->AddMessage(   
                array ("text" => 'Поле \'Описание\' обязательно для заполнения',)
            );
        } elseif (strlen($arFields['DETAIL_TEXT'])>3000){ //3000 символов с версии 3.0
            $error = true;  
            $text_error->AddMessage(   
                array ("text" => 'Описание не должно превышать 3000 символов.',)
            );
        }
        
        //Price, decimal
        $price = $arFields['PROPERTY_VALUES']['Price'];
        if (!empty($price) && !is_numeric($price)){
            $error = true;  
            $text_error->AddMessage(   
            array ("text" => 'Поле \'Цена для авито\' должно быть числом.',)
           );
        }
        
        //SaleRooms, integer
        $salerooms = $arFields['PROPERTY_VALUES']['SaleRooms'];
        if (!empty($salerooms) && (!is_numeric($salerooms) || fmod($salerooms,1)!=0)){
            $error = true;  
               $text_error->AddMessage(   
                 array ("text" => 'Поле \'Количество комнат на продажу / сдающихся\' должно быть целым числом.',)
               );
        }
        //Поле город обязательное
        $city = $arFields['PROPERTY_VALUES']['City']['VALUE'];
        if (empty($city)){
            $error = true;
            $text_error->AddMessage(
                array ("text" => 'Заполните поле \'Город\'.',)
            );
        }
        if ($city == 49 && empty($arFields['PROPERTY_VALUES']['District']['VALUE']) && $arFields['PROPERTY_VALUES'][$check_array['objectType']] != 21){
            $error = true;
            $text_error->AddMessage(   
                array ("text" => 'Заполните поле \'Район города\'.',)
            );
        }
        // Поле клиент обязательное
        $prop_client_add = $arFields['PROPERTY_VALUES']['Client']['VALUE'];
        if (empty($prop_client_add) && empty($arFields['PROPERTY_VALUES']['OBJECT_ELEMENT']) && !$USER->IsAdmin()){
            $error = true;
            $text_error->AddMessage(
                array ("text" => 'Заполните поле \'Клиент\'.',)
            );
        }
        
        
        //Поле площади обязательно у всех разделов
        if (empty($arFields['PROPERTY_VALUES']['Square'])
            && empty($arFields['PROPERTY_VALUES']['SquareFrom'])
            && empty($arFields['PROPERTY_VALUES']['SquareTo'])
            && empty($arFields['PROPERTY_VALUES']['LandArea'])
            && empty($arFields['PROPERTY_VALUES']['LandAreaFrom'])
            && empty($arFields['PROPERTY_VALUES']['LandAreaTo'])
            && empty($arFields['PROPERTY_VALUES']['SquareList'][0])
            && empty($arFields['PROPERTY_VALUES']['LandAreaList'][0])){
            $error = true;
            $text_error->AddMessage(   
                array ("text" => 'Заполните значение площади объекта. В случаях купли или найма, можно поставить значение "Любая площадь".',)
            );
        }

        
        //Поле "Подвид объекта" обязательное у раздела "Гаражи и машиноместа" при продаже/сдаче в аренду
        if (in_array('PROPERTY_ObjectSubtype1', $ar_res['UF_PROPERTY']) && in_array('PROPERTY_ObjectSubtype2', $ar_res['UF_PROPERTY'])){
            $subType1 = $arFields['PROPERTY_VALUES']['ObjectSubtype1'];
            $subType2 = $arFields['PROPERTY_VALUES']['ObjectSubtype2'];
            if (empty($subType1) && empty($subType2)){                                                  
                $error = true;
                $text_error->AddMessage(
                    array ("text" => 'Заполните поле \'Подвид объекта\'.',)
                );
            }
        }
        
        //Поле "Количество комнат на продажу" обязательное у раздела "комнаты" при продаже/сдаче в аренду
        if (in_array('PROPERTY_SaleRooms', $ar_res['UF_PROPERTY'])){
            $sale = $arFields['PROPERTY_VALUES']['SaleRooms'];
            if (empty($sale)){                                                  
                $error = true;
                $text_error->AddMessage(
                    array ("text" => 'Заполните поле \'Количество комнат на продажу/сдающихся\'.',)
                );
            }
        }
        
        //Поле "Новостройка" обязательное для типа квартиры
        if (in_array('PROPERTY_NewDevelopmentId', $ar_res['UF_PROPERTY'])){
            $NewDevelopmentId = $arFields['PROPERTY_VALUES']['NewDevelopmentId']['VALUE'];
            if (empty($NewDevelopmentId) && $arFields['PROPERTY_VALUES'][$check_array['objectType']] == 21){                                                  
                $error = true;
                $text_error->AddMessage(
                    array ("text" => 'Выберите объект новостройки',)
                );
            }
        }
        
        //Поле "Улица" обязательное для разделов, у которых оно указывается (для авито)
        if (in_array('PROPERTY_Street', $ar_res['UF_PROPERTY'])){
            $street = $arFields['PROPERTY_VALUES']['Street'];
            //Для новостроек улица поле необязательное на авито
            if (empty($street) && $arFields['PROPERTY_VALUES'][$check_array['objectType']] != 21){                                                  
                $error = true;
                $text_error->AddMessage(
                    array ("text" => 'Заполните поле \'Улица\'.',)
                );
            }
        }
        
        //Поле "Материал стен" обязательное для раздела "Дома, коттеджи" с версии 3.0
        if (in_array('PROPERTY_WallsType', $ar_res['UF_PROPERTY'])){
            $WallsType = $arFields['PROPERTY_VALUES']['WallsType'];
            if (empty($WallsType)){ 
                $error = true;
                $text_error->AddMessage(
                    array ("text" => 'Заполните поле \'Материал стен\'.',)
                );
            }
        }
        
        //Поле "Тип дома" обязательное для раздела "Квартиры" с версии 3.0
        if (in_array('PROPERTY_HouseType', $ar_res['UF_PROPERTY'])){
            $HouseType = $arFields['PROPERTY_VALUES']['HouseType'];
            if (empty($HouseType)){ 
                $error = true;
                $text_error->AddMessage(
                    array ("text" => 'Заполните поле \'Тип дома\'.',)
                );
            }
        }
        //Поле "Срок аренды" обязательное для раздела "Квартиры / Сдам"
        if (in_array('PROPERTY_LeaseType', $ar_res['UF_PROPERTY'])){
            $lease = $arFields['PROPERTY_VALUES']['LeaseType'];
            if (empty($lease)){ 
                $error = true;
                $text_error->AddMessage(
                    array ("text" => 'Заполните поле \'Тип аренды\'.',)
                );
            }
        }
        
        //Поле "Количество комнат" обязательное у раздела "комнаты" и "квартиры"
        if ((in_array('PROPERTY_Rooms', $ar_res['UF_PROPERTY']) && $arFields['PROPERTY_VALUES']['Rooms']==0)
        ||(in_array('PROPERTY_RoomsList', $ar_res['UF_PROPERTY']) && empty($arFields['PROPERTY_VALUES']['RoomsList']['0']))){
            $error = true;
            $text_error->AddMessage(   
                    array ("text" => 'Заполните поле \'Количество комнат\'.',)
            );
        }

        //Поле "Расстояние до города" обязательное у разделов "Дома" и "Земля"
        if (in_array('PROPERTY_DistanceToCity', $ar_res['UF_PROPERTY'])){
            $dist= $arFields['PROPERTY_VALUES']['DistanceToCity'];
            if (empty($dist)){                                                  
                $error = true;
                $text_error->AddMessage(
                    array ("text" => 'Заполните поле \'Расстояние до города\'.',)
                );
            }
        }
        //Поле "Страна" обязательное у раздела "Зарубежная недвижимость"
        if (in_array('PROPERTY_Country', $ar_res['UF_PROPERTY'])){
            $county= $arFields['PROPERTY_VALUES']['Country'];
            if (empty($county)){                                                  
                $error = true;
                $text_error->AddMessage(
                    array ("text" => 'Заполните поле \'Страна\'.',)
                );
            }
        }
        //Поле вид объекта должно быть заполнено для корректного составления названий объявлений
        if (isset($check_array['objectType']) && !empty($check_array['objectType']) && in_array('PROPERTY_'.$check_array['objectType'], $ar_res['UF_PROPERTY'])){
            $object = $arFields['PROPERTY_VALUES'][$check_array['objectType']];
            if (empty($object)){   
                $error = true;
                $text_error->AddMessage(
                    array ("text" => 'Заполните поле \'Вид объекта\'.',)
                );
            }
        }
    // die();
    }
    
    // Если ошибка в заполнении, выдать исключение
    if ($error){
        $APPLICATION->ThrowException($text_error);
        return false;
    }

    
    if ($arFields['IBLOCK_ID'] == 10) {//клиенты
        if ($arFields['PROPERTY_VALUES']['I']) {
            $fio = $arFields['PROPERTY_VALUES']['F'].' '.$arFields['PROPERTY_VALUES']['I'].' '.$arFields['PROPERTY_VALUES']['O'];
            $mail = $arFields['PROPERTY_VALUES']['EMAIL'];
            $arFields['NAME'] = $fio;
            $arFields['CODE'] = Cutil::translit($arFields['NAME'], 'ru', array(
                'replace_space' => '-',
                'replace_other' => '-'
            ));
            if(strpos($arFields['PROPERTY_VALUES']['PHONE'], "_")){
                global $APPLICATION;
                $APPLICATION->ThrowException('Поле \'Телефон\' заполнено не полностью');
                return false;
            } else {
                CModule::IncludeModule("iblock");
                 
                $IBLOCK_ID = 10; 
             
                $arOrder = Array("SORT"=>"ASC");
                $arSelect = Array("ID", "NAME", "IBLOCK_ID", "DETAIL_PAGE_URL", "PROPERTY_PHONE", "PROPERTY_RIELTOR", "PROPERTY_F", "PROPERTY_I");
                $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID,
                                    "ACTIVE"=>"Y",
                                    "!ID" => $arFields['ID'],
                                    array("LOGIC"=>"OR", 
                                        array("PROPERTY_PHONE" => $arFields['PROPERTY_VALUES']['PHONE']), 
                                        array("PROPERTY_PHONE" => $arFields['PROPERTY_VALUES']['DOP_PHONE']), 
                                        array("PROPERTY_DOP_PHONE" => $arFields['PROPERTY_VALUES']['PHONE']), 
                                        array("PROPERTY_DOP_PHONE" => $arFields['PROPERTY_VALUES']['DOP_PHONE']), 
                                    ),
                            );
                $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
                
                if ($ob = $res->GetNextElement()) {
                    $clients = $ob->GetFields();
                    
                    $rsUser = CUser::GetByID($clients['PROPERTY_RIELTOR_VALUE']);
                    $arUser = $rsUser->Fetch();
                    
                    $rsUser = CUser::GetByID(CUser::GetID());
                    $CurUser = $rsUser->Fetch();
                    
                    //отправлять другой шаблон письма - про О.М.
                    if (!empty($arUser['EMAIL'])){
                        $arMail = array(
                            "EMAIL" => $arUser['EMAIL'],
                            "AGENT" => '<a href=http://'.$_SERVER['SERVER_NAME'].'/agents/'.$CurUser['ID'].'/ >'.$CurUser['LAST_NAME'].' '.$CurUser['NAME'].'</a>',
                            "NAME" => $arFields['PROPERTY_VALUES']['F'].' '.$arFields['PROPERTY_VALUES']['I'],
                            "PHONE" => $arFields['PROPERTY_VALUES']['PHONE'],
                            "CLIENT" => '<a href=http://'.$_SERVER['SERVER_NAME'].$clients['DETAIL_PAGE_URL'].'>'.$clients['PROPERTY_F_VALUE'].' '.$clients['PROPERTY_I_VALUE'].'</a>',
                        );
                        CEvent::Send("ADD_OLD_CLIENT", array("s1"), $arMail, "N", 26);
                    }
                    
                    //Выдавать exception - что клиент есть, Вам открыт доступ - ссылка
                    global $APPLICATION;
                    $APPLICATION->ThrowException('Клиент с таким телефоном уже присутствует у <a target="blank" href=/agents/'.$clients['PROPERTY_RIELTOR_VALUE'].'/>'.$arUser['LAST_NAME'].' '.$arUser['NAME'].'</a>');
                    return false;
                }
            }
            if(strlen(trim($arFields['PROPERTY_VALUES']['I']))<=0){
                global $APPLICATION;
                $APPLICATION->ThrowException('Поле \'Имя\' должно быть заполнено');
                return false;
            }
            if(strlen(trim($mail))>0 && strpos($mail,'@')===false){
                global $APPLICATION;
                $APPLICATION->ThrowException('Поле \'Эл. почта\' заполнено неверно');
                return false;
            }
        }
    } elseif ($arFields['IBLOCK_ID'] == 5){
        $arFields['IPROPERTY_TEMPLATES']['ELEMENT_PAGE_TITLE'] = '';
    }
}

function OnBeforeIBlockElementUpdateHandler(&$arFields) {
    
    global $APPLICATION;
    global $USER;
    $text_error = new CAdminException();
    $error = false; 
    
    
    if ($arFields['IBLOCK_ID'] == 16 && !$USER->IsAdmin()){
        if ($arFields['PROPERTY_VALUES']['City']['VALUE'] == 49 && empty($arFields['PROPERTY_VALUES']['District']['VALUE']) ){
            $error = true;
            $text_error->AddMessage(   
                array ("text" => 'Заполните район города',)
            );
        }
        switch ($arFields['PROPERTY_VALUES']['STATUS']['VALUE']){
            case 8962://Презентация
                if (empty($arFields['PROPERTY_VALUES']['WATCHER']['VALUE'])){
                    $error = true;
                    $text_error->AddMessage(   
                        array ("text" => 'Для выбранного статуса необходимо заполнить поле Риэлтор',)
                    );
                }
            case 8940:
            case 8963:
            case 8964:
            case 8965:
            case 8966:
            case 8967:
            case 8968:
            case 8971:
            case 8972:
            case 8973:
            case 8975:
                if (empty($arFields['PROPERTY_VALUES']['CURRENT_DATE_OPTION']['VALUE'])){
                    $error = true;
                    $text_error->AddMessage(   
                        array ("text" => 'Для выбранного статуса необходимо заполнить поле Дата/Время',)
                    );
                }
                break;
            case 8977:
            case 8978:
            case 8979:
            case 11106:
            case 11377:
                if (empty($arFields['PROPERTY_VALUES']['CURRENT_COMMENT_OPTION'])){
                    $error = true;
                    $text_error->AddMessage(   
                        array ("text" => 'Для выбранного статуса необходимо заполнить поле Комментарий',)
                    );
                }
                break;
        }

        $arFields['IBLOCK_SECTION'][] = 55;//Раздел с квартирами у объектов
        $arFields['NAME'] = $arFields['PROPERTY_VALUES']['Street']." ".$arFields['PROPERTY_VALUES']['HOUSE'];
        if (!empty($arFields['PROPERTY_VALUES']['APARTMENT'])){
            $arFields['NAME'] .= ", кв. ".$arFields['PROPERTY_VALUES']['APARTMENT'];
        }
        
        $objectID = $arFields['ID']? $arFields['ID'] : $_REQUEST['CODE'];
        if(CModule::IncludeModule('iblock') && $objectID){
            $resultObject = CIBlockElement::GetByID($objectID);
            if ($arResultObject = $resultObject->GetNextElement()){
                $arObject = $arResultObject->GetFields();
                $arObject['properties'] = $arResultObject->GetProperties();
            }
            $IBLOCK_ID = 5;
            $arSelect = Array("ID", "NAME", "PROPERTY_*");
            $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "PROPERTY_OBJECT_ELEMENT" =>$objectID);
            //Текущее связанное с объектом объявление
            $res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter, false, false);
            
            //Очистка статусов "осмотра" при смене риелтора
            if ($arFields['PROPERTY_VALUES']['WATCHER']['VALUE'] != $arObject['properties']['WATCHER']['VALUE']){
                $arFields['PROPERTY_VALUES']['IS_WATCH']['VALUE'] = "";
                $arFields['PROPERTY_VALUES']['WATCH_AGREE']['VALUE'] = "";
                $arFields['PROPERTY_VALUES']['WATCHER_COMMENT']['VALUE']['TEXT'] = "";
                
                if($ob = $res->GetNextElement()){
                    $arAdvert = $ob->GetFields();
                    $textReason = "Автоматическая деактивация: смена агента у связанного объекта";
                    CIBlockElement::SetPropertyValues($arAdvert['ID'], $IBLOCK_ID, $textReason, 'REASON');   
                    $el = new CIBlockElement;
                    $resultStatus = $el->Update($arAdvert['ID'], Array("ACTIVE" => "N"));
                    addLog("Удаление", $textReason, CUser::GetID(), 'Объявление', $arAdvert['ID']);
                }
            }
            //Деактивация прикрепленного объявления при смене основного статуса (при продаже)
            if ($arFields['PROPERTY_VALUES']['STATUS']['VALUE'] != $arObject['properties']['STATUS']['VALUE']
                && in_array($arFields['PROPERTY_VALUES']['STATUS']['VALUE'], array(8977, 8978, 8979, 11106, 11377))){
                
                if($ob = $res->GetNextElement()){
                    $arAdvert = $ob->GetFields();
                    $textReason = "Автоматическая деактивация: смена статуса у связанного объекта (продано)";
                    CIBlockElement::SetPropertyValues($arAdvert['ID'], $IBLOCK_ID, $textReason, 'REASON');   
                    $el = new CIBlockElement;
                    $resultStatus = $el->Update($arAdvert['ID'], Array("ACTIVE" => "N"));
                    addLog("Удаление", $textReason, CUser::GetID(), 'Объявление', $arAdvert['ID']);
                }
            }
        }
    }
    
    //Проверка связанных вариантов при деактивации
    if ($arFields['IBLOCK_ID'] == 5 && $arFields['ACTIVE'] == 'N'){
        if(CModule::IncludeModule("iblock")){
            $IBLOCK_ID_VARIANTS = 19; 
            $arOrder = Array("ID"=>"DESC");
            $arSelect = Array("ID", "NAME", "IBLOCK_ID", "ACTIVE_FROM", "DETAIL_TEXT", "CREATED_BY", "PROPERTY_*");
            $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID_VARIANTS, "ACTIVE"=>"Y", "PROPERTY_ADVERT"=>$arFields['ID']);
            $list = CIBlockElement::GetList($arOrder, $arFilter, false, $navArParams, $arSelect);
            while ($ob = $list->GetNextElement()) {
                $arVariant = $ob->GetFields();
                $arLoadProperties = array('STATUS' => 560);
                CIBlockElement::SetPropertyValuesEx($arVariant['ID'], $IBLOCK_ID_VARIANTS, $arLoadProperties);
                $el = new CIBlockElement;
                $res = $el->Update($arVariant['ID'], Array("DETAIL_TEXT" => "Перемещен в обработанные в связи с удалением объявления"));
            }
        }
    }
    
    if ($arFields['IBLOCK_ID'] == 5 && isset($arFields['PROPERTY_VALUES'])){//Добавление объявлений
        $ar_res = getSectionProps(5,$arFields["IBLOCK_SECTION"][0]);
        //Floor, integer,от 1 до 99
        $floor = $arFields['PROPERTY_VALUES']['Floor'];
        if (!empty($floor) && (!is_numeric($floor) || fmod($floor,1)!=0 || ($floor<1 || $floor>99))){
            $error = true;  
               $text_error->AddMessage(
                 array ("text" => 'Поле \'Этаж\' должно быть целым числом от 1 до 99.',)
               );
        }
        // Сlient, Поле клиент обязательное
        $prop_client = $arFields['PROPERTY_VALUES']['Client']['VALUE'];
        if (empty($prop_client) && empty($arFields['PROPERTY_VALUES']['OBJECT_ELEMENT']) && !$USER->IsAdmin()){
            $error = true; 
            $text_error->AddMessage(   
            array ("text" => 'Укажите клиента!',)
           );
        }
                //Поле "Залог при аренде" обязательное у разделов "квартиры/комнаты/дома" при сдаче в аренду
        if (in_array('PROPERTY_LeaseDeposit', $ar_res['UF_PROPERTY'])){
            $deposit = $arFields['PROPERTY_VALUES']['LeaseDeposit'];
            if (empty($deposit)){                                                  
                $error = true;
                $text_error->AddMessage(
                    array ("text" => 'Заполните поле \'Залог при аренде\'.',)
                );
            }
        }
        
        //Поле "Комиссия" обязательное у разделов "квартиры/комнаты/дома" при сдаче в аренду
        if (in_array('PROPERTY_LeaseCommission', $ar_res['UF_PROPERTY'])){
            $deposit = $arFields['PROPERTY_VALUES']['LeaseCommission'];
            if (empty($deposit)){                                                  
                $error = true;
                $text_error->AddMessage(
                    array ("text" => 'Заполните поле \'Комиссия\'.',)
                );
            }
        }
        //Поле "Размер комиссии" обязательное у разделов "квартиры/комнаты/дома" при сдаче в аренду
        if (in_array('PROPERTY_LeaseCommissionSize', $ar_res['UF_PROPERTY'])){
            $depositSize = $arFields['PROPERTY_VALUES']['LeaseCommissionSize'];
            if (empty($depositSize) && $arFields['PROPERTY_VALUES']['LeaseCommission'] == 575){                                                  
                $error = true;
                $text_error->AddMessage(
                    array ("text" => 'Заполните поле \'Размер комиссии\'.',)
                );
            }
            // LeaseCommission, integer
            if (!empty($depositSize) && (!is_numeric($depositSize) || fmod($depositSize,1)!=0 || $depositSize >100)){
                $error = true;  
                $text_error->AddMessage(   
                    array ("text" => 'Поле \'Размер комиссии\' должно быть целым числом меньше 100 (процент).',)
                );
            }   
        }
        
        //Поле "Новостройка" обязательное для типа квартиры
        if (in_array('PROPERTY_NewDevelopmentId', $ar_res['UF_PROPERTY'])){
            $NewDevelopmentId = $arFields['PROPERTY_VALUES']['NewDevelopmentId']['VALUE'];
            if (empty($NewDevelopmentId) && $arFields['PROPERTY_VALUES']['MarketType'] == 21){                                                  
                $error = true;
                $text_error->AddMessage(
                    array ("text" => 'Выберите объект новостройки',)
                );
            }
        }
        
        //Поле "Подвид объекта" обязательное у раздела "Гаражи и машиноместа" при продаже/сдаче в аренду
        if (in_array('PROPERTY_ObjectSubtype1', $ar_res['UF_PROPERTY']) && in_array('PROPERTY_ObjectSubtype2', $ar_res['UF_PROPERTY']) && !$USER->IsAdmin()){
            $subType1 = $arFields['PROPERTY_VALUES']['ObjectSubtype1'];
            $subType2 = $arFields['PROPERTY_VALUES']['ObjectSubtype2'];
            if (empty($subType1) && empty($subType2)){                                                  
                $error = true;
                $text_error->AddMessage(
                    array ("text" => 'Заполните поле \'Подвид объекта\'.',)
                );
            }
        }
        
        //Поле "Тип стен" обязательное для раздела "Дома, коттеджи" с версии 3.0
        if (in_array('PROPERTY_WallsType', $ar_res['UF_PROPERTY']) && !$USER->IsAdmin()){
            $WallsType = $arFields['PROPERTY_VALUES']['WallsType'];
            if (empty($WallsType)){ 
                $error = true;
                $text_error->AddMessage(
                    array ("text" => 'Заполните поле \'Тип стен\'.',)
                );
            }
        }
        
        //Поле "Тип дома" обязательное для раздела "Квартиры" с версии 3.0
        if (in_array('PROPERTY_HouseType', $ar_res['UF_PROPERTY']) && !$USER->IsAdmin()){
            $HouseType = $arFields['PROPERTY_VALUES']['HouseType'];
            if (empty($HouseType)){ 
                $error = true;
                $text_error->AddMessage(
                    array ("text" => 'Заполните поле \'Тип дома\'.',)
                );
            }
        }
        //Description, обязательное поле с версии 3.0
        if (empty($arFields['DETAIL_TEXT']) && !$USER->IsAdmin()){
            $error = true;  
            $text_error->AddMessage(   
                array ("text" => 'Поле \'Описание\' обязательно для заполнения',)
            );
        } elseif (strlen($arFields['DETAIL_TEXT'])>3000){ //3000 символов с версии 3.0
            $error = true;  
            $text_error->AddMessage(   
                array ("text" => 'Описание не должно превышать 3000 символов.',)
            );
        }

    }
    
    // Если ошибка в заполнении, выдать исключение
    if ($error){
        $APPLICATION->ThrowException($text_error);
        return false;
    }
    
    
    if ($arFields['IBLOCK_ID'] == 10) {//клиенты
        if ($arFields['PROPERTY_VALUES']['I']) {
            $fio = $arFields['PROPERTY_VALUES']['F'].' '.$arFields['PROPERTY_VALUES']['I'].' '.$arFields['PROPERTY_VALUES']['O'];
            $mail = $arFields['PROPERTY_VALUES']['EMAIL'];
            $arFields['NAME'] = $fio;
            $arFields['CODE'] = Cutil::translit($arFields['NAME'], 'ru', array(
                'replace_space' => '-',
                'replace_other' => '-'
            ));
            if(strpos($arFields['PROPERTY_VALUES']['PHONE'], "_")){
                global $APPLICATION;
                $APPLICATION->ThrowException('Поле \'Телефон\' заполнено не полностью');
                return false;
            } else {
                CModule::IncludeModule("iblock");
                 
                $IBLOCK_ID = 10; 
             
                $arOrder = Array("SORT"=>"ASC");
                $arSelect = Array("ID", "NAME", "IBLOCK_ID", "DETAIL_PAGE_URL", "PROPERTY_PHONE", "PROPERTY_RIELTOR");
                $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID,
                                    "ACTIVE"=>"Y",
                                    "!ID" => $arFields['ID'],
                                    array("LOGIC"=>"OR", 
                                        array("PROPERTY_PHONE" => $arFields['PROPERTY_VALUES']['PHONE']), 
                                        array("PROPERTY_DOP_PHONE" => $arFields['PROPERTY_VALUES']['PHONE']), 
                                    ),
                            );
                $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
                
                if ($ob = $res->GetNextElement()) {
                    $clients = $ob->GetFields();
                    
                    $rsUser = CUser::GetByID($clients['PROPERTY_RIELTOR_VALUE']);
                    $arUser = $rsUser->Fetch();
                    
                    $rsUser = CUser::GetByID(CUser::GetID());
                    $CurUser = $rsUser->Fetch();
                    
                    if (!empty($arUser['EMAIL'])){
                        $arMail = array(
                            "EMAIL" => $arUser['EMAIL'],
                            "AGENT" => '<a href=http://'.$_SERVER['SERVER_NAME'].'/agents/'.$CurUser['ID'].'/ >'.$CurUser['LAST_NAME'].' '.$CurUser['NAME'].'</a>',
                            "NAME" => $arFields['PROPERTY_VALUES']['F'].' '.$arFields['PROPERTY_VALUES']['I'],
                            "PHONE" => $arFields['PROPERTY_VALUES']['PHONE'],
                            "CLIENT" => '<a href=http://'.$_SERVER['SERVER_NAME'].$clients['DETAIL_PAGE_URL'].'>'.$clients['PROPERTY_F_VALUE'].' '.$clients['PROPERTY_I_VALUE'].'</a>',
                        );
                        CEvent::Send("ADD_OLD_CLIENT", array("s1"), $arMail, "N", 26);
                    }
                    
                    global $APPLICATION;
                    $APPLICATION->ThrowException('Клиент с таким телефоном уже присутствует у <a target="blank" href=/agents/'.$clients['PROPERTY_RIELTOR_VALUE'].'/>'.$arUser['LAST_NAME'].' '.$arUser['NAME'].'</a>');
                    return false;
                }
            }
            if(strlen(trim($arFields['PROPERTY_VALUES']['I']))<=0){
                global $APPLICATION;
                $APPLICATION->ThrowException('Поле \'Имя\' должно быть заполнено');
                return false;
            }
            if(strlen(trim($mail))>0 && strpos($mail,'@')===false){
                global $APPLICATION;
                $APPLICATION->ThrowException('Поле \'Эл. почта\' заполнено неверно');
                return false;
            }
        }
    } elseif ($arFields['IBLOCK_ID'] == 5){
        $arFields['IPROPERTY_TEMPLATES']['ELEMENT_PAGE_TITLE'] = '';
    }
}

function OnAfterIBlockElementAddUpdateHandler(&$arFields) {

    global $USER;
    CModule::IncludeModule("iblock");
    // if ($USER->GetID() == 1){
        // pre($arFields);
        // die();
    // }
    
    if ($arFields['IBLOCK_ID'] == 16){
        $objectID = $arFields['ID'] ? $arFields['ID'] : $_REQUEST['CODE'];
        if($objectID){
            $IBLOCK_ID = 5;
            $arSelect = Array("ID", "NAME", "PROPERTY_*");
            $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "PROPERTY_OBJECT_ELEMENT" =>$objectID);
            //Текущее связанное с объектом объявление
            $res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter, false, false);
            
            if($ob = $res->GetNextElement()){
                $arAdvert = $ob->GetFields();
                $arAdvert['properties'] = $ob->GetProperties();
                $resultObject = CIBlockElement::GetByID($objectID);
                if ($arResultObject = $resultObject->GetNextElement()){
                    $arObject = $arResultObject->GetFields();
                    $arObject['properties'] = $arResultObject->GetProperties();
                    if ($USER->GetID() == 200){
                        $el = new CIBlockElement;

                        $arProperty = array();
                        $arPropertyKeys = array('District', 'City', 'Floor', 'Floors', 'Square', 'Street',
                                                'Rooms', 'MarketType', 'HOUSE', 'APARTMENT',
                                                );
                        foreach ($arPropertyKeys as $propKey){
                            $arProperty[$propKey] = $arObject['properties'][$propKey]['VALUE'];
                        }
                        
                        $arHardPropertyKeys = array('MarketType', 'HouseType', 'Rooms');   
                        foreach ($arHardPropertyKeys as $propKey){
                            $propertyEnum = getEnumValues(5, array($propKey));
                            $valueEnum = array_flip($propertyEnum[$propKey]);
                            $arProperty[$propKey] = $valueEnum[$arObject['properties'][$propKey]['VALUE']];
                        }            

                        $arProperty['OBJECT_ELEMENT'] = $objectID;
                        $arProperty['Price'] = $arObject['properties']['PRICE']['VALUE'];
                        $arProperty['Author'] = $arObject['properties']['WATCHER']['VALUE'];
                        
                        $arProperty['CALLC']['VALUE'] = $arAdvert['properties']['CALLC']['VALUE_ENUM_ID'][0];
                        $arProperty['AdStatus']['VALUE'] = $arAdvert['properties']['AdStatus']['VALUE_ENUM_ID'][0];
                        $arProperty['Avito']['VALUE'] = $arAdvert['properties']['Avito']['VALUE_ENUM_ID'][0];
                        $arProperty['MAIN_SITE']['VALUE'] = $arAdvert['properties']['MAIN_SITE']['VALUE_ENUM_ID'][0];

                        $arLoadProductArray = Array(
                          "MODIFIED_BY"    => $USER->GetID(),
                          "IBLOCK_ID"      => $IBLOCK_ID,
                          "PROPERTY_VALUES"=> $arProperty,
                          );
                        if($resultStatus = $el->Update($arAdvert['ID'], $arLoadProductArray)){
                            $ipropValuesRes = new \Bitrix\Iblock\InheritedProperty\ElementValues($IBLOCK_ID, $arAdvert['ID']);
                            $ipropValues = $ipropValuesRes->getValues();
                            if ($arAdvert['NAME'] != $ipropValues['ELEMENT_PAGE_TITLE']) {
                                $adsUpdateElement = new CIBlockElement;
                                $adsUpdateElement->Update($arAdvert["ID"], array('NAME'=>$ipropValues['ELEMENT_PAGE_TITLE']));
                            }
                        }
                    }
                }
            }
        }
    }
    
    $rsUser = CUser::GetByID(CUser::GetID());
    $currentUser = $rsUser->Fetch();
    
    if ($arFields['IBLOCK_ID'] == 5 && !empty($_REQUEST['CODE']) && !isset($_REQUEST['ajax'])){
        addLog("Изменение", "Было изменено объявление", $currentUser['ID'], 'Объявление', $arFields['ID']);
        return;
    }    
    if ($arFields['IBLOCK_ID'] == 10 && $_REQUEST['edit']=='Y')
    {
        if (!empty($arFields['PROPERTY_VALUES']['MANAGER']) && $arFields['PROPERTY_VALUES']['MANAGER'] != $arFields['PROPERTY_VALUES']['RIELTOR']){
            $rsManager = CUser::GetByID($arFields['PROPERTY_VALUES']['MANAGER']);
            $currentManager = $rsManager->Fetch();
            $arMail = array(
                "manager_email" => $currentManager['EMAIL'] ? $currentManager['EMAIL'] : "",
                "client_id61" => $arFields['ID'],
            );

            CEvent::Send("UPDATE_CLIENT", array("s1"), $arMail, "N", 30);
        }
        addLog("Изменение", "Был изменен клиент", $currentUser['ID'], 'Клиент', $arFields['ID']);
        return;
    }
    
    
    if ($arFields['IBLOCK_ID'] != 5) return;

    if (!empty($arFields['PROPERTY_VALUES']['ALERT']['0'])){
        $IBLOCK_ID = 5;

        $arOrder = Array("SORT"=>"ASC");
        $extra_filter = array("LOGIC" => "AND",
                            array("LOGIC" => "OR",
                                    array("=PROPERTY_SquareList_VALUE" => "Любая"),
                                    array("LOGIC" => "AND",
                                            array("LOGIC" => "OR",
                                                    array("<=PROPERTY_SquareFrom" => $arFields['PROPERTY_VALUES']['Square']),
                                                    array("=PROPERTY_SquareFrom" => false),
                                                ),
                                            array("LOGIC" => "OR",
                                                    array(">=PROPERTY_SquareTo" => $arFields['PROPERTY_VALUES']['Square']),
                                                    array("=PROPERTY_SquareTo" => false),
                                                )
                                        )
                                ),
                            array("LOGIC" => "OR",
                                    array("=PROPERTY_PriceList_VALUE" => "Любая"),
                                    array("LOGIC" => "AND",
                                            array("LOGIC" => "OR",
                                                    array("<=PROPERTY_PriceFrom" => $arFields['PROPERTY_VALUES']['Price']),
                                                    array("=PROPERTY_PriceFrom" => false),
                                                ),
                                            array("LOGIC" => "OR",
                                                    array(">=PROPERTY_PriceTo" => $arFields['PROPERTY_VALUES']['Price']),
                                                    array("=PROPERTY_PriceTo" => false),
                                                )
                                        )
                                ),
                    );
        if (!empty($arFields['PROPERTY_VALUES']['Rooms'])){
             $extra_filter[] = array("LOGIC" => "OR",
                                    array('=PROPERTY_RoomsList_VALUE'=>getValueOfEnum(5, 'Rooms', $arFields['PROPERTY_VALUES']['Rooms'])),
                                    array('=PROPERTY_RoomsList_VALUE'=>false),
                                );

        }
        
        if (!empty($arFields['PROPERTY_VALUES']['District']['VALUE'])){
            $district = $arFields['PROPERTY_VALUES']['District']['VALUE'];
        } else {
            $district = "";
        }
        $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID,
                    "ACTIVE"=>"Y",
                    "!PROPERTY_Author"=>false,
                    "PROPERTY_City"=>$arFields['PROPERTY_VALUES']['City']['VALUE'],
                    "PROPERTY_District"=>$district,
                    "SECTION_ID"=>getRevSect(5, $arFields['IBLOCK_SECTION'][0], true),
                    $extra_filter,
                );
                        
                        
        $arGroup = Array('PROPERTY_Author');

        $res = CIBlockElement::GetList($arOrder, $arFilter, $arGroup);

        while ($ob = $res->GetNextElement()){
            $authorList[] = $ob->GetFields();

        }
        // pre($arFields);
        // pre($authorList);
        foreach ($authorList as $value){
            if ($value['PROPERTY_AUTHOR_VALUE'] != CUser::GetID()){
                $rsUser = CUser::GetByID($value['PROPERTY_AUTHOR_VALUE']);
                $arUser = $rsUser->Fetch();
                // pre($arUser);
                
                $rsUser = CUser::GetByID(CUser::GetID());
                $CurUser = $rsUser->Fetch();
                
                $res = CIBlockElement::GetByID($arFields["ID"]);
                if($ar_res = $res->GetNext()){
                    // pre($ar_res);
                    if ($arUser['UF_ALERT'] &&!empty($arUser['EMAIL'])){
                        $arMail = array(
                            // "EMAIL" => 'kasimovar@picom.ru',
                            // "EMAIL" => 'kasimovanton@bk.ru',
                            "EMAIL" => $arUser['EMAIL'],
                            "LINK" => "http://".$_SERVER['SERVER_NAME'].$ar_res['DETAIL_PAGE_URL'],
                            "USER_NAME" => ''.$CurUser['NAME'].' '.$CurUser['LAST_NAME'],
                        );
                        CEvent::Send("NEW_ADS", array("s1"), $arMail, "N", 25);
                    }
                }
            }
        }

        // die();
    }
    

    
    if ($arFields["ID"]>0){
        $ipropValuesRes = new \Bitrix\Iblock\InheritedProperty\ElementValues($arFields['IBLOCK_ID'], $arFields["ID"]);
        $ipropValues = $ipropValuesRes->getValues();
        if ($arFields['NAME'] != $ipropValues['ELEMENT_PAGE_TITLE']) {
            $arFields['NAME'] = $ipropValues['ELEMENT_PAGE_TITLE'];
            $el = new CIBlockElement;
            $el->Update($arFields["ID"], array('NAME'=>$ipropValues['ELEMENT_PAGE_TITLE']));
        }
    }
    


}
function OnAfterUserAddHandler(&$arFields){
	addNewContactsChat($arFields["ID"]);
}

// function OnBeforeIBlockPropertyUpdateHandler(&$arFields){   

    // pre($arFields);
    // die();

    // if ($arFields['IBLOCK_ID'] != 5) return;
    // if(strlen($arFields["CODE"])<=0)
    // {
        // global $APPLICATION;
        // $APPLICATION->throwException("Введите символьный код. (ID:".$arFields["ID"].")");
        // return false;
    // }
// }