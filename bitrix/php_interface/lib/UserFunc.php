<?php
/**
 * Created by PhpStorm.
 * User: picom
 * Date: 02.12.14
 * Time: 12:17
 */

class UserFunc {
    function GetNewDevelopmentFieldHtml($arProperty, $value, $strHTMLControlName){
        $bWasSelect = false;
        $newDevelopment = true;
        $options = UserFunc::GetOptionsHtml($arProperty, array($value["VALUE"]), $bWasSelect, $newDevelopment);

        $html = '<select name="'.$strHTMLControlName["VALUE"].'"'.$size.$width.'>';
        if($arProperty["IS_REQUIRED"] != "Y" )
            $html .= '<option value=""'.(!$bWasSelect? ' selected': '').'>'.GetMessage("IBLOCK_PROP_ELEMENT_LIST_NO_VALUE").'</option>';
        $html .= $options;
        $html .= '</select>';
        return  $html;
    }
    function GetAgentFieldHtml($arProperty, $value, $strHTMLControlName){
        /*pre($arProperty);
        pre($value);
        pre($strHTMLControlName);*/
        $settings = CIBlockPropertyElementList::PrepareSettings($arProperty);
        //pre($settings);
        if($settings["size"] > 1)
            $size = ' size="'.$settings["size"].'"';
        else
            $size = '';

        if($settings["width"] > 0)
            $width = ' style="width:'.$settings["width"].'px"';
        else
            $width = '';

        $bWasSelect = false;
        $options = UserFunc::GetOptionsHtml($arProperty, array($value["VALUE"]), $bWasSelect);

        $html = '<select name="'.$strHTMLControlName["VALUE"].'"'.$size.$width.'>';
        if($arProperty["IS_REQUIRED"] != "Y" )
            $html .= '<option value=""'.(!$bWasSelect? ' selected': '').'>'.GetMessage("IBLOCK_PROP_ELEMENT_LIST_NO_VALUE").'</option>';
        $html .= $options;
        $html .= '</select>';
        return  $html;
    }

    function GetOptionsHtml($arProperty, $values, &$bWasSelect, $newDevelopment=false)
    {
        $options = "";
        $settings = CIBlockPropertyElementList::PrepareSettings($arProperty);
        $bWasSelect = false;

        if($settings["group"] === "Y")
        {
            $arElements = UserFunc::GetElements($arProperty["LINK_IBLOCK_ID"]);
            $arTree = CIBlockPropertyElementList::GetSections($arProperty["LINK_IBLOCK_ID"]);
            foreach($arElements as $i => $arElement)
            {
                if(
                    $arElement["IN_SECTIONS"] == "Y"
                    && array_key_exists($arElement["IBLOCK_SECTION_ID"], $arTree)
                )
                {
                    $arTree[$arElement["IBLOCK_SECTION_ID"]]["E"][] = $arElement;
                    unset($arElements[$i]);
                }
            }

            foreach($arTree as $arSection)
            {
                $options .= '<optgroup label="'.str_repeat(" . ", $arSection["DEPTH_LEVEL"]-1).$arSection["NAME"].'">';
                if(isset($arSection["E"]))
                {
                    foreach($arSection["E"] as $arItem)
                    {
                        $options .= '<option value="'.$arItem["ID"].'"';
                        if(in_array($arItem["~ID"], $values))
                        {
                            $options .= ' selected';
                            $bWasSelect = true;
                        }
                        $options .= '>'.$arItem["NAME"].'</option>';
                    }
                }
                $options .= '</optgroup>';
            }
            foreach($arElements as $arItem)
            {
                $options .= '<option value="'.$arItem["ID"].'"';
                if(in_array($arItem["~ID"], $values))
                {
                    $options .= ' selected';
                    $bWasSelect = true;
                }
                $options .= '>'.$arItem["NAME"].'</option>';
            }

        }
        else
        {
            foreach(UserFunc::GetElements($arProperty["LINK_IBLOCK_ID"]) as $arItem)
            {
                if (!$newDevelopment){
                    $options .= '<option value="'.$arItem["ID"].'"';
                    if(in_array($arItem["~ID"], $values)  || (!empty($_REQUEST['Сlient']) && $_REQUEST['Сlient'] == $arItem["ID"]))
                    {
                        $options .= ' selected';
                        $bWasSelect = true;
                    }
                    $options .= '>'.$arItem["NAME"].'</option>';
                } else {
                    $options .= '<option value="'.$arItem["NAME"].'"';
                    if(in_array($arItem["~ID"], $values)  || (!empty($_REQUEST['Сlient']) && $_REQUEST['Сlient'] == $arItem["ID"]))
                    {
                        $options .= ' selected';
                        $bWasSelect = true;
                    }
                    $options .= '>'.$arItem["PROPERTY_NAME_VALUE"].'</option>';
                }
            }
            if (!$bWasSelect && $arProperty["LINK_IBLOCK_ID"]==10 && !empty($values[0])){
                echo '<span class="red">';
                echo 'Клиент данного объявления недоступен для Вас. При редактировании объявления, поле "Клиент" будет сброшено. Обратитесь к администратору для уточнения.';
                echo '</span>';
            }
        }

        return  $options;
    }

    function GetElements($IBLOCK_ID)
    {
        static $cache = array();
        $IBLOCK_ID = intval($IBLOCK_ID);

        if(!array_key_exists($IBLOCK_ID, $cache))
        {
            $cache[$IBLOCK_ID] = array();
            if($IBLOCK_ID > 0)
            {
                $arSelect = array(
                    "ID",
                    "NAME",
                    "IN_SECTIONS",
                    "IBLOCK_SECTION_ID",
                    "PROPERTY_NAME", //for new_development
                );
                $arFilter = array (
                    "IBLOCK_ID"=> $IBLOCK_ID,
                    "ACTIVE" => "Y",
                    "CHECK_PERMISSIONS" => "Y",

                );
                if ($IBLOCK_ID == 10) {//клиенты
                    // $arFilter["PROPERTY_RIELTOR"] = CUser::GetID();
                    $arFilter["PROPERTY_RIELTOR"] = getChildsId(CUser::GetID(), false, true);
                    $arFilter["PROPERTY_RIELTOR"][] = CUser::GetID();
                    // print_r($arProperty);
                }
                $arOrder = array(
                    "SORT" => "ASC",
                    "NAME" => "ASC",
                    "ID" => "ASC",
                );
                $rsItems = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
                while($arItem = $rsItems->GetNext())
                    $cache[$IBLOCK_ID][] = $arItem;
            }
        }
        return $cache[$IBLOCK_ID];
    }
} 