<?php
/**
 * Created by Konstantin Pushin.
 * Date: 07.06.13
 * Time: 11:00
 */
namespace Apteka;

class ApArray
{
    public static function get($array, $key, $default = null)
    {
        $array = self::normalize($array);
        return isset($array[$key])? $array[$key] : $default;
    }

    public static function first($array)
    {
        return array_shift(self::normalize($array));
    }

    public static function normalize($value)
    {
        if (!$value) $value = array();
        if (!is_array($value)) $value = array($value);
        return $value;
    }

    public static function set(&$array, array $path, $value)
    {
        foreach($path as $part) {
            if (!isset($array[$part])) $array[$part] = array();
            $array = &$array[$part];
        }
        $array = $value;
    }


    /**
     * Returns the values from a single column of the input array, identified by the columnKey.
     *
     * Optionally, you may provide an indexKey to index the values in the returned array by the values from the indexKey column in the input array.
     *
     * @param array[]         $input                 A multi-dimensional array (record set) from which to pull a column of values.
     * @param int|string      $columnKey             The column of values to return. This value may be the integer key of the column you wish to retrieve, or it may be the string key name for an associative array.
     * @param int|string      $indexKey              The column to use as the index/keys for the returned array. This value may be the integer key of the column, or it may be the string key name.
     *
     * @return mixed[]
     */
    public static function column ($input, $columnKey, $indexKey = null) {
        if (!is_array($input)) {
            return false;
        }
        if ($indexKey === null) {
            foreach ($input as $i => &$in) {
                if (is_array($in) && isset($in[$columnKey])) {
                    $in    = $in[$columnKey];
                } else {
                    unset($input[$i]);
                }
            }
        } else {
            $result    = array();
            foreach ($input as $i => $in) {
                if (is_array($in) && isset($in[$columnKey])) {
                    if (isset($in[$indexKey])) {
                        $result[$in[$indexKey]]    = $in[$columnKey];
                    } else {
                        $result[]    = $in[$columnKey];
                    }
                    unset($input[$i]);
                }
            }
            $input    = &$result;
        }
        return $input;
    }

}
