<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?IncludeTemplateLangFile(__FILE__);?>
<!doctype html>
<html class="no-js content-show">

<head>
    <?$APPLICATION->ShowHead();?>
	<?CJSCore::Init(array('date'));;?>
    <title><?$APPLICATION->ShowTitle()?></title>
    <meta name="viewport" content="width=device-width">
    <link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/vendor.css"/>
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/main.css"/>
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/multuselect.css"/>
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/main_dev.css"/>
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/colorbox.css"/>
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/style_datapicker.css"/>
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/jquery-ui.css"/>
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/select2.css"/>
    <script src="<?=SITE_TEMPLATE_PATH?>/scripts/vendor.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/scripts/vendor/modernizr.js"></script>
    <?if (preg_match("/search/", $APPLICATION->GetCurDir())):?>
        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/vendor/jquery.sumoselect_ajax_search.js"></script>
    <?else:?>
        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/vendor/jquery.sumoselect.js"></script>
    <?endif;?>
    <script src="<?=SITE_TEMPLATE_PATH?>/scripts/vendor/jquery.ui.widget.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/scripts/vendor/jquery.iframe-transport.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/scripts/vendor/jquery.fileupload.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/scripts/jquery-ui.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/scripts/jquery.inputmask-multi.js"></script>
        
    <script src="<?=SITE_TEMPLATE_PATH?>/scripts/jquery.plugin.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/scripts/jquery.countdown.js"></script>

    <script src="<?=SITE_TEMPLATE_PATH?>/scripts/script.js"></script>
    <?if (preg_match("/operator/", $APPLICATION->GetCurDir())):?>
        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/script_ajax.js"></script>
    <?endif;?>
    <?if (preg_match("/search/", $APPLICATION->GetCurDir())):?>
        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/script_search.js"></script>
    <?endif;?>
    <?if (preg_match("/needs/", $APPLICATION->GetCurDir())):?>
        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/script_needs.js"></script>
    <?endif;?>
    <script src="<?=SITE_TEMPLATE_PATH?>/scripts/select2.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/scripts/select2_locale_ru.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/scripts/jquery.mousewheel.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/scripts/will_pickdate.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/scripts/main.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/scripts/filter.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/scripts/jquery.cookie.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/scripts/jquery.colorbox.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/scripts/jquery.colorbox-min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/scripts/jquery.flexslider.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/scripts/jquery.mCustomScrollbar.js"></script>
    
    <script src="<?=SITE_TEMPLATE_PATH?>/scripts/datedropper-master/datedropper.js"></script>
    <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/scripts/datedropper-master/datedropper.css"> 
    <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/styles/flexslider.css"> 
    <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/styles/jquery.mCustomScrollbar.css"> 

    <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
<!--[if lt IE 10]>
<p class="browsehappy">Вы используете старый браузер. Пожалуйста, <a href="http://browsehappy.com/">обновите ваш браузер</a></p>
<![endif]-->
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<div class="page_wrapper">
    <?if ($USER->IsAuthorized()):?>
        <?php
            $arMessage = getAlertBlock();
        ?>
        <?if (!empty($arMessage)):?>
            <?if ($_SESSION['warning'] != 'close'):?>
                <div class="system_message_block">
                    <div class="warning">
                        <span><img src="/bitrix/templates/crm/img/warning.png">Внимание!</span>
                    </div>
                    <?php
						$rusData = $arMessage['DATE_ACTIVE_TO'];
						$arDataTime = explode(" ", $rusData);
						$arData = explode(".", $arDataTime[0]);
						$arMessage['DATE_ACTIVE_TO'] = $arData[1].'.'.$arData[0].'.'.$arData[2].' '.$arDataTime[1];
					?>
                    <div class="message">
                        <span class="title"><?=$arMessage['NAME'];?></span>
                        <span class="body">
                            <?=$arMessage['DETAIL_TEXT'] ;?>
                        </span>
                        <div class="countdown_block">
                            Осталось: <div id="padZeroes"></div>
                        </div>
                        <a href="#" class="close">Закрыть[X]</a>
                    </div>
                </div>
                <script>
                    $(document).ready(function() {
                        var austDay = new Date();
                            austDay = new Date("<?=preg_replace("/\./", "/", $arMessage['DATE_ACTIVE_TO']);?>");
                        $('#padZeroes').countdown({until: austDay, format: 'dHMS', compact:true, compactLabels:['г.', 'м.', 'н.', 'д.']});
                    });
                </script>
            <?endif;?>
        <?endif;?>
    <?endif;?>
    <header class="header">
        <?$APPLICATION->ShowViewContent("auth"); //footer.php?> 
        <?/*$APPLICATION->IncludeComponent("bitrix:system.auth.form", ".default", Array(
                "REGISTER_URL" => SITE_DIR."login/",
                "PROFILE_URL" => SITE_DIR."personal/",
                "SHOW_ERRORS" => "N",
            ),
            false
        );*/?>
    </header> <!-- /header -->
    <main class="main">
        <?global $USER;?>
        <?if ($USER->IsAuthorized()) {?>
            <div class="filter_wrapper">
            <?$APPLICATION->IncludeComponent("bitrix:menu", "top", Array(
                "ROOT_MENU_TYPE" => "top",
                "MENU_CACHE_TYPE" => "A",
                "MENU_CACHE_TIME" => "36000000",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "MENU_CACHE_GET_VARS" => "",
                "MAX_LEVEL" => "2",
                "CHILD_MENU_TYPE" => "left",
                "USE_EXT" => "Y",
                "DELAY" => "N",
                "ALLOW_MULTI_SELECT" => "N",
                ),
                false
            );?>
            <?$userStatus = getUserStatus($USER->GetID());?>
                <?if (in_array(8, $userStatus)):?>
                    <div class="operator_fast_search">
                        <form class="section left_section" action="/search/">
                            <?if (!preg_match("/search/", $APPLICATION->GetCurDir())):?>
                                    <div class="block_row search_row">
                                        <label for="search_input">Искать:</label>
                                        <input type="text" name="search_input" autocomplete="off">
                                    </div>
                                    <div class="block_row">
                                        <button class="panel-seacrh-button element-seacrh" name="search_type" value="elements" title="Поиск по объявлениям и объектам"><span></span></button>
                                        <button class="panel-seacrh-button mans-seacrh" name="search_type" value="mans" title="Поиск по клиентам и сотрудникам"><span></span></button>
                                    </div>
                            <?endif;?>
                        </form>
                        <div class="section right_section">
                            <a href="/operator/add.php" class="button button_universal"> + Добавить вызов</a>
                        </div>
                    </div>
                <?endif;?>
                <?$APPLICATION->ShowViewContent("filter"); //bitrix/components/picom/search.form/templates/flat/template.php?>
            </div> <!-- /filter_wrapper -->
        <?}?>
        <?if ($USER->IsAuthorized()) {?>
            <div class="content_wrapper">
                <?if (!preg_match("/temp_clients/", $APPLICATION->GetCurDir())):?>
                    <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", ".default", array(
                        "START_FROM" => "0",
                        "PATH" => "",
                        "SITE_ID" => "-"
                        ),
                        false
                    );?>
                    <div class="content_header"><?$APPLICATION->ShowTitle();?></div>
                <?endif;?>
                <div class="content">
                    <?if (!preg_match("/temp_clients/", $APPLICATION->GetCurDir())):?>
                        <div class="active_filters">
                            <?$APPLICATION->ShowViewContent("count"); //в шаблоне вывода спиcка объявлений (picom:news.list) ?> 
                            <?$APPLICATION->ShowViewContent("params");//news/ads/picom/search.form/flat/template.php?>
                        </div>
                    <?endif;?>
        <?}?>
