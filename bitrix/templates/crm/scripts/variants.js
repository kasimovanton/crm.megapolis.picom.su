$('.status_name .choice_variant').colorbox({
    closeButton: true,
    inline:true,
    href: "#choiceVariant",
    onOpen: function(){
        $('#choiceVariant input[name=variant_id]').val($(this).parents('.variant_element').data('id'));
        $('#choiceVariant h4 span').html($(this).parents('.variant_element').find('.variant_name a').html());
    },
    onClosed: function(){
        
    }
});

$('.history_event').colorbox({
    closeButton: true,
    inline:true,
    href: "#history_event",
    onOpen: function(){
        var elementID = $(this).data('id');
        $.ajax({
            cache: false,
            data: {'ajax':'y', 'element_id':elementID},
            url: '/ajax/get_history.php',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                $('#history_event input[name=element_id]').val(elementID);
                $('.add_history_block ul').html(data);
                $.colorbox.resize();
            }
        });
    },
    onClosed: function(){
        $('#history_event textarea[name=new_comment]').val("");
    }
});  