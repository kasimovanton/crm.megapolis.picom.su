function hideDistrict(){
    $('.district-el').hide();
    $('.district-el select')[0].sumo.unSelectAll();
    $('.district-el').removeClass('with_value');
    $('.district-el').find('.del-input').hide();
}
function clearMansSearch(){
    $('.mans_form input[name=extra_man_value]').val("");
    $('.mans_form input[name=search_input]').removeAttr('disabled');
    $('.mans_tab').addClass('no_additional_row');
}
function addExtraSelect(nameSelect, nameCaption){
    $('.'+nameSelect+'').SumoSelect({captionFormat: nameCaption+": {0}",});
};
function addExtraSlider(nameSlider, minValue, maxValue, paramName, step){
    $('.'+nameSlider+'').slider({
        range:!0,
        min: minValue,
        max: maxValue,
        step: step,
        values: [minValue, maxValue],
        slide:function(event,ui){
            if(ui.values[0] == 0){
                $(".min_"+paramName).val(ui.values[0]).removeAttr('name');
            } else {
                $(".min_"+paramName).val(ui.values[0]).attr('name', ''+paramName+'][min]');
            }
            $("."+nameSlider+" .ui-slider-handle:first").html(ui.values[0]);
            
            if(ui.values[1] != maxValue){
                $("."+nameSlider+" .ui-slider-handle:last").html(ui.values[1]);
                $(".max_"+paramName).val(ui.values[1]).attr('name', ''+paramName+'][max]');
            } else {
                $("."+nameSlider+" .ui-slider-handle:last").html(maxValue+"<i>+</i>");
                $(".max_"+paramName).val(ui.values[1]).removeAttr('name');
            }
        },
        stop: function(event,ui) {
            updateContent();
        }
    });
    $(".min_"+paramName).val($('.'+nameSlider+'').slider("values",0));
    $(".max_"+paramName).val($('.'+nameSlider+'').slider("values",1));
    $("."+nameSlider+" .ui-slider-handle:first").html($('.'+nameSlider+'').slider("values",0));
    $("."+nameSlider+" .ui-slider-handle:last").html($('.'+nameSlider+'').slider("values",1)+"<i>+</i>");
}
function showSearchHeader(){
    var currentSearchType = $('.tabs.visible').data('type');
    $('.search_head span').hide();
    $('.search_head span.'+currentSearchType).show();
    
}    
function haveExactSection(){
    return ($('.ajax_filter select[name=subject]').val() != "" && $('.ajax_filter select[name=type_subjects]').val() != "");
}

function changeExtraFilter(reset){
    if (haveExactSection()){
        $.ajax({
            cache: false,
            data: {'ajax':'y', 'subject': $('.ajax_filter select[name=subject]').val(), "type":$('.ajax_filter select[name=type_subjects]').val()},
            url: 'extrafilter.php',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                $('.extra_filter').html(data).show();
                if (!reset){
                    updateContent();
                }
            }
        });
        
    } else {
        $('.extra_filter').hide();
        if(!$('.ajax_filter select[name=subject]').val()){
            $('.type_subjects-el').hide();
        }
        $('.type_district-el').hide();
        updateContent();
    }
}
function isFilterCompleted(){
    return !$.isEmptyObject(getCurrentFilter());
}
function enableSubmitButton(){
    $('.filter_submit').removeProp('disabled');
    $('.filter_submit').removeClass('gray');
    $('.filter_reset').removeProp('disabled');
    $('.filter_reset').removeClass('gray');
}
function blockSubmitButton(){
    $('.filter_submit').prop('disabled','disabled');
    $('.filter_submit').addClass('gray');
    $('.filter_reset').prop('disabled','disabled');
    $('.filter_reset').addClass('gray');
}
function changeTabsWithValue(tagType, typeValue, newValue){
    $('.tabs_head_block h3:not(.active)').triggerHandler('click');
    $('.visible '+tagType+'[name='+typeValue+']').val(newValue);
    $('body').scrollTop(0);
}
function refreshTab(){
    showSearchHeader();
    blockSubmitButton();
    $('.search_results').hide();
    var activeTab = $('.tabs.visible');
    $('.search_next').each(function(index){
        $(this).data('page', 2);
    });
    activeTab.find('form')[0].reset();
    activeTab.find('input:not([type=checkbox])').val("");
    
    activeTab.find('.ajax_sort select :first').attr("selected", "selected");
    
    $('select[name=city]')[0].sumo.selectItem(0);
    hideDistrict();
    $('.mans_tab').addClass('no_additional_row');
    $('.search_next').show();
    $('.search_turn').show();
    changeExtraFilter(true);
};
function getCurrentSort(){
    return $('.visible .ajax_sort select').val()
}

function getResultView(title){
    var response = "";
    response += '<div>';
    response += title;
    response += '</div>';
    return response;
}

function getResultFirstView(arItem, itemsType){
    var response = "";
    if (itemsType == 'mans'){
        response += '<div class="client_search_card">';
            response += '<div class="client_contacts">';
                response += '<div class="client_name">';
                    response += arItem['name'];
                response += '</div>';
                response += '<div class="agent_phone">'+arItem['phone']+'</div>';
                if(arItem['dopphone']){
                    response += '<div class="agent_phone agent_phone_extra">'+arItem['dopphone']+'<br>';
                }
                if(arItem['face']){
                    response += '<i>'+arItem['face']+'</i>';
                }
                if(arItem['dopphone']){
                    response += '</div>';
                }
            response += '</div>';
            response += '<div class="client_links">';
                if ($.trim(arItem['rieltor'])){
                    response += '<div class="client_ads">';
                        response += 'Владелец: <span class="client_ads_link">'+arItem['rieltor']+'</span>';
                    response += '</div>';
                }
                if ($.trim(arItem['manager'])){
                    response += '<div class="client_ads">';
                        response += 'Менеджер: <span class="client_ads_link">'+arItem['manager']+'</span>';
                    response += '</div>';
                }
            response += '</div>';
            response += '<div class="client_description">';
                if(arItem['face']){
                    response += '<p>'+arItem['description']+'</p>';
                }
            response += '</div>';
            response += '<div class="client_join_date">';
                response += '<span>Добавлен </span>';
                response += '<span>'+arItem['date']+' </span>';
                response += '<div class="calendar_ico"></div>';
            response += '</div>';
            if (arItem['owner']){
                response += '<a target="_blank" class="button btn_edit" href="/clients/add.php?edit=Y&CODE='+arItem['id']+'"></a>';
                response += '<div class="needs_info-wrap">';
                    response += '<div class="needs_info">';
                        response += 'Активных потребностей: <span>'+arItem['need_count']+'</span>';
                    response += '</div>';
                    if(arItem['can_add_need']){
                        response += '<a target="_blank" href="/needs/add.php?client_id='+arItem['id']+'" class="button button_universal">+ Добавить потребность</a>';
                    }
                response += '</div>';
            }
        response += '</div>';
    } else {
        response += '<div class="entry" data-id="'+arItem['id']+'">';
            if (arItem['avito']){
                response += '<mark title="Объявление будет выгружено на авито"><span class="avito_ico">';
                if (arItem['callc']){
                    response += '<b class="callc">Call</b>';
                }
                response += '</span></mark>';
            }
            response += '<div class="entry_date">';
                response += '<div class="entry_day">'+arItem['day']+'</div>';
                response += '<div class="entry_time">'+arItem['time']+'</div>';
            response += '</div>';
            response += '<div class="entry_thumb">';
                response += '<div class="entry_img">';
                    response += '<a target="_blank" href="'+arItem['href']+'">';
                        if (arItem['photo']){
                            response +='<img src="'+arItem['photo']+'" alt="">';
                        } else {
                            response +='<img width="120px" height="90px" src="/bitrix/templates/crm/img/no-photo.png" alt="">';
                        }
                    response +='</a>';
                response += '</div>';
                response += '<div class="entry_agent">Риэлтор:</div>';
                response += '<a target="_blank" class="entry_agent_link" href="/agents/'+arItem['rieltor_id']+'/">'+arItem['rieltor_name']+'</a>';
                if (arItem['client_id']){
                    response += '<div class="entry_agent">Клиент:</div>';
                    response += '<a data-id='+arItem['client_id']+' class="entry_search_client_link" href="">'+arItem['client_name']+'</a>';
                } else {
                    response += '<div class="entry_agent">Объект</div>';
                }
            response += '</div>';
            response += '<div class="entry_description">';
                response += '<a target="_blank" class="entry_header" href="'+arItem['href']+'">'+arItem['name']+'</a>';
                response += '<p class="entry_text ">';
                    response += '<span class="street">'+arItem['address']+'</span>';
                    response += '<br>';
                    response += arItem['info'];
                response += '</p>';
                if (arItem['offer']){
                    response += '<div class="propose_button check">';
                        if(!arItem['already_in_choice']){
                            if(arItem['offer_with_need']){
                                if(!arItem['already_in_need']){
                                    response += '<span class="propose">Предложить этот вариант к выбранной потребности</span>';
                                    response += '<a target="_blank" href="/needs/'+arItem['offer_with_need']+'/" class="propose_check">Перейти в потребность</a>';
                                } else {
                                    response += '<a target="_blank" href="/needs/'+arItem['offer_with_need']+'/" class="propose_check shown">Перейти в потребность</a>';
                                }
                            } else {
                                response += '<a href="/operator/" class="add_propose">Добавить в потребность</a>';
                                response += '<p>Необходимо будет зайти в нужную потребность и добавить это объявление кнопкой на панели</p>';
                            }
                        } else {
                            response += '<p>Данное объявление уже закреплено для сделки в другом варианте</p>';
                        }
                    response += '</div>';
                }
            response += '</div>';
            response += '<span class="entry_price">'+arItem['price']+'</span>';
        response += '</div>';
    }
    return response;
}

function getResultSecondView(arItem, itemsType){
    var response = "";
    if (itemsType == 'mans'){
        response +='<div class="agent_search_card">';
            response +='<a target="_blank" href="/agents/'+arItem['id']+'/">';
            if (arItem['photo']){
                response += '<img class="agent_img" src="'+arItem['photo']+'">';
            } else {
                response += '<img width="60px" height="60px" class="agent_img" src="/bitrix/templates/crm/img/nophoto.jpg">';
            }
            response +='</a>';
            response +='<div class="agent_contacts">';
                response +='<div class="agent_name">';
                    response +='<a href="/agents/'+arItem['id']+'/" target="_blank">'+arItem['name']+'</a>';
                response +='</div>';
                response +='<div class="agent_phone">'+arItem['phone']+'</div>';
            response +='</div>';
            response +='<div class="agent_description">';
                if ($.trim(arItem['head'])){
                    response +='<p>Начальник: ';
                        response +='<span>'+arItem['head']+'</span>';
                    response +='</p>';
                }
            response +='</div>';
            response +='<div class="agent_join_date">';
                response +='<span>Добавлен </span>';
                response +='<span>'+arItem['date']+' </span>';
                response +='<div class="calendar_ico"></div>';
                if (arItem['last_login']){
                    response +='<span>Последний вход: </span>';
                    response +='<span>'+arItem['last_login']+'</span>';
                }
            response +='</div>';
        response +='</div>';
    } else {
    response +='<div data-object="'+arItem['id']+'" class="entry entry_list entry_object" data-element="'+arItem['id']+'" data-line-color="#FF9900">';
        response +='<div class="entry_list__item entry_block_info">';
            response +='<div class="head">'+arItem['status_title']+'</div>';
            if (!arItem['comment']){
                if (arItem['late']){
                    response +='<div class="date danger">'+arItem['date']+'</div>';
                } else {
                    response +='<div class="date">'+arItem['date']+'</div>';
                }
                if (arItem['late']){
                    response +='<div class="time danger">'+arItem['time']+'</div>';
                } else {
                    response +='<div class="time">'+arItem['time']+'</div>';
                }
                if (arItem['late']){
                    response +='<i class="late danger">просрочено</i>';
                }
            } else {
                response += arItem['comment'];
            }
        response +='</div>';
        response +='<div class="entry_list__item entry_block_status">';
            response +='<a data-id='+arItem['client_id']+' class="entry_search_client_link" href="">'+arItem['client_name']+' </a>';
            response +='<div class="object_status">'+arItem['status_name']+'</div>';
        response +='</div>';
        response +='<div class="entry_list__item entry_block_description">';
            response +='<div class="head">'+arItem['name']+'</div>';
            response +='<div class="object_status">';
                if (arItem['is_watch']){
                    response +='<div class="green">Объект осмотрен</div>';
                } else {
                    response +='<div class="yellow">Объект ждёт осмотра</div>';
                }
            response +='</div>';
            response +='<div>';
                response +='<span>квартира '+arItem['rooms']+' к. ';
                if (arItem['square']){
                    response += arItem['square']+' м2 ';
                }
                response += +arItem['floor']+arItem['floors']+' </span>';
                if (arItem['district']){
                    response +='<span>'+arItem['district']+' р-н </span>';
                }
                response +='<span>'+arItem['markettype']+', '+arItem['housetype']+' </span>';
            response +='</div>';
            response +='<div class="entry_item__price">'+arItem['price']+'</div>';
            if (arItem['rieltor_id']){
                response +='<div class="accomplice">';
                    response +='<a target="_blank" href="/agents/'+arItem['rieltor_id']+'/">'+arItem['rieltor']+' </a>';
                response +='</div>';
            }
        response +='</div>';
        response +='<div class="entry_list__item entry_block_panel">';
            response +='<div class="info_contact"><span>Первый контакт: </span>'+arItem['first_contact']+'</div>';
            if(arItem['offer_with_need']){
                response +='<div class="object_need">';
                    if(!arItem['already_in_need']){
                        response += '<button class="button object_propose">Предложить этот вариант к выбранной потребности</button>';
                        response += '<a target="_blank" href="/needs/'+arItem['offer_with_need']+'/" class="propose_check">Перейти в потребность</a>';
                    } else {
                        response += '<a target="_blank" href="/needs/'+arItem['offer_with_need']+'/" class="propose_check shown">Перейти в потребность</a>';
                    }
                response +='</div>';
            }
        response +='</div>';
    response +='</div>';
    }
    return response;
}

function getCurrentFilter(){
    var activeTab = $('.tabs.visible');
    var arFilter = {};
    activeTab.find('.ajax_filter input').each(function(index){
        if ($(this).attr('type') != 'checkbox'){
            if ($(this).val()){
                arFilter[''+$(this).attr('name')+''] = $(this).val();
            }
        } else {
            if ($(this).prop("checked")){
                arFilter[$(this).attr('name')] = $(this).val();
            }
        }
    });
    activeTab.find('.ajax_filter select').each(function(index){
        if ($(this).val()){
            arFilter[$(this).attr('name')] = $(this).val();
        }
    });
    return arFilter;
}
function updateContent(blockType){
    $('.filter_tabs_block .preload').show();
    if (isFilterCompleted()){
        $('.searched').show();
        var currentTabsType = $('.visible').data('type');
        enableSubmitButton();
        
        if (!blockType){
           $('.search_results .preload').show(); 
        } else {
            $('.ajax_search_block_'+blockType+' .preload').show();
            $('body').scrollTop($('#'+blockType+'_block_ajax').position().top);
        }
        
        $('.search_results').show();
        $('.search_next').show().data('page', 2);
        $('.search_turn').show();

        $.ajax({
            cache: false,
            data: {'ajax':'y', 'blockType':blockType, 'type':$('.visible').data('type'), 'filter': getCurrentFilter(), 'sort': getCurrentSort()},
            dataType: 'json',
            url: 'search.php',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                // console.log(data);
                if(!blockType || blockType == 'first'){
                    if (data['first']){
                        if (data['first'].length<5 || data['first_length']==5){
                            $('.ajax_search_block_first .search_next').hide();
                            $('.ajax_search_block_first .search_turn').hide();
                        }
                        var resultList = "";
                        data['first'].forEach(function(item, i, data) {
                          resultList += getResultFirstView(item, currentTabsType);
                        });
                    } else {
                        resultList = '';
                        $('.ajax_search_block_first .search_next').hide();
                        $('.ajax_search_block_first .search_turn').hide();
                    }
                    $('.ajax_search_block_first .search_ajax_list').html(resultList);
                    $('.ajax_search_block_first .searched span').html(data['first_length']);
                }
                if(!blockType || blockType == 'second'){
                    if (data['second']){
                        if (data['second'].length<5 || data['second_length']==5){
                            $('.ajax_search_block_second .search_next').hide();
                            $('.ajax_search_block_second .search_turn').hide();
                        }
                        var resultList = "";
                        data['second'].forEach(function(item, i, data) {
                          resultList += getResultSecondView(item, currentTabsType);
                        });
                    } else {
                        resultList = '';
                        $('.ajax_search_block_second .search_next').hide();
                        $('.ajax_search_block_second .search_turn').hide();
                    }
                    $('.ajax_search_block_second .searched span').html(data['second_length']);
                    $('.ajax_search_block_second .search_ajax_list').html(resultList);
                }
                $('.preload').hide();
            }
        });
    } else {
        $('.filter_tabs_block .preload').hide();
        $('.search_results').hide();
        blockSubmitButton();
    }
}
function nextContent(search_next){
    search_next.addClass('is_loading');
    var currentPage = search_next.data('page');
    var currentTypeNext = search_next.data('type');
    var currentTabsType = $('.visible').data('type');
    search_next.data('page', currentPage+1);
    
    $.ajax({
        cache: false,
        data: {'ajax':'y', 'type':$('.visible').data('type'), 'filter': getCurrentFilter(), 'sort': getCurrentSort(), 'page':currentPage},
        dataType: 'json',
        url: 'search.php',
        error: function(){
            console.log('Connecting error');
        },
        success: function(data){
            if (data[currentTypeNext]){
                if (data[currentTypeNext].length<5 || data[currentTypeNext+'_length']==5){
                    search_next.hide();
                }
                var resultList = "";
                data[currentTypeNext].forEach(function(item, i, data) {
                    if(currentTypeNext == 'first'){
                        resultList += getResultFirstView(item, currentTabsType);
                    } else {
                        resultList += getResultSecondView(item, currentTabsType);
                    }
                });
            }
            $('.ajax_search_block_'+currentTypeNext+' .search_ajax_list').append(resultList);
            search_next.removeClass('is_loading');
        }
    });
}

$(function(){
    $(document).on("slidechange", ".Floor_dragger", function( event, ui ) {
        var floorsMax = $('.Floors_dragger').slider("values",1);

        if (floorsMax<ui.values[1]){
            $('.Floor_dragger').slider("values",1, floorsMax);
            $(".Floor_dragger .ui-slider-handle:last").html(floorsMax);
            $(".max_Floors").val(floorsMax).attr('name', 'Floors][max]');
            
        }
        updateContent();
    });
    $(document).on("slidechange", ".Floors_dragger", function( event, ui ) {
        var floorMin = $('.Floor_dragger').slider("values",0);
        var floorMax = $('.Floor_dragger').slider("values",1);
        
        if (floorMin>ui.values[1]){
            var newMinValue;
            if (ui.values[1]-5>0){
                newMinValue = ui.values[1]-5;
            } else {
                newMinValue = 0;
            }
            $('.Floor_dragger').slider("values",0, newMinValue);
            $(".Floor_dragger .ui-slider-handle:first").html(newMinValue);
            $(".min_Floor").val(newMinValue).attr('name', 'Floor][min]');
        }
        
        if (floorMax>ui.values[1]){
            $('.Floor_dragger').slider("values",1, ui.values[1]);
            $(".Floor_dragger .ui-slider-handle:last").html(ui.values[1]);
            $(".max_Floor").val(ui.values[1]).attr('name', 'Floor][max]');
        }

    });
    
    $('.ajax_filter form').on('submit', function(){
        event.preventDefault();
        updateContent();
    });

    $('li.city-el select[name=city]').SumoSelect({captionFormat: "Городов: {0}",});
    $('li.district-el select[name=district]').SumoSelect({captionFormat: "Районов: {0}",});
    
    $('.city-el select').on('change', function(){
        if ($(this).val() != "" && $(this).val()==49){
            $('.district-el').show();
        } else {
            hideDistrict();
        }
    });
    
    $('.filter_reset').on('click', function(event){
        if (event.clientX != 0){
            $('body').scrollTop(0);
            refreshTab();
            $('.input_with_value').removeClass('input_with_value');
            $('.with_value .del-input').hide();
            $('.with_value').removeClass('with_value');
        }
    });
    
    $(document).on('click', '.mans_form .search_mans_row .input-wrap button.del-input', function(event){
        $(this).hide();
        $('.mans_form input[name=search_input]').val("");
        $('.mans_form input[name=extra_man_value]').val("");
        $('.mans_tab').addClass('no_additional_row');
        $('.mans_form input[name=search_input]').removeAttr('disabled');
        updateContent();
    });
    $(document).on('change', 'input[name=min_Price], input[name=max_Price]', function(event){
        $(this).val($(this).val().replace(/\s+/g, ''));
        if (!isFinite($(this).val())){
            $(this).val("");
        }
    });

  
    $(document).on('click', '.extra_man_row .input-wrap button.del-input', function(event){
        clearMansSearch();
    });
    $(document).on('click', '.search_ajax_list .entry_search_client_link', function(event){
        event.preventDefault();
        $('.mans_form input[name=search_input]').val($(this).html()).prop('disabled', 'disabled');
        changeTabsWithValue('input', 'extra_man_value', $(this).data('id'))
        $('.extra_man_row .man_name').html($(this).html());
        $('.mans_tab').removeClass('no_additional_row');
        $('.mans_form .search_mans_row .input-wrap button.del-input').show();
        updateContent();
    });
    
    
    if($('.ajax_filter select[name=subject]').val()){
        $('.type_subjects-el').show();
    }
    
    if(isFilterCompleted()){
        updateContent();
    } else {
        blockSubmitButton();
    }
    
    $('.filter_tabs_block .preload').hide();
    
    $('input[name=call_phone]').inputmask('mask', {'mask': '+7 (999) 999-99-99'});
    $('input[name=call_phone]').on('change', function(){
        if ($(this).val().indexOf('_') != -1){
            $(this).val("");
        }
    })
    $(document).on('change','.ajax_filter input, .ajax_sort select', function(){
        updateContent();
    })

    $(document).on('change','.ajax_filter .select2_row:not(.multi_select_row) select', function(){
        updateContent();
    })

    $('li.search_block:not(.district-el, .city-el) select').on('change', function(){
        changeExtraFilter(false);
    })
    
    $(document).on('click', '.input-wrap button.del-input', function(event){
        event.preventDefault();
        var currentInput = $(this).parent('.input-wrap').find('input[type=text]');
        if (currentInput.val()){
            currentInput.val("").trigger("change");
        } else {
            if ($(this).parents('.elements_form').length){
                var currentSumoSelect = $(this).parents('.input-wrap');
                if (currentSumoSelect){
                    currentSumoSelect.find('select')[0].sumo.unSelectAll();
                    currentSumoSelect.find('select').trigger("change");
                    $(this).hide();
                }
            }
        }
    });
    
    $(document).on('change', '.price_ajax_search input[name*=Price],'+
                                '.elements_form input[name=search_input],'+
                                '.ajax_search_date input[name*=date]', function(event){
        if ($(this).val()){
            $(this).parent('.input-wrap').addClass('input_with_value');
        } else {
            $(this).parent('.input-wrap').removeClass('input_with_value');
        }
    });
    $(document).on('change', '.mans_form input[name=search_input]', function(event){
        if ($(this).val()){
            $(this).parent('.input-wrap').find('.del-input').show();
        } else {
            $(this).parent('.input-wrap').find('.del-input').hide();
        }
    });
    
    $(document).on('change', '.SumoSelect select:not([name=price_type], [name=city], [name=district])', function(event){
         if ($(this).val()){
            $(this).parents('.SumoSelect').find('.CaptionCont label').hide();
            $(this).parents('.input-wrap-select').find('.del-input').show();
        } else {
            $(this).parents('.SumoSelect').find('.CaptionCont label').show();
            $(this).parents('.input-wrap-select').find('.del-input').hide();
        }
    });
    
    $('.SumoSelect select[name=district]').on('change', function(event){
        if ($(this).val()){
            $(this).parents('.district-el').addClass('with_value');
            $(this).parents('.input-wrap').find('.del-input').show();
        } else {
            $(this).parents('.district-el').removeClass('with_value');
            $(this).parents('.input-wrap').find('.del-input').hide();
        }
    });    
    $(document).on('change', '.SumoSelect select[name=city]', function(event){
        if ($(this).val()){
            $(this).parents('.city-el').addClass('with_value');
            $(this).parents('.input-wrap').find('.del-input').show();
        } else {
            $(this).parents('.city-el').removeClass('with_value');
            $(this).parents('.input-wrap').find('.del-input').hide();
        }
    }); 


    $('.search_next').on('click', function(){
        nextContent($(this));
    });
    $('.search_turn').on('click', function(){
        updateContent($(this).data('type'));
    });
    $('.subjects-el select').on('change', function(){
        if ($(this).val() != ""){
            $('.type_subjects-el').show();
        } else {
            $('.type_subjects-el').hide();
        }
    });
    
    $('h3').on('click', function(){
        if (!$(this).hasClass('active')){
            $('h3').removeClass('active');
            $(this).addClass('active');
            var activeTabClass = $(this).attr('for');
            $('.tabs').removeClass('visible');
            $('.'+activeTabClass).addClass('visible');
            // refreshTab();
            showSearchHeader();
            updateContent();
        }
    });


    $(document).on('click', '.search_ajax_list .agent_description span', function(event){
        event.preventDefault();
        if ($('.mans_form input[name=search_input]').val() != $(this).html()){
            $('.mans_form input[name=search_input]').val($(this).html());
            updateContent();
        }
    });
    
    $(document).on('click', '.propose_button a.add_propose', function(event){
        var currentElement = $(this).parents('.entry');
        if(currentElement.data('id')){
            $.cookie('search_element_id', currentElement.data('id'), { path: '/operator'});
            $.cookie('search_element_path', '/ads/prodam/', { path: '/operator'});
            $.cookie('search_element_header', currentElement.find('.entry_header').html(), { path: '/operator'});
            $.cookie('search_element_description', currentElement.find('.entry_text').html(), { path: '/operator'});
        }
    });
    
    $(document).on('click', '.object_need .object_propose', function(event){
        event.preventDefault();
        $(this).hide();
        var currentElement = $(this).parents('.entry');
        currentElement.find('.propose_check').show();
        $.ajax({
            cache: false,
            data: {'ajax':'y', 'obj_id':currentElement.data('object'), 'need_id':$.cookie('search_element_id')},
            url: '/ajax/add_object_variant.php',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                console.log(data);
            }
        });
    });
    
    $(document).on('click', '.search_ajax_list .propose_button span.propose', function(event){
        event.preventDefault();
        $.ajax({
            cache: false,
            data: {'ajax':'y', 'ads_id':$(this).parents('.entry').data('id'), 'need_id':$.cookie('search_element_id')},
            url: '/ajax/add_variant.php',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                if (data){
                }
            }
        });
        $(this).hide();
        $(this).parents('.entry').find('.propose_check').css('display', 'block');
    });
    
    $(document).on('click', '.search_ajax_list .client_ads_link', function(event){
        event.preventDefault();
        if ($('.mans_form input[name=search_input]').val() != $(this).html()){
            clearMansSearch();
            $('.mans_form input[name=search_input]').val($(this).html());
            $('.mans_form .del-input.select2-search-choice-close').show();
            updateContent();
        }
    });
});