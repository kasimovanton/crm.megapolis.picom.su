/**
 * Created by picom on 20.11.14.
 */
	
$(function(){
	$('.open_fixed_block').on('click', function(s){
		s.preventDefault();
        $('.fixed_block_text').toggleClass("open");
    });
	$('.close_form').on('click', function(s){
		s.preventDefault();
        $('.fixed_block_text').toggleClass("close");
        $.cookie('search_element_id', "", { path: '/search'});
        $.cookie('search_element_path', "", { path: '/search'});
        $.cookie('search_element_header', "", { path: '/search'});
        $.cookie('search_element_description', "", { path: '/search'});
        $.cookie('search_element_id', "", { path: '/operator'});
        $.cookie('search_element_path', "", { path: '/operator'});
        $.cookie('search_element_header', "", { path: '/operator'});
        $.cookie('search_element_description', "", { path: '/operator'});
        location.reload();
    });

    $('.flexslider').flexslider({
        animation: "fade",
        controlNav: false   
    });
    $(".object_preview_list-wrap").mCustomScrollbar({scrollInertia: 001});
			
    $('input[name=PERSONAL_PHONE]').inputmask('mask', {'mask': '+7 (999) 999-99-99'});
    $('input.cell_phone').inputmask('mask', {'mask': '+7 (999) 999-99-99'});
    $('#search_number').inputmask('mask', {'mask': '+7 (999) 999-99-99'});
    $('input[name=client_phone]').inputmask('mask', {'mask': '+7 (999) 999-99-99'});

    var path = '/ads/';
    
    $('.sort select').change(function(){
        location.href = $(this).val();
    })
    
    $('input[name=date_time]').change(function(){
        if ($(this).val().indexOf('_') != -1){
            $(this).val("");
        }
    })    
    $('input[name=status_time]').change(function(){
        if ($(this).val().indexOf('_') != -1){
            $(this).val("");
        }
    })    
    $('input.cell_phone').change(function(){
        if ($(this).val().indexOf('_') != -1){
            $(this).val("");
        }
    })    
    $('#search_number').change(function(){
        if ($(this).val().indexOf('_') != -1){
            $(this).val("");
        }
    })
    
    $('.boss select').change(function(){
        $.ajax({
            cache: false,
            data: {'ajax':'y','boss': $('.boss select option:selected').attr('value')},
            url: '/ajax/session.php/',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                location.reload();
            }
        });
    })
    $('.alf_filter select').change(function(){
        $.ajax({
            cache: false,
            data: {'ajax':'y','alf_filter': $('.alf_filter select option:selected').attr('value')},
            url: '/ajax/session.php/',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                location.reload();
            }
        });
    })
    $('.agent_page select').change(function(){
        $.ajax({
            cache: false,
            data: {'ajax':'y','agent_page': $('.agent_page select option:selected').attr('numb')},
            url: '/ajax/session.php/',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                location.reload();
            }
        });
    })
    $('.panel_object .tab').click(function(){
        $.ajax({
            cache: false,
            data: {'ajax':'y','tab': $(this).data('type')},
            url: '/ajax/session.php/',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                location.reload();
            }
        });
        return false;
    });
    
    $('.panel_watch .tab').click(function(){
        $.ajax({
            cache: false,
            data: {'ajax':'y','tab_watch': $(this).data('type')},
            url: '/ajax/session.php/',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                location.reload();
            }
        });
        return false;
    });
    
    $('.panel_show .tab').click(function(){
        $.ajax({
            cache: false,
            data: {'ajax':'y','tab_show': $(this).data('type')},
            url: '/ajax/session.php/',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                location.reload();
            }
        });
        return false;
    });
    $('.paging select').change(function(){
        $.ajax({
            cache: false,
            data: {'ajax':'y','paging': $('.paging select option:selected').attr('numb')},
            url: '/ajax/session.php/',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                location.reload();
            }
        });
    })
    $('.sort_ord select').change(function(){
        $.ajax({
            cache: false,
            data: {'ajax':'y','sort': $('.sorting select option:selected').attr('sort'),'order': $('.sorting select option:selected').attr('order')},
            url: '/ajax/session.php/',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                location.reload();
            }
        });
    })
    
    $('.cboxElement').colorbox({
        closeButton: true,
        rel: 'galGroup',
        current: "Изображение {current} из {total}",
        arrowKey: true,
        opacity : '0.4',
        maxWidth: 700
    });
    
    $('.popup').colorbox({
        closeButton: true,
        inline:true
    });
    
    $('.new_show_status button').colorbox({
        closeButton: true,
        inline:true,
        href: "#show_comment",
        onOpen: function(){
            $('.comment_block .error').hide();
            if($(this).data('status') == "Y"){
                $('#show_comment h2').html('Объект показан');
                $('#show_comment input[name=new_show_status]').val('Y');
            }
            if($(this).data('status') == "N"){
                $('#show_comment h2').html('Отказ от показа');
                $('#show_comment input[name=new_show_status]').val('N');
            }
            $('#show_comment input[name=variant_id]').val($(this).parents('.entry').data('variant'));
        },
    });
    
    $('.changeStatus').colorbox({
        closeButton: true,
        inline:true,
        href: "#popupChange",
    });  
    
    $('.popupAddObjectPhoto').colorbox({
        closeButton: true,
        inline:true,
        href: "#popupAddObjectPhoto",
        onOpen: function(){
            $('body').css('overflow', 'hidden');
        },
        onClosed: function(){
            $('body').css('overflow', 'visible');
            $('.canvas_canvas').remove();
            $('.canvas_img').remove();
            $('#formLoader')[0].reset();
        }
    });
    
    $('.popupAddObjectPhoto').click(function(){
        $.ajax({
            cache: false,
            data: {'ajax':'y','object': $(this).parents('.entry').data('object')},
            url: '/ajax/current_files.php/',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                // console.log(data);
                $('#popupAddObjectPhoto .object_preview_list .download').before(data);
            }
        });
    });
    
    $('.galleryLoad').click(function(){
        $.ajax({
            cache: false,
            data: {'ajax':'y','object': $(this).parents('.entry').data('object')},
            url: '/ajax/gallery.php/',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                $('#popup_ad .slider').html(data);
            }
        });
    });
    $('.galleryLoad').colorbox({
        closeButton: true,
        inline:true
    });
    
    $('.view_watch').colorbox({
        closeButton: true,
        inline:true
    });
    
    $('.button_move').colorbox({
        closeButton: true,
        inline:true,
        href: "#button_move",
        onClosed: function(){
            $('.object_watch_form')[0].reset();
            $('.object_watch_form .error_text').hide();
            $('#button_move .second-preloader').hide();
            $('#button_move .first_step_watch').show();
            $('#success').hide();
        },
    });
    
    $('.disable').click(function(){
        $.colorbox.close();
        return false;
    });      
    $('.close_button').click(function(){
        $.colorbox.close();
        return false;
    });  
    
    $('.changeStatus').click(function(){
        $('.current_status').html('"'+$(this).parents('.entry').find('.object_status').html()+'"');
        $('.status_selected').removeClass('status_selected');
        $('.form_row').hide();
		$('input[name=object_id]').val($(this).parents('.entry').data('element'));
    });
    
    $('.status_list li').each(function(){
        $(this).css('color', $(this).data('text-color'));
    });
    $('.status_list li').click(function(){
        $('.error').hide();
        $('.status_selected').removeClass('status_selected');
        $(this).toggleClass('status_selected');
        switch (parseInt($(this).attr('value'))){
          case 8940:
          case 8962:
          case 8963:
          case 8964:
          case 8965:
          case 8966:
          case 8967:
          case 8968:
          case 8971:
          case 8972:
          case 8973:
          case 8975:
          case 11107:
            $('.form_row_date').show();
            $('.form_row_comment').hide();
            break;
          case 8977:
          case 8978:
          case 8979:
          case 11106:
          case 11377:
            $('.form_row_date').hide();
            $('.form_row_comment').show();
            break;
        }
        $('.form_head').show().html(statusList[parseInt($(this).attr('value'))]);
        $('.row_submit').show();
        $('.changeStatus').colorbox.resize();

    });
    
    $('.form_status form').submit(function(){
        switch (parseInt($('.status_selected').attr('value'))){
          case 8940:
          case 8962:
          case 8963:
          case 8964:
          case 8965:
          case 8966:
          case 8967:
          case 8968:
          case 8971:
          case 8972:
          case 8973:
          case 8975:
          case 11107:
                if($('input[name=date_time]').val() == ""){
                    $('.form_row_date .error').show();
					sendFlag = false;
                } else {
                    $('.form_row_date .error').hide();
					sendFlag = true;
                }
            break;
          case 8977:
          case 8978:
          case 8979:
          case 11106:
          case 11377:
            if($('textarea[name=comment]').val() == ""){
                $('.form_row_comment .error').show();
				sendFlag = false;
            } else {
                $('.form_row_comment .error').hide();
				sendFlag = true;
            }
            break;
        }

        if($('.status_selected').length == 0){
            $('.status_error').removeClass('hidden');
        } else {
            $('.status_error').addClass('hidden');
        }
		if (sendFlag){
			$('.second-preloader').show();
			formStatus = $('.formStatus');
			actionSerializ = formStatus.attr('action')+'?'+formStatus.serialize();
			$.ajax({
				cache: false,
				dataType: 'json',
				data: {'ajax':'y','new_status': $('.status_list li.status_selected').attr('value')},
				url: actionSerializ,
				error: function(){
					console.log('Connecting error');
				},
				success: function(data){
					if (data['error']){
						$('.response_status').show();
						$('.response_status').html(data['text']);
					} else {
						$('.popup-client').html('<div class="form">'+
											'<div>'+data['text']+'</div>'+
											'<a href="#" class="button button_universal close_changeStatus">Закрыть окно и перезагрузить список объектов</a>'+
											'</div>'
											);
					}
					$('.changeStatus').colorbox.resize();
				},
			});
			$('.second-preloader').hide();
		}

        return false;
    });
    
    
    $('.entry_buttons_object .button_move').click(function(){
        $('#object_id_value').val($(this).parents('.entry').data('object'));
    });
    
    $('.object_watch_form .watched_button').click(function(){
        if ($('input[name=agree]:checked').val() == undefined || $('textarea[name=watch_comment]').val() == "" ){
            $('.object_watch_form .error_text').show();
        } else {
            $('#button_move .second-preloader').show();
            $('.object_watch_form .error_text').hide();
            $.ajax({
                cache: false,
                data: {'ajax':'y','object':$('#object_id_value').val(), 'comment': $('textarea[name=watch_comment]').val(),'agree': $('input[name=agree]:checked').val()},
                url: '/ajax/watch_status.php/',
                error: function(){
                    console.log('Connecting error');
                },
                success: function(data){
                    if (data){
                        $('#button_move .second-preloader').hide();
                        $('#button_move .first_step_watch').hide();
                        if ($('input[name=agree]:checked').val() == 'no'){
                            location.reload();
                        } else {
                            $('#success').show();
                            $('#success .ads_button').attr('href', '/ads/ads_by_object.php?object_id='+data);
                        }
                    }
                },
            });
        }
        $.colorbox.resize();
        return false;
    });
    
	$(document).on('click','.close_changeStatus', function(){
		location.reload();
        return false;
    });
    
    $('.view_watch').click(function() {
        $.ajax({
            cache: false,
            data: {'ajax':'y','object_id': $(this).parents('.entry').data('object')},
            url: '/ajax/agent_comment.php/',
            success: function(data){
                $('#view_watch .comment_block').html(data);
                $.colorbox.resize();
            },
            error: function(){
                console.log('Connecting error');
            }
        });
    });
    
    $('.system_message_block .message .close').click(function(){
        $(this).parents('.system_message_block').hide();
        $.ajax({
            cache: false,
            data: {'ajax':'y','warning': 'close'},
            url: '/ajax/session.php/',
            error: function(){
                console.log('Connecting error');
            }
        });
        return false;
    });  
    
    $('#reassign .wide input[type=submit]').click(function(){
        if (!$(this).hasClass("button_disable")){
            $('.second-preloader').show();
        }
    });
    
    function getClientId(clientBlock){
        var data="";
        data += "<input type='hidden' name='client_id[]' value='"+clientBlock.val()+"' >";
        $('.clients_ids').html(data);
    }
    function showFirstFormTable(shell){
        $("#reassign .col_client p").html(shell.find(".client_name a").html());
        $("#reassign .col_ads p").html(shell.find('.client_ads_link span').html());
        $("#reassign .col_old_agent p").html(shell.find("input[name=old_agent]").val());
        $('.one_client').show();
        $('.multi_client').hide();
    }
    
    $('.one_reassign').click(function(){
        getClientId($(this).parents(".temp_client").find('input:checkbox'));
        showFirstFormTable($(this).parents(".temp_client"));
        $('#reassign .new_agent').val('default');
        $('.reassign_form input[type=submit]').addClass('button_disable');
    });
    
    
    $('.one_reassign_on_me').click(function(){
        getClientId($(this).parents(".temp_client").find('input:checkbox'));
        showFirstFormTable($(this).parents(".temp_client"));
        $('#reassign .new_agent').val($("#reassign input[name=user_id]").val());
        $('.reassign_form input[type=submit]').removeClass('button_disable');
    });	   
    
    $('input:checkbox').change(function(){
        if ($('.temp_client input:checkbox:checked').length){
            $('.sort-client .button-card-client a').removeClass('button_disable');
        } else {
            $('.sort-client .button-card-client a').addClass('button_disable');
        }
    });
    $('.button_disable').click(function(){
        if ($(this).hasClass('button_disable')){
            return false;  
        }
    });
    
    
    function getClientsIdsList(){
        var data="";
        $('.temp_client input:checkbox:checked').each(function(indx){
            data += "<input type='hidden' name='client_id[]' value='"+$(this).val()+"' >";
        });
        $('.clients_ids').html(data);
    }
    function showSecondFormTable(){
        $("#reassign .col_client p").html($('.temp_client input:checkbox:checked').length);
        $('.one_client').hide();
        $('.multi_client').show();
    }
    
    $('.multi_reassign').click(function(){
        $('#reassign .new_agent').val('default');
        showSecondFormTable();
        getClientsIdsList();
        $('.reassign_form input[type=submit]').addClass('button_disable');

    });	
    
    $('.multi_reassign_on_me').click(function(){
        $('#reassign .new_agent').val($("#reassign input[name=user_id]").val());
        showSecondFormTable();
        getClientsIdsList();
        $('.reassign_form input[type=submit]').removeClass('button_disable');
    });	
    
    $('#reassign .new_agent').change(function(){
        if ($(this).val() !== "default"){
            $('.reassign_form input[type=submit]').removeClass('button_disable');
        } else {
            $('.reassign_form input[type=submit]').addClass('button_disable');
        }
    });
    
    
	$('label[for=select-all]').click(function(){
        if ($("#select-all").prop("checked")){
            $('.temp_client input[id*=check]').prop({"checked":false});
        } else {
            $('.temp_client input[id*=check]').prop({"checked":true});
        }
    });
    
    $( "#datefrom" ).dateDropper({'minYear':'2014', 'animate_current':false, 'format':'d.m.Y', 'years_multiple':'1', 'color':'#7accc8'});
    $( "#dateto" ).dateDropper({'minYear':'2014', 'animate_current':false, 'format':'d.m.Y', 'years_multiple':'1', 'color':'#7accc8'});
    
    $(document).on('click','.del_list', function(){
        $(this).css('display', 'none');
        $(this).parent().parent().find('.delete_form').removeClass("hidden");
        return false;
    });
    
    $(document).on('click','.det_del', function(){
        $(this).css('display', 'none');
        $('.delete_form').removeClass("hidden");
        return false;
    });
    

    $(document).on('click','.area_remove', function(){
        $('#fileprev').html("");
        $('#fileFoto').val("");
        return false;
    });
    
    $(document).on('click','#alert', function(){
        if ($('#noalert').attr('checked') === undefined){
            $('#noalert').attr('checked', 'checked');
            $('#alert').removeAttr('checked');
        } else {
            $('#noalert').removeAttr('checked');
            $('#alert').attr('checked', 'checked');
        }
    });
    
    $(document).on('click','.toggle_btn ', function(){
        $.ajax({
            cache: false,
            data: {'ajax':'y','status': $(this).attr('status')},
            url: '/ajax/session.php/',
            error: function(){
                console.log('Connecting error');
            }
        });
    });
    
    $(document).on('click','.end_search', function(){
        $.ajax({
            cache: false,
            data: {'ajax':'y','end_search': 'y'},
            url: '/ajax/session.php/',
            error: function(){
                console.log('Connecting error');
            }
        });
    });
    
    $(document).on('submit','.delete_form', function(){
        if (!$(this).find($('input[type=text]')).val()){
            $('.input_error').html('Укажите причину');
        } else {

            var btn = $(this),
			form = btn.closest('form'),
			actionSerializ = form.attr('action')+'?'+form.serialize();
            // console.log(actionSerializ);

            $.ajax({
				cache: false,
				data: {'ajax':'y'},
				url: actionSerializ,
				error: function() {
                        console.log('fdsdf');
				},
				success: function(data) {

				}
			});
            $(this).find('div').css('display', 'none');
            $(this).parent().parent().find('.success_del').html('Удаление успешно завершено!');
            $(this).parents('div.entry').html('<span class="success_del">Удаление успешно завершено!</span>');
        }
        return false;
    });
    
    
    $(document).on('click','.image_grid_thumb', function(){
        $('.content_pane_image').attr('src', $(this).attr('for_det_src'))
        $('.content_pane_image').attr('height', $(this).attr('detheight'))
        $('.content_pane_image').attr('width', $(this).attr('detwidth'))
        $('.content_pane_image').parent('.cboxElement').attr('href', $(this).attr('for_det_href'))
        $('.image_grid_thumb').addClass("half_opacity");
        $(this).removeClass("half_opacity");
        setInterval(image_check(290,380,$('.content_pane_image')), 200);
        return false;
    });    
    
    image_check(290,380,$('.content_pane_image'));

	
    $(document).on('change','.min_price', function(){
        if ($(this).val() != ""){
            if (parseInt($(this).val())<$(".price_dragger").slider("values",1)){
                $(".price_dragger").slider("values",0,$(this).val());
            } else {
                $(this).val($(".price_dragger").slider("values",1));
                $(".price_dragger").slider("values",0,$(".price_dragger").slider("values",1));
            }
        } else {
            $(this).val(0);
            $(".price_dragger").slider("values",0,0);
        }
        $(this).attr('name', 'min_price');
    });   
    $(document).on('change','.max_price', function(){
        if ($(this).val() != ""){
            if (parseInt($(this).val())>$(".price_dragger").slider("values",0)){
                if(parseInt($(this).val()) > $(".price_dragger").slider("option", "max")){
                    $(this).val($(".price_dragger").slider("option", "max"));
                    $(".price_dragger").slider("values",1,$(".price_dragger").slider("option", "max"));
                 } else {
                   $(".price_dragger").slider("values",1,$(this).val());
                 }
            } else {
                $(this).val($(".price_dragger").slider("values",0));
                $(".price_dragger").slider("values",1,$(".price_dragger").slider("values",0));
            }
        } else {
            $(this).val($(".price_dragger").slider("option", "max"));
            $(".price_dragger").slider("values",1,$(".price_dragger").slider("option", "max"));
        }
        $(this).attr('name', 'max_price');
    });
    
    
    $(document).on('click','.cancel_del', function(){
        $(this).parent().parent().addClass("hidden");
        $('.btn_delete').css('display', 'block');
        return false;
    });
    
    $(document).on('change','#objecttypegaraj', function(){
        $('.find_btn').attr('disabled','disabled');
        $.ajax({
            cache: false,
            data: {'ajax':'y','obj': $('#objecttypegaraj option:selected').val()},
            url: '/ajax/garaj.php/',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                if(data !== '') {
                    $(".extra_form_row").html(data);
                }
            },
            complete: function(){
                $('.find_btn').removeAttr("disabled");
            }
        });
    });
    
    
   /*Выбор значения "Любое" или "От-до" в ads/add.php*/ 
   $(document).on('change','#property_442', function(){
        if (!$('#iPriceFrom').attr('disabled')){
            $('#iPriceFrom').attr('disabled','disabled');
        } else {
            $('#iPriceFrom').removeAttr("disabled");
        }
        if (!$('#iPriceTo').attr('disabled')){
            $('#iPriceTo').attr('disabled','disabled');
        } else {
            $('#iPriceTo').removeAttr("disabled");
        }
    });   
    $(document).on('change','#property_444', function(){
        if (!$('#iSquareFrom').attr('disabled')){
            $('#iSquareFrom').attr('disabled','disabled');
        } else {
            $('#iSquareFrom').removeAttr("disabled");
        }
        if (!$('#iSquareTo').attr('disabled')){
            $('#iSquareTo').attr('disabled','disabled');
        } else {
            $('#iSquareTo').removeAttr("disabled");
        }
    });       
    $(document).on('change','#property_446', function(){
        if (!$('#iLandAreaFrom').attr('disabled')){
            $('#iLandAreaFrom').attr('disabled','disabled');
        } else {
            $('#iLandAreaFrom').removeAttr("disabled");
        }
        if (!$('#iLandAreaTo').attr('disabled')){
            $('#iLandAreaTo').attr('disabled','disabled');
        } else {
            $('#iLandAreaTo').removeAttr("disabled");
        }
    });   
    $(document).on('change','#property_428', function(){
        if (!$('#iFloorsFrom').attr('disabled')){
            $('#iFloorsFrom').attr('disabled','disabled');
        } else {
            $('#iFloorsFrom').removeAttr("disabled");
        }
        if (!$('#iFloorsTo').attr('disabled')){
            $('#iFloorsTo').attr('disabled','disabled');
        } else {
            $('#iFloorsTo').removeAttr("disabled");
        }
    });
   

    $('a.change_show_status').on('click', function(event){
        event.preventDefault();
        if(!$('textarea[name=show_comment]').val()){
           $('.comment_block .error').show();
           $.colorbox.resize();
        } else {
            $.ajax({
                cache: false,
                data: {'ajax':'y', "show_comment": $('textarea[name=show_comment]').val(), "new_show_status" : $('input[name=new_show_status]').val(), "variant_id" : $('input[name=variant_id]').val()},
                url: '/ajax/change_show_status.php/',
                error: function(){
                    console.log('Connecting error');
                },
                success: function(data){
                    if (data){
                        location.reload();
                    }
                }
            });
        }
    });
    $('.form_select .subject').change(function(){
        cat = $(this).val();
        $('.form_filter').attr('action', path+cat+'/');
        
        $('.filter_row .type').removeClass("hidden");
        $('.prop_list').html("");
        removePrice();
        $('.form_filter .rooms').addClass('hidden');
        $('#rooms')[0].sumo.unSelectAll();
        $('.form_filter .roomslist').addClass('hidden');
        $('#roomslist')[0].sumo.unSelectAll();
        $('.filter_row .type select option:selected').attr('selected',false);
        $('.filter_row .type select option:first').attr('selected',true);

    })
    //list.php - возвращает списковые данные, относильно запрошенной категории
    $('.form_select .type').change(function(){
        
        cat_type = $(this).val();
        if (cat_type){
            full_path = path+cat+'_'+cat_type+'/';
        } else {
            full_path = path+cat+cat_type+'/';
        }
        $('.form_filter').attr('action', full_path);
        $('.find_btn').attr('disabled','disabled');
        
        removePrice();
        $.ajax({
            cache: false,
            dataType: 'json',
            data: {'ajax':'y','category': $('.form_select .subject option:selected').val(), 'type': cat_type},
            url: '/ajax/list.php/',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                if (data !== '' && data['success'] == true){
                    ////////////////////////

                    if(data['maxprice']!=0 && data['maxprice']!==null){
                        if (data['maxprice'] < 100000){
                            step = data['maxprice']/2;
                        } else {
                            if (data['maxprice'] < 1000000){
                                step = 1e4;
                            } else {
                                step = 1e5;
                            }
                        }
                        if (!data['demand']){
                            firstName = 'min_price';
                            secondName = 'max_price';
                        } else {
                            firstName = 'min_price_dem';
                            secondName = 'max_price_dem';
                        }
                        $(".price_dragger").slider({
                            range:!0,
                            min:0,
                            max: data['maxprice'],
                            step:step,
                            values: [0, data['maxprice']],
                            slide:function(a,b){
                                $(".min_price").val(b.values[0]).attr('name', firstName);
                                $(".max_price").val(b.values[1]).attr('name', secondName);
                            }
                        });
                        $(".min_price").val($(".price_dragger").slider("values",0));
                        $(".max_price").val($(".price_dragger").slider("values",1));
                       
                        $(".price_row").show();
                    }
                    
        
                    $('.prop_list').removeClass("hidden");
                    $('.prop_list').html(data['data']);
                    $(".prop_list").append('<div class="extra_form_row"></div>');
                } else {
                    $('.prop_list').html("");
                    $('.prop_list').addClass("hidden");

                }
            },
            complete: function(){
                $('.find_btn').removeAttr("disabled");
            }
        });
        $.ajax({
            dataType: 'json',
            cache: false,
            data: {'ajax':'y','category': $('.form_select .subject option:selected').val(), 'type': cat_type},
            url: '/ajax/rooms.php/',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                if (data !== '' && data['success'] == true){
                    $('.prop_list').addClass("list_with_empty_block");
                    if (data['roomstype'] == 'Rooms'){
                        $('.form_filter .roomslist').addClass('hidden');
                        $('#roomslist')[0].sumo.unSelectAll();
                        $('.form_filter .rooms').removeClass('hidden');
                        $('#rooms')[0].sumo.unSelectAll();
                    } else{
                        if (data['roomstype'] == 'RoomsList'){
                            $('.form_filter .rooms').addClass('hidden');
                            $('#rooms')[0].sumo.unSelectAll();
                            $('.form_filter .roomslist').removeClass('hidden');
                            $('#roomslist')[0].sumo.unSelectAll();
                        }
                    }

                } else {
                    $('.prop_list').removeClass("list_with_empty_block");
                    $('.form_filter .rooms').addClass('hidden');
                    $('#rooms')[0].sumo.unSelectAll();
                    $('.form_filter .roomslist').addClass('hidden');
                    $('#roomslist')[0].sumo.unSelectAll();
                }

            },
            complete: function(){
                $('.find_btn').removeAttr("disabled");
            }
        });

    })
    
    $('.form_select .city').change(function(){
        $('.find_btn').attr('disabled','disabled');
        $.ajax({
            dataType: 'json',
            cache: false,
            data: {'ajax':'y','district': $(this).val()},
            url: '/ajax/district.php/',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                if(data !== '' && data['error'] == '') {
                    for (var key in data['response']) { 
                        var val = data['response'][key];
                        $('#district')[0].sumo.add(key, val, 0);
                    } 
                    $('.filter_row .district').removeClass("hidden");
                } else {
                    console.log('empty');
                    $('#district').html("");
                    $('.district .options').html("");
                    $('.district .CaptionCont span').html("Район");
                    $('.filter_row .district').addClass("hidden");
                }
            },
            complete: function(){
                $('.find_btn').removeAttr("disabled");
            }
        });
            
    })

    $('.profile_contacts_form').on('click','.photo_remove',function(e){
        //
        $('#PERSONAL_PHOTO_del').prop('checked','checked')
        $(this).parents('.main_photo_thumb').remove();
    })
    

    $('#onefileupload').fileupload({
        dataType: 'json',
        //paramName: $('#onefileupload').prop('name'),
        //fileInput: $('#onefileupload'),
        done: function (e, data) {
            var propName = $('#onefileupload').prop('name')
            $.each(data.result[propName], function (index, file) {
                console.log(file);
                var photo = '<div class="main_photo_thumb">\
                    <img src="'+file.thumbnailUrl+'" alt="">\
                    <a class="photo_remove" href="'+file.deleteUrl+'" title="removes photo"></a>\
                    <div class="main_photo_name">'+file.name+'</div></div>';
                $('.main_photo').html(photo);
            })
        }
    });
    /*<li class="extra_photo_thumb">
        <img src="images/add_object_extra_photo.png" alt="">
            <a class="photo_remove" href="#rem" title="removes photo"></a>
            <div class="extra_photo_name">repina_ivaniv2.jpg</div>
        </li>*/
    $('#fileupload').fileupload({
        dataType: 'json',
        done: function (e, data) {
            var propName = $('#fileupload').prop('name')
            $.each(data.result[propName], function (index, file) {
                console.log(file);
                var photo =
                    '<li class="extra_photo_thumb"> \
                        <img src="'+file.thumbnailUrl+'" alt="">\
                        <a class="photo_remove" href="'+file.deleteUrl+'" title="removes photo"></a>\
                        <div class="extra_photo_name">'+file.name+'</div>\
                    </li>';
                $(photo).appendTo($('.extra_photo'));
            });
        }
    });

})

$(document).ready(function() {

var fileSelect = document.getElementById("fileSelect"),
    fileElem = document.getElementById("fileElem");

// fileSelect.addEventListener("click", function (e) {
  // if (fileElem) {
    // fileElem.click();
  // }
  // e.preventDefault(); // prevent navigation to "#"
// }, false);

//$("#other").click(function() {
//  $("#target").click();
;

var o = ($(".district .optWrapper .options li"), $("#district")),
    t = ($(".rooms"), $("#rooms")),
    ts = ($(".roomslist"), $("#roomslist"));
    
    o.SumoSelect({
        csvDispCount: 1,
        captionFormat: "{0} Выбрано",
        // placeholder: "Район"
    });
    $(".district .SumoSelect .optWrapper .options li").on("click", function() {
        $(this).hasClass("selected") && $(this).prependTo($(".district .SumoSelect .optWrapper .options"))
    });
    
    t.SumoSelect({
        csvDispCount: 8,
        captionFormat: "{0} Выбрано",
        placeholder: "Количество комнат"
    });
    $(".rooms .optWrapper .options li").on("click", function() {
        $(this).hasClass("selected") && $(this).prependTo($(".rooms .optWrapper .options"))
    });
    ts.SumoSelect({
        csvDispCount: 8,
        captionFormat: "{0} Выбрано",
        placeholder: "Количество комнат"
    });
    $(".roomslist .optWrapper .options li").on("click", function() {
        $(this).hasClass("selected") && $(this).prependTo($(".roomslist .optWrapper .options"))
    });


$('#fileElem').change(function()
{
  handleFiles(this.files);
});

$('#fileFoto').change(function()
{
  handleFile(this.files);
});


function handleFiles(files) {
  var list = $('<ul></ul>');
  $("#filelist").append(list);

  for (var i = 0, f; f = files[i]; i++) {

    var reader = new FileReader();

    reader.onload = (function(f) {
      return function(e) {
        var li = $('<li class="extra_photo_thumb"></li>');
        $(list).append(li);

        var a = $('<a href="#"></a>');
        $(li).append(a);

        $(a).append("<img src='"+e.target.result +"'/>");

        $(a).append('<div class="extra_photo_name">' + f.name + '</div>');

        $(li).append('<a href="#rem" class="photo_remove" onclick="removeImg($(this));" title="removes photo"></a>');
      };
    })(f);

    reader.readAsDataURL(f);

  }
}

function handleFile(files) {
  var div = $('<div></div>');
  $("#fileprev").html(div);

  for (var i = 0, f; f = files[i]; i++) {

    var reader = new FileReader();

    reader.onload = (function(f) {
      return function(e) {

        $(div).append("<img src='"+e.target.result +"'/>");

        $(div).append('<span class="extra_photo_name">' + f.name + '</span>');

        $(div).append('<a href="#rem" class="area_remove" title="remove photo"></a>');
      };
    })(f);

    reader.readAsDataURL(f);

  }
}

    var imageLoader = document.getElementById('imageLoaderPhoto');
    if (imageLoader){
        imageLoader.addEventListener('change', handleImage, false);
    }
    var newForm = new FormData();
    
    function handleImage(e){
        $('#cboxLoadingGraphic').show();
        var fileNameArray = [];
        var canvasObjects = {};
        var ctxCanvasObjects = {};
        var nameImageCounter = 0;

        for (var i = 0, arFile; arFile = e.target.files[i]; i++) {
        
            var reader = new FileReader();
            fileNameArray.push(arFile.name);
            reader.onload = function(event){
                var img = new Image();
                $('.object_preview_list .download').before('<div class="preview_canvas canvas_canvas"><div class="img"><canvas id="imageCanvas'+imageCounter+'"></canvas></div><a class="photo_remove"></a><span class="new_file_name">'+fileNameArray[nameImageCounter]+'</span><input id="Shown'+imageCounter+'" type="checkbox" value="Y" name="Shown[]"><label for="Shown'+imageCounter+'">Показывать</label></div>');
                
                canvasObjects['imageCanvas'+imageCounter] = document.getElementById('imageCanvas'+imageCounter);
                ctxCanvasObjects['imageCanvas'+imageCounter] = canvasObjects['imageCanvas'+imageCounter].getContext('2d');
                
                img.src = event.target.result;
                canvasObjects['imageCanvas'+imageCounter].width = img.width;
                canvasObjects['imageCanvas'+imageCounter].height = img.height;
                ctxCanvasObjects['imageCanvas'+imageCounter].drawImage(img,0,0);
                imageCounter++;
                nameImageCounter++;
                
                $('#popupAddObjectPhoto').colorbox.resize();
                $('#cboxLoadingGraphic').hide();
            }
            newForm.append('imageCanvas'+realFileArrayCounter, arFile, arFile.name);
            realFileArrayCounter++;
            console.log(arFile);
            reader.readAsDataURL(arFile);
        }
        $('#formLoader')[0].reset();
    }

    $(document).on('click','#popupAddObjectPhoto .button_adder', function(){
            $('#popupAddObjectPhoto .second-preloader').show();
            newForm.append('object', $('input[name=current_object]').val());
            $.ajax({
                url: '/ajax/add_files.php?ajax=y',
                type: 'POST',
                xhr: function() { 
                    var myXhr = $.ajaxSettings.xhr();
                    return myXhr;
                },
                success: function (data) {
                    location.reload();
                    // if(data){
                        // console.log(data);
                        // $('#popupAddObjectPhoto .second-preloader').hide();
                    // }
                },
                error: function (error) {
                    console.log(error);
                 
                },
                data: newForm,
                cache: false,
                contentType: false,
                processData: false
            });
        return false;
    });

    $(document).on('change','#imageLoaderPhoto', function(){

    });


    $(document).on('click','.preview_canvas .photo_remove', function(){
        var blockRange = $(this).parent('.preview_canvas');
        if (blockRange.hasClass('canvas_canvas')){
            newForm.append(blockRange.find('canvas').attr('id'), 'canvas_delete');
        } else {
            newForm.append(blockRange.find('img').data('file'), 'image_delete');
        }
        blockRange.remove();
    });    


});
    function alertButton(){
        $('#imageLoaderPhoto').click();
    }
    
    function removeImg(e){
        $(e).parent('.extra_photo_thumb').css('display', 'none');
        return false;
    }
    function addBookmark() {
    if (navigator.userAgent.indexOf('Chrome')>=0) {
        alert('Воспользуйтесь комбинацией "Ctrl+D для добавления сайта в закладки');
        return false;
    } else{
        if (document.all) window.external.addFavorite('http://crm.megapolis18.ru/', 'CRM Megapolis Центр недвижимости');
    }
    }

    function image_check(max_height, max_width, img) {
        var imgw=img.attr('width');
        var imgh=img.attr('height');
        var koef=imgw/imgh;
        if (koef*max_height<max_width) {
            $('.content_pane_image').addClass('vertical');
            $('.content_pane_image').removeAttr("style");
        }
        else {
            $('.content_pane_image').removeClass('vertical');
            $('.content_pane_image').css("top",(max_height-max_width/koef)/2)
        }
    }

    function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ' ' + '$2');
        }
        return x1 + x2;
    }

    function removePrice() {
        if ($(".price_dragger").slider( "instance" )){
            $(".price_dragger").slider("destroy");
            $('.min_price').removeAttr('name');
            $('.max_price').removeAttr('name');
            $(".price_row").hide();
        }
    }