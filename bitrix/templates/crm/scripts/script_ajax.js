$(function(){
    var rieltorSelect = $('#select_rieltor'),
    managerSelect = $('#select_manager'),
    operatorSelect = $('#select_operator'),
    contactSelect = $('#select_contact_type'),
    allSelectFramework = $('#select_rieltor, #select_manager, #select_operator, #select_contact_type');
    updateContentFlag = true;
    
    
    var timeoutId;
    
    var globalAutocompleteFocus = false;
    var searchArea = $('.client_search_ajax'),
        changedResult = $('.changed_result'),
        resultArea = $('.ajax_search_result'),
        infoArea = $('.extra_result_info'),
        inputSearch = $('#global_search'),
        addForm = $('.fast_client_add'),
        selectedResultId = $('#result_id');
    
    function checkFilterValue(){
    
        updateContentFlag = false;
        if ($('input[name=call_phone]').val()){
            contactSelect.prop("disabled", true).val(null).trigger("change");
        } else {
            contactSelect.prop("disabled", false);
        }
        if (contactSelect.val() == 'agent'){
            managerSelect.prop("disabled", true).val(null).trigger("change");
            rieltorSelect.prop("disabled", true).val(null).trigger("change");
        } else {
            managerSelect.prop("disabled", false);
            rieltorSelect.prop("disabled", false);
        }
        updateContentFlag = true;
    }
    function checkResult(){
        if($('.result_row').length>0){
            $('.no_result_block').hide();
        } else {
            $('.no_result_block').show();
        }
    }
    function loadNextPage(){
        var lastElement = $('.result_row:last');
        var ajaxBlock = $('.ajax_next_block');
        if (ajaxBlock.data('get') == 1 && ($(document).scrollTop()+screen.height >= ($(document).height() - lastElement.height() - ajaxBlock.height()))) {
            ajaxBlock.data('get',0);
            nextContent();
        }
    }
    function refreshTab(){
        var activeTab = $('.tabs.visible');
        $('.ajax_next_block').data('page', 2);
        $('.ajax_next_block').data('get', 1);
        activeTab.find('form')[0].reset();
        if (activeTab.find('.calls_form').length>0){
            updateContentFlag = false;
            allSelectFramework.each(function(index){
                $(this).prop("disabled", false);
                $(this).val(null).trigger("change");
            });
            updateContentFlag = true;
        }
        activeTab.find('.ajax_sort select :first').attr("selected", "selected");
        $('select[name=city]')[0].sumo.selectItem(0);
        $('select[name=subject]')[0].sumo.selectItem(0);
        $('select[name=district]')[0].sumo.unSelectAll();
        $('.needs_form .filter_row').removeClass('with_value');
        $('.needs_form .district_select').hide();
        $('select[name=subject]').trigger('change');
    };
    function getCurrentSort(){
        return $('.visible .ajax_sort select').val()
    }
    function getCurrentFilter(){
        var activeTab = $('.tabs.visible');
        var arFilter = {};
        activeTab.find('input').each(function(index){
            if ($(this).attr('type') != 'checkbox'){
                arFilter[$(this).attr('name')] = $(this).val();
            } else {
                if ($(this).prop("checked")){
                    arFilter[$(this).attr('name')] = $(this).val();
                }
            }
        });
        activeTab.find('select').each(function(index){
            arFilter[$(this).attr('name')] = $(this).val();
        });
        return arFilter;
    }
    function updateContent(){
        checkFilterValue();
        $('.preload').show();
        $('.ajax_next_block').data('page', 2);
        $('.ajax_next_block').data('get', 1);
        $.ajax({
            cache: false,
            data: {'ajax':'y', 'type':$('.visible').data('type'), 'filter': getCurrentFilter(), 'sort': getCurrentSort()},
            url: 'update.php',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                $('.current_ajax_list').html(data);
                $('.preload').hide();
                loadNextPage();
                checkResult();
            }
        });
    }
    function nextContent(){
        var currentPage = $('.ajax_next_block').data('page');
        $.ajax({
            cache: false,
            data: {'ajax':'y', 'type':$('.visible').data('type'), 'filter': getCurrentFilter(), 'sort': getCurrentSort(), 'page':currentPage},
            url: 'update.php',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                if (data){
                    $('.current_ajax_list').append(data);
                    $('.ajax_next_block').data('page', currentPage+1);
                    $('.ajax_next_block').data('get', 1);
                    loadNextPage();
                }
                checkResult();
            }
        });
    }

    /*add call*/
    function getChar(event) {
        if (event.which == null) {
            if (event.keyCode < 32) return null;
            return String.fromCharCode(event.keyCode)
        }
        if (event.which != 0) {
            if (event.which < 32) return null;
            return String.fromCharCode(event.which);
        }
        return null;
    }
    
    function updateResult(request){
        $('.call_form_preloader').show();
        $.ajax({
            cache: false,
            data: {'ajax':'y','request': request},
            url: '/ajax/globalsearch.php',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                $('.call_form_preloader').hide();
                $('.search_row').removeClass('help_search_area');
                if (data){
                    resultArea.html(data).show();
                    $('.fast_client_add').hide();
                } else {
                    resultArea.hide();
                    $('.fast_client_add').show();
                }
            }
        });
    }
    function closeForm(){
        addForm.hide();
    }
    function closeSearch(stage){
        switch (stage){
            case 'all':
                searchArea.hide();
            case 'results':
                closeForm();
                resultArea.hide();
                globalAutocompleteFocus = false;
            case 'info':
                infoArea.hide();
        }

    }

    function openSearch(initElement, stage){
        switch (stage){
            case 'all':
                initElement.hide();
                searchArea.show();
            case 'focus':
                inputSearch.focus();
        }
    }
    
    function selectResult(result) {
        selectedResultId.val(result.data('value'));
        changedResult.html(result.find('span').html()).show();
        closeSearch('all');
    }
    
    
    $('input[name=client_phone]').change(function(){
        if ($(this).val().indexOf('_') != -1){
            $(this).val("");
        } else {
            $('.fast_client_add .preload').show();
            $.ajax({
                cache: false,
                data: {'ajax':'y', 'number': $(this).val()},
                url: '/ajax/checkphonenumber.php',
                error: function(){
                    console.log('Connecting error');
                },
                success: function(data){
                    if (data){
                        $('.fast_client_add .form_row.phone_row span').hide();
                        $('button.fast_add_button').removeAttr('disabled').removeClass('gray');
                    } else {
                        $('.fast_client_add .form_row.phone_row span').show();
                        $('button.fast_add_button').attr('disabled', 'disabled').addClass('gray');
                    }
                    $('.fast_client_add .preload').hide();
                }
            });
        }
    })

    inputSearch.on('focus', function(e){
        if ($(this).val().length >=2){
            updateResult($(this).val());
        }
    });
    inputSearch.on('blur', function(e){
        if (!globalAutocompleteFocus){
            closeSearch('results');
        }
    });
    inputSearch.on('keyup', function(e){
        if ((getChar(e) || event.which == 8) &&  $(this).val().length >=2){
            if (e.which != 38 && e.which != 40){
                clearTimeout(timeoutId);
                var currentValue = $(this).val();
                object = {
                   func: function() {updateResult(currentValue)}

                }
                timeoutId = setTimeout(object.func, 150);
            } else {
                if (e.which == 40){
                    if ($('.selected_result').length == 0){
                        $('.search_result').first().addClass('selected_result');
                    } else {
                        var selectedRow = $('.selected_result');
                        if(selectedRow.next('.search_result').height() != null){
                            selectedRow.next('.search_result').addClass('selected_result');
                            selectedRow.removeClass('selected_result');
                        }
                    }
                }
                if (e.which == 38){
                    if ($('.selected_result').length == 0){
                        $('.search_result').last().addClass('selected_result');
                    } else {
                        var selectedRow = $('.selected_result');
                        if(selectedRow.prev('.search_result').height() != null){
                            selectedRow.prev('.search_result').addClass('selected_result');
                            selectedRow.removeClass('selected_result');
                        }
                    }
                    
                }
            }
        } else {
            if (event.which == 13){
                selectResult($('.selected_result'));
            }
        }
        if ((getChar(e) || event.which == 8) &&  $(this).val().length <2){
            closeSearch('results');
        }
    });
    
     
    $(document).on('mouseenter', '.ajax_search_result, .fast_client_add',function(e){
        globalAutocompleteFocus = true;
    });    
    $(document).on('mouseleave', '.ajax_search_result, .fast_client_add',function(e){
        globalAutocompleteFocus = false;
    });

    $(document).on('mouseenter', '.search_result',function(e){
        var currentResult = $(this);
        $('.search_result').removeClass('selected_result');
        currentResult.addClass('selected_result');
        if (currentResult.data('value') != ""){
            /*$.ajax({
                cache: false,
                data: {'ajax':'y','client': $(this).data('value')},
                url: '/ajax/moreinfo.php',
                error: function(){
                    console.log('Connecting error');
                },
                success: function(data){
                    if(data != false){
                        infoArea.html(data);
                        if (resultArea.height()<=(infoArea.height()+currentResult.position().top)){
                            if (infoArea.css('top') != resultArea.height() - infoArea.height()+currentResult.height()){
                                infoArea.css('top', resultArea.height() - infoArea.height()-currentResult.height());
                            }
                        } else {
                            infoArea.css('top', currentResult.position().top);
                        }
                        infoArea.show();
                    } else {
                        closeSearch('info');
                    }
                }
            });*/
        }
    });
    $(document).on('mouseleave', '.search_result',function(e){
        closeSearch('info');
        $('.search_result').removeClass('selected_result');
    });
    $(document).on('click', '.number_result',function(e){
        openSearch($(this), 'focus');
    });
    $(document).on('click', '.changed_result, .easy_add_client',function(e){
        event.preventDefault();
        if(selectedResultId.val()){
            selectedResultId.val("");
        }
        openSearch($(this), 'all');
    });
    $(document).on('click', '.ajax_search_result .search_result',function(e){
        selectResult($(this));
    });
    
    $('.fast_client_add button').on('click', function(event){
        event.preventDefault();
        if (!$('input[name=client_name]').val()){
            $('.fast_client_add .form_row.name_row span').css('color', 'red');
            $('.fast_client_add .form_row.name_row').addClass('errorInput');
        } else {
            $('.fast_client_add .form_row.name_row span').css('color', '#00746b');
            $('.fast_client_add .form_row.name_row').removeClass('errorInput');
            $.ajax({
                cache: false,
                data: {'ajax':'y','phone': $('input[name=client_phone]').val(),'name': $('input[name=client_name]').val(), 'extra_client':$('input[name=extra_client]:checked').val()},
                url: '/ajax/add_client.php',
                error: function(){
                    console.log('Connecting error');
                },
                success: function(data){
                    if (data){
                        selectedResultId.val(data);
                        inputSearch.val($('input[name=client_phone]').val());
                        changedResult.html($('input[name=client_phone]').val()+' '+$('input[name=client_name]').val()).show();
                        closeSearch('all');
                        $('.new_call_form')[0].reset();
                    }
                    
                }
            });
        }

    });
    
    $('.close_add_form').on('click', function(){
        closeForm();
    });
    
    $('.bot_form_section .submit_row input').on('click', function(){
        $('.new_call_form .preload').show();
        var errorFormStatus = false;
        if ($('input[name=date_call]').val()){
            $('.time_error').hide();
        } else {
            errorFormStatus = true;
            $('.time_error').show();
        }
        if (selectedResultId.val()){
            $('.client_error').hide();
        } else {
            errorFormStatus = true;
            $('.client_error').show();
        }
        if (errorFormStatus){
            event.preventDefault();
        }
        $('.new_call_form .preload').hide();
    });
    /////////////////////////////////////////

    updateContent();
      
    $('input[name=call_phone]').inputmask('mask', {'mask': '+7 (999) 999-99-99'});
    $('input[name=call_phone]').on('change', function(){
        if ($(this).val().indexOf('_') != -1){
            $(this).val("");
        }
    })
    $(document).on('change', '.city_select select[name=city]', function(event){
        if ($(this).val() && $(this).val()==49){
            $('.district_select').css('display', 'inline-block');
        } else {
            $('.district_select').hide();
            $('select[name=district]')[0].sumo.unSelectAll();
        }
    });
    $('.ajax_filter input, .ajax_filter select, .ajax_sort select').on('change', function(){
        if (updateContentFlag){
            updateContent();
        }
    })
    rieltorSelect.select2({
        placeholder: "Риэлтор:",
        allowClear: true,
        theme: "classic"
    });
    managerSelect.select2({
        placeholder: "Менеджер:",
        allowClear: true,
        theme: "classic"
    });
    operatorSelect.select2({
        placeholder: "Оператор:",
        allowClear: true,
        theme: "classic"
    });
    contactSelect.select2({
        placeholder: "Тип контакта",
        minimumResultsForSearch: "Infinity",
        allowClear: true,
        theme: "classic"
    });
    $('h3').on('click', function(){
        if (!$(this).hasClass('active')){
            $('h3').removeClass('active');
            $(this).addClass('active');
            var activeTabClass = $(this).attr('for');
            $('.tabs').removeClass('visible');
            $('.'+activeTabClass).addClass('visible');
            refreshTab();
            updateContent();
        }
    });
    $('.ajax_filter form').on('submit', function(){
        updateContent();
        return false;
    });

    $(document).on('click', '.result_row .needs_info-wrap a.btn_edit', function(event){
        $(this).hide();
        $(this).parent('.needs_info-l').css('width', '36px');
        var resultRow = $(this).parents('.result_row');
        resultRow.find('.review_info').hide();
        resultRow.find('.edit_info').show(0, function(){resultRow.find('textarea').focus()});
        event.preventDefault();
    });
    $(document).on('click', '.result_row button.info_cancel', function(event){
        var resultRow = $(this).parents('.result_row');

        resultRow.find('.review_info').show();
        resultRow.find('textarea').val($.trim(resultRow.find('.review_info').html().replace(/<br>/g,"")));
        resultRow.find('.edit_info').hide();
        resultRow.find('a.btn_edit').show();
        resultRow.find('.needs_info-l').css('width', '');
        event.preventDefault();
    });
    $(document).on('click', '.result_row button.info_update', function(event){
        $('.edit_info .preload').show();
        var resultRow = $(this).parents('.result_row');
        var newComment = resultRow.find('textarea').val();
        $.ajax({
            cache: false,
            data: {'ajax':'y', 'call_id':resultRow.data('value'), 'comment_value':newComment},
            url: '/ajax/update_comment.php',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                if (data){
                    resultRow.find('.review_info').show().html(newComment.replace(/\r\n|\r|\n/g,"<br>"));
                    resultRow.find('.edit_info').hide();
                    resultRow.find('a.btn_edit').show();
                    resultRow.find('.needs_info-l').css('width', '');
                    $('.edit_info .preload').hide();
                }
            }
        });
        event.preventDefault();
    });
    
    $(document).on('click', '.phone span', function(){
        if ($('input[name=call_phone]').val() != $(this).html()){
            $('input[name=call_phone]').val($(this).html());
            updateContent();
        }
        return false;
    });
    
    $(document).on('click', '.propose_button a.add_propose', function(event){
        var currentElement = $(this).parents('.entry');
        if(currentElement.data('id')){
            $.cookie('search_element_id', currentElement.data('id'), { path: '/search'});
            $.cookie('search_element_path', '/needs/', { path: '/search'});
            $.cookie('search_element_header', currentElement.find('.entry_header').html(), { path: '/search'});
            $.cookie('search_element_description', currentElement.find('.entry_text').html(), { path: '/search'});
        }
    });
    
    $(document).on('click', '.propose_button span.propose', function(event){
        event.preventDefault();
        $.ajax({
            cache: false,
            data: {'ajax':'y', 'need_id':$(this).parents('.entry').data('id'), 'ads_id':$.cookie('search_element_id')},
            url: '/ajax/add_variant.php',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                if (data){
                }
            }
        });
        $(this).hide();
        $(this).parents('.entry').find('.propose_check').css('display', 'block');
    });
        
    $(document).on('click', '.input-wrap button.del-input', function(event){
        var currentInput = $(this).parent('.input-wrap').find('input');
        if (currentInput.val()){
            currentInput.val("");
            updateContent();
        }
        event.preventDefault();
    });
    
    $(window).scroll(function(){
        loadNextPage();
    });
    
    $('.filter_row select[name=city]').SumoSelect({captionFormat: "Городов: {0}",});
    $('.filter_row select[name=district]').SumoSelect({captionFormat: "Районов: {0}",});
    $('.filter_row select[name=subject]').SumoSelect({captionFormat: "Категорий: {0}",});
    
    $(document).on('click', '.input-wrap button.del-input', function(event){
        event.preventDefault();
        var currentInput = $(this).parent('.input-wrap').find('input[type=text]');
        if (currentInput.val()){
            currentInput.val("").trigger("change");
        } else {
            var currentSumoSelect = $(this).parents('.input-wrap');
            if (currentSumoSelect){
                currentSumoSelect.find('select')[0].sumo.unSelectAll();
                currentSumoSelect.find('select').trigger("change");
                $(this).hide();
            }
        }
    });
    

    $(document).on('change', '.SumoSelect select[name=city], .SumoSelect select[name=district], .SumoSelect select[name=subject]', function(event){
        if ($(this).val()){
            $(this).parents('.filter_row').addClass('with_value');
            $(this).parents('.input-wrap').find('.del-input').show();
        } else {
            $(this).parents('.filter_row').removeClass('with_value');
            $(this).parents('.input-wrap').find('.del-input').hide();
        }
    });
    $('.filter_reset').on('click', function(event){
        if (event.clientX != 0){
            $('body').scrollTop(0);
            refreshTab();
            $('.input_with_value').removeClass('input_with_value');
            $('.with_value .del-input').hide();
            $('.with_value').removeClass('with_value');
        }
    });
    
});