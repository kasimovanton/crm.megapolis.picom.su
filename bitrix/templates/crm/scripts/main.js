function thumbimageCheck() {
    0 == $(".image_grid_thumb").length ? $(".content_pane_image_selector").hide() : $(".image_grid_thumb").length > 12 ? $(".arrow_up, .arrow_down").css("display", "block") : $(".arrow_up, .arrow_down").css("display", "none")
}! function(a) {
    "function" == typeof define && define.amd ? define(["jquery"], a) : a(window.jQuery || window.Zepto)
}(function(a) {
    var b = function(b, c, d) {
        b = a(b);
        var e, f = this,
            g = b.val();
        c = "function" == typeof c ? c(b.val(), void 0, b, d) : c;
        var h = {
            invalid: [],
            getCaret: function() {
                try {
                    var a, c = 0,
                        d = b.get(0),
                        e = document.selection,
                        f = d.selectionStart;
                    return e && !~navigator.appVersion.indexOf("MSIE 10") ? (a = e.createRange(), a.moveStart("character", b.is("input") ? -b.val().length : -b.text().length), c = a.text.length) : (f || "0" === f) && (c = f), c
                } catch (g) {}
            },
            setCaret: function(a) {
                try {
                    if (b.is(":focus")) {
                        var c, d = b.get(0);
                        d.setSelectionRange ? d.setSelectionRange(a, a) : d.createTextRange && (c = d.createTextRange(), c.collapse(!0), c.moveEnd("character", a), c.moveStart("character", a), c.select())
                    }
                } catch (e) {}
            },
            events: function() {
                b.on("keyup.mask", h.behaviour).on("paste.mask drop.mask", function() {
                    setTimeout(function() {
                        b.keydown().keyup()
                    }, 100)
                }).on("change.mask", function() {
                    b.data("changed", !0)
                }).on("blur.mask", function() {
                    g === b.val() || b.data("changed") || b.trigger("change"), b.data("changed", !1)
                }).on("keydown.mask, blur.mask", function() {
                    g = b.val()
                }).on("focusout.mask", function() {
                    d.clearIfNotMatch && !e.test(h.val()) && h.val("")
                })
            },
            getRegexMask: function() {
                for (var a, b, d, e, g = [], h = 0; h < c.length; h++)(a = f.translation[c[h]]) ? (b = a.pattern.toString().replace(/.{1}$|^.{1}/g, ""), d = a.optional, (a = a.recursive) ? (g.push(c[h]), e = {
                    digit: c[h],
                    pattern: b
                }) : g.push(d || a ? b + "?" : b)) : g.push(c[h].replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&"));
                return g = g.join(""), e && (g = g.replace(RegExp("(" + e.digit + "(.*" + e.digit + ")?)"), "($1)?").replace(RegExp(e.digit, "g"), e.pattern)), RegExp(g)
            },
            destroyEvents: function() {
                b.off("keydown keyup paste drop blur focusout ".split(" ").join(".mask "))
            },
            val: function(a) {
                var c = b.is("input") ? "val" : "text";
                return 0 < arguments.length ? (b[c]() !== a && b[c](a), c = b) : c = b[c](), c
            },
            getMCharsBeforeCount: function(a, b) {
                for (var d = 0, e = 0, g = c.length; g > e && a > e; e++) f.translation[c.charAt(e)] || (a = b ? a + 1 : a, d++);
                return d
            },
            caretPos: function(a, b, d, e) {
                return f.translation[c.charAt(Math.min(a - 1, c.length - 1))] ? Math.min(a + d - b - e, d) : h.caretPos(a + 1, b, d, e)
            },
            behaviour: function(b) {
                b = b || window.event, h.invalid = [];
                var c = b.keyCode || b.which;
                if (-1 === a.inArray(c, f.byPassKeys)) {
                    var d = h.getCaret(),
                        e = h.val().length,
                        g = e > d,
                        i = h.getMasked(),
                        j = i.length,
                        k = h.getMCharsBeforeCount(j - 1) - h.getMCharsBeforeCount(e - 1);
                    return h.val(i), !g || 65 === c && b.ctrlKey || (8 !== c && 46 !== c && (d = h.caretPos(d, e, j, k)), h.setCaret(d)), h.callbacks(b)
                }
            },
            getMasked: function(a) {
                var b, e, g = [],
                    i = h.val(),
                    j = 0,
                    k = c.length,
                    l = 0,
                    m = i.length,
                    n = 1,
                    o = "push",
                    p = -1;
                for (d.reverse ? (o = "unshift", n = -1, b = 0, j = k - 1, l = m - 1, e = function() {
                        return j > -1 && l > -1
                    }) : (b = k - 1, e = function() {
                        return k > j && m > l
                    }); e();) {
                    var q = c.charAt(j),
                        r = i.charAt(l),
                        s = f.translation[q];
                    s ? (r.match(s.pattern) ? (g[o](r), s.recursive && (-1 === p ? p = j : j === b && (j = p - n), b === p && (j -= n)), j += n) : s.optional ? (j += n, l -= n) : s.fallback ? (g[o](s.fallback), j += n, l -= n) : h.invalid.push({
                        p: l,
                        v: r,
                        e: s.pattern
                    }), l += n) : (a || g[o](q), r === q && (l += n), j += n)
                }
                return a = c.charAt(b), k !== m + 1 || f.translation[a] || g.push(a), g.join("")
            },
            callbacks: function(a) {
                var e = h.val(),
                    f = e !== g,
                    i = [e, a, b, d],
                    j = function(a, b, c) {
                        "function" == typeof d[a] && b && d[a].apply(this, c)
                    };
                j("onChange", !0 === f, i), j("onKeyPress", !0 === f, i), j("onComplete", e.length === c.length, i), j("onInvalid", 0 < h.invalid.length, [e, a, b, h.invalid, d])
            }
        };
        f.mask = c, f.options = d, f.remove = function() {
            var a = h.getCaret();
            return h.destroyEvents(), h.val(f.getCleanVal()), h.setCaret(a - h.getMCharsBeforeCount(a)), b
        }, f.getCleanVal = function() {
            return h.getMasked(!0)
        }, f.init = function(c) {
            c = c || !1, d = d || {}, f.byPassKeys = a.jMaskGlobals.byPassKeys, f.translation = a.jMaskGlobals.translation, f.translation = a.extend({}, f.translation, d.translation), f = a.extend(!0, {}, f, d), e = h.getRegexMask(), !1 === c ? (d.placeholder && b.attr("placeholder", d.placeholder), b.attr("autocomplete", "off"), h.destroyEvents(), h.events(), c = h.getCaret(), h.val(h.getMasked()), h.setCaret(c + h.getMCharsBeforeCount(c, !0))) : (h.events(), h.val(h.getMasked()))
        }, f.init(!b.is("input"))
    };
    a.maskWatchers = {};
    var c = function() {
            var c = a(this),
                e = {},
                f = c.attr("data-mask");
            return c.attr("data-mask-reverse") && (e.reverse = !0), c.attr("data-mask-clearifnotmatch") && (e.clearIfNotMatch = !0), d(c, f, e) ? c.data("mask", new b(this, f, e)) : void 0
        },
        d = function(b, c, d) {
            d = d || {}, b = a(b).data("mask");
            var e = JSON.stringify;
            try {
                return "object" != typeof b || e(b.options) !== e(d) || b.mask !== c
            } catch (f) {}
        };
    a.fn.mask = function(c, e) {
        e = e || {};
        var f = this.selector,
            g = a.jMaskGlobals,
            h = a.jMaskGlobals.watchInterval,
            i = function() {
                return d(this, c, e) ? a(this).data("mask", new b(this, c, e)) : void 0
            };
        a(this).each(i), f && "" !== f && g.watchInputs && (clearInterval(a.maskWatchers[f]), a.maskWatchers[f] = setInterval(function() {
            a(document).find(f).each(i)
        }, h))
    }, a.fn.unmask = function() {
        return clearInterval(a.maskWatchers[this.selector]), delete a.maskWatchers[this.selector], this.each(function() {
            a(this).data("mask") && a(this).data("mask").remove().removeData("mask")
        })
    }, a.fn.cleanVal = function() {
        return this.data("mask").getCleanVal()
    };
    var e = {
        maskElements: "input,td,span,div",
        dataMaskAttr: "*[data-mask]",
        dataMask: !0,
        watchInterval: 300,
        watchInputs: !0,
        watchDataMask: !1,
        byPassKeys: [9, 16, 17, 18, 36, 37, 38, 39, 40, 91],
        translation: {
            0: {
                pattern: /\d/
            },
            9: {
                pattern: /\d/,
                optional: !0
            },
            "#": {
                pattern: /\d/,
                recursive: !0
            },
            A: {
                pattern: /[a-zA-Z0-9]/
            },
            S: {
                pattern: /[a-zA-Z]/
            }
        }
    };
    a.jMaskGlobals = a.jMaskGlobals || {}, e = a.jMaskGlobals = a.extend(!0, {}, e, a.jMaskGlobals), e.dataMask && a(e.dataMaskAttr).each(c), setInterval(function() {
        a.jMaskGlobals.watchDataMask && a(document).find(a.jMaskGlobals.maskElements).filter(e.dataMaskAttr).each(c)
    }, e.watchInterval)
}), $(function() {
    $status = $(".toggle_btn").attr("status"), "closed" == $status ? ($(".filter_bottom").addClass("hidden"), $(".toggle_btn").html("развернуть"), $(".toggle_btn").attr("status", "closed"), $(".toggle_btn").removeClass("toggle_up"), $(".toggle_btn").addClass("toggle_down")) : ($(".filter_bottom").removeClass("hidden"), $(".toggle_btn").html("свернуть"), $(".toggle_btn").attr("status", "opened"), $(".toggle_btn").removeClass("toggle_down"), $(".toggle_btn").addClass("toggle_up")), $(".toggle_btn").on("click", function() {
        $status = $(this).attr("status"), "opened" == $status ? ($(".filter_bottom").addClass("hidden"), $(this).html("развернуть"), $(this).attr("status", "closed"), $(this).removeClass("toggle_up"), $(this).addClass("toggle_down")) : ($(".filter_bottom").removeClass("hidden"), $(this).html("свернуть"), $(this).attr("status", "opened"), $(this).removeClass("toggle_down"), $(this).addClass("toggle_up"))
    })
}), $.datepicker.regional.ru = {
    closeText: "Закрыть",
    prevText: "&#x3c;Пред",
    nextText: "След&#x3e;",
    currentText: "Сегодня",
    monthNames: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
    monthNamesShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
    dayNames: ["воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"],
    dayNamesShort: ["вск", "пнд", "втр", "срд", "чтв", "птн", "сбт"],
    dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
    dateFormat: "dd.mm.yy",
    firstDay: 1,
    isRTL: !1
}, $(".date").datepicker($.datepicker.regional.ru), $(function() {
    function a() {
        c > e ? $(".arrow_down").css("background-color", "gray") : $(".arrow_down").removeAttr("style"), 0 != c ? $(".arrow_up").css("background-color", "gray") : $(".arrow_up").removeAttr("style")
    }
    var b = 65,
        c = 0,
        d = Math.ceil($(".image_grid_thumb").length / 4),
        e = -b * (d - 3);
    a(), $(".arrow_down").on("click", function() {
        d = Math.ceil($(".image_grid_thumb").length / 4), c -= b, -b * (d - 3) > c && (c = -b * (d - 3)), $(".image_grid_inner").animate({
            marginTop: c + "px"
        }, 200), a()
    }), $(".arrow_up").on("click", function() {
        d = Math.ceil($(".image_grid_thumb").length / 4), c += b, c > 0 && (c = 0), $(".image_grid_inner").animate({
            marginTop: c + "px"
        }, 200), a()
    }), thumbimageCheck()
});
var browser_ = {
    is_chrome: navigator.userAgent.indexOf("Chrome") > -1,
    is_explorer: navigator.userAgent.indexOf("MSIE") > -1,
    is_firefox: navigator.userAgent.indexOf("Firefox") > -1,
    is_safari: navigator.userAgent.indexOf("Safari") > -1,
    is_Opera: navigator.userAgent.indexOf("Presto") > -1
};
browser_.is_chrome && browser_.is_safari && (browser_.is_safari = !1), $(function() {
    $("input, select").change(function() {
        if ($(this).parents('.ajax_filter').length==0){
            $(this).val() ? $(this).css("border-color", "#00746b") : $(this).removeAttr("style")
        }
    }),
    $("select").on("change", function() {
        $(this).css("color", "#000")
    }), $("#cprice, #cfloor, #cfloors, #crooms").on("click", function() {
        var a = $("#cprice").is(":checked"),
            b = $("#cfloor").is(":checked"),
            c = $("#cfloors").is(":checked"),
            d = $("#crooms").is(":checked");
        $(".min_price, .max_price").prop("disabled", a), a ? $(".price_any").addClass("half_opacity") : $(".price_any").removeClass("half_opacity"), $("#cnotfirstfloor, #cnotlastfloor, .floor_from, .floor_to").prop("disabled", b), b ? $(".floor_any").addClass("half_opacity") : $(".floor_any").removeClass("half_opacity"), $(".floors_from, .floors_to").prop("disabled", c), c ? $(".floors_any").addClass("half_opacity") : $(".floors_any").removeClass("half_opacity"), $(".rooms_from, .rooms_to").prop("disabled", d), d ? $(".rooms_any").addClass("half_opacity") : $(".rooms_any").removeClass("half_opacity")
    }), $("[data-color]").each(function() {
        var a = $(this).data("color"),
            b = $(this).data("selector");
        a || (a = "#fff"), b || (b = ".entry_text");
        var c = "background-image:" + a + ";";
        c += "background-image:-webkit-linear-gradient(rgba(0, 0, 0, 0) 60%, " + a + ");", c += "background-image:-o-linear-gradient(rgba(0, 0, 0, 0) 60%, " + a + ");", c += "background-image:-moz-linear-gradient(rgba(0, 0, 0, 0) 60%, " + a + ");", c += "background-image:linear-gradient(rgba(1, 0, 0, 0) 60%, " + a + " 40%);", c += "background-image:-webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(60%, rgba(0, 0, 0, 0)), color-stop(100%, " + a + "));", browser_.is_firefox && (c += "opacity:0.8;"), $(this).css({
            background: a
        }), $(this).find(b).append('<span class="after" style="' + c + '"></span>')
    })
});
