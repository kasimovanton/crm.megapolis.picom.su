function changeVariantShowTime(variant_id, new_time){
    $.ajax({
        cache: false,
        data: {'ajax':'y', 'variant_id':variant_id, 'new_time':new_time},
        url: '/ajax/changeshowtime.php',
        error: function(){
            console.log('Connecting error');
        },
        success: function(data){
            if(data){
                getVariant(variant_id);
            }
        }
    });
}

function getObjectVariants(){
    $.ajax({
        cache: false,
        data: {'ajax':'y', 'need_id':$('input[name=needs_id]').val()},
        url: '/ajax/get_objects_variants.php',
        error: function(){
            console.log('Connecting error');
        },
        success: function(data){
            $('.object_variant_block ul').html(data);
        }
    });
}
function getVariant(variant_id){
    $.ajax({
        cache: false,
        data: {'ajax':'y', 'variant_id':variant_id},
        url: '/ajax/getvariant.php',
        error: function(){
            console.log('Connecting error');
        },
        success: function(data){
            var currentVariant = $('.variant_element[data-id='+variant_id+']');
            if(data){
                currentVariant.html(data);
            } else {
                currentVariant.html('Произошла ошибка');
            }
        }
    });
}
function getVariants(block_type, page){
    $.ajax({
        cache: false,
        data: {'ajax':'y', 'block_type':block_type, 'needs_id':$('input[name=needs_id]').val(), 'page_counter':$('select[name=elements_count]').val(), 'page':page},
        url: '/ajax/getvariants.php',
        error: function(){
            console.log('Connecting error');
        },
        success: function(data){
            if(data){
                $('.tabs.'+block_type+'_option').html(data);
            } else {
                $('.tabs.'+block_type+'_option').html('Варианты отсутствуют');
            }
            $('.variants_block .tabs .preload').hide();
        }
    });
}
function closeVariant(variant_id){
    $.ajax({
        cache: false,
        data: {'ajax':'y', 'variant_id':variant_id},
        url: '/ajax/closevariant.php',
        error: function(){
            console.log('Connecting error');
        },
        success: function(data){
            if(data){
                console.log(data);
            }
        }
    });
}
function checkVariants(){
    $.ajax({
        cache: false,
        data: {'ajax':'y', 'needs_id':$('input[name=needs_id]').val(), 'page_counter':$('select[name=elements_count]').val()},
        url: '/ajax/checkvariants.php',
        error: function(){
            console.log('Connecting error');
        },
        success: function(data){
            if(data){
                $('.status_block span').html('Поиск вариантов');
                $('.tabs_main_block').show();
                $('.variants_header span').html(data);
                getVariants('current', 1);
                $('.variants_block .preload').hide();
            } else {
                $('.status_block span').html('Создана');
                $('.empty_variant_block').show();
            }
        }
    });
}

$(function(){
    $(document).on('change', 'select[name=elements_count]', function(event){
        $('.variants_block .tabs .preload').show();
        getVariants($('h3.active').data('type'), 1);
    });
    $(document).on('click', '.needs_detail .needs_variants h3', function(event){
        event.preventDefault();
        $('.variants_block .tabs .preload').show();
        if (!$(this).hasClass('active')){
            $('h3').removeClass('active');
            $(this).addClass('active');
            var activeTabClass = $(this).attr('for');
            $('.tabs').removeClass('visible');
            $('.'+activeTabClass).addClass('visible');
            getVariants($(this).data('type'), 1);
        }
    });
    
    $(document).on('click', '.variant_element .variant_to_close', function(event){
        event.preventDefault();
        $(this).hide();
        var acceptBlock = $(this).parents('.variant_element').find('.acception_block');
        acceptBlock.find('.preload').show();
        acceptBlock.show();
        object = {
           funcHidePreload: function() {acceptBlock.find('.preload').hide();}
        }
        timeoutId = setTimeout(object.funcHidePreload, 500);
        
    });    
    $(document).on('click', '.variant_element .cancel_action', function(event){
        event.preventDefault();
        $(this).parents('.variant_element').find('.acception_block').hide();
        $(this).parents('.variant_element').find('.variant_to_close').show();
    });
    $(document).on('click', '.variant_element .accept_action', function(event){
        event.preventDefault();
        $('.variants_block .tabs .preload').show();
        closeVariant($(this).parents('.variant_element').data('id'));
        object = {
           afterClose: function() {getVariants($('h3.active').data('type'), 1);}
        }
        setTimeout(object.afterClose, 0);
        
    });
    $(document).on('click', '.page_navigation_block ul li', function(event){
        event.preventDefault();
        $('.variants_block .tabs .preload').show();
        $('li.active').removeClass('active');
        $(this).addClass('active');
        getVariants($(this).parents('.tabs_main_block').find('h3.active').data('type'), $(this).data('page'));
    });
    
    $(document).on('click', '#choiceVariant .accept_button', function(event){
        event.preventDefault();
        $.ajax({
            cache: false,
            data: {'ajax':'y', 'need_id':$('.needs_info input[name=needs_id]').val() ,'variant_id':$('#choiceVariant input[name=variant_id]').val()},
            url: '/ajax/choice_variant.php',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                location.reload();
            }
        });
    });
    
    $(document).on('click', '.status_column .status_name .show_variant', function(event){
        event.preventDefault();
        $(this).hide().parents('.status_name').find('.change_time_block').show();
    });
    $(document).on('click', '.change_time a', function(event){
        event.preventDefault();
        var currentBlock = $(this).parents('.status_name');
        currentBlock.find('.show_time').hide();
        currentBlock.find('.status_var_name').hide();
        currentBlock.find('.change_time').hide();
        currentBlock.find('.change_time_block').show();
    });
    $(document).on('click', '.change_time_block button.decline_new_time', function(event){
        event.preventDefault();
        var currentElement = $(this).parents('.variant_element');
        currentElement.find('.change_time_block').hide();
        currentElement.find('.show_variant').show();
        currentElement.find('.show_time').show();
        currentElement.find('.status_var_name').show();
        currentElement.find('.change_time').show();
    });
    $(document).on('click', '.change_time_block button.accept_new_time', function(event){
        event.preventDefault();
        var currentElement = $(this).parents('.variant_element');
        if (currentElement.find('input[name=show_time]').val()){
            changeVariantShowTime(currentElement.data('id'), currentElement.find('input[name=show_time]').val());
        }
    });
    
    $('.close_need_block .close_need').colorbox({
        closeButton: true,
        inline:true,
        href: "#closeNeed",
        onOpen: function(){
            $('#closeNeed h4 span').html($('.content_wrapper .content_header').html());
        },
        onClosed: function(){
        }
    }); 
    
    $(document).on('click', '#history_event .send_new_comment', function(event){
        event.preventDefault();
        if($('#history_event textarea[name=new_comment]').val()){
            $.ajax({
                cache: false,
                data: {'ajax':'y', 'comment':$('#history_event textarea[name=new_comment]').val(), "element_history_id":$('#history_event input[name=element_id]').val()},
                url: '/ajax/add_history.php',
                error: function(){
                    console.log('Connecting error');
                },
                success: function(data){
                    $('.add_history_block ul').append('<li>'+data+'</li>');
                    $('#history_event textarea[name=new_comment]').val("");
                    $.colorbox.resize();
                }
            });

        }
    });
    
    $(document).on('click', '#closeNeed .accept_button', function(event){
        event.preventDefault();
        $.ajax({
            cache: false,
            data: {'ajax':'y', 'need_id':$('#closeNeed input[name=need_id]').val()},
            url: '/ajax/close_need.php',
            error: function(){
                console.log('Connecting error');
            },
            success: function(data){
                document.location.href = "/operator/";
            }
        });
    });
    checkVariants();
    getObjectVariants();

});