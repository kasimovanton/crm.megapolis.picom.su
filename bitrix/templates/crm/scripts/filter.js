// console.log('\'Allo \'Allo!');
$(function(){
/////////////////////////////////////////////////////////////////////
// Кнопка свернуть/развернуть в фильтрах
	/*$status = $('.toggle_btn').attr('status');
	if($status == 'closed') {
			$('.filter_bottom').addClass('hidden');
			$('.filter').css('height','110px');
			$('.toggle_btn').html('развернуть');
			$('.toggle_btn').attr('status','closed');
			$('.toggle_btn').removeClass('toggle_up');
			$('.toggle_btn').addClass('toggle_down');
		} else {
			$('.filter_bottom').removeClass('hidden');
			$('.filter').css('height','220px');
			$('.toggle_btn').html('свернуть');
			$('.toggle_btn').attr('status','opened');
			$('.toggle_btn').removeClass('toggle_down');
			$('.toggle_btn').addClass('toggle_up');
		}
	$('.toggle_btn').on('click', function() {
		$status = $(this).attr('status');
        console.log($status);
		if($status == 'opened') {
			$('.filter_bottom').addClass('hidden');
			$('.filter').css('height','110px');
			$(this).html('развернуть');
			$(this).attr('status','closed');
			$(this).removeClass('toggle_up');
			$(this).addClass('toggle_down');
		} else {
			$('.filter_bottom').removeClass('hidden');
			$('.filter').css('height','220px');
			$(this).html('свернуть');
			$(this).attr('status','opened');
			$(this).removeClass('toggle_down');
			$(this).addClass('toggle_up');
		}
	});*/

/////////////////////////////////////////////////////////////////////
// Выбор диапазона цены
	/*$( ".price_dragger" ).slider({
                range: true,
                min: 0,
                max: maxprice,
                step: 100000,
                slide: function( event, ui ) {
                    $( ".min_price" ).val( ui.values[ 0 ] );
                    $( ".max_price" ).val( ui.values[ 1 ] );
                }
            });
    $( ".min_price" ).val( $( ".price_dragger" ).slider( "values", 0 ) );
    $( ".max_price" ).val( $( ".price_dragger" ).slider( "values", 1 ) );*/
});

/////////////////////////////////////////////////////////////////////
// datepicker
$.datepicker.regional['ru'] = { 
	closeText: 'Закрыть', 
	prevText: '&#x3c;Пред', 
	nextText: 'След&#x3e;', 
	currentText: 'Сегодня', 
	monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь', 
	'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'], 
	monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн', 
	'Июл','Авг','Сен','Окт','Ноя','Дек'], 
	dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'], 
	dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'], 
	dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'], 
	dateFormat: 'dd.mm.yy', 
	firstDay: 1, 
	isRTL: false 
};
$( ".date" ).datepicker( $.datepicker.regional[ "ru" ] );