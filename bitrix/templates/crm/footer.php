<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<?if ($USER->IsAuthorized()) {?>
        <?if (preg_match("/search/", $APPLICATION->GetCurDir()) || preg_match("/operator/", $APPLICATION->GetCurDir())):?>
            <?if(!empty($_COOKIE['search_element_id'])):?>
                <div class="fixed_block_text" id="fixed_block_text">
                    <div class="wrap">
                        <input type="hidden" name="search_element_id" value="<?=$_COOKIE['search_element_id'];?>">
                        <button type="button" class="close_form"></button>
                        <span><a target="_blank" href="<?=$_COOKIE['search_element_path'];?><?=$_COOKIE['search_element_id'];?>/"><?=$_COOKIE['search_element_header'];?></a>:</span>
                        <p>
                            <?=$_COOKIE['search_element_description'];?>
                        </p>                        
                    </div>
                    <a href="#" class="open_fixed_block"></a>
                </div>
            <?endif;?>
        <?endif;?>
    </div>
    </div>
<?}?>
</main> <!-- /main -->
</div> <!-- /page_wrapper -->


    <?
    
        $APPLICATION->IncludeComponent("bitrix:system.auth.form", ".default", Array(
                "REGISTER_URL" => SITE_DIR."login/",	// Страница регистрации
                "PROFILE_URL" => SITE_DIR."personal/",	// Страница профиля
                "SHOW_ERRORS" => "N",	// Показывать ошибки
            ),
            false
        );
        
    ?>
    
   <?/* <script src="<?=SITE_TEMPLATE_PATH?>/scripts/main.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/scripts/filter.js"></script>*/?>

	</body>

</html>