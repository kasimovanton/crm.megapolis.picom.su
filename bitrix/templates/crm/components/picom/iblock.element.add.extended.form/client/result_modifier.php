<?php
/**
 * Created by PhpStorm.
 * User: picom
 * Date: 21.11.14
 * Time: 11:08
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
//pre($arResult);

$arResult["PROPERTY_LIST"]=array("F","I","O","PHONE","DOP_PHONE", "DOP_FACE","EMAIL","PREVIEW_TEXT","NAME","RIELTOR", "MANAGER");

global $USER;
$arResult["PROPERTY_LIST_FULL"]["RIELTOR"]["DEFAULT_VALUE"] = $USER->GetID();
$arResult["PROPERTY_LIST_FULL"]["NAME"]["DEFAULT_VALUE"] = 'Клиент';

$userArStatus = getUserStatus($USER->GetID());
if (in_array(9, $userArStatus)){
    $arResult["PROPERTY_LIST_FULL"]["MANAGER"]["DEFAULT_VALUE"] = $USER->GetID();
}

$errorNames   = array_map(function ($e) { return preg_replace('~^.*\'(.*?)\'.*$~', '$1', $e); }, $arResult['ERRORS']);
//pre($errorNames);
foreach ($arResult['PROPERTY_LIST_FULL'] as $propertyCode => &$property) {
    $isProp = !in_array($propertyCode,$arResult['IBLOCK_FIELDS']);
    $name   = $isProp ? $property['NAME'] : (!empty($arParams["CUSTOM_TITLE_$propertyCode"]) ? $arParams["CUSTOM_TITLE_$propertyCode"] : GetMessage("IBLOCK_FIELD_$propertyCode"));

    $errorIndex = array_search($name, $errorNames);
    $property['ERROR'] = $errorIndex !== false;
    if ($property['ERROR']) {
        $errorIndexes[] = $errorIndex;
    }
}
/*
foreach ($errorIndexes as $errorIndex) {
    unset($arResult['ERRORS'][$errorIndex]);
}*/