<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
    $isFirefox = strstr($_SERVER['HTTP_USER_AGENT'], 'Firefox');
?>
<?/*
<style>
.entry_agent_link{
    position:absolute;
    width:125px;
    overflow:hidden;
    text-overflow:ellipsis;
    white-space:nowrap;
}
.entry_agent_link:hover{
    white-space:normal;
}
</style>*/?>
<?if (count($arResult["ITEMS"])==0):?>
    <p>Объявления не найдены</p>
<?else:?>
    <?php
        $this->SetViewTarget('count'); //Вывод в шаблоне компонента bitrix:search.form
    ?> 
    <?if (isset($_REQUEST['iteration'])):?>
        <?$count = $arResult["ALL_COUNT"];?>
        <div class="active_filters_header">
            <span>Найдено</span>
            <span><?=$count;?></span>
            <span>подходящ<?if ($count == 1 || ($count-1)%10==0):?>ее объявление<?else:?>их объявлени<?if ($count<5):?>я<?else:?>й<?endif;?> <?endif;?></span>
        </div>
    <?endif;?>
    <?php
        $this->EndViewTarget();
    ?>
    <div class="search_menu">
        <?$userStatus = getUserStatus($USER->GetID());?>
        <?if (!in_array(8, $userStatus)):?>
            <a href="/ads/add.php" class="button button_universal">Добавить объявление</a>
        <?endif;?>
        <div class="sorting">
            <span>Сортировать:</span>
            <div class="form_select sort_ord">
                <select>
                    <option order="desc" sort="ACTIVE_FROM" <?if (isset($_SESSION['sort']) && $_SESSION['sort'] == 'ACTIVE_FROM'):?>selected<?endif?>>По дате добавления</option>
                    <option order="asc" sort="PROPERTY_FIO_AUTHOR" <?if (isset($_SESSION['sort']) && $_SESSION['sort'] == 'PROPERTY_FIO_AUTHOR'):?>selected<?endif?>>По ФИО риэлтора</option>
                    <option order="asc" sort="PROPERTY_Price" <?if (isset($_SESSION['sort']) && $_SESSION['sort'] == 'PROPERTY_Price' && $_SESSION['order'] == 'asc'):?>selected<?endif?>>Дешевле</option>
                    <option order="desc" sort="PROPERTY_Price" <?if (isset($_SESSION['sort']) && $_SESSION['sort'] == 'PROPERTY_Price' && $_SESSION['order'] == 'desc'):?>selected<?endif?>>Дороже</option>
                </select>
            </div>
            <span>Элементов на странице:</span>
            <div class="form_select paging">
                <select class="short">
                    <option <?if($_SESSION['paging'] == 5):?>selected<?endif;?> numb="5">5</option>
                    <option <?if($_SESSION['paging'] == 10 || !isset($_SESSION['paging'])):?>selected<?endif;?> numb="10">10</option>
                    <option <?if($_SESSION['paging'] == 20):?>selected<?endif;?> numb="20">20</option>
                    <option <?if($_SESSION['paging'] == 50):?>selected<?endif;?> numb="50">50</option>
                    <option <?if($_SESSION['paging'] == 100):?>selected<?endif;?> numb="100">100</option>
                    <?/*<option <?if($_SESSION['paging'] == 'all'):?>selected<?endif;?> numb="all">Все</option>*/?>
                </select>
            </div>
        </div>
    </div>
<?endif?>
<div class="search_results">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <?$checkAuthor = in_array($arItem['PROPERTIES']['Author']['VALUE'], $arResult['checkAuthor']);?>
    <?php
        $color_class = "ad-red";
        if ($checkAuthor){
            $color_class = "ad-blue";
            if ($arItem['PROPERTIES']['Author']['VALUE'] == $arParams['USER_ID']){
                $color_class = "ad-green";
            }
        }

    ?>
    <?if ((date("d.m.Y",  MakeTimeStamp($arItem['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS")) == date("d.m.Y"))
            &&(MakeTimeStamp(date("d.m.Y H:i:s"), "DD.MM.YYYY HH:MI:SS")- MakeTimeStamp($arItem['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS")<=10000)):?>

        <div class="entry <?=$color_class;?> recent_entry" <?if (!$isFirefox):?> data-color="#cafffc"<?endif;?> id="<?=$this->GetEditAreaId($arItem['ID']);?>">
    <?else:?>
        <div class="entry <?=$color_class;?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
    <?endif;?>
        <?if ($arItem['PROPERTIES']['Avito']['VALUE']):?>
            <mark title="Объявление будет выгружено на авито">
                <span class="avito_ico">
                    <?if ($arItem['PROPERTIES']['CALLC']['VALUE'] && $arItem['PROPERTIES']['Avito']['VALUE'] ):?>
                        <b class="callc">Call</b>
                    <?endif;?>
                </span>
            </mark>
        <?endif;?>
        <div class="entry_date">
            <?$datetime = preg_split("/[\s]+/", $arItem['ACTIVE_FROM']);
                if (date("d.m.Y",  MakeTimeStamp($arItem['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS")) == date("d.m.Y")){
                    $entry_day = 'Сегодня';
                }else{
                    $entry_day = date("d.m.Y",  MakeTimeStamp($arItem['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS"));
                }
            ?>
            <div class="entry_day"><?=$entry_day?></div>
            <div class="entry_time"><?=date("H:i",  MakeTimeStamp($arItem['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS"));?></div>
        </div>
        <div class="entry_thumb">
            <div class="entry_img">
                <?if (!empty($arItem['DETAIL_PICTURE'])):?>
                    <?$mainPhoto = $arItem['DETAIL_PICTURE'];?>
                <?else:?>
                    <?if (!empty($arItem['PROPERTIES']['Images']['VALUE'][0])):?>
                        <?$mainPhoto = $arItem['PROPERTIES']['Images']['VALUE'][0];?>
                    <?else:?>
                        <?$mainPhoto = $arResult["object_image"][$arItem['ID']];?>
                    <?endif;?>
                <?endif;?>
                <?if (!empty($mainPhoto)):?>
                    <?$renderImage = CFile::ResizeImageGet($mainPhoto, Array("width" => '120px', "height" => '90px'),BX_RESIZE_IMAGE_EXACT);?>
                    <a href="<?=$arItem['DETAIL_PAGE_URL'];?>"><img src="<?=$renderImage['src'];?>" alt="<?=$renderImage['alt'];?>"></a>
                <?endif;?>
            </div>
                <?if (CUser::GetID() == $arItem['PROPERTIES']['Author']['VALUE']):?>
                    <?$client_list = getEnumValues(5, array("Client"));?>
                    <?if (!empty($client_list['Client'][$arItem['PROPERTIES']['Client']['VALUE']])):
                        $link_client = '/clients/'.$arItem['PROPERTIES']['Client']['VALUE'].'/';
                        $name_client = $client_list['Client'][$arItem['PROPERTIES']['Client']['VALUE']]; //result_modifier
                    ?>
                        <div class="entry_agent">Клиент:</div>
                        <a class="entry_agent_link" href="<?=$link_client;?>"><?=$name_client?></a>
                    <?else:?>
                        <?if (!empty($arItem['PROPERTIES']['OBJECT_ELEMENT']['VALUE'])):?>
                            <?php
                                $db_props = CIBlockElement::GetProperty(16, $arItem['PROPERTIES']['OBJECT_ELEMENT']['VALUE'], array("sort" => "asc"), Array("CODE"=>"Author"));
                                if($ar_props = $db_props->Fetch()){
                                    $officeManagerId = IntVal($ar_props["VALUE"]);
                                    // $rsUser = CUser::GetByID($officeManagerId);
                                    // $currentUser = $rsUser->Fetch();
                                }
                                ?>
                                <div class="entry_agent">Офис-менеджер:</div>
                                <?php
                                    $link_agent = '/agents/'.$officeManagerId.'/';
                                    $name_agent = $arResult['agents_list']["Author"][$officeManagerId];//result_modifier
                                ?>
                                <a class="entry_agent_link" href="<?=$link_agent;?>"><?=$name_agent;?></a>
                        <?else:?>
                            Не указан
                        <?endif;?>
                    <?endif;?>
                <?else:?>
                    <div class="entry_agent">Агент:</div>
                    <?php
                        $link_agent = '/agents/'.$arItem['PROPERTIES']['Author']['VALUE'].'/';
                        $name_agent = $arResult['agents_list']["Author"][$arItem['PROPERTIES']['Author']['VALUE']] //result_modifier
                    ?>
                    <a class="entry_agent_link" href="<?=$link_agent;?>"><?=$name_agent;?></a>
                <?endif;?>
        </div>
        <div class="entry_description">
            <a class="entry_header" href="<?=$arItem['DETAIL_PAGE_URL'];?>" ><?=$arItem['NAME'];?></a>
            <p class="entry_text <?if ($isFirefox):?>firefox_text<?endif;?>">
                <span class="street">
                    <?if (!empty($arItem['PROPERTIES']['Street']['VALUE'])):?>
                        <?=$arItem['PROPERTIES']['Street']['VALUE'];?>
                    <?endif;?>
                    <?if (!empty($arItem['PROPERTIES']['HOUSE']['VALUE'])):?>
                        <?=$arItem['PROPERTIES']['HOUSE']['VALUE'];?>
                    <?endif;?>
                    <?if (!empty($arItem['PROPERTIES']['APARTMENT']['VALUE'])):?>
                        , <i><?=$arItem['PROPERTIES']['APARTMENT']['VALUE'];?>
                        <?if ($arItem['PROPERTIES']['Avito']['VALUE']):?>
                            (номер квартиры не выгружается на авито)
                        <?endif;?>
                        </i>
                    <?endif;?>
                </span>
                <br/>
                <?=$arItem['DETAIL_TEXT'];?>
            </p>
        </div>
            <?$entry_price = "";
                if (!empty($arItem['PROPERTIES']['Price']['VALUE'])){
                    $entry_price = number_format($arItem['PROPERTIES']['Price']['VALUE'], 0, ',', ' ').' руб.';
                } elseif (!empty($arItem['PROPERTIES']['PriceFrom']['VALUE']) || !empty($arItem['PROPERTIES']['PriceTo']['VALUE'])){
                    if (!empty($arItem['PROPERTIES']['PriceFrom']['VALUE'])) {
                        $entry_price = 'от '.number_format($arItem['PROPERTIES']['PriceFrom']['VALUE'], 0, ',', ' ').' руб.';
                    }
                    if (!empty($arItem['PROPERTIES']['PriceTo']['VALUE'])){
                        $entry_price .= ' до '.number_format($arItem['PROPERTIES']['PriceTo']['VALUE'], 0, ',', ' ').' руб.';
                    }
                } else {
                    if (!empty($arItem['PROPERTIES']['PriceList']['VALUE'][0])){
                        $entry_price = 'Любая цена';
                    } else {
                        $entry_price = 'Цена не указана';
                    }
            }?>
            <span class="entry_price"><?=$entry_price;?></span>
            <div class="entry_buttons">
                <?/*if ($arItem['PROPERTIES']['CALLC']['VALUE'] && $arItem['PROPERTIES']['Avito']['VALUE'] ):?>
                    <span class="call_center" title="При выгрузке объявления указан номер колл-центра <?=getCallcenterNumber();?>">колл-центр</span>
                <?endif;*/?>
                <?if ($checkAuthor):?>
                    <mark title="Редактировать объявление">
                        <a class="btn_edit" href="/ads/add.php?CODE=<?=$arItem['ID'];?>"></a>
                    </mark>
                    <mark title="Удалить объявление">
                        <a href="#" class="btn_delete del_list">&#215;</a>
                    </mark>
                <?endif;?>
                <?if ($checkAuthor):?>
                    <form action="/ajax/delads.php" method="POST" class="delete_form delete_form_list hidden">
                        <div class="form_row">
                            <input type="hidden" name="CODE" value="<?=$arItem['ID'];?>" />
                        </div> 
                        <div class="input_with_error error">
                            <span class="input_error"></span>
                            <input class="content_input_long" size="25" type="text" name="reason" placeholder="Укажите причину удаления"/>
                            <span class="starrequired">*</span>
                        </div>    
                        <div class="form_row_btn">
                            <input type="submit" value="Удалить"/>
                            <input type="button" value="Отмена" class="cancel_del"/>
                        </div>
                        <span class="success_del"></span>
                    </form>
                <?endif;?>
                <?if (isset($arResult['reverse'][$arItem['ID']]) && !empty($arResult['reverse'][$arItem['ID']])):?>
                    <mark title="Подобрать подходящие объявления">
                        <form action="/ads/<?=$arResult['reverse'][$arItem['ID']]?>/" method="POST">
                            <?foreach ($arResult['iteration'][$arItem['ID']] as $prop => $value):?>
                                <?if (!is_array($value)):?>
                                    <input type="hidden" value="<?=$value;?>" name="iteration[<?=$prop;?>]">
                                <?else:?>
                                    <?foreach ($value as $filt_key => $filt_val):?>
                                        <input type="hidden" value="<?=$filt_val;?>" name="iteration[<?=$prop;?>][<?=$filt_key;?>]">
                                    <?endforeach;?>
                                <?endif;?>
                            <?endforeach;?>
                            <?foreach ($arResult['iter_prop'][$arItem['ID']] as $prop => $value):?>
                                <input type="hidden" value="<?=$value;?>" name="iter_prop[<?=$prop;?>]">
                            <?endforeach;?>
                            <?if (!isset($_SESSION['search_begin'])):?>
                                <input type="hidden" value="<?="http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];?>" name="path_begin">
                            <?endif?>
                            <input class="btn_connections" type="submit">
                        </form>
                    </mark>
                <?endif;?>
            </div>
        
	</div>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>

<?//pre($arResult['NAV_RESULT']['NavRecordCount'], false);?>
