<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule('iblock');


//Список клиентов при добавлении/редактировании объекта
$arOrder = Array("SORT"=>"ASC");
$arFilter = Array("IBLOCK_ID"=>10, "ACTIVE"=>"Y");
$new_array = array();
$new_array["LOGIC"] = "OR";
$new_array[] = array('PROPERTY_RIELTOR'=>$USER->GetID());
$new_array[] = array('PROPERTY_MANAGER'=>$USER->GetID());
$arFilter[] = $new_array;

$list = CIBlockElement::GetList($arOrder, $arFilter, false, false);

$clientsList = array();
while ($ob = $list->GetNextElement()){
    $arFields = $ob->GetFields();
    $clientsList[$arFields['ID']] = $arFields['NAME'];
}
$arResult['PROPERTY_LIST_FULL']['Client']['IENUM'] = $clientsList;


//Список районов при добавлении/редактировании объекта
$arResult['PROPERTY_LIST_FULL']['District']['IENUM'] = getEnumValues(16, array('District'), false);


//Список городов при добавлении/редактировании объекта
$arResult['PROPERTY_LIST_FULL']['City']['IENUM'] = getEnumValues(16, array('City'), false);


//Список статусов при добавлении/редактировании объекта
$arResult['PROPERTY_LIST_FULL']['STATUS']['IENUM'] = getEnumValues(16, array('STATUS'), false);

//Список агентов 
$users = getUsers(array("UF_HEAD"),array("ID",'LAST_NAME','SECOND_NAME','NAME'),array('ACTIVE'=>'Y', "!ID"=>1, "!GROUPS_ID"=>9),"LAST_NAME");
if ($USER->GetID() != 200){
    foreach($users as $userKey => $userAr){
        $groupsUser = CUser::GetUserGroup($userAr['ID']);
        if (in_array(9, $groupsUser)){
            unset($users[$userKey]);
        }
    }
}
$arResult['PROPERTY_LIST_FULL']['WATCHER']['GetPublicEditHTML'] = true;
$arResult['PROPERTY_LIST_FULL']['WATCHER']['IENUM'] = $users;