<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
// pre($arParams);
// pre($arResult);
$arResult['PROPERTY_LIST'][] = $arResult['PROPERTY_LIST'][0];
$arResult['PROPERTY_LIST'][] = $arResult['PROPERTY_LIST'][1];
unset($arResult['PROPERTY_LIST'][0]);
unset($arResult['PROPERTY_LIST'][1]);

?>


<?if (count($arResult["ERRORS"])):?>
	<?=ShowError(implode("<br />", $arResult["ERRORS"]))?>
<?endif?>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
	<?=ShowNote($arResult["MESSAGE"])?>
<?endif?>
<form class="<?=$arParams["FORM_CLASS"]?>" name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">

	<?=bitrix_sessid_post()?>

	<?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>
	
		<?if (is_array($arResult["PROPERTY_LIST"]) && count($arResult["PROPERTY_LIST"] > 0)):?>		
			<?foreach ($arResult["PROPERTY_LIST"] as $propertyCode):?>
				<div class="<?=$arParams["ROW_CLASS"]?> <?=$propertyCode;?>" <?if ($arParams["CUSTOM_CLASS_".$propertyCode]=='hidden'):?>style="display:none"<?endif?>>
                    
                    <label for="i<?=$propertyCode?>">
                        <?if (!in_array($propertyCode,$arResult['IBLOCK_FIELDS'])):?>
                            <?=$arResult["PROPERTY_LIST_FULL"][$propertyCode]["NAME"]?>:
                        <?else:?>
                            <?=!empty($arParams["CUSTOM_TITLE_".$propertyCode]) ? $arParams["CUSTOM_TITLE_".$propertyCode] : GetMessage("IBLOCK_FIELD_".$propertyCode)?>:
                        <?endif?>  
                        <?if(in_array($propertyCode, $arResult["PROPERTY_REQUIRED"]) || in_array($propertyCode, $arParams['PROPERTY_CODES_SOME_REQUIRED'])):?>
                            <span class="starrequired">*</span>
                        <?endif?>  
                    </label>
                    <?
                    //echo "<pre>"; print_r($arResult["PROPERTY_LIST_FULL"]); echo "</pre>";
                    if (!in_array($propertyCode,$arResult['IBLOCK_FIELDS']))
                    {
                        if (
                            $arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] == "T"
                            &&
                            $arResult["PROPERTY_LIST_FULL"][$propertyCode]["ROW_COUNT"] == "1"
                        )
                            $arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] = "S";
                        elseif (
                            (
                                $arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] == "S"
                                ||
                                $arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] == "N"
                            )
                            &&
                            $arResult["PROPERTY_LIST_FULL"][$propertyCode]["ROW_COUNT"] > "1"
                        )
                            $arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] = "T";
                    }
                    elseif (($propertyCode == "TAGS") && CModule::IncludeModule('search'))
                        $arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] = "TAGS";

                    if ($arResult["PROPERTY_LIST_FULL"][$propertyCode]["MULTIPLE"] == "Y")
                    {
                        $inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyCode]) : 0;
                        $inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyCode]["MULTIPLE_CNT"];
                    }
                    else
                    {
                        $inputNum = 1;
                    }

                    if($arResult["PROPERTY_LIST_FULL"][$propertyCode]["GetPublicEditHTML"])
                        $INPUT_TYPE = "USER_TYPE";
                    else
                        $INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"];

                    switch ($INPUT_TYPE):
                        case "USER_TYPE":
                            switch ($propertyCode){
                                case "City":?>
                                    <select class="<?=$arParams["CUSTOM_CLASS_".$propertyCode]?>" id="i<?=$propertyCode?>" name="PROPERTY[<?=$propertyCode?>][0][VALUE]">
                                        <option selected="" value=""><?echo GetMessage("CT_BIEAF_PROPERTY_VALUE_NA")?></option>
                                        <?foreach ($arResult['PROPERTY_LIST_FULL']['City']['IENUM']['City'] as $key => $cityOption):?>
                                            <?php
                                                $checked = false;
                                                if ($arResult['ELEMENT_PROPERTIES']['City'][0]['VALUE'] == $key){
                                                    $checked = true;
                                                }
                                            ?>
                                            <option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$cityOption;?></option>
                                        <?endforeach;?>
                                    </select>
                                <?break;
                                case "District":?>
                                    <select class="<?=$arParams["CUSTOM_CLASS_".$propertyCode]?>" id="i<?=$propertyCode?>" name="PROPERTY[<?=$propertyCode?>][0][VALUE]">
                                        <option selected="" value=""><?echo GetMessage("CT_BIEAF_PROPERTY_VALUE_NA")?></option>
                                        <?foreach ($arResult['PROPERTY_LIST_FULL']['District']['IENUM']['District'] as $key => $districtOption):?>
                                            <?php
                                                $checked = false;
                                                if ($arResult['ELEMENT_PROPERTIES']['District'][0]['VALUE'] == $key){
                                                    $checked = true;
                                                }
                                            ?>
                                            <option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$districtOption;?></option>
                                        <?endforeach;?>
                                    </select>
                                <?break;
                                case "STATUS":?>
                                    <select class="<?=$arParams["CUSTOM_CLASS_".$propertyCode]?>" id="i<?=$propertyCode?>" name="PROPERTY[<?=$propertyCode?>][0][VALUE]">
                                        <option selected="" value=""><?echo GetMessage("CT_BIEAF_PROPERTY_VALUE_NA")?></option>
                                        <?foreach ($arResult['PROPERTY_LIST_FULL']['STATUS']['IENUM']['STATUS'] as $key => $statusOption):?>
                                            <?php
                                                $checked = false;
                                                if ($arResult['ELEMENT_PROPERTIES']['STATUS'][0]['VALUE'] == $key){
                                                    $checked = true;
                                                }
                                            ?>
                                            <option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$statusOption;?></option>
                                        <?endforeach;?>
                                    </select>
                                <?break;
                                case "Client":?>
                                    <select class="<?=$arParams["CUSTOM_CLASS_".$propertyCode]?>" id="i<?=$propertyCode?>" name="PROPERTY[<?=$propertyCode?>][0][VALUE]">
                                        <option selected="" value=""><?echo GetMessage("CT_BIEAF_PROPERTY_VALUE_NA")?></option>
                                        <?foreach ($arResult['PROPERTY_LIST_FULL']['Client']['IENUM'] as $key => $clientOption):?>
                                            <?php
                                                $checked = false;
                                                if ($arResult['ELEMENT_PROPERTIES']['Client'][0]['VALUE'] == $key){
                                                    $checked = true;
                                                }
                                            ?>
                                            <option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$clientOption;?></option>
                                        <?endforeach;?>
                                    </select>
                                <?break;
                                case "WATCHER":?>
                                    <select class="<?=$arParams["CUSTOM_CLASS_".$propertyCode]?>" id="i<?=$propertyCode?>" name="PROPERTY[<?=$propertyCode?>][0][VALUE]">
                                        <option selected="" value=""><?echo GetMessage("CT_BIEAF_PROPERTY_VALUE_NA")?></option>
                                        <?foreach ($arResult['PROPERTY_LIST_FULL']['WATCHER']['IENUM'] as $key => $watchOption):?>
                                            <?php
                                                $checked = false;
                                                if ($arResult['ELEMENT_PROPERTIES']['WATCHER'][0]['VALUE'] == $key){
                                                    $checked = true;
                                                }
                                            ?>
                                           
                                            <option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$watchOption['LAST_NAME'].' '.$watchOption['NAME'].' '.$watchOption['SECOND_NAME'];?></option>
                                       <?endforeach;?>
                                    </select>
                                <?break;
                                default:
                                    for ($i = 0; $i<$inputNum; $i++)
                                    {
                                        if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                        {
                                            $value = !in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyCode];
                                            $description = !in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["DESCRIPTION"] : "";
                                        }
                                        elseif ($i == 0)
                                        {
                                            $value = in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyCode]["DEFAULT_VALUE"];
                                            $description = "";
                                        }
                                        else
                                        {
                                            $value = "";
                                            $description = "";
                                        }
                                        //CIBlockPropertyElementList::GetPropertyFieldHtml()
                                        echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][$propertyCode]["GetPublicEditHTML"],
                                            array(
                                                $arResult["PROPERTY_LIST_FULL"][$propertyCode],
                                                array(
                                                    "VALUE" => $value,
                                                    "DESCRIPTION" => $description,
                                                ),
                                                array(
                                                    "VALUE" => "PROPERTY[".$propertyCode."][".$i."][VALUE]",
                                                    "DESCRIPTION" => "PROPERTY[".$propertyCode."][".$i."][DESCRIPTION]",
                                                    "FORM_NAME"=>"iblock_add",
                                                ),
                                            ));
                                    
                                    }
                            }?>
                        <?break;
                        case "TAGS":
                            $APPLICATION->IncludeComponent(
                                "bitrix:search.tags.input",
                                "",
                                array(
                                    "VALUE" => $arResult["ELEMENT"][$propertyCode],
                                    "NAME" => "PROPERTY[".$propertyCode."][0]",
                                    "TEXT" => "size=\"{$arResult["PROPERTY_LIST_FULL"][$propertyCode]["COL_COUNT"]}\" id=\"i{$propertyCode}\" class=\"{$arParams["CUSTOM_CLASS_".$propertyCode]}\"",
                                ), null, array("HIDE_ICONS"=>"Y")
                            );
                            break;
                        case "HTML":
                            $LHE = new CLightHTMLEditor;
                            $LHE->Show(array(
                                'id' => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[".$propertyCode."][0]"),
                                'width' => '100%',
                                'height' => '200px',
                                'inputName' => "PROPERTY[".$propertyCode."][0]",
                                'content' => $arResult["ELEMENT"][$propertyCode],
                                'bUseFileDialogs' => false,
                                'bFloatingToolbar' => false,
                                'bArisingToolbar' => false,
                                'toolbarConfig' => array(
                                    'Bold', 'Italic', 'Underline', 'RemoveFormat',
                                    'CreateLink', 'DeleteLink', 'Image', 'Video',
                                    'BackColor', 'ForeColor',
                                    'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull',
                                    'InsertOrderedList', 'InsertUnorderedList', 'Outdent', 'Indent',
                                    'StyleList', 'HeaderList',
                                    'FontList', 'FontSizeList',
                                ),
                            ));
                            break; 
                        case "T":
                            for ($i = 0; $i<$inputNum; $i++)
                            {

                                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                {
                                    $value = !in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE"] : $arResult["ELEMENT"][$propertyCode];
                                }
                                elseif ($i == 0)
                                {
                                    $value = !in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyCode]["DEFAULT_VALUE"];
                                }
                                else
                                {
                                    $value = "";
                                }
                            ?>
                                <textarea class="<?=$arParams["CUSTOM_CLASS_".$propertyCode]?>" id="i<?=$propertyCode?>" cols="<?=$arResult["PROPERTY_LIST_FULL"][$propertyCode]["COL_COUNT"]?>" rows="<?=$arResult["PROPERTY_LIST_FULL"][$propertyCode]["ROW_COUNT"]?>" name="PROPERTY[<?=$propertyCode?>][<?=$i?>]"><?=$value?></textarea>
                            <?
                            }
                        break;

                        case "S":
                        case "N":
                            for ($i = 0; $i<$inputNum; $i++)
                            {
                                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                {
                                    $value = !in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE"] : $arResult["ELEMENT"][$propertyCode];
                                }
                                elseif ($i == 0)
                                {
                                    $value = in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyCode]["DEFAULT_VALUE"];

                                }
                                else
                                {
                                    $value = "";
                                }
                            ?>
                                <input class="<?=$arParams["CUSTOM_CLASS_".$propertyCode]?>" id="i<?=$propertyCode?>" type="text" name="PROPERTY[<?=$propertyCode?>][<?=$i?>]" size="25" value="<?=$value?>" /><?
                                if($arResult["PROPERTY_LIST_FULL"][$propertyCode]["USER_TYPE"] == "DateTime"):?><?
                                    $APPLICATION->IncludeComponent(
                                        'bitrix:main.calendar',
                                        '',
                                        array(
                                            'FORM_NAME' => 'iblock_add',
                                            'INPUT_NAME' => "PROPERTY[".$propertyCode."][".$i."]",
                                            'INPUT_VALUE' => $value,
                                        ),
                                        null,
                                        array('HIDE_ICONS' => 'Y')
                                    );
                                    ?><br /><small><?=GetMessage("IBLOCK_FORM_DATE_FORMAT")?><?=FORMAT_DATETIME?></small><?
                                endif
                                ?><?
                            }
                        break;

                        case "F":
                            for ($i = 0; $i<$inputNum; $i++)
                            {
                                $value = !in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE"] : $arResult["ELEMENT"][$propertyCode];
                                ?>
                                <input type="hidden" name="PROPERTY[<?=$propertyCode?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE_ID"] : $i?>]" value="<?=$value?>" />
                                <input type="file" size="<?=$arResult["PROPERTY_LIST_FULL"][$propertyCode]["COL_COUNT"]?>"  name="PROPERTY_FILE_<?=$propertyCode?>_<?=$arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE_ID"] : $i?>" /><br />
                                <?

                                if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value]))
                                {
                                    ?>
                                    <input type="checkbox" name="DELETE_FILE[<?=$propertyCode?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE_ID"] : $i?>]" id="file_delete_<?=$propertyCode?>_<?=$i?>" value="Y" /><label for="file_delete_<?=$propertyCode?>_<?=$i?>"><?=GetMessage("IBLOCK_FORM_FILE_DELETE")?></label><br />
                                    <?

                                    if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"])
                                    {
                                        ?>
                                        <img src="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>" height="<?=$arResult["ELEMENT_FILES"][$value]["HEIGHT"]?>" width="<?=$arResult["ELEMENT_FILES"][$value]["WIDTH"]?>" border="0" /><br />
                                        <?
                                    }
                                    else
                                    {
                                        ?>
                                        <?=GetMessage("IBLOCK_FORM_FILE_NAME")?>: <?=$arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"]?><br />
                                        <?=GetMessage("IBLOCK_FORM_FILE_SIZE")?>: <?=$arResult["ELEMENT_FILES"][$value]["FILE_SIZE"]?> b<br />
                                        [<a href="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>"><?=GetMessage("IBLOCK_FORM_FILE_DOWNLOAD")?></a>]<br />
                                        <?
                                    }
                                }
                            }

                        break;
                        case "L":

                            if ($arResult["PROPERTY_LIST_FULL"][$propertyCode]["LIST_TYPE"] == "C")
                                $type = $arResult["PROPERTY_LIST_FULL"][$propertyCode]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
                            else
                                $type = $arResult["PROPERTY_LIST_FULL"][$propertyCode]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";

                            switch ($type):
                                case "checkbox":
                                case "radio":

                                    //echo "<pre>"; print_r($arResult["PROPERTY_LIST_FULL"][$propertyCode]); echo "</pre>";

                                    foreach ($arResult["PROPERTY_LIST_FULL"][$propertyCode]["ENUM"] as $key => $arEnum)
                                    {
                                        $checked = false;
                                        if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                        {
                                            if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyCode]))
                                            {
                                                foreach ($arResult["ELEMENT_PROPERTIES"][$propertyCode] as $arElEnum)
                                                {
                                                    if ($arElEnum["VALUE"] == $key) {$checked = true; break;}
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if ($arEnum["DEF"] == "Y") $checked = true;
                                        }

                                        ?>
                                        <input class="<?=$arParams["CUSTOM_CLASS_".$propertyCode]?>" type="<?=$type?>" name="PROPERTY[<?=$propertyCode?>]<?=$type == "checkbox" ? "[".$key."]" : ""?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> /><label for="property_<?=$key?>"><?=$arEnum["VALUE"]?></label><br />
                                        <?
                                    }
                                break;

                                case "dropdown":
                                case "multiselect":
                                ?>
                                    <select class="<?=$arParams["CUSTOM_CLASS_".$propertyCode]?>" id="i<?=$propertyCode?>" name="PROPERTY[<?=$propertyCode?>]<?=$type=="multiselect" ? "[]\" size=\"".$arResult["PROPERTY_LIST_FULL"][$propertyCode]["ROW_COUNT"]."\" multiple=\"multiple" : ""?>">
                                        <option selected="" value="0"><?echo GetMessage("CT_BIEAF_PROPERTY_VALUE_NA")?></option>
                                        <?
                                        if (!in_array($propertyCode,$arResult['IBLOCK_FIELDS'])) $sKey = "ELEMENT_PROPERTIES";
                                        else $sKey = "ELEMENT";

                                        foreach ($arResult["PROPERTY_LIST_FULL"][$propertyCode]["ENUM"] as $key => $arEnum)
                                        {
                                            $checked = false;
                                            if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                            {
                                                foreach ($arResult[$sKey][$propertyCode] as $elKey => $arElEnum)
                                                {
                                                    if ($key == $arElEnum["VALUE"]) {$checked = true; break;}
                                                }
                                            }
                                            else
                                            {
                                                if ($arEnum["DEF"] == "Y") $checked = true;
                                            }
                                            ?>
                                            <option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option>
                                            <?
                                        }
                                        ?>
                                    </select>
                                <?
                                break;

                            endswitch;
                        break;
                    endswitch;?>
                </div>
			<?endforeach;?>
			<?if($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0):?>
				<div class="<?=$arParams["ROW_CLASS"]?>">
                    <?//=GetMessage("IBLOCK_FORM_CAPTCHA_TITLE")?>
					<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
					<label for="captcha"><?=GetMessage("IBLOCK_FORM_CAPTCHA_PROMPT")?><span class="starrequired">*</span></label>
					<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
					<input id="captcha" type="text" name="captcha_word" maxlength="50" value=""/>
				</div>
			<?endif?>		
		<?endif?>
		<div class="<?=$arParams["ROW_CLASS"]?>">
			<input class="<?=$arParams["SUBMIT_CLASS"]?>" type="submit" name="iblock_submit" value="<?=GetMessage("IBLOCK_FORM_SUBMIT")?>" />
			<?if (strlen($arParams["LIST_URL"]) > 0 && $arParams["ID"] > 0):?>
                <input class="<?=$arParams["SUBMIT_CLASS"]?>" type="submit" name="iblock_apply" value="<?=GetMessage("IBLOCK_FORM_APPLY")?>" />
            <?endif?>
			<?/*<input type="reset" value="<?=GetMessage("IBLOCK_FORM_RESET")?>" />*/?>
        </div>	
	<?if (strlen($arParams["LIST_URL"]) > 0):?>
        <a href="<?=$arParams["LIST_URL"]?>"><?=GetMessage("IBLOCK_FORM_BACK")?></a>
    <?endif?>
</form>
<script>
    $('.NAME').hide();
    $('.CURRENT_DATE_OPTION input').attr('autocomplete', 'off');
    $('.FIRST_CONTACT input').attr('autocomplete', 'off');
    $('.CURRENT_DATE_OPTION input').css('pointer-events', 'none');
    $('.FIRST_CONTACT input').css('pointer-events', 'none');
    
    
    $('form[name=iblock_add]').submit(function(){
        $('input#iNAME').val('No name');
    });    
    
    $('#iSTATUS').change(function(){
    
        <?if (!isset($_REQUEST['CODE']) || empty($_REQUEST['CODE'])):?>
            $('.CURRENT_DATE_OPTION input').val("");
            $('.CURRENT_COMMENT_OPTION textarea').val("");
            //$('.WATCHER select option:nth-child(1)').attr("selected", "selected");
            if (
                    $(this).val() != 8977 &&
                    $(this).val() != 8978 &&
                    $(this).val() != 8979 &&
                    $(this).val() != 11106 &&
                    $(this).val() != 11377 
                ){
                $('.CURRENT_DATE_OPTION').show();
                $('.CURRENT_COMMENT_OPTION').hide();
            } else {
                $('.CURRENT_DATE_OPTION').hide();
                $('.CURRENT_COMMENT_OPTION').show();
            }
            if ($(this).val() == ""){
                $('.CURRENT_DATE_OPTION').hide();
                $('.CURRENT_COMMENT_OPTION').hide();
            }
        <?else:?>
            if (
                $(this).val() == 8977 ||
                $(this).val() == 8978 ||
                $(this).val() == 8979 ||
                $(this).val() == 11106 ||
                $(this).val() == 11377 
            ){
                $('.CURRENT_DATE_OPTION').hide();
                $('.CURRENT_COMMENT_OPTION').show();
            } else {
                $('.CURRENT_DATE_OPTION').show();
                $('.CURRENT_COMMENT_OPTION').hide();
            }
        <?endif;?>
        $('.WATCHER').show();
        $('.OPEN_DATA').show();

    });
    
    <?if (!isset($_REQUEST['CODE']) || empty($_REQUEST['CODE'])):?>
        $('input[name*=FIRST_CONTACT]').val('<?=date("d.m.Y H:i:s");?>');
        
        // $('.CURRENT_DATE_OPTION').hide();
        <?if (empty($arResult['ELEMENT_PROPERTIES']['STATUS'][0]['VALUE'])):?>
            $('.CURRENT_DATE_OPTION').hide();
        <?endif;?>
        <?if (
                $arResult['ELEMENT_PROPERTIES']['STATUS'][0]['VALUE'] == 8977 ||
                $arResult['ELEMENT_PROPERTIES']['STATUS'][0]['VALUE'] == 8978 ||
                $arResult['ELEMENT_PROPERTIES']['STATUS'][0]['VALUE'] == 8979 ||
                $arResult['ELEMENT_PROPERTIES']['STATUS'][0]['VALUE'] == 11106 ||
                $arResult['ELEMENT_PROPERTIES']['STATUS'][0]['VALUE'] == 11377
                ):?>
            $('.CURRENT_COMMENT_OPTION').show();
            $('.CURRENT_DATE_OPTION').hide();
        <?else:?>
            $('.CURRENT_COMMENT_OPTION').hide();
            // $('.CURRENT_DATE_OPTION').show();
        <?endif;?>

        $('.WATCHER').hide();
        $('.OPEN_DATA').hide();
        <?if (!empty($arResult['ELEMENT_PROPERTIES']['STATUS'][0]['VALUE'])):?>
            $('.WATCHER').show();
            $('.OPEN_DATA').show();
        <?endif;?>
    <?else:?>
        <?if (  $arResult['ELEMENT_PROPERTIES']['STATUS'][0]['VALUE'] != 8977 &&
                $arResult['ELEMENT_PROPERTIES']['STATUS'][0]['VALUE'] != 8978 &&
                $arResult['ELEMENT_PROPERTIES']['STATUS'][0]['VALUE'] != 8979 &&
                $arResult['ELEMENT_PROPERTIES']['STATUS'][0]['VALUE'] != 11106 &&
                $arResult['ELEMENT_PROPERTIES']['STATUS'][0]['VALUE'] != 11377 
                ):?>
            $('.CURRENT_COMMENT_OPTION').hide();
            
        <?else:?>
            $('.CURRENT_DATE_OPTION').hide();
        <?endif;?>
    <?endif;?>
    
    <?if (isset($_REQUEST['client']) && !empty($_REQUEST['client'])):?>
        $('#iClient').val(<?=$_REQUEST['client'];?>);
    <?endif;?>
    
    if ($('#iCity').val() != 49){
        $('.District').hide();
    }
    $('#iCity').change(function(){
        if ($(this).val() != 49){
            $('.District').hide();
            $('.District select option:nth-child(1)').attr("selected", "selected");
        } else {
            $('.District').show();
        }
    });

</script>

<?//pre($arParams['PROPERTY_CODES_SOME_REQUIRED'], false);?>