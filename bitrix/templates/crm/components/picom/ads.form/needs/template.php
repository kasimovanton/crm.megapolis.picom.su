<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
// pre($arResult["ELEMENT"]);
// pre($arResult["SECTION_TREE"][0]['SECTIONS']);

?>
<?if (isset($_REQUEST['client_id']) && !empty($_REQUEST['client_id']) ||
        (isset($_REQUEST['CODE'])  && !empty($_REQUEST['CODE']))):?>
    <?$userStatus = getUserStatus($USER->GetID());?>
    <?if (count($arResult["ERRORS"])):?>
        <?=ShowError(implode("<br />", $arResult["ERRORS"]))?>
    <?endif?>

    <?if (strlen($arResult["MESSAGE"]) > 0):?>
        <?=ShowNote($arResult["MESSAGE"])?>
    <?endif?>

    <?//POST_FORM_ACTION_URI?>
    <form class="<?=$arParams["FORM_CLASS"]?>" name="iblock_add" id="iblock_add" action="<?=$APPLICATION->GetCurPageParam("",array("strIMessage"))?>" method="post" enctype="multipart/form-data">
        <?=bitrix_sessid_post()?>
        <input name="PROPERTY[Client][0][VALUE]" type="hidden" value="<?=$_REQUEST['client_id'];?>">
        <?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>
            <input type="hidden" name="PROPERTY[IBLOCK_SECTION]" id="iIBLOCK_SECTION_NEED" val="">
            <div class="<?=$arParams["ROW_CLASS"]?>">
                <label for="first_section">Категория</label>
                <select name="first_section" id="first_section">
                    <option selected="" disabled="" class="hidden"><?echo GetMessage("CT_BIEAF_PROPERTY_VALUE_NA")?></option>
                    <?foreach ($arResult["SECTION_TREE"][0]['SECTIONS'] as $val):?>
                        <?
                        $checked = false;
                        $key = $val;
                        $value = $arResult["SECTION_LIST"][$val]['VALUE'];
                        ?>
                        <?if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {

                            if (in_array($arResult["ELEMENT"]['IBLOCK_SECTION'][0]['VALUE'],$arResult["SECTION_TREE"][$val]['SECTIONS'])) {
                                /*echo $arResult["ELEMENT"]['IBLOCK_SECTION'][0]['VALUE'];
                                echo '-'.$val;
                                pre($arResult["SECTION_TREE"][$val]['SECTIONS']);*/
                                $checked = true;
                            }
                        }?>
                        <option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$value?></option>
                    <?endforeach?>
                </select>
            </div>

            <?if (is_array($arResult["PROPERTY_LIST"]) && count($arResult["PROPERTY_LIST"] > 0)):?>		
                <?foreach ($arResult["PROPERTY_LIST"] as $propertyCode):?>
                    <? switch ($propertyCode):
                    case 'Images': ?>
                    <div class="special special_<?=$propertyCode?> <?=$arParams["ROW_CLASS"]?><?if ($arParams["CUSTOM_CLASS_".$propertyCode]=='hidden'):?> hidden<?endif?>">
                        <label class="extra_photo_span">Дополнительные фото:</label>
                        <input id="download_images_button" class="button download_btn" onClick="downloadButton()" type="button" value="загрузить">
                        <input accept="image/*" id="imgInp0" multiple name="newimages[]"  onchange="loadimage(this)"   type="file" style="display:none">
                    </div>

                    <div class="special special_<?=$propertyCode?> <?=$arParams["ROW_CLASS"]?><?if ($arParams["CUSTOM_CLASS_".$propertyCode]=='hidden'):?> hidden<?endif?>">
                        <ul class="extra_photo">
                            <?foreach($arResult['ELEMENT_PROPERTIES']['Images'] as $image):?>
                                <?if($image['VALUE']):?>
                                    <?$file = CFile::GetFileArray($image['VALUE']);?>
                                    <?$fileResized =  CFile::ResizeImageGet($image['VALUE'], array('width'=>100, 'height'=>75), BX_RESIZE_IMAGE_PROPORTIONAL, true);  ?>
                                    <li class="extra_photo_thumb">
                                        <a href="#"><image src="<?=$fileResized['src']?>"></a>
                                        <input accept="image/*" onchange="multiplePhotoReadURL(this)" style="display:none"  type="file">
                                        <div class="extra_photo_name"><?=$file['ORIGINAL_NAME']?></div>
                                        <a href="#rem" class="photo_remove" onclick="removeImg($(this),<?=$image['VALUE_ID']?>);" title="removes photo"></a>				
                                    </li>			
                                <?endif?>
                            <?endforeach?>
                            <?foreach($arResult['ELEMENT_PROPERTIES']['preAddImages'] as $image):?>
                                <?if($image['VALUE']):?>
                                    <?$file = CFile::GetFileArray($image['VALUE']);?>
                                    <?$fileResized =  CFile::ResizeImageGet($image['VALUE'], array('width'=>100, 'height'=>75), BX_RESIZE_IMAGE_PROPORTIONAL, true);  ?>
                                    <li class="extra_photo_thumb">
                                        <a href="#"><image src="<?=$fileResized['src']?>"></a>
                                        <input accept="image/*" onchange="multiplePhotoReadURL(this)" style="display:none"  type="file">
                                        <div class="extra_photo_name"><?=$file['ORIGINAL_NAME']?></div>
                                        <a href="#rem" class="photo_remove" onclick="removeImg($(this),<?=$image['VALUE']?>);" title="removes photo"></a>
                                        <input type="hidden" name="addedPhoto[]" value="<?=$image['VALUE']?>">									
                                    </li>			
                                <?endif?>
                            <?endforeach?>						
                            
                        </ul>
                    </div>
                    <?foreach($arResult['ELEMENT_PROPERTIES']['Images'] as $image):?>
                        <input type="hidden" name="PROPERTY[Images][<?=$image['VALUE_ID']?>]" value="<?=$image['VALUE']?>">
                        <input type="hidden" id="deleteImage<?=$image['VALUE_ID']?>" name="DELETE_FILE[Images][<?=$image['VALUE_ID']?>]"  value="N">
                    <?endforeach?>
                    <? break; ?>
                    <? case 'DETAIL_PICTURE': ?>
                    <div class="special special_<?=$propertyCode?> <?=$arParams["ROW_CLASS"]?><?if ($arParams["CUSTOM_CLASS_".$propertyCode]=='hidden'):?> hidden<?endif?>">
                        <label class="main_photo_span">Основное фото:</label>
                        <input class="button download_btn" onClick="mainPhotoButton();" id="main_photo_button" type="button" value="загрузить">
                    </div>
                    
                    <input type="file" size="" id="main_photo_input" onChange="loadimageMain(this);" style="display:none" name="PROPERTY_FILE_DETAIL_PICTURE_0" >
                    
                    <div class="special special_<?=$propertyCode?> <?=$arParams["ROW_CLASS"]?><?if ($arParams["CUSTOM_CLASS_".$propertyCode]=='hidden'):?> hidden<?endif?>">
                        <div class="main_photo">
                            <div class="main_photo_thumb">
                                <?if($arResult['ELEMENT']['DETAIL_PICTURE']):?>
                                    <?$fileResized =  CFile::ResizeImageGet($arResult['ELEMENT']['DETAIL_PICTURE'], array('width'=>150, 'height'=>110), BX_RESIZE_IMAGE_PROPORTIONAL, true);  ?>
                                    <?$file = CFile::GetFileArray($arResult['ELEMENT']['DETAIL_PICTURE']);?>
                                    <img src="<?=$fileResized['src']?>" alt="">
                                    <a class="photo_remove" onClick="mainPhotoRemove()" href="#rem" title="removes photo"></a>
                                    <div class="main_photo_name"><?=$file['ORIGINAL_NAME']?></div>
                                    <input type="hidden" name="mainPhotoId" value="<?=$arResult['ELEMENT']['DETAIL_PICTURE']?>">	
                                <?endif?>
                            </div>
                        </div>
                        
                        <?if($arResult['ELEMENT']['DETAIL_PICTURE']):?>					
                        <input type="hidden" name="PROPERTY[DETAIL_PICTURE][0]" value="<?=$arResult['ELEMENT']['DETAIL_PICTURE']?>">
                        <input type="hidden" name="DELETE_FILE[DETAIL_PICTURE][0]" id="file_delete_DETAIL_PICTURE_0" value="N">
                        <?endif?>
                    </div>		
        
                    <? break; ?>
                    <? default: ?>
                    <?if($propertyCode == 'IBLOCK_SECTION'):?>
                        <?continue;?>
                    <?endif;?>
                    <div class="special special_<?=$propertyCode?> <?=$arParams["ROW_CLASS"]?><?if ($arParams["CUSTOM_CLASS_".$propertyCode]=='hidden'):?> hidden<?endif?>" >
                        <label for="i<?=$propertyCode?>">
                            <?if (!in_array($propertyCode,$arResult['IBLOCK_FIELDS'])):?>
                                <?=$arResult["PROPERTY_LIST_FULL"][$propertyCode]["NAME"]?>:
                            <?else:?>
                                <?=!empty($arParams["CUSTOM_TITLE_".$propertyCode]) ? $arParams["CUSTOM_TITLE_".$propertyCode] : GetMessage("IBLOCK_FIELD_".$propertyCode)?>:
                            <?endif?>     
                            <?if(in_array($propertyCode, $arResult["PROPERTY_REQUIRED"])):?>
                                <span class="starrequired">*</span>
                            <?endif?>                        
                        </label>
                        <?
                        if (!in_array($propertyCode,$arResult['IBLOCK_FIELDS']))
                        {
                            if (
                                $arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] == "T"
                                &&
                                $arResult["PROPERTY_LIST_FULL"][$propertyCode]["ROW_COUNT"] == "1"
                            )
                                $arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] = "S";
                            elseif (
                                (
                                    $arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] == "S"
                                    ||
                                    $arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] == "N"
                                )
                                &&
                                $arResult["PROPERTY_LIST_FULL"][$propertyCode]["ROW_COUNT"] > "1"
                            )
                                $arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] = "T";
                        }
                        elseif (($propertyCode == "TAGS") && CModule::IncludeModule('search'))
                            $arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] = "TAGS";

                        if ($arResult["PROPERTY_LIST_FULL"][$propertyCode]["MULTIPLE"] == "Y")
                        {
                            $inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyCode]) : 0;
                            $inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyCode]["MULTIPLE_CNT"];
                        }
                        else
                        {
                            $inputNum = 1;
                        }

                        if($arResult["PROPERTY_LIST_FULL"][$propertyCode]["GetPublicEditHTML"])
                            $INPUT_TYPE = "USER_TYPE";
                        else
                            $INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"];

                        switch ($INPUT_TYPE):
                            case "USER_TYPE":
                                for ($i = 0; $i<$inputNum; $i++)
                                {
                                    if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                    {
                                        $value = !in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyCode];
                                        $description = !in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["DESCRIPTION"] : "";
                                    }
                                    elseif ($i == 0)
                                    {
                                        $value = in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyCode]["DEFAULT_VALUE"];
                                        $description = "";
                                    }
                                    else
                                    {
                                        $value = "";
                                        $description = "";
                                    }
                                    //CIBlockPropertyElementList::GetPropertyFieldHtml()
                                    echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][$propertyCode]["GetPublicEditHTML"],
                                        array(
                                            $arResult["PROPERTY_LIST_FULL"][$propertyCode],
                                            array(
                                                "VALUE" => $value,
                                                "DESCRIPTION" => $description,
                                            ),
                                            array(
                                                "VALUE" => "PROPERTY[".$propertyCode."][".$i."][VALUE]",
                                                "DESCRIPTION" => "PROPERTY[".$propertyCode."][".$i."][DESCRIPTION]",
                                                "FORM_NAME"=>"iblock_add",
                                            ),
                                        ));
                                ?><?
                                }
                            break;
                            case "TAGS":
                                $APPLICATION->IncludeComponent(
                                    "bitrix:search.tags.input",
                                    "",
                                    array(
                                        "VALUE" => $arResult["ELEMENT"][$propertyCode],
                                        "NAME" => "PROPERTY[".$propertyCode."][0]",
                                        "TEXT" => "size=\"{$arResult["PROPERTY_LIST_FULL"][$propertyCode]["COL_COUNT"]}\" id=\"i{$propertyCode}\" class=\"{$arParams["CUSTOM_CLASS_".$propertyCode]}\"",
                                    ), null, array("HIDE_ICONS"=>"Y")
                                );
                                break;
                            case "HTML":
                                $LHE = new CLightHTMLEditor;
                                $LHE->Show(array(
                                    'id' => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[".$propertyCode."][0]"),
                                    'width' => '100%',
                                    'height' => '200px',
                                    'inputName' => "PROPERTY[".$propertyCode."][0]",
                                    'content' => $arResult["ELEMENT"][$propertyCode],
                                    'bUseFileDialogs' => false,
                                    'bFloatingToolbar' => false,
                                    'bArisingToolbar' => false,
                                    'toolbarConfig' => array(
                                        'Bold', 'Italic', 'Underline', 'RemoveFormat',
                                        'CreateLink', 'DeleteLink', 'Image', 'Video',
                                        'BackColor', 'ForeColor',
                                        'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull',
                                        'InsertOrderedList', 'InsertUnorderedList', 'Outdent', 'Indent',
                                        'StyleList', 'HeaderList',
                                        'FontList', 'FontSizeList',
                                    ),
                                ));
                                break;
                            case "T":
                                for ($i = 0; $i<$inputNum; $i++)
                                {

                                    if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                    {
                                        $value = !in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE"] : $arResult["ELEMENT"][$propertyCode];
                                    }
                                    elseif ($i == 0)
                                    {
                                        $value = !in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyCode]["DEFAULT_VALUE"];
                                    }
                                    else
                                    {
                                        $value = "";
                                    }
                                ?>
                                    <textarea class="<?=$arParams["CUSTOM_CLASS_".$propertyCode]?>" id="i<?=$propertyCode?>" cols="<?=$arResult["PROPERTY_LIST_FULL"][$propertyCode]["COL_COUNT"]?>" rows="<?=$arResult["PROPERTY_LIST_FULL"][$propertyCode]["ROW_COUNT"]?>" name="PROPERTY[<?=$propertyCode?>][<?=$i?>]"><?=$value?></textarea>
                                <?
                                }
                            break;

                            case "S":
                            case "N":
                                for ($i = 0; $i<$inputNum; $i++)
                                {
                                    if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                    {
                                        $value = !in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE"] : $arResult["ELEMENT"][$propertyCode];
                                    }
                                    elseif ($i == 0)
                                    {
                                        //$value = in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyCode]["DEFAULT_VALUE"];
                                        $value = $arResult["PROPERTY_LIST_FULL"][$propertyCode]["DEFAULT_VALUE"];
                                    }
                                    else
                                    {
                                        $value = "";
                                    }
                                ?>
                                    <input class="<?=$arParams["CUSTOM_CLASS_".$propertyCode]?>" id="i<?=$propertyCode?>" type="text" name="PROPERTY[<?=$propertyCode?>][<?=$i?>]" size="25" value="<?=$value?>" /><?
                                    if($arResult["PROPERTY_LIST_FULL"][$propertyCode]["USER_TYPE"] == "DateTime"):?><?
                                        $APPLICATION->IncludeComponent(
                                            'bitrix:main.calendar',
                                            '',
                                            array(
                                                'FORM_NAME' => 'iblock_add',
                                                'INPUT_NAME' => "PROPERTY[".$propertyCode."][".$i."]",
                                                'INPUT_VALUE' => $value,
                                            ),
                                            null,
                                            array('HIDE_ICONS' => 'Y')
                                        );
                                        ?><br /><small><?=GetMessage("IBLOCK_FORM_DATE_FORMAT")?><?=FORMAT_DATETIME?></small><?
                                    endif
                                    ?><?
                                }
                            break;

                            case "F":
                                for ($i = 0; $i<$inputNum; $i++)
                                {
                                    $value = !in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE"] : $arResult["ELEMENT"][$propertyCode];
                                    ?>
                                    <input type="hidden" name="PROPERTY[<?=$propertyCode?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE_ID"] : $i?>]" value="<?=$value?>" />
                                    <input type="file" size="<?=$arResult["PROPERTY_LIST_FULL"][$propertyCode]["COL_COUNT"]?>"  name="PROPERTY_FILE_<?=$propertyCode?>_<?=$arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE_ID"] : $i?>" /><br />
                                    <?

                                    if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value]))
                                    {
                                        ?>
                                        <input type="checkbox" name="DELETE_FILE[<?=$propertyCode?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE_ID"] : $i?>]" id="file_delete_<?=$propertyCode?>_<?=$i?>" value="Y" /><label for="file_delete_<?=$propertyCode?>_<?=$i?>"><?=GetMessage("IBLOCK_FORM_FILE_DELETE")?></label><br />
                                        <?

                                        if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"])
                                        {
                                            ?>
                                            <img src="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>" height="75px" width="100px" border="0" /><br/>
                                            <?
                                        }
                                        else
                                        {
                                            ?>
                                            <?=GetMessage("IBLOCK_FORM_FILE_NAME")?>: <?=$arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"]?><br />
                                            <?=GetMessage("IBLOCK_FORM_FILE_SIZE")?>: <?=$arResult["ELEMENT_FILES"][$value]["FILE_SIZE"]?> b<br />
                                            [<a href="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>"><?=GetMessage("IBLOCK_FORM_FILE_DOWNLOAD")?></a>]<br />
                                            <?
                                        }
                                    }
                                }

                            break;
                            case "L":

                                if ($arResult["PROPERTY_LIST_FULL"][$propertyCode]["LIST_TYPE"] == "C")
                                    $type = $arResult["PROPERTY_LIST_FULL"][$propertyCode]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
                                else
                                    $type = $arResult["PROPERTY_LIST_FULL"][$propertyCode]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";

                                switch ($type):
                                    case "checkbox":
                                    case "radio":?>
                                        <div class="inrow_block">
                                            <div class="label_wrap">
                                        <?//echo "<pre>"; print_r($arResult["PROPERTY_LIST_FULL"][$propertyCode]); echo "</pre>";

                                        foreach ($arResult["PROPERTY_LIST_FULL"][$propertyCode]["ENUM"] as $key => $arEnum)
                                        {
                                            $checked = false;
                                            if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                            {
                                                if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyCode]))
                                                {
                                                    foreach ($arResult["ELEMENT_PROPERTIES"][$propertyCode] as $arElEnum)
                                                    {
                                                        if ($arElEnum["VALUE"] == $key) {$checked = true; break;}
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if ($arEnum["DEF"] == "Y") $checked = true;
                                            }

                                            ?>
                                            <?/*if($propertyCode == "CALLC"):?>
                                                <?$key=513;?>
                                                <input class="<?=$arParams["CUSTOM_CLASS_".$propertyCode]?>" type="<?=$type?>" name="PROPERTY[<?=$propertyCode?>]<?=$type == "checkbox" ? "[".$key."]" : ""?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> />
                                                <label class="inrow_label" for="property_<?=$key?>"><?=$arEnum["VALUE"]?></label>
                                            <?else:*/?>
                                                <input class="<?=$arParams["CUSTOM_CLASS_".$propertyCode]?>" type="<?=$type?>" name="PROPERTY[<?=$propertyCode?>]<?=$type == "checkbox" ? "[".$key."]" : ""?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> />
                                                <label class="inrow_label" for="property_<?=$key?>"><?=$arEnum["VALUE"]?></label>
                                            <?//endif;?>
                                            <?
                                        }?>
                                            </div>
                                        </div>
                                            <?
                                    break;

                                    case "dropdown":
                                    case "multiselect":
                                    ?>
                                        <select class="<?=$arParams["CUSTOM_CLASS_".$propertyCode]?>" id="i<?=$propertyCode?>" name="PROPERTY[<?=$propertyCode?>]<?=$type=="multiselect" ? "[]\" size=\"".$arResult["PROPERTY_LIST_FULL"][$propertyCode]["ROW_COUNT"]."\" multiple=\"multiple" : ""?>">
                                            <option selected="" value="0"><?echo GetMessage("CT_BIEAF_PROPERTY_VALUE_NA")?></option>
                                            <?
                                            if (!in_array($propertyCode,$arResult['IBLOCK_FIELDS'])) $sKey = "ELEMENT_PROPERTIES";
                                            else $sKey = "ELEMENT";

                                            foreach ($arResult["PROPERTY_LIST_FULL"][$propertyCode]["ENUM"] as $key => $arEnum)
                                            {
                                                $checked = false;
                                                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                                {
                                                    foreach ($arResult[$sKey][$propertyCode] as $elKey => $arElEnum)
                                                    {
                                                        if ($key == $arElEnum["VALUE"]) {$checked = true; break;}
                                                    }
                                                }
                                                else
                                                {
                                                    if ($arEnum["DEF"] == "Y") $checked = true;
                                                }
                                                ?>
                                                <option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option>
                                                <?
                                            }
                                            ?>
                                        </select>
                                    <?
                                    break;

                                endswitch;
                            break;
                        endswitch;?>
                    </div>
                    <? endswitch; ?>				
                <?endforeach;?>
                <?if($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0):?>
                    <div class="<?=$arParams["ROW_CLASS"]?>">
                        <?//=GetMessage("IBLOCK_FORM_CAPTCHA_TITLE")?>
                        <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                        <label for="captcha"><?=GetMessage("IBLOCK_FORM_CAPTCHA_PROMPT")?><span class="starrequired">*</span>:</label>
                        <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                        <input id="captcha" type="text" name="captcha_word" maxlength="50" value=""/>
                    </div>
                <?endif?>		
            <?endif?>
            <div class="<?=$arParams["ROW_CLASS"]?>">
                <input style="display:none;" class="<?=$arParams["SUBMIT_CLASS"]?>" type="submit" name="iblock_submit" value="<?=GetMessage("IBLOCK_FORM_SUBMIT")?>" />
                <?if (strlen($arParams["LIST_URL"]) > 0 && $arParams["ID"] > 0):?>
                    <input class="<?=$arParams["SUBMIT_CLASS"]?>" type="submit" name="iblock_apply" value="<?=GetMessage("IBLOCK_FORM_APPLY")?>" />
                <?endif?>
                <?/*<input type="reset" value="<?=GetMessage("IBLOCK_FORM_RESET")?>" />*/?>
            </div>	
        <?if (strlen($arParams["LIST_URL"]) > 0):?>
            <a href="<?=$arParams["LIST_URL"]?>"><?=GetMessage("IBLOCK_FORM_BACK")?></a>
        <?endif?>
        <?if (in_array(8, $userStatus)):?>
           <?/* <input type="hidden" name="PROPERTY[FORUM_TOPIC_ID]" value="1">*/?>
            <input type="hidden" name="PROPERTY[IS_NEED][564]" value="564">
        <?endif;?>
        
    </form>

    <script>
        var props = JSON.parse('<?=json_encode($arResult['PROPERTY_SECTION_LIST'])?>');
        var section_tree = JSON.parse('<?=json_encode($arResult["SECTION_TREE"])?>');
        var sections = JSON.parse('<?=json_encode($arResult["SECTION_LIST"])?>');
        var curSection = <?=$arResult["ELEMENT"]['IBLOCK_SECTION'][0]['VALUE']?$arResult["ELEMENT"]['IBLOCK_SECTION'][0]['VALUE']:0?>

        
        
        function loadCommerce() {
            var curCommerce = $('#iObjectTypeCommerce option:selected').text();
            if (curCommerce =='Офисное помещение' || curCommerce == 'Складское помещение') {
                $('.special_BuildingClass').show();
            } else {
                $('.special_BuildingClass').hide();
                $('#iBuildingClass').val(0)
            }
        }

        function loadDistrict(distID){
            $.ajax({
                cache: false,
                dataType: 'json',
                data: {'ajax':'y','district': distID},
                url: '/ajax/district.php/',
                error: function(){
                    console.log('Connecting error');
                },
                success: function(data){
                    if(data['error'] == 'empty') {
                        $('.special_District').hide();
                        $(".special_District select option:nth-child(1)").attr("selected", "selected");
                    } else {
                        // $('.special_District').show();
                    }
                },
            });

        }
        $('.special_City select').change(function(){
            loadDistrict($(this).val());
        })
        
        function loadGaraj(){
            if ($('#iObjectTypeGaraj option:selected').text()=='Гараж') {
                $('.special_ObjectSubtype1').show();
                $('.special_ObjectSubtype2').hide();
            } else if ($('#iObjectTypeGaraj option:selected').text()=='Машиноместо') {
                $('.special_ObjectSubtype2').show();
                $('.special_ObjectSubtype1').hide();
            } else {
                $('.special_ObjectSubtype2').hide();
                $('.special_ObjectSubtype1').hide();
            }
        }

        function loadProps(){
        
            $('.special').hide();
            $('.add_object_btn').show();
            $('.special.special_IBLOCK_SECTION').show();
            var sectionProps = props[$('#iIBLOCK_SECTION_NEED').val()]['UF_PROPERTY'];
            
            for(var i=0; i<sectionProps.length; i++) {
                if (sectionProps[i] != 'Client'){
                    $('.special_'+sectionProps[i]).show();
                }
            }
            $('input[type=checkbox]').attr('checked', false);
            $('input[type=text]').attr('disabled', false);
            $('.special_AdStatus').css('display', 'none');
            $('.special_FIO_AUTHOR').css('display', 'none');

            $('.special:hidden input').not('#iNAME,#iAuthor,#main_photo_button,#download_images_button, #iFIO_AUTHOR, input[type=checkbox]').val('');
            $('.special:hidden select').not('#iNAME,#iAuthor').val('0');
            loadGaraj();
            loadCommerce();
            loadDistrict($('.special_City select').val());
        }

        function loadSection() {
            if ($('#first_section').val()>0) {
                $.each(section_tree[$('#first_section').val()]['SECTIONS'], function(i){
                    var checked = '';
                    if (curSection == this) {
                        checked = 'selected="selected"';
                    }
                    if (sections[this]['VALUE'] == 'Куплю'){
                        $('#iIBLOCK_SECTION_NEED').val(this);
                    }
                });
            }
        }

        <?if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) :?>
            loadSection();
            loadProps();
        <?else:?>
            $('.special').hide();
        <?endif?>

        $('#iIBLOCK_SECTION_NEED').change(function(){
            loadProps();
        })

        $('#first_section').change(function(){
            $('.special').hide();
            $('.add_object_btn').hide();
            loadSection();loadProps();
            $('.special.special_IBLOCK_SECTION').show();
        })   

        $('#iObjectTypeGaraj').change(function(){
            loadGaraj();
        })    


        $('#iObjectTypeCommerce').change(function(){
            $('#iNAME').val($('#iObjectTypeCommerce option:selected').text())
            loadCommerce();
        })

            $('.special_AdStatus').css('display', 'none');
            <?
                global $USER;
                $rsUser = CUser::GetByID(CUser::GetID());
                $arUser = $rsUser->Fetch();
            ?>
            $('#iFIO_AUTHOR').val('<?=$arUser['LAST_NAME'].' '.$arUser['NAME'].' '.$arUser['SECOND_NAME']?>');
            $('.special_FIO_AUTHOR').css('display', 'none');
            
            $('.special_Client select').attr('disabled', 'disabled');
            $('.special_Client').css('display', 'none');
            
            <?if (isset($_REQUEST['CODE']) && !empty($_REQUEST['CODE'])):?>
                $('.special_Client').append('<input type="hidden" value='+$('.special_Client select').val()+' name='+$('.special_Client select').attr('name')+' >');
                $('label[for=first_section], select[name=first_section]').hide();
                
                <?php
                    //695 строка - открывашка для ajax районов
                    $slaves = getUsers(array('UF_HEAD'),array('ID'),array('UF_HEAD'=> $arUser['ID'], 'GROUPS_ID'=>6,'ACTIVE'=>'Y'));  
                    if (count($slaves)==0):
                ?>
                   
                    <?if (!empty($arResult['ELEMENT_PROPERTIES']['OBJECT_ELEMENT'][0]['VALUE'])):?>
                        $('.special_DETAIL_PICTURE').hide();
                        $('.special_Images').hide();
                        $('.special_Price').hide();
                        $('.prop_view').html('Данное объявление создано на основе объекта.<br>Редактирование изображений доступно у связанного объекта.<br>Для редактирования остальных полей обратитесь к Вашему руководителю.');
                    <?endif;?>
                <?else:?>
                    <?if (!empty($arResult['ELEMENT_PROPERTIES']['OBJECT_ELEMENT'][0]['VALUE'])):?>
                        $('.special_DETAIL_PICTURE').hide();
                        $('.special_Images').hide();
                        $('.add_object_btn').before('<span>Данное объявление создано на основе объекта.<br>Редактирование изображений доступно у связанного объекта.</span>');
                    <?endif;?>
                <?endif;?>
            <?endif;?>
            <?if (in_array(8, $userStatus)):?>
                $('.special_IS_NEED input').prop('checked','checked');
            <?endif;?>
    </script>
<?else:?>
    <span class="error_text">
        Не указан клиент для выбранной потребности
    </span>
<?endif;?>