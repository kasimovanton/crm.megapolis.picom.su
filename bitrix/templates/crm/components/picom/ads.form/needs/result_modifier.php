<?php
/**
 * Created by PhpStorm.
 * User: picom
 * Date: 01.12.14
 * Time: 11:34
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
global $props;
$props = $arResult['PROPERTY_LIST_FULL'];

function cmpProp($a,$b) {
    global $props;
    $a = intval($props[$a]['SORT']);
    $b = intval($props[$b]['SORT']);    
    if ($a == $b) {
        return 0;
    }
    return ($a < $b) ? -1 : 1;
}

usort($arResult['PROPERTY_LIST'], "cmpProp");

//pre($arResult['PROPERTY_LIST_FULL']);//GetClientFieldHtml UserFunc
$arResult['PROPERTY_LIST_FULL']['Client']['GetPublicEditHTML'] = array('UserFunc','GetAgentFieldHtml');
$arResult['PROPERTY_LIST_FULL']['District']['GetPublicEditHTML'] = array('UserFunc','GetAgentFieldHtml');
$arResult['PROPERTY_LIST_FULL']['City']['GetPublicEditHTML'] = array('UserFunc','GetAgentFieldHtml');
$arResult["PROPERTY_LIST_FULL"]['NAME']["DEFAULT_VALUE"] = 'No name';

// if (isset($_REQUEST['CODE']) && !empty($_REQUEST['CODE'])){


    // $IBLOCK_ID = 5; 
 
    // $arOrder = Array("SORT"=>"ASC");
    // $arSelect = Array("ID", "NAME", "IBLOCK_ID", "DETAIL_PAGE_URL", "SECTION_ID", "PROPERTY_CALLC");
    // $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "ID"=>$_REQUEST['CODE']);
    // $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

    // while($ob = $res->GetNextElement()) 
    // {
        // $arFields = $ob->GetFields();
        // echo '<pre style="display:none;">';
        // print_r($arFields);
        // echo '</pre>';
        // if (!empty($arFields['PROPERTY_CALLC_ENUM_ID'])){
            // echo '<input name="PROPERTY[CALLC][441]" value="'.$arFields['PROPERTY_CALLC_ENUM_ID'].'" type="hidden"';
        // }
    // }
    
    
// }
// pre($arResult['PROPERTY_LIST_FULL']);