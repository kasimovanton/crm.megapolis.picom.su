<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
CModule::IncludeModule("iblock");

if (isset($arResult['PROPERTIES']['IS_NEED']['VALUE']) && empty($arResult['PROPERTIES']['IS_NEED']['VALUE'])){
    $arResult = array();
}

$arResult['arRelateUsers'] = getClientUsers($arResult['PROPERTIES']['Client']['VALUE']);

$res = CIBlockElement::GetByID($arResult['PROPERTIES']['Client']['VALUE']);
if ($ob = $res->GetNextElement()) {
    $arResult['arClient'] = $ob->GetFields();
    $arResult['arClient']['prop'] = $ob->GetProperties();
}
?>