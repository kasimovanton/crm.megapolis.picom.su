<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if (empty($arResult)):?>
    Неверный запрос потребности
<?else:?>
    <div class="needs_info">
        <input type="hidden" name="needs_id" value="<?=$arResult['ID'];?>">
        <div class="header_info">
            <div class="status_block">
                Статус: <span>...</span>
            </div>
            <div class="action_panel">
                Действия и связи:
                <a class="btn_connections" href="/search/?subject=kvartiry&type_subjects=prodam"></a>
                <?if($USER->GetID() == $arResult['PROPERTIES']['Author']['VALUE']):?>
                    <a class="btn_edit" href="/needs/add.php?CODE=<?=$arResult['ID'];?>"></a>
                <?endif;?>
            </div>
        </div>
        <div class="main_info">
            <h2>Потребность</h2>
            <div class="left_column">
                <?php
                    $displayVarArray = array("City", "District");
                ?>
                <ul class="properties_list">
                    <?foreach($arResult['PROPERTIES'] as $keyProperties => $arProperties):?>
                        <?if (in_array($keyProperties, $displayVarArray) && !empty($arProperties['VALUE'])):?>
                            <li><span><?=$arProperties['NAME'];?>:</span> <?=getElemName($arProperties['VALUE']);?></li>
                        <?endif;?>
                    <?endforeach;?>
                    <?if(!empty($arResult['PROPERTIES']['PriceFrom']['VALUE']) || !empty($arResult['PROPERTIES']['PriceTo']['VALUE'])):?>
                        <li>
                            <span>Цена:</span>
                            <?if($arResult['PROPERTIES']['PriceFrom']['VALUE']):?>
                               от <?=number_format($arResult['PROPERTIES']['PriceFrom']['VALUE'], 0, ',', ' ');?> руб. 
                            <?endif;?>
                            <?if($arResult['PROPERTIES']['PriceTo']['VALUE']):?>
                                до <?=number_format($arResult['PROPERTIES']['PriceTo']['VALUE'], 0, ',', ' ');?> руб.
                            <?endif;?>
                        </li>
                    <?endif;?>
                    <?if(!empty($arResult['PROPERTIES']['FloorFrom']['VALUE']) || !empty($arResult['PROPERTIES']['FloorTo']['VALUE'])):?>
                        <li>
                            <span>Этаж:</span>
                            <?if($arResult['PROPERTIES']['FloorFrom']['VALUE']):?>
                               от <?=number_format($arResult['PROPERTIES']['FloorFrom']['VALUE'], 0, ',', ' ');?> 
                            <?endif;?>
                            <?if($arResult['PROPERTIES']['FloorTo']['VALUE']):?>
                                до <?=number_format($arResult['PROPERTIES']['FloorTo']['VALUE'], 0, ',', ' ');?>
                            <?endif;?>
                        </li>
                    <?endif;?>
                    <?if(!empty($arResult['PROPERTIES']['SquareFrom']['VALUE']) || !empty($arResult['PROPERTIES']['SquareTo']['VALUE'])):?>
                        <li>
                            <span>Прощадь:</span>
                            <?if($arResult['PROPERTIES']['SquareFrom']['VALUE']):?>
                               от <?=number_format($arResult['PROPERTIES']['SquareFrom']['VALUE'], 0, ',', ' ');?> м2 
                            <?endif;?>
                            <?if($arResult['PROPERTIES']['SquareTo']['VALUE']):?>
                                до <?=number_format($arResult['PROPERTIES']['SquareTo']['VALUE'], 0, ',', ' ');?> м2
                            <?endif;?>
                        </li>
                    <?endif;?>
                </ul>
            </div>
            <div class="right_column">
                <?=$arResult['DETAIL_TEXT'];?>
            </div>
        </div>
        <div class="footer_info">
            <div class="needs_client client_list">
                <div class="client_contacts">
                    <div class="client_name"><?=$arResult['arClient']['NAME'];?></div>
                    <div class="agent_phone"><?=$arResult['arClient']['prop']['PHONE']['VALUE'];?></div>
                    <div class="agent_phone agent_phone_extra">
                        <?if (!empty($arResult['arClient']['prop']['DOP_PHONE']['VALUE'])):?>
                            <?=$arResult['arClient']['prop']['DOP_PHONE']['VALUE'];?>
                        <?endif;?>
                        <?if (!empty($arResult['arClient']['prop']['DOP_FACE']['VALUE'])):?>
                            <br>
                            <i><?=$arResult['arClient']['prop']['DOP_FACE']['VALUE'];?></i>
                        <?endif;?>
                    </div>
                </div>
            </div>
			
			<div class="client_info_wrap">
				<?if (isset($arResult['arRelateUsers']['agent'])):?>
					<div class="needs_agent client_list">
						<h4>Агент</h4>
						<?if ($arResult['arRelateUsers']['agent'] == $arResult['arRelateUsers']['rieltor']):?>
							<i>Создатель контакта</i>
						<?endif;?>
						<div class="client_name"><?=$arResult['arRelateUsers']['agent']['FULL_NAME'];?></div>
						<div class="agent_phone"><?=$arResult['arRelateUsers']['agent']['PERSONAL_PHONE'];?></div>
					</div>
				<?endif;?>
				<?if (isset($arResult['arRelateUsers']['manager'])):?>
					<div class="needs_manager client_list">
						<h4>Офис-менеджер</h4>
						<?if ($arResult['arRelateUsers']['manager'] == $arResult['arRelateUsers']['rieltor']):?>
							<i>Создатель контакта</i>
						<?endif;?>
						<div class="client_name"><?=$arResult['arRelateUsers']['manager']['FULL_NAME'];?></div>
						<div class="agent_phone"><?=$arResult['arRelateUsers']['manager']['PERSONAL_PHONE'];?></div>
					</div>
				<?endif;?>
				<?if (isset($arResult['arRelateUsers']['operator'])):?>
					<div class="needs_operator client_list">
						<h4>Оператор</h4>
						<?if ($arResult['arRelateUsers']['operator'] == $arResult['arRelateUsers']['rieltor']):?>
							<i>Создатель контакта</i>
						<?endif;?>
						<div class="client_name"><?=$arResult['arRelateUsers']['operator']['FULL_NAME'];?></div>
						<div class="agent_phone"><?=$arResult['arRelateUsers']['operator']['PERSONAL_PHONE'];?></div>
					</div>
				<?endif;?>
			</div>
        </div>
    </div>
    <div class="needs_variants">
        <div class="variants_block">
            <h2 class="variants_header">Предложение вариантов объявлений<span></span></h2>
            <div class="sort_variants_block form_select">
                <label for="elements_count">Показывать на странице по </label>
                <select name="elements_count" class="elements_count">
                    <option value="4">4</option>
                    <option value="8">8</option>
                    <option value="16">16</option>
                    <option value="32">32</option>
                    <option value="all">Все</option>
                </select>
            </div>
            <div class="tabs_main_block">
                <div class="filter_tabs_block">
                    <div class="tabs_head_block">
                        <h3 for="current_option" class="active" data-type="current">Активные (предложить)</h3>
                        <h3 for="close_option" data-type="close">Обработанные</h3>
                    </div>
                </div>
                <div class="tabs current_option visible">
                    <div class="preload">
                        <div class="text-loader">
                            Загрузка
                        </div>
                    </div>
                </div>
                <div class="tabs close_option">
                    <div class="preload">
                        <div class="text-loader">
                            Загрузка
                        </div>
                    </div>
                </div>
            </div>
            <div class="empty_variant_block">
                Подобранные варианты у текущей потребности отсутствуют
            </div>
            <div class="preload">
                <div class="text-loader">
                    Загрузка
                </div>
            </div>
        </div>
        <div class="variants_block object_variant_block">
            <h2>Предложение вариантов объектов</h2>
            <ul>
            </ul>
        </div>
    </div>
    <?if($USER->GetID() == $arResult['PROPERTIES']['Author']['VALUE']):?>
        <div class="close_need_block">
            <a href="#" class="button button_universal close_need">Закрыть потребность</a>
        </div>
    <?endif;?>
    <div style="display: none;">
        <div id="choiceVariant" class="popup_ad">
            <h2>Подтверждение</h2>
            <input type="hidden" name="variant_id">
            <section class="comment_block attention_block">
                <h4>Выбор варианта: "<span>Квартира, Новостройка, 32 м2, 16/16 этаж, район и город</span>"</h4>
                <div>
                    Данный вариант будет закреплен за потребностью как "Выбранный для сделки"
                    Дальнейшая работа с этим вариантов будет доступна только из этой потребности
                    <br>
                    Вы уверены?
                </div>
                <a href="#" class="close_button button button_universal">Отмена</a>
                <a href="#" class="accept_button button button_universal">Выбрать</a>
            </section>
        </div>
    </div>
    <div style="display: none;">
        <div id="closeNeed" class="popup_ad">
            <h2>Подтверждение</h2>
            <input value="<?=$arResult['ID'];?>" type="hidden" name="need_id">
            <section class="comment_block attention_block">
                <h4>Закрытие потребности: "<span>Тестовый Клиент админа – Объявление 18493</span>"</h4>
                <div>
                    Это действие закрывает потребность, что освобождает текущего клиента для возможности создания потребностей другими операторами, если у клиента больше нет Ваших активных потребностей
                    <br>
                    Вы уверены?
                </div>
                <a href="#" class="close_button button button_universal">Отмена</a>
                <a href="#" class="accept_button button button_universal">Выбрать</a>
            </section>
        </div>
    </div>
    <div style="display: none;">
        <div id="history_event" class="popup_ad">
            <h2>Комментарии</h2>
            <input name="element_id" type="hidden">
            <section class="add_history_block">
                <ul>
                </ul>
            </section>
            <section class="add_history_block">
                <textarea name="new_comment" placeholder="Новый комментарий"></textarea>
                <a href="#" class="send_new_comment button button_universal">Отправить</a>
            </section>
        </div>
    </div>
<?endif;?>