<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="temp_clients_list">
    <div class="sort-client">
        <input id="select-all" type="checkbox">
        <label for="select-all">Выбрать всех</label>
        
        <div class="button-card-client">
            <a class="button button_universal popup multi_reassign button_disable" href="#reassign">выбрать агента</a>
            <a class="button button_universal popup multi_reassign_on_me button_disable" href="#reassign">переназначить на себя</a>					
        </div>
    </div>
    <?foreach($arResult["ITEMS"] as $arItem):?>

        <div class="client_wrapper temp_client">
            <div class="client_search_card">
                <input type="checkbox" id="check<?=$arItem['ID'];?>" value="<?=$arItem['ID'];?>" />
                <label for="check<?=$arItem['ID'];?>"></label>
                <div class="client_contacts">
                    <div class="client_name">
                        <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME"]?></a>
                    </div>
                    <?if ($arItem['DISPLAY_PROPERTIES']['PHONE']['DISPLAY_VALUE']):?>
                        <div class="agent_phone"><?=$arItem['DISPLAY_PROPERTIES']['PHONE']['DISPLAY_VALUE']?></div>
                    <?endif?>
                    <?if ($arItem['DISPLAY_PROPERTIES']['DOP_PHONE']['DISPLAY_VALUE']):?>
                        <div class="agent_phone"><?=$arItem['DISPLAY_PROPERTIES']['DOP_PHONE']['DISPLAY_VALUE']?></div>
                    <?endif?>
                    <?if ($arItem['DISPLAY_PROPERTIES']['EMAIL']['DISPLAY_VALUE']):?>
                        <a class="agent_email" href="mailto:<?=$arItem['DISPLAY_PROPERTIES']['EMAIL']['DISPLAY_VALUE']?>"><?=$arItem['DISPLAY_PROPERTIES']['EMAIL']['DISPLAY_VALUE']?></a>
                    <?endif?>
                </div>
                <div class="client_links">
                    <div class="client_ads">
                        <div class="ads_ico"></div>
                        <a href="/ads/?Client=<?=$arItem['ID'];?>" class="client_ads_link">
                            Объявления
                            <?/*$arResult[TOTAL_ADS] - формируется в result_modifier*/?>
    (<span><?if(isset($arResult['TOTAL_ADS'][$arItem['ID']])):?><?=$arResult['TOTAL_ADS'][$arItem['ID']];?><?else:?>0<?endif;?></span>)
                        </a>
                    </div>
                </div>
                <div class="client_description">
                    <p><?echo $arItem["PREVIEW_TEXT"];?></p>
                </div>
                <div class="client_join_date">
                    <span>Добавлен</span>
                    <span><?=$arItem['DISPLAY_ACTIVE_FROM']?></span>
                    <div class="calendar_ico"></div>				
                    <div class="button-card-client">
                        <a class="button button_universal popup one_reassign_on_me" href="#reassign">переназначить на себя</a>
                        <a class="button button_universal popup one_reassign" href="#reassign">выбрать агента</a>
                    </div>
                </div>
            </div>
            <input type="hidden" name="old_agent" value="<?=getElemName($arItem['PROPERTIES']['LAST_OWNER']['VALUE']);?>"> 
        </div>
    <?endforeach;?>
    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <br /><?=$arResult["NAV_STRING"]?>
    <?endif;?>


    
    <div style="display: none;">
        <div class="popup-client" id="reassign">
            <div class="warning">
                <span><img src="/bitrix/templates/crm/img/warning.png">Внимание!</span>
                <p>Это действие нельзя будет отменить</p>
            </div>
            <div class="form">
                <form action="/temp_clients/reassign.php" class="reassign_form" method="POST">
                    <div class="one_client">
                        <div class="col col_client">
                            <span>Клиент:</span>
                            <p></p>
                        </div>
                        <div class="col col_ads">
                            <span>Объявлений:<p></p></span>
                        </div>
                        <div class="col col_old_agent">
                            <span>Старый агент:</span>
                            <p></p>
                        </div>
                    </div>
                    <div class="multi_client hidden">
                        <div class="col col_client">
                            <span>Клиент:</span>
                            <p></p>
                        </div>
                    </div>
                    <div class="col wide">
                        <label>Новый агент:</label>
                        <div class="form_select subject">
                            <?php
                                $agents = getChildsId($arParams['USER_ID'], false, true);
                                $agents[] = $arParams['USER_ID'];
                                $filter = Array("ACTIVE"=>"Y");
                                $rsUsers = CUser::GetList(($by="LAST_NAME"), ($order="asc"), $filter, array("SELECT"=>array()));
                            ?>
                            <select class="new_agent" name="agent_id">
                                <option value="default">Не назначено</option>
                                <?php
                                    while($subUser = $rsUsers->Fetch()) {
                                        if (in_array($subUser['ID'],$agents)){
                                            echo '<option value="'.$subUser['ID'].'">'.$subUser['LAST_NAME'].' '.$subUser['NAME'].' '.$subUser['SECOND_NAME'].'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>	
                    <div class="col wide">
                        <input class="button button_universal button_disable" type="submit" value="переназначить">
                        <a class="button disable" href="#">Отмена</a>
                    </div>	
                    <div class="clients_ids hidden">
                        <input type="hidden" name="client_id[]" value="default">
                    </div>
                    <input type="hidden" name="user_id" value="<?=$arParams['USER_ID'];?>">					
                </form>
            <div class="second-preloader" style="display:none;">
            </div>
            </div>
        </div>
    </div>
</div>