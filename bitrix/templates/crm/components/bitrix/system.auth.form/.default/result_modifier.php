<?php
//pre($arResult);
if ($arResult["USER_LOGIN"]) {
    $rsUser = CUser::GetByLogin($arResult["USER_LOGIN"]);
    $arUser = $rsUser->Fetch();
    //pre($arUser);
    $arResult['USER'] = $arUser;
    $arResult['USER']['PHOTO'] = 'https://api.fnkr.net/testimg/62x62/00CED1/FFF?text=no photo';
    if ($arUser['PERSONAL_PHOTO']) {
        $file = CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array('width'=>62, 'height'=>62), BX_RESIZE_IMAGE_PROPORTIONAL, true,array());
        if (!empty($file['src'])) {
            $arResult['USER']['PHOTO']=$file['src'];
        }
    }
}
