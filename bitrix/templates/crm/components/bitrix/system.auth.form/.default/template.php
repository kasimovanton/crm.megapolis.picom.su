 <?php
    $this->SetViewTarget('auth'); //Вывод в header.php в теге <header>
?> 

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arResult["FORM_TYPE"] == "login"):?>

    <a href="http://crm.megapolis18.ru" rel="sidebar" onclick="addBookmark();" class="add_to_fav">Добавить сайт в избранное</a>
    
    <img class="company_logo" src="/upload/img/megalogo.png" alt="Центр недвижимости &laquot;Мегаполис&raquot;">
<?else:?>
    <div class="realtor_card realtor_card--header">
        <div class="realtor_photo realtor_photo--header">
            <a href="<?=$arResult['PROFILE_URL']?>">
                <img src="<?=$arResult['USER']['PHOTO']?>" alt="realtor photo">
            </a>
        </div>
        <div class="realtor_name realtor_name--header">
            <? echo $arResult['USER']['LAST_NAME']." ".$arResult['USER']['NAME']." ".$arResult['USER']['SECOND_NAME']?>
		<mark title="Редактировать персональные данные">
	            <a class="btn_edit" href="<?=$arResult['PROFILE_URL']?>"></a>
		</mark>
        </div>
        <div class="realtor_position realtor_position--header">
            <?=$arResult['USER']['WORK_POSITION']?>
        </div>
    </div>
    <a href="<?echo $APPLICATION->GetCurPageParam(
        "logout=yes",
        array(
            "login",
            "logout",
            "register",
            "forgot_password",
            "change_password"
        )
        );?>" class="logout_link">
        Выход из системы
        <div class="logout_icon"></div>
    </a>
<?endif?>


<?php
    $this->EndViewTarget();
?>
