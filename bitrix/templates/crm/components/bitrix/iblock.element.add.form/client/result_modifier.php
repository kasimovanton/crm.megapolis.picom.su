<?php
/**
 * Created by PhpStorm.
 * User: picom
 * Date: 21.11.14
 * Time: 11:08
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
//pre($arResult);
$arResult['CLASSES'] = array(
    'PREVIEW_TEXT'=>'content_input_long',//описание
    '46' => 'content_input_long',//фамилия
    '47' => 'content_input_long',//имя
    '48' => 'content_input_long',//отчество
    '49' => 'content_input_medium cell_phone',//телефон
    '50' => 'content_input_medium cell_phone',//доп.телефон
    '51' => 'content_input_long',//почта
);
$arResult['HIDDEN']=array("NAME","52");
$arResult["PROPERTY_LIST"]=array('46','47','48','49','50','51','PREVIEW_TEXT','NAME','52');
global $USER;
$arResult["PROPERTY_LIST_FULL"]["52"]["DEFAULT_VALUE"] = $USER->GetID();
$arResult["PROPERTY_LIST_FULL"]["NAME"]["DEFAULT_VALUE"] = 'Клиент';


$errorNames   = array_map(function ($e) { return preg_replace('~^.*\'(.*?)\'.*$~', '$1', $e); }, $arResult['ERRORS']);
//pre($errorNames);
foreach ($arResult['PROPERTY_LIST_FULL'] as $propertyId => &$property) {
    $isProp = intval($propertyId) > 0;
    $name   = $isProp ? $property['NAME'] : (!empty($arParams["CUSTOM_TITLE_$propertyId"]) ? $arParams["CUSTOM_TITLE_$propertyId"] : GetMessage("IBLOCK_FIELD_$propertyId"));

    $errorIndex = array_search($name, $errorNames);
    $property['ERROR'] = $errorIndex !== false;
    if ($property['ERROR']) {
        $errorIndexes[] = $errorIndex;
    }
}
/*
foreach ($errorIndexes as $errorIndex) {
    unset($arResult['ERRORS'][$errorIndex]);
}*/