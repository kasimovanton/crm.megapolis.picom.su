<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if ((isset($_SESSION['tab_watch']) && $_SESSION['tab_watch']== "watch") || !isset($_SESSION['tab_watch'])) {
    $GLOBALS['filter_obj']['PROPERTY_IS_WATCH'] = false;
} elseif (isset($_SESSION['tab_watch']) && $_SESSION['tab_watch']== "watched") {
    $GLOBALS['filter_obj']['PROPERTY_IS_WATCH'] = 549;
}