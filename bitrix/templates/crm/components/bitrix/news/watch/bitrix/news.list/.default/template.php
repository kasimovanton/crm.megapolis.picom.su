<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="search_menu">
    <div class="sorting">
        <?/*<span>Сортировать:</span>*/?>
        <span>Элементов на странице:</span>
        <div class="form_select paging">
            <select class="short">
                <option <?if($_SESSION['paging'] == 5):?>selected<?endif;?> numb="5">5</option>
                <option <?if($_SESSION['paging'] == 10 || !isset($_SESSION['paging'])):?>selected<?endif;?> numb="10">10</option>
                <option <?if($_SESSION['paging'] == 20):?>selected<?endif;?> numb="20">20</option>
                <option <?if($_SESSION['paging'] == 50):?>selected<?endif;?> numb="50">50</option>
                <option <?if($_SESSION['paging'] == 100):?>selected<?endif;?> numb="100">100</option>
            </select>
        </div>
    </div>
</div>
<div class="search_results">
    <?if (count($arResult["ITEMS"])==0):?>
        На данный момент у Вас нет активных объектов
    <?else:?>
        <?php
            $client_list = getEnumValues(16, array("Client"));
        ?>
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?php
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <?
                $name_client = $client_list['Client'][$arItem['PROPERTIES']['Client']['VALUE']]; //result_modifier
                $db_props = CIBlockElement::GetProperty(10, $arItem['PROPERTIES']['Client']['VALUE'], array("sort" => "asc"), Array("CODE"=>"PHONE"));
                if($ar_props = $db_props->Fetch()){
                    $phone_client = $ar_props["VALUE"];
                } else {
                    $phone_client = "Номер отсутствует";
                }
            ?>
            <div data-object="<?=$arItem['ID'];?>" class="entry entry_list entry_object" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <div class="entry_list__item entry_block_info">
                    <div class="head"><?=$arResult['STATUS_LIST'][$arItem['PROPERTIES']['STATUS']['VALUE']]['PROPERTY_TITLE_VALUE'];?></div>
                    <?if (!empty($arItem['PROPERTIES']['CURRENT_DATE_OPTION']['VALUE'])):?>
                        <?php
                            $lateFlag = false;
                            $arr = ParseDateTime($arItem['PROPERTIES']['CURRENT_DATE_OPTION']['VALUE'], FORMAT_DATETIME);
                            $checkPointA = timeExplode($arItem['PROPERTIES']['CURRENT_DATE_OPTION']['VALUE']);
                            if ($checkPointA - timeExplode(date("d.m.Y H:i:s"))<0){
                                $lateFlag = true;
                            }
                        ?>
                        <div class="date<?if ($lateFlag):?> danger<?endif;?>"><?=$arr["DD"]." ".ToLower(GetMessage("MONTH_".intval($arr["MM"])."_S"))." ".$arr["YYYY"];?> г.</div>
                        <?php
                            $timeString = date("H:i",  MakeTimeStamp($arItem['PROPERTIES']['CURRENT_DATE_OPTION']['VALUE'], "DD.MM.YYYY HH:MI:SS"))
                        ?>
                        <?if ($timeString != "00:00"):?>
                            <div class="time<?if ($lateFlag):?> danger<?endif;?>"><?=$timeString;?></div>
                        <?endif;?>
                        <?if ($lateFlag):?>
                            <i class="late danger">просрочено</i>
                        <?endif?>
                    <?endif;?>
                </div>
                <div class="entry_list__item entry_block_status">
                    <?if ($arItem['PROPERTIES']['OPEN_DATA']['VALUE'][0] == "Да"):?>
                        <span class="entry_client_link">
                            <?=$name_client;?>
                        </span>    
                        <div class="entry_client__phone"><?=$phone_client;?></div>
                    <?else:?>
                        Данные клиента скрыты
                    <?endif;?>
                </div>
                <div class="entry_list__item entry_block_description">
                        <div class="head"><?=$arItem['PROPERTIES']['Street']['VALUE'];?> <?=$arItem['PROPERTIES']['HOUSE']['VALUE'];?><?if (!empty($arItem['PROPERTIES']['APARTMENT']['VALUE'])):?>, кв. <?=$arItem['PROPERTIES']['APARTMENT']['VALUE'];?><?endif;?></div>
                        <div>
                            <span>
                                квартира <?=$arItem['PROPERTIES']['Rooms']['VALUE'];?> к. 
                                <?if ($arItem['PROPERTIES']['Square']['VALUE']):?><?=$arItem['PROPERTIES']['Square']['VALUE'];?> м<sup>2</sup> <?endif;?>
                                <?=$arItem['PROPERTIES']['Floor']['VALUE'];?><?=$arItem['PROPERTIES']['Floors']['VALUE'] ? '/'.$arItem['PROPERTIES']['Floors']['VALUE'] : " этаж" ;?>
                            </span>
                            <span><?=getElemName($arItem['PROPERTIES']['District']['VALUE']);?> р-н</span>
                            <span><?=$arItem['PROPERTIES']['MarketType']['VALUE'];?>, <?=$arItem['PROPERTIES']['HouseType']['VALUE'];?></span>
                        </div>
                        <div class="entry_item__price"><?=number_format($arItem['PROPERTIES']['PRICE']['VALUE'], 0, ',', ' ');?> руб.</div>
                        <div class="accomplice">
                            <a href="/agents/<?=$arItem['PROPERTIES']['AUTHOR']['VALUE'];?>/"><?=$arResult['USER_LIST'][$arItem['PROPERTIES']['AUTHOR']['VALUE']]['FULLNAME'];?></a>
                        </div>
                </div>
                <div class="entry_list__item entry_block_panel">
                    <?php
                        $dayObject = date("d.m.Y H:m",  MakeTimeStamp($arItem['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS"))
                    ?>
                    <div class="info_contact">
                        <span class="entry_item__inf">
                            <i class="entry_item__founder">Создано офис-менеджером:</i><br>
                            <i><?=$dayObject?></i>
                        </span>
                        <span class="calendar_ico"></span>
                    </div>
                    <div class="entry_buttons_object create_ads">
                        <ul>
                            <li class="min-item">
                                <a href="#" class="popupAddObjectPhoto">
                                    <span class="img">
                                        <?if (isset($arResult['OBJECT_PHOTO_LIST'][$arItem['ID']])):?>
                                            <span class="count"><?=$arResult['OBJECT_PHOTO_LIST'][$arItem['ID']];?></span>
                                        <?endif?>
                                        <img src="/bitrix/templates/crm/img/edit-photos.png">
                                    </span>
                                </a>
                            </li>
                            <?if (isset($arResult['OBJECT_PHOTO_LIST'][$arItem['ID']])):?>
                                <li class="min-item">
                                    <a href="#popup_ad" class="galleryLoad">
                                        <span class="img"><img src="/bitrix/templates/crm/img/slide-show.png"></span>
                                    </a>
                                </li>
                            <?endif;?>
                        </ul>
                    </div>
                    <?if (empty($arItem['PROPERTIES']['IS_WATCH']['VALUE'])):?>
                        <div class="entry_buttons_object">
                            <a href="#" class="button button_universal button_move">Переместить в просмотренные</a>
                        </div>
                    <?else:?>
                        <div class="create_ads">	
                            <ul>
                                <?if (empty($arItem['PROPERTIES']['WATCH_AGREE']['VALUE'])):?>
                                    <li><a class="view_watch" href="#view_watch"><span class="img"><img src="/bitrix/templates/crm/img/cross.png"></span><span class="text black">Отказ от работы</span></a></li>
                                <?else:?>
                                    <li><a class="view_watch" href="#view_watch"><span class="img"><img src="/bitrix/templates/crm/img/check.png"></span><span class="text black">Согласие на работу</span></a></li>
                                    <?if(!isset($arResult['OBJECT_ADV'][$arItem['ID']])):?>
                                        <li><a href="/ads/ads_by_object.php?object_id=<?=$arItem['ID'];?>"><span class="img"><img src="/bitrix/templates/crm/img/create_ad.png"></span><span class="text">Создать объявление</span></a></li>
                                    <?else:?>
                                        <li class="ready_ads"><a target="_blank" href="/ads/kvartiry_prodam/<?=$arResult['OBJECT_ADV'][$arItem['ID']];?>/"><span class="img"></span><span class="text">Объявление</span></a></li>
                                    <?endif;?>
                                <?endif;?>
                            </ul>
                        </div>
                    <?endif;?>
                </div>

              </div>
        <?endforeach;?>
        <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
            <br /><?=$arResult["NAV_STRING"]?>
        <?endif;?>
    <?endif;?>
</div>
<div style="display: none;">
    <div id="popup_ad" class="popup_ad">
        <h2>Название объекта</h2>
        <section class="slider">
        </section>
    </div>
</div>
<div style="display: none;">
    <div id="button_move" class="popup_comment">
        <div class="first_step_watch">
            <h2>Перемещение в осмотренные</h2>
            <form class="object_watch_form">
                <input type="hidden" name="object_value" id="object_id_value">
                <div class="row">
                    <label for="textarea">Комментарий<span class="red">*</span></label>
                    <textarea id="textarea" name="watch_comment"></textarea>
                </div>
                <div class="row">
                    <span class="error_text">Не заполнены все обязательные поля</span>
                </div>
                <div class="row">
                    <ul>
                        <li>
                            <input type="radio" name="agree" id="radio" value="yes">
                            <label for="radio" class="radio">Согласен работать с объектом</label>
                        </li>
                        <li>
                            <input type="radio" name="agree" id="radio2" value="no">
                            <label for="radio2" class="radio">Не согласен</label>
                        </li>
                    </ul>
                </div>
                <div class="row">
                    <a href="#" class="button button_universal watched_button">Переместить в осмотренные</a>
                    <a href="#" class="button gray close_button">Отмена</a>
                </div>
            </form>
        </div>
        <div id="success" class="popup_comment">
            <h2>Перемещение в осмотренные</h2>
            <div class="row">
                <p class="success">Объект перемещён в осмотренные</p>
            </div>
            <div class="row">
                <a href="#" class="button button_universal length ads_button">Создать объявление на основе объекта</a>
                <a href="#" class="button gray close_button">Закрыть</a>
            </div>
        </div>
        <div class="second-preloader" style="display:none;"></div>
    </div>
</div>
<div style="display: none;">
    <div id="popupAddObjectPhoto" class="popup_comment">
        <h2>Фотографии объекта</h2>
        <div class="row">													
            <div class="object_preview_list-wrap">
                <div class="object_preview_list">
                    <div id="cboxLoadingGraphic" style="float: left; display: none;"></div>
                    <div class="download">
                        <input id="download_images_button_objects" class="button download_btn" type="button" value="Добавить фото" onclick="alertButton()">
                        <form id="formLoader">
                            <input type="file" id="imageLoaderPhoto" accept="image/*" multiple="" enctype="multipart/form-data" name="imageLoaderPhoto[]"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <a href="#" class="button button_universal button_adder">Сохранить</a>
            <a href="#" class="button gray close_button">Отмена</a>
        </div>
        <div class="second-preloader" style="display:none;"></div>
    </div>
</div>
<div style="display: none;">
    <div id="view_watch" class="popup_ad">
        <h2>Комментарий риэлтора</h2>
        <section class="comment_block">
        </section>
    </div>
</div>
<?//pre($arResult, false);?>
