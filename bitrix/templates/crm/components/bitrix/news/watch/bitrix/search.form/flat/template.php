<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
 <?php
   $this->SetViewTarget('filter'); //Вывод в header.php в теге <main>-><div class="filter_wrapper">
?> 
    <div class="button_panel panel_watch">
        <?if (!isset($_SESSION['tab_watch'])||(isset($_SESSION['tab_watch']) && $_SESSION['tab_watch']=="watch")):?>
            <span class="tab _active">На осмотр</span>
        <?else:?>
            <a class="tab" data-type="watch" href="#">На осмотр</a>
        <?endif;?>
        
        <?if (isset($_SESSION['tab_watch']) && $_SESSION['tab_watch']=="watched"):?>
            <span class="tab _active">Осмотрено</span>
        <?else:?>
            <a class="tab" data-type="watched" href="#">Осмотрено</a>
        <?endif;?>
        
    </div>
<?php
    $this->EndViewTarget();
?>