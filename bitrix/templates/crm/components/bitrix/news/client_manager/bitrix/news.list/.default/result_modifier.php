<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * Created by PhpStorm.
 * User: picom
 * Date: 24.11.14
 * Time: 13:48
 */

 
$arResult['EDIT'] = array();

    foreach($arResult["ITEMS"] as $arItem){
        if($arItem['PROPERTIES']['RIELTOR']['VALUE'] == $arParams['USER_ID']){
            $arResult['EDIT'][$arItem['ID']] = true;
        } else {
            $arResult['EDIT'][$arItem['ID']] = false;
        }
    }