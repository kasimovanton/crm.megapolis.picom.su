<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
    <?php
    if ($arResult['MESSAGE']) {
        echo $arResult['MESSAGE'];
        return;
    }
    ?>
<div class="client_wrapper" >
    <?if (isset($_REQUEST['errorMsg']) && !empty($_REQUEST['errorMsg'])):?>
        <span class="error">
            <?=$_REQUEST['errorMsg'];?>
        </span>
     <?endif;?>
    <div class="client_card">
        <div class="client_contacts">
            <div class="client_name">
                <?echo $arResult["NAME"]?>
            </div>
            <?if ($arResult['PROPERTIES']['PHONE']['VALUE']):?>
                <div class="agent_phone"><?=$arResult['PROPERTIES']['PHONE']['VALUE']?></div>
            <?endif?>
            <?if ($arResult['PROPERTIES']['DOP_PHONE']['VALUE']):?>
                <div class="agent_phone agent_phone_extra"><?=$arResult['PROPERTIES']['DOP_PHONE']['VALUE']?>
                    <?if($arResult['PROPERTIES']['DOP_FACE']['VALUE']):?>
                        <br/>
                        <i><?=$arResult['PROPERTIES']['DOP_FACE']['VALUE'];?></i>
                    <?endif;?>
                </div>
            <?endif?>
            <?if ($arResult['PROPERTIES']['EMAIL']['VALUE']):?>
                <a class="agent_email" href="mailto:<?=$arResult['PROPERTIES']['EMAIL']['VALUE']?>"><?=$arResult['PROPERTIES']['EMAIL']['VALUE']?></a>
            <?endif?>
        </div>
        <div class="client_links">
            <?if (!empty($arResult['PROPERTIES']['RIELTOR']['VALUE'])):?>
                <?if ($arResult['PROPERTIES']['RIELTOR']['VALUE'] == $arParams['CUR_USER']):?>
                    Мой клиент
                <?else:?>
                    <div class="client_ads">
                        <a href="/agents/<?=$arResult['PROPERTIES']['RIELTOR']['VALUE']?>/" class="client_objects_link">Агент клиента</a>
                    </div>
                <?endif;?>
            <?else:?>
                Агент отсутсвует
            <?endif?>
            <?/*<div class="client_ads">
                <div class="objects_ico"></div>
                <a href="/ads/?Client=<?=$arResult['ID']?>" class="client_ads_link">
                Объявления 
                    (<?if(isset($arResult['TOTAL_ADS'][$arResult['ID']])):?><?=$arResult['TOTAL_ADS'][$arResult['ID']];?><?else:?>0<?endif;?>)
                </a>
                <?if ($arResult['EDIT']):?><a href="/ads/add.php?Сlient=<?=$arResult['ID']?>" class="button button_universal">Добавить объявление</a><?endif?>
            </div>*/?>
        </div>
        <a class="button button_universal" href="/objects/add.php?client=9458">Добавить объект</a>
        <div class="client_join_date">
            <span>Добавлен</span>
            <span><?=$arResult['DISPLAY_ACTIVE_FROM']?></span>
            <div class="calendar_ico"></div>
        </div>
        <div class="client_description">
            <p><?echo $arResult["PREVIEW_TEXT"];?></p>
        </div>
    </div>
</div>
<?//pre($arParams);?>