<?php

global $USER;
$userID=$arParams['CUR_USER'];

if ($arResult['PROPERTIES']['MANAGER']['VALUE']==$userID) {
    $arResult['LOOK'] = true;
} else {
    $arResult['LOOK'] = false;
    $arResult['MESSAGE'] = 'Недостаточно прав для просмотра клиента';
}