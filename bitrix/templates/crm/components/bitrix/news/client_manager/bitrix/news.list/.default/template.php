<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

    <div class="client_wrapper" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="client_search_card">
            <div class="client_contacts">
                <div class="client_name">
                    <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME"]?></a>
                </div>
                <?if ($arItem['PROPERTIES']['PHONE']['VALUE']):?>
                    <div class="agent_phone"><?=$arItem['PROPERTIES']['PHONE']['VALUE']?></div>
                <?endif?>
                <?if ($arItem['PROPERTIES']['DOP_PHONE']['VALUE']):?>
                    <div class="agent_phone agent_phone_extra"><?=$arItem['PROPERTIES']['DOP_PHONE']['VALUE']?>
                        <?if($arItem['PROPERTIES']['DOP_FACE']['VALUE']):?>
                            <br/>
                            <i><?=$arItem['PROPERTIES']['DOP_FACE']['VALUE'];?></i>
                        <?endif;?>
                    </div>
                <?endif?>
                <?if ($arItem['PROPERTIES']['EMAIL']['VALUE']):?>
                    <a class="agent_email" href="mailto:<?=$arItem['PROPERTIES']['EMAIL']['VALUE']?>"><?=$arItem['PROPERTIES']['EMAIL']['VALUE']?></a>
                <?endif?>
            </div>
            <div class="client_links">
                <?/*<div class="client_ads">
                    <div class="ads_ico"></div>
                    <a href="/ads/?Client=<?=$arItem['ID'];?>" class="client_ads_link">
                        Объявления
                        (<?if(isset($arResult['TOTAL_ADS'][$arItem['ID']])):?><?=$arResult['TOTAL_ADS'][$arItem['ID']];?><?else:?>0<?endif;?>)
                    </a>
                </div>*/?>
            </div>
            <div class="client_description">
                <p><?echo $arItem["PREVIEW_TEXT"];?></p>
            </div>
            <div class="client_join_date">
                <span>Добавлен</span>
                <span><?=$arItem['DISPLAY_ACTIVE_FROM']?></span>
                <div class="calendar_ico"></div>
            </div>
            <?if ($arResult['EDIT'][$arItem['ID']]):?>
                <mark title="Редактировать данные клиента">
                    <a href="/clients/add.php?edit=Y&CODE=<?=$arItem['ID']?>" class="button btn_edit"></a>
                </mark>
            <?endif;?>
        </div>
    </div>
<?endforeach;?>
<?if (count($arResult["ITEMS"])==0):?>
    <p>Клиенты не найдены</p>
<?endif?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

<?//pre($arResult, false)?>