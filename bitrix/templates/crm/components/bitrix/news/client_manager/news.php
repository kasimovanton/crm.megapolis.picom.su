<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if($arParams["USE_RSS"]=="Y"):?>
	<?
	if(method_exists($APPLICATION, 'addheadstring'))
		$APPLICATION->AddHeadString('<link rel="alternate" type="application/rss+xml" title="'.$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["rss"].'" href="'.$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["rss"].'" />');
	?>
	<a href="<?=$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["rss"]?>" title="rss" target="_self"><img alt="RSS" src="<?=$templateFolder?>/images/gif-light/feed-icon-16x16.gif" border="0" align="right" /></a>
<?endif?>

<?if($arParams["USE_SEARCH"]=="Y"):?>
<?$this->SetViewTarget("filter");?>
<?$APPLICATION->IncludeComponent("bitrix:search.title","manager", Array(
        "SHOW_INPUT" => "Y",
        "INPUT_ID" => "title-search-input",
        "CONTAINER_ID" => "title-search",
        "PRICE_CODE" => array(),
        "PRICE_VAT_INCLUDE" => "Y",
        "PREVIEW_TRUNCATE_LEN" => "150",
        "SHOW_PREVIEW" => "N",
        "PREVIEW_WIDTH" => "30",
        "PREVIEW_HEIGHT" => "30",
        "CONVERT_CURRENCY" => "Y",
        "CURRENCY_ID" => "RUB",
        "PAGE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["search"],
        "NUM_CATEGORIES" => "1",
        "TOP_COUNT" => "10",
        "ORDER" => "date",
        "USE_LANGUAGE_GUESS" => "N",
        "CHECK_DATES" => "Y",
        "SHOW_OTHERS" => "N",
        "CATEGORY_0_TITLE" => "Клиенты",
        "CATEGORY_0" => array("iblock_directories"),   
        "CATEGORY_0_iblock_directories" => array("10"),
        "CATEGORY_OTHERS_TITLE" => "Другое"
    ),
	$component
);?>
<?$this->EndViewTarget();?>
<?endif?>
<?if($arParams["USE_FILTER"]=="Y"):?>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.filter",
	"",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"FILTER_NAME" => $arParams["FILTER_NAME"],
		"FIELD_CODE" => $arParams["FILTER_FIELD_CODE"],
		"PROPERTY_CODE" => $arParams["FILTER_PROPERTY_CODE"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
	),
	$component
);
?>
<br />
<?endif?>
<?php
    $curLink = $APPLICATION->GetCurPageParam('',array());
    $sortArr = array(
    "REFERENCE" =>
        array(
            "по алфавиту от А до Я",
            "по алфавиту от Я до А",
            "по дате добавления от 1 до N",
            "по дате добавления от N до 1"
            ),
    "REFERENCE_ID" =>
        array(
            $APPLICATION->GetCurPageParam('filter='.$_GET['filter'].'&sort=name&order=asc', array('filter','sort', 'order')),
            $APPLICATION->GetCurPageParam('filter='.$_GET['filter'].'&sort=name&order=desc', array('filter','sort', 'order')),
            $APPLICATION->GetCurPageParam('filter='.$_GET['filter'].'&sort=created&order=asc', array('filter','sort', 'order')),
            $APPLICATION->GetCurPageParam('filter='.$_GET['filter'].'&sort=created&order=desc', array('filter','sort', 'order')),
            ),
    );
?>
<div class="search_menu">
    <div class="sorting">
        <span>Сортировать:</span>
        <div class="form_select sort">
            <?echo SelectBoxFromArray("sort", $sortArr, $curLink)?>
        </div>
    </div>
    <a class="button button_universal" href="/clients/add.php">Добавить клиента</a>
    <a class="button button_universal" href="/clients/check_number.php">Проверка номера</a>
</div>
<?$GLOBALS['filter_obj']=array("PROPERTY_MANAGER"=>CUser::GetID())?>

<?if (isset($_REQUEST['sort']) && !empty($_REQUEST['sort'])){
    $sort = $_REQUEST['sort'];
} else {
    $sort = 'NAME';
}
if (isset($_REQUEST['order']) && !empty($_REQUEST['order'])){
    $order = $_REQUEST['order'];
} else {
    $order = 'ASC';
}
?>
    <?php
        /*if (isset($_REQUEST['q']) && !empty($_REQUEST['q'])){
            $new_array = array();
            $new_array["LOGIC"] = "OR";
            $new_array[] = array('PROPERTY_PHONE'=>'%'.$_REQUEST['q'].'%');
            $new_array[] = array('PROPERTY_DOP_PHONE'=>'%'.$_REQUEST['q'].'%');
            $new_array[] = array('NAME'=>'%'.$_REQUEST['q'].'%');
            $new_array[] = array('SEARCHABLE_CONTENT'=>'%'.$_REQUEST['q'].'%');
            $GLOBALS['filter_obj'][] = $new_array;
        }*/
    ?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"",
	Array(
		"IBLOCK_TYPE"	=>	$arParams["IBLOCK_TYPE"],
		"IBLOCK_ID"	=>	$arParams["IBLOCK_ID"],
		"NEWS_COUNT"	=>	$arParams["NEWS_COUNT"],
		"SORT_BY1"	=>	$sort,
		"SORT_ORDER1"	=>	$order,
		"SORT_BY2"	=>	$arParams["SORT_BY2"],
		"SORT_ORDER2"	=>	$arParams["SORT_ORDER2"],
		"FIELD_CODE"	=>	$arParams["LIST_FIELD_CODE"],
		"PROPERTY_CODE"	=>	$arParams["LIST_PROPERTY_CODE"],
		"DETAIL_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
		"SECTION_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"IBLOCK_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
		"DISPLAY_PANEL"	=>	$arParams["DISPLAY_PANEL"],
		"SET_TITLE"	=>	$arParams["SET_TITLE"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"INCLUDE_IBLOCK_INTO_CHAIN"	=>	$arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
		"CACHE_TYPE"	=>	$arParams["CACHE_TYPE"],
		"CACHE_TIME"	=>	$arParams["CACHE_TIME"],
		"CACHE_FILTER"	=>	$arParams["CACHE_FILTER"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"DISPLAY_TOP_PAGER"	=>	$arParams["DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER"	=>	$arParams["DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE"	=>	$arParams["PAGER_TITLE"],
		"PAGER_TEMPLATE"	=>	$arParams["PAGER_TEMPLATE"],
		"PAGER_SHOW_ALWAYS"	=>	$arParams["PAGER_SHOW_ALWAYS"],
		"PAGER_DESC_NUMBERING"	=>	$arParams["PAGER_DESC_NUMBERING"],
		"PAGER_DESC_NUMBERING_CACHE_TIME"	=>	$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
		"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
		"DISPLAY_DATE"	=>	$arParams["DISPLAY_DATE"],
		"DISPLAY_NAME"	=>	"Y",
		"DISPLAY_PICTURE"	=>	$arParams["DISPLAY_PICTURE"],
		"DISPLAY_PREVIEW_TEXT"	=>	$arParams["DISPLAY_PREVIEW_TEXT"],
		"PREVIEW_TRUNCATE_LEN"	=>	$arParams["PREVIEW_TRUNCATE_LEN"],
		"ACTIVE_DATE_FORMAT"	=>	$arParams["LIST_ACTIVE_DATE_FORMAT"],
		"USE_PERMISSIONS"	=>	$arParams["USE_PERMISSIONS"],
		"GROUP_PERMISSIONS"	=>	$arParams["GROUP_PERMISSIONS"],
		"USER_ID"	=>	$arParams["USER_ID"],
		"FILTER_NAME"	=>	$arParams["~FILTER_NAME"],
		"HIDE_LINK_WHEN_NO_DETAIL"	=>	$arParams["HIDE_LINK_WHEN_NO_DETAIL"],
		"CHECK_DATES"	=>	$arParams["CHECK_DATES"],
	),
	$component
);?>
