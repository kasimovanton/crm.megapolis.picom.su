<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->SetViewTarget("filter");?>
<?$arElements = $APPLICATION->IncludeComponent(
	"bitrix:search.page",
	"",
	Array(
		"CHECK_DATES" => $arParams["CHECK_DATES"]!=="N"? "Y": "N",
		"arrWHERE" => Array("iblock_".$arParams["IBLOCK_TYPE"]),
		"arrFILTER" => Array("iblock_".$arParams["IBLOCK_TYPE"]),
		"SHOW_WHERE" => "N",
		//"PAGE_RESULT_COUNT" => "",
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"SET_TITLE" => $arParams["SET_TITLE"],
		"arrFILTER_iblock_".$arParams["IBLOCK_TYPE"] => Array($arParams["IBLOCK_ID"])
	),
	$component
);?>
<?$this->EndViewTarget();?>
<?
// pre($arElements,false);
// pre($arParams,false);
if (!empty($arElements) && is_array($arElements))
{
global $searchFilter;
$searchFilter = array("=ID" => $arElements,);
$searchFilter['PROPERTY_MANAGER'] = CUser::GetID();

$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "",
    Array(
        "IBLOCK_TYPE"	=>	$arParams["IBLOCK_TYPE"],
        "IBLOCK_ID"	=>	$arParams["IBLOCK_ID"],
        "NEWS_COUNT"	=>	$arParams["NEWS_COUNT"],
        "SORT_BY1"	=>	$arParams["SORT_BY1"],
        "SORT_ORDER1"	=>	$arParams["SORT_ORDER1"],
        "SORT_BY2"	=>	$arParams["SORT_BY2"],
        "SORT_ORDER2"	=>	$arParams["SORT_ORDER2"],
        "FIELD_CODE"	=>	$arParams["LIST_FIELD_CODE"],
        "PROPERTY_CODE"	=>	$arParams["LIST_PROPERTY_CODE"],
        "DETAIL_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
        "SECTION_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
        "IBLOCK_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
        "DISPLAY_PANEL"	=>	$arParams["DISPLAY_PANEL"],
        "SET_TITLE"	=>	$arParams["SET_TITLE"],
        "SET_STATUS_404" => $arParams["SET_STATUS_404"],
        "INCLUDE_IBLOCK_INTO_CHAIN"	=>	$arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
        "CACHE_TYPE"	=>	$arParams["CACHE_TYPE"],
        "CACHE_TIME"	=>	$arParams["CACHE_TIME"],
        "CACHE_FILTER"	=>	$arParams["CACHE_FILTER"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "DISPLAY_TOP_PAGER"	=>	$arParams["DISPLAY_TOP_PAGER"],
        "DISPLAY_BOTTOM_PAGER"	=>	$arParams["DISPLAY_BOTTOM_PAGER"],
        "PAGER_TITLE"	=>	$arParams["PAGER_TITLE"],
        "PAGER_TEMPLATE"	=>	$arParams["PAGER_TEMPLATE"],
        "PAGER_SHOW_ALWAYS"	=>	$arParams["PAGER_SHOW_ALWAYS"],
        "PAGER_DESC_NUMBERING"	=>	$arParams["PAGER_DESC_NUMBERING"],
        "PAGER_DESC_NUMBERING_CACHE_TIME"	=>	$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
        "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
        "DISPLAY_DATE"	=>	$arParams["DISPLAY_DATE"],
        "DISPLAY_NAME"	=>	"Y",
        "DISPLAY_PICTURE"	=>	$arParams["DISPLAY_PICTURE"],
        "DISPLAY_PREVIEW_TEXT"	=>	$arParams["DISPLAY_PREVIEW_TEXT"],
        "PREVIEW_TRUNCATE_LEN"	=>	$arParams["PREVIEW_TRUNCATE_LEN"],
        "ACTIVE_DATE_FORMAT"	=>	$arParams["LIST_ACTIVE_DATE_FORMAT"],
        "USE_PERMISSIONS"	=>	$arParams["USE_PERMISSIONS"],
        "GROUP_PERMISSIONS"	=>	$arParams["GROUP_PERMISSIONS"],
        "FILTER_NAME"	=>	'searchFilter',
        "USER_ID" => CUser::GetID(),
        "HIDE_LINK_WHEN_NO_DETAIL"	=>	$arParams["HIDE_LINK_WHEN_NO_DETAIL"],
        "CHECK_DATES"	=>	$arParams["CHECK_DATES"],
    ),
    $component
);
}?>
<p><a href="<?=$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"]?>"><?=GetMessage("T_NEWS_DETAIL_BACK")?></a></p>
