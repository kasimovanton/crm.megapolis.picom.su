<?php
    if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

    //������ ������� ������
    $rsUser = CUser::GetByID($arResult['PROPERTIES']['Author']['VALUE']);
    $arResult['agent_prop_list'] = $rsUser->Fetch();
    
    //����� �������, ���������������� ����������, ��� ������ ������������� ������
    // $IBLOCK_ID = 5; 
    // $arOrder = Array("SORT"=>"ASC");
    // $arSelect = Array("ID", "NAME", "IBLOCK_ID", "CODE", "UF_*");
    // $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "ID" =>$arResult['IBLOCK_SECTION_ID']);
    // $res_sec = CIBlockSection::GetList($arOrder, $arFilter, true, $arSelect);
    // $ob = $res_sec->GetNextElement();
    // $ar_res = $ob->GetFields();

    // $res = CIBlockSection::GetByID($ar_res['UF_REVERSE']);
    // $ar_res = $res->GetNext();
    
    // $arResult['reverse'] = $ar_res['CODE'];
    
    //����� �������, ���������������� ����������, ��� ������ ������������� ������
    $arResult['reverse'] = getRevSect(5, $arResult['IBLOCK_SECTION_ID']);
    
    $arResult['checkAuthor'] = getChildsId($arParams['USER_ID'], false, true);
    $arResult['checkAuthor'][] = $arParams['USER_ID'];
    
    
    //����� ������� ��� ������� � ������������ ������
    /*������� � ����� ��������, ������� � ������ ����� �������� *, *From, *To, *List - ��� true. ��� false - ���������� ������ ������� ������� */
    $search_property = iterArray(true);
    $search_single_property = iterArray(false);
    
    foreach ($search_property as $single_prop){
        if ($single_prop != 'Rooms'){
            if (!empty($arResult['PROPERTIES'][$single_prop]['VALUE'])){
                $arResult['iteration'][$single_prop] = $arResult['PROPERTIES'][$single_prop]['VALUE'];
            } else {
                if (empty($arResult['PROPERTIES'][$single_prop.'List']['VALUE'])){
                    if (!empty($arResult['PROPERTIES'][$single_prop.'From']['VALUE'])){
                        $arResult['iteration'][$single_prop][0] = $arResult['PROPERTIES'][$single_prop.'From']['VALUE'];
                    }
                    if (!empty($arResult['PROPERTIES'][$single_prop.'To']['VALUE'])){
                        $arResult['iteration'][$single_prop][1] = $arResult['PROPERTIES'][$single_prop.'To']['VALUE'];
                    }
                }
            }
        } else {
            if (!empty($arResult['PROPERTIES'][$single_prop]['VALUE'])){
                $arResult['iteration'][$single_prop] = $arResult['PROPERTIES'][$single_prop]['VALUE'];
            }
            if (count($arResult['PROPERTIES'][$single_prop.'List']['VALUE']) > 0){
                foreach ($arResult['PROPERTIES'][$single_prop.'List']['VALUE'] as $val){
                    $arResult['iteration'][$single_prop.'List'][] = $val;
                }
            }
        }
    }
    
    foreach ($search_single_property as $single_prop){
        if (!empty($arResult['PROPERTIES'][$single_prop]['VALUE'])){
            $arResult['iter_prop'][$single_prop] = $arResult['PROPERTIES'][$single_prop]['VALUE'];
        }
    }
    if (!empty($arResult['PROPERTIES']['OBJECT_ELEMENT']['VALUE'])){
        $arFilter = Array("IBLOCK_ID"=>16, "ACTIVE"=>"Y", "ID" =>$arResult['PROPERTIES']['OBJECT_ELEMENT']['VALUE']);
        $resultObject = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter, false, false);
        if ($arResultObject = $resultObject->GetNextElement()){
            $arObject['properties'] = $arResultObject->GetFields();
            $arObject['properties'] = $arResultObject->GetProperties();
            if (!empty($arObject['properties']['FILES_OBJECTS']['VALUE'])){
                $arResult["object_image"][$arResult['ID']] = $arObject['properties']['FILES_OBJECTS']['VALUE'];
            }
        }
    }
    // pre($arResult);