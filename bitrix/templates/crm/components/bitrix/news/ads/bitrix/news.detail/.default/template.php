<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="content_pane">
    <div class="content_pane_left">
    
        <?if (!empty($arResult["DETAIL_PICTURE"])):?>
            <?
                $mainPhoto = $arResult["DETAIL_PICTURE"];
                $haveDetail = true;
            ?>
        <?else:?>
            <?if (!empty($arResult['PROPERTIES']['Images']['VALUE'][0])):?>
                <?
                    $mainPhoto = $arResult['PROPERTIES']['Images']['VALUE'][0];
                    $haveDetail = false;
                ?>
            <?else:?>
                <?
                    $mainPhoto = $arResult["object_image"][$arResult['ID']][0];
                    // unset($arResult["object_image"][$arResult['ID']][0]);
                    $haveDetail = false;
                ?>
            <?endif;?>
        <?endif;?>
        
    
        <?$renderImageMini = CFile::ResizeImageGet($mainPhoto, Array("width" => '90px', "height" => '60px'), BX_RESIZE_IMAGE_EXACT);?>
        <?$renderImageMedium = CFile::ResizeImageGet($mainPhoto, Array("width" => '380px', "height" => '290px'), BX_RESIZE_IMAGE_EXACT);?>
        <?$renderImageMaxi = CFile::ResizeImageGet($mainPhoto, Array("width" => '700px', "height" => '525px'));?>
        <?if ($renderImageMedium['width']!=0){
            $width = $arResult["DETAIL_PICTURE"]['WIDTH'];
            $height = $arResult["DETAIL_PICTURE"]['HEIGHT'];
        } else {
            $width = 380;
            $height = 290;
        }?>
        <?if (!empty($mainPhoto)):?>
            <a rel="galGroup" class="cboxElement" href="<?=$renderImageMaxi['src'];?>">
                <img class="content_pane_image" width="<?=$width;?>" height="<?=$height;?>" src="<?=$renderImageMedium['src'];?>" alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>">
            </a>
        <?endif;?>
        <div class="content_pane_image_selector">
            <div class="arrow_up"></div>
            <?if (count($arResult["object_image"][$arResult['ID']])>0):?>
                <div class="image_grid">
                    <div class="image_grid_inner">
                            <?if ($haveDetail):?>
                                <img rel="galGroup" class="image_grid_thumb" src="<?=$renderImageMini['src'];?>" alt="<?=$renderImageMini['alt'];?>" detwidth="<?=$width?>" detheight="<?=$height?>" for_det_src="<?=$renderImageMedium['src'];?>" for_det_href="<?=$arResult["DETAIL_PICTURE"]["SRC"];?>">
                            <?endif;?>
                        <?foreach ($arResult["object_image"][$arResult['ID']] as $foto_id):?>
                            <?$renderImageMini = CFile::ResizeImageGet($foto_id, Array("width" => '90px', "height" => '60px'), BX_RESIZE_IMAGE_EXACT);?>
                            <?$renderImageMedium = CFile::ResizeImageGet($foto_id, Array("width" => '380px', "height" => '290px'), BX_RESIZE_IMAGE_EXACT);?>
                            <?$renderImageMaxi = CFile::ResizeImageGet($foto_id, Array("width" => '700px', "height" => '525px'));?>
                            <?$rsFile = CFile::GetByID($foto_id);$arFile = $rsFile->Fetch();
                                if ($renderImageMedium['width']!=0){
                                    $width = $arFile['WIDTH'];
                                    $height = $arFile['HEIGHT'];
                                } else {
                                    $width = 380;
                                    $height = 290;
                                }
                            ?>
                            <a class="cboxElement" href="<?=$renderImageMaxi['src'];?>" rel="galGroup" style="display:none"></a>
                            <img class="image_grid_thumb half_opacity" src="<?=$renderImageMini['src'];?>" alt="<?=$renderImageMini['alt'];?>" detwidth="<?=$width;?>" detheight="<?=$height;?>" for_det_src="<?=$renderImageMedium['src'];?>" for_det_href="<?=$renderImageMaxi['src'];?>">
                        <?endforeach;?>
                    </div>
                </div>
            <?else:?>
                <?if (!empty($arResult['PROPERTIES']['Images']['VALUE'])):?>
                    <div class="image_grid">
                        <div class="image_grid_inner">
                                <?if ($haveDetail):?>
                                    <img rel="galGroup" class="image_grid_thumb" src="<?=$renderImageMini['src'];?>" alt="<?=$renderImageMini['alt'];?>" detwidth="<?=$width?>" detheight="<?=$height?>" for_det_src="<?=$renderImageMedium['src'];?>" for_det_href="<?=$arResult["DETAIL_PICTURE"]["SRC"];?>">
                                <?endif;?>
                            <?foreach ($arResult['PROPERTIES']['Images']['VALUE'] as $foto_id):?>
                                <?$renderImageMini = CFile::ResizeImageGet($foto_id, Array("width" => '90px', "height" => '60px'), BX_RESIZE_IMAGE_EXACT);?>
                                <?$renderImageMedium = CFile::ResizeImageGet($foto_id, Array("width" => '380px', "height" => '290px'), BX_RESIZE_IMAGE_EXACT);?>
                                <?$renderImageMaxi = CFile::ResizeImageGet($foto_id, Array("width" => '700px', "height" => '525px'));?>
                                <?$rsFile = CFile::GetByID($foto_id);$arFile = $rsFile->Fetch();
                                    if ($renderImageMedium['width']!=0){
                                        $width = $arFile['WIDTH'];
                                        $height = $arFile['HEIGHT'];
                                    } else {
                                        $width = 380;
                                        $height = 290;
                                    }
                                ?>
                                <a class="cboxElement" href="<?=$renderImageMaxi['src'];?>" rel="galGroup" style="display:none"></a>
                                <img class="image_grid_thumb half_opacity" src="<?=$renderImageMini['src'];?>" alt="<?=$renderImageMini['alt'];?>" detwidth="<?=$width;?>" detheight="<?=$height;?>" for_det_src="<?=$renderImageMedium['src'];?>" for_det_href="<?=$renderImageMaxi['src'];?>">
                            <?endforeach;?>
                        </div>
                    </div>
                <?endif;?>
            <?endif;?>
            <div class="arrow_down"></div>
        </div>
    </div>
    <div class="content_pane_right">
        <div class="content_pane_ur_corner">
            <div class="content_pane_date">
                <div>Добавлен</div>
                <span><?=date("d.m.Y",  MakeTimeStamp($arResult['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS"));?></span>
                <div class="calendar_ico"></div>
            </div>
        </div>
        <ul>
           <li class="content_pane_row">
                <?if (!empty($arResult['PROPERTIES']['Price']['VALUE'])):?>
                <span class="property"><?=$arResult['PROPERTIES']['Price']['NAME'];?>:</span>
                <span class="value">
                    <?=number_format($arResult['PROPERTIES']['Price']['VALUE'], 0, ',', ' ');?>
                </span>
                <?endif;?>
            </li>
            <?foreach ($arResult['PROPERTIES'] as $key => $value):?>
                <?if (!empty($value['VALUE']) && !is_array($value['VALUE'])&& $key != 'Author' && $key != 'FIO_AUTHOR' && $key != 'AdStatus' && $key != 'Price' && $key != 'CALLC' && $key != 'OBJECT_ELEMENT' && $key != 'REASON'):?>
                    <?if (!preg_match('/Client/', $key) || $arParams['USER_ID']==$arResult['agent_prop_list']['ID'] || $arParams['USER_ID']==$arResult['agent_prop_list']['UF_HEAD']):?>
                        <li class="content_pane_row">
                            <span class="property"><?=$value['NAME'];?>:</span>
                            <span class="value">
                                <?if (($key == 'City')  || (preg_match('/District/', $key))|| (preg_match('/Region/', $key))|| (preg_match('/Client/', $key))):?>
                                    <?if (preg_match('/Client/', $key)):?>
                                        <a href="/clients/<?=$value['VALUE'];?>/"><?=getElemName($value['VALUE']);?></a>
                                    <?else:?>
                                        <?=getElemName($value['VALUE']);?>
                                    <?endif;?>
                                    
                                <?else:?>
                                    <?if (!is_numeric($value['VALUE'])):?>
                                        <?=$value['VALUE'];?>
                                    <?else:?>
                                        <?=number_format($value['VALUE'], 0, ',', ' ');?>
                                    <?endif;?>
                                    <?if ($key == 'APARTMENT' && $arResult['PROPERTIES']['Avito']['VALUE']):?>
                                        <i>(номер квартиры не выгружается на авито)</i>
                                    <?endif;?>
                                <?endif;?>
                            </span>
                        </li>
                    <?endif;?>  
                <?endif;?>
            <?endforeach;?>
            <?if ($arResult['PROPERTIES']['Avito']['VALUE']):?>
                <li class="content_pane_row">
                    <span class="property">Выгрузка:</span>
                    <mark title="Объявление будет выгружено на авито">
                        <span class="value avito_ico"></span>
                    </mark>
                </li>
            <?endif;?>
            <?if ($arResult['PROPERTIES']['CALLC']['VALUE'] && $arResult['PROPERTIES']['Avito']['VALUE']):?>
                <li class="alert">
                    <b>Объявление будет выгружено на авито с телефоном колл-центра(<?=getCallcenterNumber();?>)</b>
                </li>
            <?endif;?>
            <li class="content_pane_row">
                <p class="value">
                    <?=$arResult['DETAIL_TEXT'];?>
                </p>
            </li>
        </ul>
    </div>   
</div><!-- /content_pane -->

<div class="content_footer">
    <div class="content_footer_column">
        <div class="content_footer_row">
            <div class="row_head">
                <span title="Текст">Действия и связи:</span>
            </div>
            <div class="row_content">
                <div class="extra_buttons">
                    <?if (isset($arResult['reverse']) && !empty($arResult['reverse'])):?>
                        <mark title="Подобрать подходящие объявления">
                            <form action="/ads/<?=$arResult['reverse']?>/" method="POST">
                                <?foreach ($arResult['iteration'] as $prop => $value):?>
                                    <?if (!is_array($value)):?>
                                        <input type="hidden" value="<?=$value;?>" name="iteration[<?=$prop;?>]">
                                    <?else:?>
                                        <?foreach ($value as $filt_key => $filt_val):?>
                                            <input type="hidden" value="<?=$filt_val;?>" name="iteration[<?=$prop;?>][<?=$filt_key;?>]">
                                        <?endforeach;?>
                                    <?endif;?>
                                <?endforeach;?>
                                <?foreach ($arResult['iter_prop'] as $prop => $value):?>
                                    <input type="hidden" value="<?=$value;?>" name="iter_prop[<?=$prop;?>]">
                                <?endforeach;?>
                                <input type="hidden" value="y" name="new_search">
                                <?if (!isset($_SESSION['search_begin'])):?>
                                    <input type="hidden" value="<?="http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];?>" name="path_begin">
                                    <input type="hidden" value='карточку объявления "<?=$arResult['NAME'];?>"' name="back_name">
                                <?endif?>
                                <input type="submit"  class="btn_connections" />
                            </form>
                        </mark>
                    <?endif;?>
                    <?//if (CUser::GetID()==$arResult['agent_prop_list']['ID']):?>
                    <?if (in_array($arResult['PROPERTIES']['Author']['VALUE'], $arResult['checkAuthor'])):?>
                        <mark title="Редактировать объявление">
                            <a class="btn_edit" href="/ads/add.php?CODE=<?=$arResult['ID'];?>"></a>
                        </mark>
                        <mark title="Удалить объявление">
                            <a href="#" class="btn_delete det_del">&#215;</a>
                        </mark>
                    <?endif;?>
                </div>
                <?/*<div class="ad_links">
                    <a href="#">Количество объявлений: <span class="ad_count">1</span></a>
                    <a href="#">Добавить объявление</a>
                </div>
                <div class="ad_links_grayed hidden">
                    <div>Пока нет объявлений</div>
                    <a href="#">Добавить объявление</a>
                </div>*/?>
            </div>
            <?//if (CUser::GetID()==$arResult['agent_prop_list']['ID']):?>
            <?if (in_array($arResult['PROPERTIES']['Author']['VALUE'], $arResult['checkAuthor'])):?>
                <form action="/ajax/delads.php" method="POST" class="delete_form hidden">
                    <div class="form_row">
                        <input type="hidden" name="CODE" value="<?=$arResult['ID'];?>" />
                    </div> 
                    <div class="input_with_error error">
                        <span class="input_error"></span>
                        <input class="content_input_long" size="25" type="text" name="reason" placeholder="Укажите причину удаления"/>
                        <span class="starrequired">*</span>
                    </div>    
                    <div class="form_row_btn">
                        <input type="submit" value="Удалить"/>
                        <input type="button" value="Отмена" class="cancel_del"/>
                    </div>
                <span class="success_del"></span>
                </form>
            <?endif;?>
        </div>
    </div>
    <div class="content_footer_column">
        <div class="content_footer_row">
            <div class="row_head">
                Агент:
            </div>
            <div class="row_content">
                <div class="agent_contacts">
                    <div class="agent_name">
                        <?=$arResult['agent_prop_list']['NAME'].' '.$arResult['agent_prop_list']['LAST_NAME'] ?>
                    </div>
                    <div class="agent_phone">
                    <?
                        //$patterns[0] = "|[^\d\w ]+|i";
                        $patterns[0] = "/[()-]+/";
                        $patterns[1] = "|[\s]+|i";
                    ?>
                        <a class="agent_phone" href="callto:<?=preg_replace($patterns,"",$arResult['agent_prop_list']['PERSONAL_PHONE']);?>"><?=$arResult['agent_prop_list']['PERSONAL_PHONE'];?></a>
                    </div>
                        <a class="agent_email" href="mailto:<?=$arResult['agent_prop_list']['EMAIL'];?>"><?=$arResult['agent_prop_list']['EMAIL'];?></a>
                </div>
            </div>
        </div>
    </div>
</div>

<?//pre($arResult);?>