 <?php
    $this->SetViewTarget('filter'); //Вывод в header.php в теге <main>-><div class="filter_wrapper">
?> 

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="filter">
    <form action="<?=$arResult["FORM_ACTION"]?>">
        <div class="filter_left_block">
            <div class="filter_row">
                <input class="keywords" name="q" type="text" placeholder="Поиск по ключевым словам">
                <input id="cphoto" class="with_photo" type="checkbox">
                <label for="cphoto">с фото</label>
            </div>
            <!-- -->
            <div class="filter_bottom">
                <div class="filter_row">
                    <span class="price_text">Цена:</span>
                    <div class="price_dragger"></div>
                    <input class="price min_price" type="text">
                    <span class="price_separator">&mdash;</span>
                    <input class="price max_price" type="text">
                </div>
                <div class="filter_row">
                    <span class="date_text">Дата подачи объявления</span>
                    <input class="date" id="datefrom" type="text">
                    <input type="button" class="calendar_btn" value="">
                    <span>до</span>
                    <input class="date" id="dateto" type="text">
                    <input type="button" class="calendar_btn" value="">
                </div>
            </div>
        </div><!-- /filter_left_block -->
        <div class="filter_right_block">
        </div>
        <div class="filter_footer">
            <input class="button find_btn" name="s" type="submit" value="<?=GetMessage("BSF_T_SEARCH_BUTTON");?>" />
        </div>
    </form>
</div>

<?php
    $this->EndViewTarget();
?>