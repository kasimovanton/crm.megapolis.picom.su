<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?//pre($arResult)?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?
// pre($arResult,false);?>

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <div class="client_wrapper" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="client_search_card">
            <div class="client_contacts">
                <div class="client_name">
                    <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME"]?></a>
                </div>
                <?if ($arItem['DISPLAY_PROPERTIES']['PHONE']['DISPLAY_VALUE']):?>
                    <div class="agent_phone"><?=$arItem['DISPLAY_PROPERTIES']['PHONE']['DISPLAY_VALUE']?></div>
                <?endif?>
                <?if ($arItem['DISPLAY_PROPERTIES']['DOP_PHONE']['DISPLAY_VALUE']):?>
                    <div class="agent_phone agent_phone_extra"><?=$arItem['DISPLAY_PROPERTIES']['DOP_PHONE']['DISPLAY_VALUE']?>
                        <?if($arItem['DISPLAY_PROPERTIES']['DOP_FACE']['DISPLAY_VALUE']):?>
                            <br/>
                            <i><?=$arItem['DISPLAY_PROPERTIES']['DOP_FACE']['DISPLAY_VALUE'];?></i>
                        <?endif;?>
                    </div>
                <?endif?>
                <?if ($arItem['DISPLAY_PROPERTIES']['EMAIL']['DISPLAY_VALUE']):?>
                    <a class="agent_email" href="mailto:<?=$arItem['DISPLAY_PROPERTIES']['EMAIL']['DISPLAY_VALUE']?>"><?=$arItem['DISPLAY_PROPERTIES']['EMAIL']['DISPLAY_VALUE']?></a>
                <?endif?>
            </div>
            <div class="client_links">
                <div class="client_ads">
                    <div class="ads_ico"></div>
                    <a href="/ads/?Client=<?=$arItem['ID'];?>" class="client_ads_link">
                        Объявления
                        <?/*$arResult[TOTAL_ADS] - формируется в result_modifier*/?>
                        (<?if(isset($arResult['TOTAL_ADS'][$arItem['ID']])):?><?=$arResult['TOTAL_ADS'][$arItem['ID']];?><?else:?>0<?endif;?>)
                    </a>
                </div>
            </div>
            <div class="client_description">
                <p><?echo $arItem["PREVIEW_TEXT"];?></p>
            </div>
            <div class="client_join_date">
                <span>Добавлен</span>
                <span><?=$arItem['DISPLAY_ACTIVE_FROM']?></span>
                <div class="calendar_ico"></div>
            </div>
            <?if ($arItem['EDIT']):?>
                <mark title="Редактировать данные клиента">
                    <a href="/clients/add.php?edit=Y&CODE=<?=$arItem['ID']?>" class="button btn_edit"></a>
                </mark>
                <?/*<a href="/clients/add.php?delete=Y&CODE=<?=$arItem['ID']?>&<?=bitrix_sessid_get()?>"
                   class="button btn_delete"
                   onClick="return confirm('<?echo CUtil::JSEscape(str_replace("#ELEMENT_NAME#", $arItem["NAME"], GetMessage("IBLOCK_ADD_LIST_DELETE_CONFIRM")))?>')">
                    <?=GetMessage("IBLOCK_ADD_LIST_DELETE")?>
                </a>*/?>
            <?endif?>
        </div>
    </div>
<?endforeach;?>
<?if (count($arResult["ITEMS"])==0):?>
    <p>Клиенты не найдены</p>
<?endif?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

