<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
//pre($arResult);
if ($arResult['MESSAGE']) {
    echo $arResult['MESSAGE'];
    return;
}

?>
<div class="client_wrapper" >
    <?if (isset($_REQUEST['errorMsg']) && !empty($_REQUEST['errorMsg'])):?>
        <span class="error">
            <?=$_REQUEST['errorMsg'];?>
        </span>
     <?endif;?>
    <div class="client_card">
        <div class="client_contacts">
            <div class="client_name">
                <?echo $arResult["NAME"]?>
            </div>
            <?if ($arResult['DISPLAY_PROPERTIES']['PHONE']['DISPLAY_VALUE']):?>
                <div class="agent_phone"><?=$arResult['DISPLAY_PROPERTIES']['PHONE']['DISPLAY_VALUE']?></div>
            <?endif?>
            <?if ($arResult['DISPLAY_PROPERTIES']['DOP_PHONE']['DISPLAY_VALUE']):?>
                <div class="agent_phone agent_phone_extra">
                    <?=$arResult['DISPLAY_PROPERTIES']['DOP_PHONE']['DISPLAY_VALUE']?>
                    <?if($arResult['DISPLAY_PROPERTIES']['DOP_FACE']['DISPLAY_VALUE']):?>
                        <br/>
                        <i><?=$arResult['DISPLAY_PROPERTIES']['DOP_FACE']['DISPLAY_VALUE'];?></i>
                    <?endif;?>
                </div>
            <?endif?>
            <?if ($arResult['DISPLAY_PROPERTIES']['EMAIL']['DISPLAY_VALUE']):?>
                <a class="agent_email" href="mailto:<?=$arResult['DISPLAY_PROPERTIES']['EMAIL']['DISPLAY_VALUE']?>"><?=$arResult['DISPLAY_PROPERTIES']['EMAIL']['DISPLAY_VALUE']?></a>
            <?endif?>
        </div>
        <div class="client_links">
            <?if (!$arResult['EDIT']):?>
                <div class="client_ads">
                    <div class="ads_ico"></div>
                    <a href="/agents/<?=$arResult['DISPLAY_PROPERTIES']['RIELTOR']['VALUE']?>/" class="client_objects_link">Агент клиента</a>
                </div>
            <?endif?>
            <div class="client_ads">
                <div class="objects_ico"></div>
                <a href="/ads/?Client=<?=$arResult['ID']?>" class="client_ads_link">
                Объявления 
                    (<?if(isset($arResult['TOTAL_ADS'][$arResult['ID']])):?><?=$arResult['TOTAL_ADS'][$arResult['ID']];?><?else:?>0<?endif;?>)
                </a>
                <?if ($arResult['EDIT']):?><a href="/ads/add.php?Сlient=<?=$arResult['ID']?>" class="button button_universal">Добавить объявление</a><?endif?>
            </div>
        </div>
        <div class="client_join_date">
            <span>Добавлен</span>
            <span><?=$arResult['DISPLAY_ACTIVE_FROM']?></span>
            <div class="calendar_ico"></div>
        </div>
        <div class="client_description">
            <p><?echo $arResult["PREVIEW_TEXT"];?></p>
        </div>
        <?if ($arResult['REAL_EDIT']):?>
            <mark title="Редактировать данные клиента">
                <a href="/clients/add.php?edit=Y&CODE=<?=$arResult['ID']?>" class="button btn_edit"></a>
            </mark>
            <?if(!isset($arResult['TOTAL_ADS'][$arResult['ID']])):?>
                <a href="#" class="btn_delete det_del">Удалить</a>
                <form action="/clients/del.php" method="POST" class="delete_form hidden">
                    <div class="form_row">
                        <input type="hidden" name="CODE" value="<?=$arResult['ID'];?>" />
                    </div> 
                    <div class="input_with_error error">
                        <span class="input_error"></span>
                        <input class="content_input_long" size="25" type="text" name="reason" placeholder="Укажите причину удаления"/>
                        <span class="starrequired">*</span>
                    </div>    
                    <div class="form_row_btn">
                        <input type="submit" value="Удалить"/>
                        <input type="button" value="Отмена" class="cancel_del"/>
                    </div>
                    <span class="success_del"></span>
                </form>
            <?endif;?>
            <?/*<a href="/clients/add.php?delete=Y&CODE=<?=$arResult['ID']?>&<?=bitrix_sessid_get()?>"
               class="button btn_delete"
               onClick="return confirm('<?echo CUtil::JSEscape(str_replace("#ELEMENT_NAME#", $arResult["NAME"], GetMessage("IBLOCK_ADD_LIST_DELETE_CONFIRM")))?>')">
                <?=GetMessage("IBLOCK_ADD_LIST_DELETE")?>
            </a>*/?>
        <?endif?>
    </div>
</div>
<?pre($arResult, false);?>