<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if($arParams["USE_RSS"]=="Y"):?>
	<?
	if(method_exists($APPLICATION, 'addheadstring'))
		$APPLICATION->AddHeadString('<link rel="alternate" type="application/rss+xml" title="'.$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["rss"].'" href="'.$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["rss"].'" />');
	?>
	<a href="<?=$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["rss"]?>" title="rss" target="_self"><img alt="RSS" src="<?=$templateFolder?>/images/gif-light/feed-icon-16x16.gif" border="0" align="right" /></a>
<?endif?>
<?if($arParams["USE_SEARCH"]=="Y"):?>
<?$this->SetViewTarget("filter");?>
<?$APPLICATION->IncludeComponent("bitrix:search.title","",Array(
        "SHOW_INPUT" => "Y",
        "INPUT_ID" => "title-search-input",
        "CONTAINER_ID" => "title-search",
        "PRICE_CODE" => array(),
        "PRICE_VAT_INCLUDE" => "Y",
        "PREVIEW_TRUNCATE_LEN" => "150",
        "SHOW_PREVIEW" => "N",
        "PREVIEW_WIDTH" => "30",
        "PREVIEW_HEIGHT" => "30",
        "CONVERT_CURRENCY" => "Y",
        "CURRENCY_ID" => "RUB",
        "PAGE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["search"],
        "NUM_CATEGORIES" => "1",
        "TOP_COUNT" => "10",
        "ORDER" => "date",
        "USE_LANGUAGE_GUESS" => "N",
        "CHECK_DATES" => "Y",
        "SHOW_OTHERS" => "N",
        "CATEGORY_0_TITLE" => "Клиенты",
        "CATEGORY_0" => array("iblock_directories"),   
        "CATEGORY_0_iblock_directories" => array("10"),
        "CATEGORY_OTHERS_TITLE" => "Другое"
    ),
	$component
);?>
<?$this->EndViewTarget();?>
<?endif?>
<?if($arParams["USE_FILTER"]=="Y"):?>
<?$this->SetViewTarget("filter");?>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.filter",
	"",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"FILTER_NAME" => $arParams["FILTER_NAME"],
		"FIELD_CODE" => $arParams["FILTER_FIELD_CODE"],
		"PROPERTY_CODE" => $arParams["FILTER_PROPERTY_CODE"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
	),
	$component
);
?>
<?$this->EndViewTarget();?>
<?endif?>
<?
$curLink = $APPLICATION->GetCurPageParam('',array());//'clear_cache','login','logout','forgot_password','change_password','back_url',
$sortArr = array(
    "REFERENCE" =>
        array(
            "по алфавиту от А до Я",
            "по алфавиту от Я до А",
            "по дате добавления от 1 до N",
            "по дате добавления от N до 1"
            ),
    "REFERENCE_ID" =>
        array(
            $APPLICATION->GetCurPageParam('filter='.$_GET['filter'].'&sort=name&order=asc', array('filter','sort', 'order')),
            $APPLICATION->GetCurPageParam('filter='.$_GET['filter'].'&sort=name&order=desc', array('filter','sort', 'order')),
            $APPLICATION->GetCurPageParam('filter='.$_GET['filter'].'&sort=created&order=asc', array('filter','sort', 'order')),
            $APPLICATION->GetCurPageParam('filter='.$_GET['filter'].'&sort=created&order=desc', array('filter','sort', 'order')),
            ),
    );
$userID=$USER->GetID();
$users = getUsers(array("UF_HEAD"),array("ID",'LAST_NAME','SECOND_NAME','NAME'),array('ACTIVE'=>'Y'),"LAST_NAME");
//pre($users);

// $childUsersId = getChildsId($userID,$users);
$childUsersIdTree = getChildsId($userID,$users,false,true);

$filterArr = array(
    "REFERENCE" =>
        array(
            'Я',//$users[$userID]['LAST_NAME'].' '.$users[$userID]['NAME'].' '.$users[$userID]['SECOND_NAME'],
            'Любой',
        ),
    "REFERENCE_ID" =>
        array(
            $APPLICATION->GetCurPageParam("filter={$userID}&sort={$_GET['sort']}&order={$_GET['order']}", array('filter','sort','order')),
            $APPLICATION->GetCurPageParam("filter=0&sort={$_GET['sort']}&order={$_GET['order']}", array('filter','sort','order')),
        ),
);
foreach ($childUsersIdTree as $childId => $lvlTree) {
    $childUsersId[] = $childId;//массив пользователей для фильтра (строка 123)
    $treeStr = "-";
    for ($i=0; $i<$lvlTree; $i++) $treeStr .= "-";
    $filterArr['REFERENCE'][] = $treeStr.$users[$childId]['LAST_NAME'].' '.$users[$childId]['NAME'].' '.$users[$childId]['SECOND_NAME'];
    $filterArr['REFERENCE_ID'][] =  $APPLICATION->GetCurPageParam('filter='.$childId, array('filter'));
}
// pre($filterArr,false);
?>
<div class="search_menu">
    <div class="sorting">
        <span>Сортировать:</span>
        <div class="form_select sort">
            <?echo SelectBoxFromArray("sort", $sortArr, $curLink)?>
        </div>
    </div>
    <div class="sorting">
        <span>Агент:</span>
        <div class="form_select sort">
            <?echo SelectBoxFromArray("filter", $filterArr, $curLink)?>
        </div>
    </div>
    <a class="button button_universal" href="/clients/add.php">Добавить клиента</a>
</div>
<?
global $arrFilter;

if ($_GET['filter']) {
    if (in_array($_GET['filter'],array_merge($childUsersId,array($userID)))) {
        $arrFilter['PROPERTY_RIELTOR'] = $_GET['filter'];
    } else {
        $arrFilter['PROPERTY_RIELTOR'] = $userID;
    }
} else {
    if ($_GET['filter']!='0') {
        $arrFilter['PROPERTY_RIELTOR'] = $userID;
    } else {
        if (empty($childUsersId)){
            $arrFilter['PROPERTY_RIELTOR'] = $userID;
        } else {
            $arrFilter['PROPERTY_RIELTOR'] = array_merge($childUsersId,array($userID));
        }
    }
}
// pre ($arrFilter, false);
?>


<?if (isset($_REQUEST['sort']) && !empty($_REQUEST['sort'])){
    $sort = $_REQUEST['sort'];
} else {
    $sort = 'NAME';
}
if (isset($_REQUEST['order']) && !empty($_REQUEST['order'])){
    $order = $_REQUEST['order'];
} else {
    $order = 'ASC';
}
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"",
	Array(
		"IBLOCK_TYPE"	=>	$arParams["IBLOCK_TYPE"],
		"IBLOCK_ID"	=>	$arParams["IBLOCK_ID"],
		"NEWS_COUNT"	=>	$arParams["NEWS_COUNT"],
		"SORT_BY1"	=>	$sort,
		"SORT_ORDER1"	=>	$order,
		"SORT_BY2"	=>	$arParams["SORT_BY2"],
		"SORT_ORDER2"	=>	$arParams["SORT_ORDER2"],
		"FIELD_CODE"	=>	$arParams["LIST_FIELD_CODE"],
		"PROPERTY_CODE"	=>	$arParams["LIST_PROPERTY_CODE"],
		"DETAIL_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
		"SECTION_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"IBLOCK_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
		"DISPLAY_PANEL"	=>	$arParams["DISPLAY_PANEL"],
		"SET_TITLE"	=>	$arParams["SET_TITLE"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"INCLUDE_IBLOCK_INTO_CHAIN"	=>	$arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
		"CACHE_TYPE"	=>	$arParams["CACHE_TYPE"],
		"CACHE_TIME"	=>	$arParams["CACHE_TIME"],
		"CACHE_FILTER"	=>	$arParams["CACHE_FILTER"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"DISPLAY_TOP_PAGER"	=>	$arParams["DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER"	=>	$arParams["DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE"	=>	$arParams["PAGER_TITLE"],
		"PAGER_TEMPLATE"	=>	$arParams["PAGER_TEMPLATE"],
		"PAGER_SHOW_ALWAYS"	=>	$arParams["PAGER_SHOW_ALWAYS"],
		"PAGER_DESC_NUMBERING"	=>	$arParams["PAGER_DESC_NUMBERING"],
		"PAGER_DESC_NUMBERING_CACHE_TIME"	=>	$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
		"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
		"DISPLAY_DATE"	=>	$arParams["DISPLAY_DATE"],
		"DISPLAY_NAME"	=>	"Y",
		"DISPLAY_PICTURE"	=>	$arParams["DISPLAY_PICTURE"],
		"DISPLAY_PREVIEW_TEXT"	=>	$arParams["DISPLAY_PREVIEW_TEXT"],
		"PREVIEW_TRUNCATE_LEN"	=>	$arParams["PREVIEW_TRUNCATE_LEN"],
		"ACTIVE_DATE_FORMAT"	=>	$arParams["LIST_ACTIVE_DATE_FORMAT"],
		"USE_PERMISSIONS"	=>	$arParams["USE_PERMISSIONS"],
		"GROUP_PERMISSIONS"	=>	$arParams["GROUP_PERMISSIONS"],
		"FILTER_NAME"	=>	'arrFilter',
		"HIDE_LINK_WHEN_NO_DETAIL"	=>	$arParams["HIDE_LINK_WHEN_NO_DETAIL"],
		"CHECK_DATES"	=>	$arParams["CHECK_DATES"],
	),
	$component
);?>
