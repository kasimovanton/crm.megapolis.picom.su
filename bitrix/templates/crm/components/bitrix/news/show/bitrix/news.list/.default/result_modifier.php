<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arOrder = Array("SORT"=>"asc", "ID"=>"asc");
$arSelect = Array("ID", "NAME", "PROPERTY_PHONE");
$arFilter = Array("IBLOCK_ID"=>10, "ID" => CIBlockElement::SubQuery("PROPERTY_Client", array(
                                                "IBLOCK_ID" => 5,
                                                "PROPERTY_IS_NEED" => 564,
                                              )),);
$res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

$arClients = array();

while ($ob = $res->GetNextElement()){
    $statusFields = $ob->GetFields();
    $arClients[$statusFields['ID']] = $statusFields;
}

$arResult['CLIENTS'] = $arClients;

