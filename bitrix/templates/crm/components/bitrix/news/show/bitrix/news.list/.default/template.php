<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="search_results">
    <?if(count($arResult["ITEMS"]) >0):?>
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="entry entry_list entry_show" data-variant="<?=$arItem['ID'];?>">
                <div class="entry_list__item entry_block_status">
                    <div class="info_contact">
                        <span class="calendar_ico"></span>
                        <span class="entry_item__inf">
                            <i class="entry_item__founder">Создано оператором:</i><br>
                            <i><?=$arItem['ACTIVE_FROM'];?></i>
                        </span>
                    </div>
                    <div>
                        <a href="#" onclick="if (BX.IM){BXIM.openMessenger(<?=$arItem['CREATED_BY'];?>); return false;}">Чат с оператором</a>
                    </div>
                </div>
                <div class="entry_list__item entry_block_info">
                    <div class="head">Время показа</div>
                    <?if (!empty($arItem['PROPERTIES']['SHOW_TIME']['VALUE'])):?>
                        <?php
                            $lateFlag = false;
                            $arr = ParseDateTime($arItem['PROPERTIES']['SHOW_TIME']['VALUE'], FORMAT_DATETIME);
                            $checkPointA = timeExplode($arItem['PROPERTIES']['SHOW_TIME']['VALUE']);
                            if ($checkPointA - timeExplode(date("d.m.Y H:i:s"))<0){
                                $lateFlag = true;
                            }
                        ?>
                        <div class="date<?if ($lateFlag):?> danger<?endif;?>"><?=$arr["DD"]." ".ToLower(GetMessage("MONTH_".intval($arr["MM"])."_S"))." ".$arr["YYYY"];?> г.</div>
                        <?php
                            $timeString = date("H:i",  MakeTimeStamp($arItem['PROPERTIES']['SHOW_TIME']['VALUE'], "DD.MM.YYYY HH:MI:SS"))
                        ?>
                        <?if ($timeString != "00:00"):?>
                            <div class="time<?if ($lateFlag):?> danger<?endif;?>"><?=$timeString;?></div>
                        <?endif;?>
                        <?if ($lateFlag):?>
                            <i class="late danger">просрочено</i>
                        <?endif?>
                    <?endif;?>
                </div>
                <div class="entry_list__item entry_block_description">
                    <div class="head">Ваш объект для показа:</div>
                    <div class="accomplice">
                        <a target="_blank" href="/ads/prodam/<?=$arItem['PROPERTIES']['ADVERT']['VALUE'];?>/"><?=getElemName($arItem['PROPERTIES']['ADVERT']['VALUE']);?></a>
                    </div>
                    <div class="head">Клиент:</div>
                    <?php
                        $db_props = CIBlockElement::GetProperty(5, $arItem['PROPERTIES']['NEEDS']['VALUE'], array("sort" => "asc"), Array("CODE"=>"Client"));
                        if($ar_props = $db_props->Fetch())
                            $CLIENT_ID = IntVal($ar_props["VALUE"]);
                        else
                            $CLIENT_ID = false;
                    ?>
                    <?if($CLIENT_ID):?>
                        <?=$arResult['CLIENTS'][$CLIENT_ID]['PROPERTY_PHONE_VALUE'];?>, <?=$arResult['CLIENTS'][$CLIENT_ID]['NAME'];?>
                    <?else:?>
                        Отсутствуют
                    <?endif;?>
                </div>
                <div class="entry_list__item entry_block_panel">
                    <?if($arItem['PROPERTIES']['SHOW_STATUS']['VALUE_ENUM_ID']==565):?>
                        <div class="entry_buttons_object new_show_status">
                            <ul>
                                <li class="min-item">
                                    <button class="button button_universal" data-status="N">Отказ от показа</button>
                                </li>
                                <li class="min-item">
                                    <button class="button button_universal" data-status="Y">Объект показан</button>
                                </li>
                            </ul>
                        </div>
                    <?else:?>
                        <div class="status_var_name <?=$arItem['PROPERTIES']["SHOW_STATUS"]['VALUE_XML_ID'];?>">
                            <?=$arItem['PROPERTIES']['SHOW_STATUS']['VALUE'];?>
                        </div>
                        <?=$arItem['DETAIL_TEXT'];?>
                    <?endif;?>
                </div>
            </div>
        <?endforeach;?>
    <?else:?>
        На данный момент запросы на показ отсутствуют.
    <?endif;?>
    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <br /><?=$arResult["NAV_STRING"]?>
    <?endif;?>
</div>
<div style="display:none">
    <div id="show_comment" class="show_comment popup_ad">
        <h2></h2>
        <input type="hidden" name="new_show_status">
        <input type="hidden" name="variant_id">
        <section class="comment_block">
            <div>
                <span class="error">Заполните комментарий</span>
                <textarea placeholder="Комментарий*" name="show_comment"></textarea>
            </div>
            <a href="#" class="close_button button button_universal">Отмена</a>
            <a href="#" class="change_show_status button button_universal">Подтвердить</a>
        </section>
    </div>
</div>