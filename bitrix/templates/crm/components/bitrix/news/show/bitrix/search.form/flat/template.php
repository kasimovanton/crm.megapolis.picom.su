<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
 <?php
   $this->SetViewTarget('filter'); //Вывод в header.php в теге <main>-><div class="filter_wrapper">
?> 
    <div class="button_panel panel_show">
        <?if (!isset($_SESSION['tab_show'])||(isset($_SESSION['tab_show']) && $_SESSION['tab_show']=="show")):?>
            <span class="tab _active">На показ</span>
        <?else:?>
            <a class="tab" data-type="show" href="#">На показ</a>
        <?endif;?>
        
        <?if (isset($_SESSION['tab_show']) && $_SESSION['tab_show']=="shown"):?>
            <span class="tab _active">Показано</span>
        <?else:?>
            <a class="tab" data-type="shown" href="#">Показано</a>
        <?endif;?>
        
    </div>
<?php
    $this->EndViewTarget();
?>