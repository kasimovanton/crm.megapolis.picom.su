<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="search_menu">
    <div class="sorting">
        <?/*<span>Сортировать:</span>*/?>
        <span>Элементов на странице:</span>
        <div class="form_select paging">
            <select class="short">
                <option <?if($_SESSION['paging'] == 5):?>selected<?endif;?> numb="5">5</option>
                <option <?if($_SESSION['paging'] == 10 || !isset($_SESSION['paging'])):?>selected<?endif;?> numb="10">10</option>
                <option <?if($_SESSION['paging'] == 20):?>selected<?endif;?> numb="20">20</option>
                <option <?if($_SESSION['paging'] == 50):?>selected<?endif;?> numb="50">50</option>
                <option <?if($_SESSION['paging'] == 100):?>selected<?endif;?> numb="100">100</option>
            </select>
        </div>
    </div>
</div>
<div class="search_results">
    <?if (count($arResult["ITEMS"])==0):?>
        На данный момент у Вас нет активных объектов
    <?else:?>
        <?php
            $client_list = getEnumValues(16, array("Client"));
        ?>
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?php
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <?
                $name_client = $client_list['Client'][$arItem['PROPERTIES']['Client']['VALUE']]; //result_modifier
                $db_props = CIBlockElement::GetProperty(10, $arItem['PROPERTIES']['Client']['VALUE'], array("sort" => "asc"), Array("CODE"=>"PHONE"));
                if($ar_props = $db_props->Fetch()){
                    $phone_client = $ar_props["VALUE"];
                } else {
                    $phone_client = "Номер отсутствует";
                }
            ?>
            <div class="entry entry_list entry_object" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <div class="entry_list__item entry_block_info">
                    <div class="head"><?=$arResult['STATUS_LIST'][$arItem['PROPERTIES']['STATUS']['VALUE']]['PROPERTY_TITLE_VALUE'];?></div>
                    <?if (!empty($arItem['PROPERTIES']['CURRENT_DATE_OPTION']['VALUE'])):?>
                        <?php
                            $lateFlag = false;
                            $arr = ParseDateTime($arItem['PROPERTIES']['CURRENT_DATE_OPTION']['VALUE'], FORMAT_DATETIME);
                            $checkPointA = timeExplode($arItem['PROPERTIES']['CURRENT_DATE_OPTION']['VALUE']);
                            if ($checkPointA - timeExplode(date("d.m.Y H:i:s"))<0){
                                $lateFlag = true;
                            }
                        ?>
                        <div class="date<?if ($lateFlag):?> danger<?endif;?>"><?=$arr["DD"]." ".ToLower(GetMessage("MONTH_".intval($arr["MM"])."_S"))." ".$arr["YYYY"];?> г.</div>
                        <?php
                            $timeString = date("H:i",  MakeTimeStamp($arItem['PROPERTIES']['CURRENT_DATE_OPTION']['VALUE'], "DD.MM.YYYY HH:MI:SS"))
                        ?>
                        <?if ($timeString != "00:00"):?>
                            <div class="time<?if ($lateFlag):?> danger<?endif;?>"><?=$timeString;?></div>
                        <?endif;?>
                        <?if ($lateFlag):?>
                            <i class="late danger">просрочено</i>
                        <?endif?>
                    <?endif;?>
                </div>
                <div class="entry_list__item entry_block_status">
                    <?if ($arItem['PROPERTIES']['OPEN_DATA']['VALUE'][0] == "Да"):?>
                        <span class="entry_client_link">
                            <?=$name_client;?>
                        </span>    
                        <div class="entry_client__phone">(<?=$phone_client;?>)</div>
                    <?else:?>
                        Данные клиента скрыты
                    <?endif;?>
                </div>
                <div class="entry_list__item entry_block_description">
                        <div class="head"><?=$arItem['PROPERTIES']['Street']['VALUE'];?> <?=$arItem['PROPERTIES']['HOUSE']['VALUE'];?><?if (!empty($arItem['PROPERTIES']['APARTMENT']['VALUE'])):?>, кв. <?=$arItem['PROPERTIES']['APARTMENT']['VALUE'];?><?endif;?></div>
                        <div>
                            <span>
                                квартира <?=$arItem['PROPERTIES']['Rooms']['VALUE'];?> к. 
                                <?if ($arItem['PROPERTIES']['Square']['VALUE']):?><?=$arItem['PROPERTIES']['Square']['VALUE'];?> м<sup>2</sup> <?endif;?>
                                <?=$arItem['PROPERTIES']['Floor']['VALUE'];?><?=$arItem['PROPERTIES']['Floors']['VALUE'] ? '/'.$arItem['PROPERTIES']['Floors']['VALUE'] : " этаж" ;?>
                            </span>
                            <span><?=getElemName($arItem['PROPERTIES']['District']['VALUE']);?> р-н</span>
                            <span><?=$arItem['PROPERTIES']['MarketType']['VALUE'];?>, <?=$arItem['PROPERTIES']['HouseType']['VALUE'];?></span>
                        </div>
                        <div class="entry_item__price"><?=number_format($arItem['PROPERTIES']['PRICE']['VALUE'], 0, ',', ' ');?> руб.</div>
                        <div class="accomplice">
                            <a href="/agents/<?=$arItem['PROPERTIES']['AUTHOR']['VALUE'];?>/"><?=$arResult['USER_LIST'][$arItem['PROPERTIES']['AUTHOR']['VALUE']]['FULLNAME'];?></a>
                        </div>
                </div>
                <div class="entry_list__item entry_block_panel">
                    <?php
                        $dayObject = date("d.m.Y H:m",  MakeTimeStamp($arItem['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS"))
                    ?>
                    <div class="info_contact">
                        <span class="entry_item__inf">
                            <i class="entry_item__founder">Создано офис-менеджером:</i><br>
                            <i><?=$dayObject?></i>
                        </span>
                        <span class="calendar_ico"></span>
                    </div>
                    <?if (empty($arItem['PROPERTIES']['IS_WATCH']['VALUE'])):?>
                        <div class="entry_buttons_object">
                            <a href="#" data-element="<?=$arItem['ID'];?>" class="button button_universal button_move">Переместить в просмотренные</a>
                        </div>
                    <?else:?>
                        <div class="entry_buttons_object">
                            Осмотрено
                        </div>
                    <?endif;?>
                </div>
								
								<div class="create_ad">
									<ul>
										<li><a href="#"><span><img src=""></span>Редактировать фото</a></li>
										<li><a href="#"><span><img src=""></span>Согласие риэлтора</a></li>
										<li><a href="#"><span><img src=""></span>Слайд шоу</a></li>
										<li><a href="#"><span><img src=""></span>Создать объявление</a></li>
									</ul>
								</div>
            </div>
        <?endforeach;?>
        <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
            <br /><?=$arResult["NAV_STRING"]?>
        <?endif;?>
    <?endif;?>
</div>
<div style="display: none;">
        <div class="popup-client place_watched" id="placeWatched">
            <div class="warning">
                <h2>Добавить фотографии</h2>
            </div>
            <div class="form form_status">
                <a href="#" class="button button_universal add_image">Добавить</a>
                <div class="second-preloader" style="display:none;"></div>
            </div>
        </div>
</div>
<?//pre($arResult, false);?>
