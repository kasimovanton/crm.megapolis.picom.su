 <?php
   $this->SetViewTarget('filter'); //Вывод в header.php в теге <main>-><div class="filter_wrapper">
?> 
<script>

    maxprice = <?=$_REQUEST['for_script'];?>;
    step = 1e5;
    firstName = 'min_price';
    secondName = 'max_price';
    
    if (maxprice<100000){
        step = maxprice/2;
    } else {
        if (maxprice < 1000000){
            step = 1e4;
        } else {
            step = 1e5;
        }
    }

    <?if (isset($_REQUEST['min_price']) && !empty($_REQUEST['min_price'])):?>
        nowFirPrice = '<?=$_REQUEST['min_price']?>';
    <?else:?>
        nowFirPrice = 0;
    <?endif;?>
    <?if (isset($_REQUEST['max_price']) && !empty($_REQUEST['max_price'])):?>
        nowSecPrice = '<?=$_REQUEST['max_price']?>';
    <?else:?>
        nowSecPrice = maxprice;
    <?endif;?>
    
    $(document).ready(function() {
        if(maxprice!=0 && maxprice !== null){
            $(".price_dragger").slider({
                range:!0,
                min:0,
                max: maxprice,
                step: step,
                values: [ nowFirPrice, nowSecPrice ],
                slide:function(a,b){
                    $(".min_price").val(b.values[0]).attr('name', firstName);
                    $(".max_price").val(b.values[1]).attr('name', secondName);
                }
            });
            $(".min_price").val($(".price_dragger").slider("values",0));
            $(".max_price").val($(".price_dragger").slider("values",1));
            $('.price_row').show();
        }
    });
</script>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="filter filter_objects">
    <form action="<?=$arResult["FORM_ACTION"]?>">
        <div class="filter_left_block">
            <div class="filter_row">
                <div class="form_select city">
                    <select class="city" name="City">
                        <option value="0" selected>Любой город</option>
                        <?foreach($arResult["VALUE_FORM_FIELDS"]['City'] as $key => $option):?>
                            <option value="<?=$key?>" <?if($_REQUEST['City'] == $key):?>selected<?endif;?>><?=$option?></option>
                        <?endforeach;?>
                    </select>
                </div>
               <div class="form_select district<?if ((isset($_REQUEST['District'])&&!empty($_REQUEST['District'])) || (isset($_REQUEST['City'])&& $_REQUEST['City'] == 49)):?><?else:?> hidden<?endif;?>">
                    <select id="district" name="District[]" multiple="multiple" placeholder="Район">
                        <?php
                            if ((isset($_REQUEST['District'])&&!empty($_REQUEST['District'])) || (isset($_REQUEST['City'])&& $_REQUEST['City'] == 49)){
                                $dist_list = getEnumValues(5, array("District"));
                                if (!empty($dist_list)){
                                    echo setHtmlMultiSelectOption($dist_list);
                                }
                            }
                        ?>
                     </select>
                </div>
                <div class="form_select rooms">
                    <select name="Rooms[]" id="rooms" multiple="multiple" placeholder="Количество комнат">
                        <?php
                            $dist_list = getEnumValues(16, array("Rooms"));
                            if (!empty($dist_list)){
                                echo setHtmlMultiSelectOption($dist_list);
                            }
                        ?>
                    </select>
                </div>
                <div class="form_select rieltor">
                    <select class="rieltor" name="Rieltor">
                        <option value="0" selected>Любой риэлтор</option>
                        <?foreach($arResult['all_users'] as $key => $option):?>
                            <option value="<?=$key?>" <?if($_REQUEST['Rieltor'] == $key):?>selected<?endif;?>><?=$option['LAST_NAME'].' '.$option['NAME'].' '.$option['SECOND_NAME']?></option>
                        <?endforeach;?>
                    </select>
                </div>
            </div>
        </div> <!-- /filter_left_block -->
        <div class="filter_right_block">
            <div class="filter_row price_row">
                <span class="price_text">Цена:</span>
                <div class="price_dragger"></div>
                <input class="price min_price" autocomplete="off" type="text" <?if(!empty($_REQUEST['min_price'])):?>name="min_price" value="<?=$_REQUEST['min_price']?>"<?endif;?>>
                <span class="price_separator">&mdash;</span>
                <input class="price max_price" autocomplete="off" type="text" <?if(!empty($_REQUEST['max_price'])):?>name="max_price" value="<?=$_REQUEST['max_price']?>"<?endif;?>>
            </div>
        </div> <!-- /filter_right_block -->
        <div class="filter_footer clearfix">
            <div class="filter_row filter_row__inline clearfix">
                <div class="form_input input_street">
                    <label for="seacrhObject[Street]">Улица</label>
                    <input type="text" name="Street" placeholder="Например: Удмуртская" <?if (isset($_REQUEST['Street'])):?> value="<?=$_REQUEST['Street'];?>"<?endif;?>/>
                </div>
                <div class="form_input input_house">
                    <label for="seacrhObject[HOUSE]">Дом</label>
                    <input type="text" name="seacrhObject[HOUSE]"<?if (isset($_REQUEST['seacrhObject']['HOUSE'])):?> value="<?=$_REQUEST['seacrhObject']['HOUSE'];?>"<?endif;?> />
                </div>
                <div class="form_input input_apartment">
                    <label for="seacrhObject[APARTMENT]">Квартира</label>
                    <input type="text" name="seacrhObject[APARTMENT]"<?if (isset($_REQUEST['seacrhObject']['APARTMENT'])):?> value="<?=$_REQUEST['seacrhObject']['APARTMENT'];?>"<?endif;?> />
                </div>
            </div>
            <div class="filter_row filter_row__inline clearfix">

            </div>
            <div class="filter_row filter_row__inline clearfi">
                <span class="date_text">Дата</span>
                <input class="date" id="datefrom_obj" onclick="BX.calendar({node: this, field: this, bTime: false, bHideTime:false});" type="text"<?if(!empty($_REQUEST['start_date'])):?> value="<?=$_REQUEST['start_date']?>"<?endif;?> name="start_date" readonly="readonly">
                <input type="button" onclick="BX.calendar({node: this, field: datefrom_obj, bTime: false, bHideTime:false});" class="calendar_btn">
                <span>до</span>
                <input class="date dd_locked" id="dateto_obj" onclick="BX.calendar({node: this, field: this, bTime: false, bHideTime:false});" type="text"<?if(!empty($_REQUEST['end_date'])):?> value="<?=$_REQUEST['end_date']?>"<?endif;?> name="end_date" readonly="readonly">
                <input type="button" onclick="BX.calendar({node: this, field: dateto_obj, bTime: false, bHideTime:false});" class="calendar_btn">
                <div class="form_input float_right">
                    <input class="button find_btn" type="submit" value="<?=GetMessage("BSF_T_SEARCH_BUTTON");?>">
                </div>
            </div>
        </div> <!-- /filter_footer -->
    </form>
</div>

<div class="button_panel panel_object">
    <?if (!isset($_SESSION['tab'])||(isset($_SESSION['tab']) && $_SESSION['tab']=="current")):?>
        <span class="tab _active">Текущие</span>
    <?else:?>
        <a class="tab" data-type="current" href="#">Текущие</a>
    <?endif;?>
    
    <?if (isset($_SESSION['tab']) && $_SESSION['tab']=="active"):?>
        <span class="tab _active">Активные</span>
    <?else:?>
        <a class="tab" data-type="active" href="#">Активные</a>
    <?endif;?>

    <?if (isset($_SESSION['tab']) && $_SESSION['tab']=="waiting"):?>
        <span class="tab _active">Ожидание</span>
    <?else:?>
        <a class="tab" data-type="waiting" href="#">Ожидание</a>
    <?endif;?>
    <?if (isset($_SESSION['tab']) && $_SESSION['tab']=="sold"):?>
        <span class="tab _active">Не активные</span>
    <?else:?>
        <a class="tab" data-type="sold" href="#">Не активные</a>
    <?endif;?>
    
</div>
<?php
    $this->EndViewTarget();
?>
