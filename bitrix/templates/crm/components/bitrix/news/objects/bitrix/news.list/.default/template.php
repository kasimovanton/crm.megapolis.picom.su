<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="search_menu">
    <a href="/objects/add.php" class="button button_universal">Добавить объект</a>
    <div class="sorting">
       <?/*<span>Сортировать:</span>*/?>
        <span>Элементов на странице:</span>
        <div class="form_select paging">
            <select class="short">
                <option <?if($_SESSION['paging'] == 5):?>selected<?endif;?> numb="5">5</option>
                <option <?if($_SESSION['paging'] == 10 || !isset($_SESSION['paging'])):?>selected<?endif;?> numb="10">10</option>
                <option <?if($_SESSION['paging'] == 20):?>selected<?endif;?> numb="20">20</option>
                <option <?if($_SESSION['paging'] == 50):?>selected<?endif;?> numb="50">50</option>
                <option <?if($_SESSION['paging'] == 100):?>selected<?endif;?> numb="100">100</option>
            </select>
        </div>
    </div>
</div>
<div class="success_block"><?if(isset($_REQUEST['strMessege']))echo $_REQUEST['strMessege'];?></div>
<div class="search_results">
    <?if (count($arResult["ITEMS"])==0):?>
        Объекты отсутствуют
    <?else:?>
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?if ($arItem['PROPERTIES']['AUTHOR']['VALUE'] != CUser::GetID()) {continue;}?>
            
            
            <?php
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <?
                $client_list = getEnumValues(16, array("Client"));
                
                $link_client = '/clients/'.$arItem['PROPERTIES']['Client']['VALUE'].'/';
                $name_client = $client_list['Client'][$arItem['PROPERTIES']['Client']['VALUE']]; 

            ?>
            <div data-object="<?=$arItem['ID'];?>" class="entry entry_list entry_object" data-element="<?=$arItem['ID'];?>" data-line-color="<?=$arResult['STATUS_LIST'][$arItem['PROPERTIES']['STATUS']['VALUE']]['PROPERTY_COLOR_VALUE'];?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <div class="entry_list__item entry_block_info">
                    <div class="head"><?=$arResult['STATUS_LIST'][$arItem['PROPERTIES']['STATUS']['VALUE']]['PROPERTY_TITLE_VALUE'];?></div>
                    <?if (in_array($arItem['PROPERTIES']['STATUS']['VALUE'], array(8977, 8978, 8979, 11106, 11377))):?>
                        <?=$arItem['PROPERTIES']['CURRENT_COMMENT_OPTION']['VALUE'];?>
                    <?else:?>
                        <?if (!empty($arItem['PROPERTIES']['CURRENT_DATE_OPTION']['VALUE'])):?>
                            <?php
                                $lateFlag = false;
                                $arr = ParseDateTime($arItem['PROPERTIES']['CURRENT_DATE_OPTION']['VALUE'], FORMAT_DATETIME);
                                $checkPointA = timeExplode($arItem['PROPERTIES']['CURRENT_DATE_OPTION']['VALUE']);
                                if ($checkPointA - timeExplode(date("d.m.Y H:i:s"))<0){
                                    $lateFlag = true;
                                }
                            ?>
                            <div class="date<?if ($lateFlag):?> danger<?endif;?>"><?=$arr["DD"]." ".ToLower(GetMessage("MONTH_".intval($arr["MM"])."_S"))." ".$arr["YYYY"];?> г.</div>
                            <?php
                                $timeString = date("H:i",  MakeTimeStamp($arItem['PROPERTIES']['CURRENT_DATE_OPTION']['VALUE'], "DD.MM.YYYY HH:MI:SS"))
                            ?>
                            <?if ($timeString != "00:00"):?>
                                <div class="time<?if ($lateFlag):?> danger<?endif;?>"><?=$timeString;?></div>
                            <?endif;?>
                            <?if ($lateFlag):?>
                                <i class="late danger">просрочено</i>
                            <?endif?>
                        <?endif;?>
                    <?endif?>
                </div>
                <div class="entry_list__item entry_block_status">
                    <a class="entry_client_link" href="<?=$link_client;?>"><?=$name_client;?></a>
                    <div class="object_status"><?=$arResult['STATUS_LIST'][$arItem['PROPERTIES']['STATUS']['VALUE']]['NAME'];?></div>
                    <a href="#" class="entry_link__status changeStatus">Изменить статус</a>
                </div>
                <div class="entry_list__item entry_block_description">
                    <div class="head"><?=$arItem['PROPERTIES']['Street']['VALUE'];?> <?=$arItem['PROPERTIES']['HOUSE']['VALUE'];?><?if (!empty($arItem['PROPERTIES']['APARTMENT']['VALUE'])):?>, кв. <?=$arItem['PROPERTIES']['APARTMENT']['VALUE'];?><?endif;?></div>
                    <div class="object_status">
                        <?if (!empty($arItem['PROPERTIES']['IS_WATCH']['VALUE'])):?>
                            <div class="green">Объект осмотрен</div>
                        <?else:?>
                            <div class="yellow">Объект ждёт осмотра</div>
                        <?endif;?>
                    </div>
                    <div>
                        <span>
                            квартира <?=$arItem['PROPERTIES']['Rooms']['VALUE'];?> к. 
                            <?if ($arItem['PROPERTIES']['Square']['VALUE']):?><?=$arItem['PROPERTIES']['Square']['VALUE'];?> м2 <?endif;?>
                            <?=$arItem['PROPERTIES']['Floor']['VALUE'];?><?=$arItem['PROPERTIES']['Floors']['VALUE'] ? '/'.$arItem['PROPERTIES']['Floors']['VALUE'] : " этаж" ;?>
                        </span>
                        <span><?=getElemName($arItem['PROPERTIES']['District']['VALUE']);?> р-н</span>
                        <span><?=$arItem['PROPERTIES']['MarketType']['VALUE'];?>, <?=$arItem['PROPERTIES']['HouseType']['VALUE'];?></span>
                    </div>
                    <div class="entry_item__price"><?=number_format($arItem['PROPERTIES']['PRICE']['VALUE'], 0, ',', ' ');?> руб.</div>

                    <?//if (!empty($arItem['PROPERTIES']['WATCHER']['VALUE'])):?>
                        <div class="accomplice">
                            <a href="/agents/<?=$arItem['PROPERTIES']['WATCHER']['VALUE'];?>/"><?=$arResult['USER_LIST'][$arItem['PROPERTIES']['WATCHER']['VALUE']]['FULLNAME'];?></a>
                        </div>
                    <?//endif;?>
                </div>
                <div class="entry_list__item entry_block_panel">
                    <?php
                        $dayObject = date("d.m.Y",  MakeTimeStamp($arItem['PROPERTIES']['FIRST_CONTACT']['VALUE'], "DD.MM.YYYY HH:MI:SS"));
                    ?>
                    <div class="info_contact"><span>Первый контакт:</span><?=$dayObject?></div>
                    <div class="entry_buttons_object create_ads">
                        <ul>
                            <li class="min-item">
                                <a href="#" class="popupAddObjectPhoto">
                                    <span class="img">
                                        <?if (isset($arResult['OBJECT_PHOTO_LIST'][$arItem['ID']])):?>
                                            <span class="count"><?=$arResult['OBJECT_PHOTO_LIST'][$arItem['ID']];?></span>
                                        <?endif?>
                                        <img src="/bitrix/templates/crm/img/edit-photos.png">
                                    </span>
                                </a>
                            </li>
                            <?if (isset($arResult['OBJECT_PHOTO_LIST'][$arItem['ID']])):?>
                                <li class="min-item">
                                    <a href="#popup_ad" class="galleryLoad">
                                        <span class="img"><img src="/bitrix/templates/crm/img/slide-show.png"></span>
                                    </a>
                                </li>
                            <?endif;?>
                            <li class="min-item">
                                <form action="/ads/kvartiry_kuplyu/" method="POST">
                                    <?$filterArProperty = array('PRICE','Square', 'Rooms','Floor','Floors');?>
                                    <?foreach ($arItem['PROPERTIES'] as $objectProperty => $objectPropertyValue):?>
                                        <?if (in_array($objectProperty, $filterArProperty)):?>
                                            <input type="hidden" value="<?=$objectPropertyValue['VALUE'];?>" name="iteration[<?=$objectProperty;?>]">
                                        <?endif;?>
                                    <?endforeach;?>
                                    <input class="btn_connections" type="submit">
                                </form>
                            </li>												
                            <li class="min-item">
                                <a title="Редактировать объект" class="btn_edit" href="/objects/add.php?CODE=<?=$arItem['ID'];?>"></a>
                            </li>
                        </ul>
                    </div>
                </div>
                    <?if (!empty($arItem['PROPERTIES']['IS_WATCH']['VALUE'])):?>
                        <div class="create_ads object">
                            <ul>
                                <?if (empty($arItem['PROPERTIES']['WATCH_AGREE']['VALUE'])):?>
                                    <li><a class="view_watch" href="#view_watch"><span class="img"><img src="/bitrix/templates/crm/img/cross.png"></span><span class="text black">Отказ от работы</span></a></li>
                                <?else:?>
                                    <li><a class="view_watch" href="#view_watch"><span class="img"><img src="/bitrix/templates/crm/img/check.png"></span><span class="text black">Согласие риэлтора</span></a></li>
                                   <?if(!isset($arResult['OBJECT_ADV'][$arItem['ID']])):?>
                                        <li><a href="/ads/ads_by_object.php?object_id=<?=$arItem['ID'];?>"><span class="img"><img src="/bitrix/templates/crm/img/create_ad.png"></span><span class="text">Создать объявление</span></a></li>
                                    <?else:?>
                                        <li class="ready_ads"><a target="_blank" href="/ads/kvartiry_prodam/<?=$arResult['OBJECT_ADV'][$arItem['ID']];?>/"><span class="img"></span><span class="text">Объявление</span></a></li>
                                    <?endif;?>
                                <?endif;?>

                            </ul>
                        </div>
                    <?endif;?>
            </div>
        <?endforeach;?>
        <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
            <br><?=$arResult["NAV_STRING"]?>
        <?endif;?>
    <?endif;?>
        
</div>
<div style="display: none;">
    <div class="popup-client popup-changeStatus" id="popupChange">
        <div class="warning">
            <h2>Смена статуса</h2>
            <strong>Текущий статус:</strong> <span class="current_status"></span>
        </div>
        <div class="form form_status">
            <h3>Выберите статус:</h3>
            <span class="status_error hidden">Не выбран статус</span>
            <ul class="status_list">
                <?foreach ($arResult['STATUS_LIST'] as $status):?>
                    <li value="<?=$status['ID'];?>" data-text-color="<?=$status['PROPERTY_COLOR_VALUE'];?>"><?=$status['NAME'];?></li>
                <?endforeach;?>
            </ul>
            <h3 class="form_head hidden"></h3>
            <form class="formStatus" action="/objects/status_change.php/" method="POST" name="curform">
                <input type="hidden" name="object_id">
                <div class="form_row clearfix hidden form_row_date">
                    <div class="form_cell">
                        <label for="date_time">Дата/время<span class="starrequired">*</span>:</label>
                        <span class="error hidden">Поле должно быть заполнено:</span>
                        <input type="text" name="date_time" onclick="BX.calendar({node: this, field: this, bTime: true, bHideTime:false});">
                    </div>
                </div>
                <div class="form_row hidden form_row_who">
                    <label for="watcher">Риэлтору</label>
                    <input checked="checked" type="radio" name="watcher" value="riel">
                    <label for="watcher">Клиенту</label>
                    <input type="radio" name="watcher" value="cl">
                </div>
                <div class="form_row hidden form_row_comment">
                    <span class="error hidden">Поле должно быть заполнено:</span>
                    <textarea name="comment" class="status_textarea" rows="6"></textarea>
                </div>
                <div class="form_row row_submit hidden">
                    <div class="response_status" style="display:none"></div>
                    <input class="button button_universal" type="submit" value="Изменить статус">
                </div>
            </form>
            <div class="second-preloader" style="display:none;"></div>
        </div>
    </div>
</div>
<div style="display: none;">
    <div id="popupAddObjectPhoto" class="popup_comment popup">
        <h2>Фотографии объекта</h2>
        <div class="row">													
            <div class="object_preview_list-wrap">
                <div class="object_preview_list">
                    <div id="cboxLoadingGraphic" style="float: left; display: none;"></div>
                    <div class="download">
                        <input id="download_images_button_objects" class="button download_btn" type="button" value="Добавить фото" onclick="alertButton()">
                        <form id="formLoader">
                            <input type="file" id="imageLoaderPhoto" accept="image/*" multiple="" enctype="multipart/form-data" name="imageLoaderPhoto[]"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <a href="#" class="button button_universal button_adder">Сохранить</a>
            <a href="#" class="button gray close_button">Отмена</a>
        </div>
        <div class="second-preloader" style="display:none;"></div>
    </div>
</div>
<div style="display: none;">
    <div id="popup_ad" class="popup_ad popup">
        <h2>Название объекта</h2>
        <section class="slider">
        </section>
    </div>
</div>
<div style="display: none;">
    <div id="view_watch" class="popup_ad popup">
        <h2>Комментарий риэлтора</h2>
        <section class="comment_block">
        </section>
    </div>
</div>
<script>
    statusList = {
    <?foreach ($arResult['STATUS_LIST'] as $key=>$value):?>
        <?echo $key.': \''.$value['PROPERTY_TITLE_VALUE'].'\',';?>
    <?endforeach;?>
    }
</script>
<?//pre($arResult['STATUS_LIST'],false);?>