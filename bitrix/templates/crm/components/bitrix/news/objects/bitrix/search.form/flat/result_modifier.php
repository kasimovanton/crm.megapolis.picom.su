<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$iblock = 16;
$arFilter = array(
    "city",
);
     
$groupCount = getArrayOfCntByProp($iblock, "PROPERTY_WATCHER", array("PROPERTY_AUTHOR"=>CUser::GetID()));
$userIDs = array_keys($groupCount);
$logicString = "";
foreach ($userIDs as $userId){
    $logicString .= $userId." |";
}
// echo $logicString;
$arResult['all_users'] = getUsers(array('UF_HEAD'), array('ID','LOGIN','LAST_NAME','SECOND_NAME','NAME'), array('GROUPS_ID'=>6,'ACTIVE'=>'Y', 'ID'=>$logicString), "LAST_NAME");
unset($arResult['all_users'][1]);
$arResult["VALUE_FORM_FIELDS"] = getEnumValues($iblock,$arFilter);

if (isset($_SESSION['tab']) && $_SESSION['tab']== "active") {
        $GLOBALS['filter_obj']['PROPERTY_STATUS'] = array(8940, 8962, 8963, 8964, 8965, 8966);
    } elseif (isset($_SESSION['tab']) && $_SESSION['tab'] == "waiting") {
        $GLOBALS['filter_obj']['PROPERTY_STATUS'] = array(8967, 8968, 8971, 8972, 8973, 8975, 11107);
    } elseif (isset($_SESSION['tab']) && $_SESSION['tab'] == "sold") {
        $GLOBALS['filter_obj']['PROPERTY_STATUS'] = array(8977, 8978, 8979, 11106, 11377);
    } else {
        $GLOBALS['filter_obj']['!PROPERTY_STATUS'] = array(8977, 8978, 8979, 11106, 11377);
    }

    $maxPrice = getMaxObjectsPrice($USER->GetID());
    if($maxPrice){
        $_REQUEST['for_script'] = $maxPrice;
    } else {
        $_REQUEST['for_script'] = 15000000;
    }
    
if (isset($_REQUEST['seacrhObject']) && is_array($_REQUEST['seacrhObject'])){
    $filterArray = array();
    $filterArray["LOGIC"] = "OR";
    foreach($_REQUEST['seacrhObject'] as $propertyKey => $propertyValue){
        $filterArray[] = array('PROPERTY_'.$propertyKey => $propertyValue);
    }
    $GLOBALS['filter_obj'][] = $filterArray;
}
if (isset($_REQUEST['Street']) && !empty($_REQUEST['Street'])){
    $GLOBALS['filter_obj']['PROPERTY_Street'] = '%'.$_REQUEST['Street'].'%';
}
if (isset($_REQUEST['City']) && !empty($_REQUEST['City']) && $_REQUEST['City'] !=0){
    $GLOBALS['filter_obj']['PROPERTY_City'] = $_REQUEST['City'];
}
if (isset($_REQUEST['District']) && !empty($_REQUEST['District'])){
    $GLOBALS['filter_obj']['PROPERTY_District'] = $_REQUEST['District'];
}
if (isset($_REQUEST['Rooms']) && !empty($_REQUEST['Rooms'])){
    $GLOBALS['filter_obj']['PROPERTY_Rooms'] = $_REQUEST['Rooms'];
}
if (isset($_REQUEST['min_price']) && !empty($_REQUEST['min_price'])){
    $GLOBALS['filter_obj']['>=PROPERTY_Price'] = $_REQUEST['min_price'];
}
if (isset($_REQUEST['max_price']) && !empty($_REQUEST['max_price'])){
    $GLOBALS['filter_obj']['<=PROPERTY_Price'] = $_REQUEST['max_price'];
}
if (isset($_REQUEST['Rieltor']) && !empty($_REQUEST['Rieltor']) && $_REQUEST['Rieltor'] != 0){
    $GLOBALS['filter_obj']['PROPERTY_WATCHER'] = $_REQUEST['Rieltor'];
}

if (isset($_REQUEST['start_date']) && !empty($_REQUEST['start_date'])){
    $GLOBALS['filter_obj']['>=PROPERTY_CURRENT_DATE_OPTION'] = date('Y-m-d H:i:s', timeExplode($_REQUEST['start_date']));
}    
if (isset($_REQUEST['end_date']) && !empty($_REQUEST['end_date'])){
    $GLOBALS['filter_obj']['<=PROPERTY_CURRENT_DATE_OPTION'] = date('Y-m-d H:i:s', timeExplode($_REQUEST['end_date'])+86400);
} 
