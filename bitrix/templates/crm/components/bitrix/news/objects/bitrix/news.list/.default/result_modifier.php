<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$IBLOCK_ID = 17; 
$arOrder = Array("SORT"=>"asc", "ID"=>"asc");
$arSelect = Array("ID", "NAME", "PROPERTY_COLOR", "PROPERTY_TITLE");
$arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y");
$res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

while ($ob = $res->GetNextElement()){
    $statusFields = $ob->GetFields();
    $arFieldsStatusItem[$statusFields['ID']] = $statusFields;
}

$arResult['STATUS_LIST'] = $arFieldsStatusItem;
            
//������ ������� 
$users = getUsers(array("UF_HEAD"),array("ID",'LAST_NAME','SECOND_NAME','NAME'),array('ACTIVE'=>'Y', "!ID"=>1),"LAST_NAME");
foreach ($users as $key=>$user){
    $users[$key]['FULLNAME'] = $user['LAST_NAME'].' '.$user['NAME'].' '.$user['SECOND_NAME'];
}
$arResult['USER_LIST'] = $users;

$objectListIds = array();
$IBLOCK_ID = 5; 
$arOrder = Array("SORT"=>"asc", "ID"=>"asc");
$arSelect = Array("ID", "NAME", "IBLOCK_ID");
foreach($arResult["ITEMS"] as $arItem){
    $objectListIds[] = $arItem['ID'];
    $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "PROPERTY_OBJECT_ELEMENT"=>$arItem['ID']);
    $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
    if ($ob = $res->GetNextElement()){
        $objectAdv = $ob->GetFields();
        $arResult['OBJECT_ADV'][$arItem['ID']] = $objectAdv['ID'];
    }
}
$arResult['OBJECT_PHOTO_LIST'] = getObjectsPhotoCount($objectListIds);