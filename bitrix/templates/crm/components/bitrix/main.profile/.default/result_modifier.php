<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/** @global CMain $APPLICATION */
/** @var CBitrixComponentTemplate $this */
/** @var CBitrixComponent $component */
/** @var array $arParams */
/** @var array $arResult */?>
<?//pre($arResult["strProfileError"])?>
<?
//$arResult["arUser"]["PERSONAL_PHOTO_INPUT"] = CFile::InputFile("PERSONAL_PHOTO", 20, $arResult["arUser"]["PERSONAL_PHOTO"], false, 0, "IMAGE",'class="button download_btn"',0,"",'class="hidden"',false,false);
if (strlen($arResult["arUser"]["PERSONAL_PHOTO"])>0) {
	$arResult["arUser"]["PERSONAL_PHOTO_HTML"] = CFile::ShowImage($arResult["arUser"]["PERSONAL_PHOTO"], 150, 150);
	$arResult["arUser"]["PERSONAL_PHOTO_PATH"] = CFile::GetPath($arResult["arUser"]["PERSONAL_PHOTO"]);
}
return;


$errorNames   = array_map(function ($e) { return preg_replace('~^.*\'(.*?)\'.*$~', '$1', $e); }, $arResult['ERRORS']);
$errorIndexes = array();
foreach ($arResult['PROPERTY_LIST_FULL'] as $propertyId => &$property) {
	$isProp = intval($propertyId) > 0;
	$name   = $isProp ? $property['NAME'] : (!empty($arParams["CUSTOM_TITLE_$propertyId"]) ? $arParams["CUSTOM_TITLE_$propertyId"] : GetMessage("IBLOCK_FIELD_$propertyId"));

	$errorIndex = array_search($name, $errorNames);
	$property['ERROR'] = $errorIndex !== false;
	if ($property['ERROR']) {
		$errorIndexes[] = $errorIndex;
	}
} unset ($propertyId);

foreach ($errorIndexes as $errorIndex) {
	unset($arResult['ERRORS'][$errorIndex]);
}

