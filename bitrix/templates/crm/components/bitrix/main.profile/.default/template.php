<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>


<?ShowError($arResult["strProfileError"]);?>
<?
if ($arResult['DATA_SAVED'] == 'Y')
	ShowNote(GetMessage('PROFILE_DATA_SAVED'));
?>
<?//pre($arResult)?>
<?/*
<style>
	#fileprev img{
		width:150px;
		height: 113px;
	}
	.area_remove{
	    background: url("../img/photo_remove.png") no-repeat scroll 0 0 transparent;
	    height: 20px;
	    position: absolute;
	    right: 3px;
	    top: 3px;
	    width: 20px;
	}
	#fileprev div{
		position: relative;
		width:150px;
	}

	#fileprev{
		margin-left: 345px;
	}
</style>
*/?>



<form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">
<div class="profile_contacts_form">
<?=$arResult["BX_SESSION_CHECK"]?>
    <input type="hidden" name="lang" value="<?=LANG?>" />
    <input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
    <div class="form_row">
        <label class="main_photo_span"><?=GetMessage("USER_PHOTO")?></label>
        <a class="button download_btn">
            <p class="download-text">Загрузить</p>
            <input id="fileFoto" onchange="handleFile(this.files)" class="hidden-file button download_btn" name="PERSONAL_PHOTO" size="0" type="file">
        </a>
        <input type="checkbox" name="PERSONAL_PHOTO_del" value="Y" id="PERSONAL_PHOTO_del" class="hidden">
        <?//=$arResult["arUser"]["PERSONAL_PHOTO_INPUT"]?>
    </div>
    <div class="form_row">
         <div id="fileprev"></div>
    </div>
    <?if (strlen($arResult["arUser"]["PERSONAL_PHOTO"])>0):?>
        <div class="form_row">
            <div class="main_photo">
                <div class="main_photo_thumb">
                    <?=$arResult["arUser"]["PERSONAL_PHOTO_HTML"]?>
                    <a class="photo_remove" href="#rem" title="удалить фото"></a>
                    <div class="main_photo_name hidden"><?=$arResult["arUser"]["PERSONAL_PHOTO_PATH"]?></div>
                </div>
            </div>
        </div>
	<?endif?>
    <div class="form_row">
            <label for="icellphone"><?=GetMessage('USER_PHONE')?></label>
            <div class="input_with_error">
                <span class="bad_cellphone input_error hidden">Обязательно заполнить</span>
                <input class="content_input_medium cell_phone" id="icellphone" type="text"  name="PERSONAL_PHONE" maxlength="255" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>">
            </div>
    </div>
    
    <div class="form_row">
            <label for="iextraphone"><?=GetMessage('USER_MOBILE')?></label>
            <input class="content_input_medium" id="iextraphone" type="text" name="PERSONAL_MOBILE" maxlength="255" value="<?=$arResult["arUser"]["PERSONAL_MOBILE"]?>">
    </div>
   <?/* <div class="form_row">
            <label for="iemail"><?=GetMessage('EMAIL')?><span class="starrequired">*</span></label>
            <div class="input_with_error">
                <span class="bad_email input_error hidden">Неверный ввод данных</span>
                <input class="content_input_long" id="iemail" type="text" name="EMAIL" maxlength="50" value="<? echo $arResult["arUser"]["EMAIL"]?>" />
            </div>
    </div>*/?>
    <div class="form_row">
            <label><?=GetMessage('EMAIL')?></label>
            <div class="input_with_error">
                <?echo $arResult["arUser"]["EMAIL"];?>
				<br/>
				<?$rsAdm = CUser::GetByID(1);
				$arAdm = $rsAdm->Fetch();?>
				<small>Для изменения обратитесь к <a href="mailto:<?=$arAdm['EMAIL'];?>">администратору сайта</a></small>
                <input type="hidden" name="EMAIL" value="<? echo $arResult["arUser"]["EMAIL"]?>" />
            </div>
			
    </div>
    <div class="form_row">
            <label for="ilogin"><?=GetMessage('LOGIN')?><span class="starrequired">*</span></label>
            <div class="input_with_error">
                <span class="bad_email input_error hidden">Неверный ввод данных</span>
                <input class="content_input_long" id="ilogin" type="text" name="LOGIN" maxlength="50" value="<? echo $arResult["arUser"]["LOGIN"]?>" />
            </div>
    </div>
    <div class="form_row alert">
        <label>Оповещение:</label>
        <div class="input_with_error">
            <input id="noalert" type="hidden" name="UF_ALERT" value="0" <?if (!$arResult["arUser"]["UF_ALERT"]):?>checked<?endif;?>>
            <input id="alert" class="content_input_medium" type="checkbox" name="UF_ALERT" value="1"  <?if ($arResult["arUser"]["UF_ALERT"]):?>checked<?endif;?>>
            <label id="for_alert" class="inrow_label" for="alert">Принимать почтовые оповещения</label>
        </div>
    </div>
    <div class="form_row">
            <input class="button save_profile_btn" name="save" type="submit" value="<?=(($arResult["ID"]>0) ? GetMessage("SAVE") : GetMessage("ADD"))?>">
    </div>


<?if($arResult["arUser"]["EXTERNAL_AUTH_ID"] == ''):?>
    <div class="profile_password_form">

        <div class="form_row">
            <label for="ipass"><?=GetMessage('NEW_PASSWORD_REQ')?></label>
            <div class="input_with_error">
                <span class="bad_password input_error hidden">Неверный ввод</span>
                <input class="content_input_long" id="ipass" type="password" name="NEW_PASSWORD" maxlength="50" value="" autocomplete="off">
                <?if($arResult["SECURE_AUTH"]):?>
                    <span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
                        <div class="bx-auth-secure-icon"></div>
                    </span>
                    <noscript>
                    <span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
                        <div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
                    </span>
                    </noscript>
                    <script type="text/javascript">
                    document.getElementById('bx_auth_secure').style.display = 'inline-block';
                    </script>                        
                <?endif?>
            </div>
        </div>
        <div class="form_row">
                <label for="irepeatpass"><?=GetMessage('NEW_PASSWORD_CONFIRM')?></label>
                <div class="input_with_error">
                    <span class="bad_repeatpassword input_error hidden">Повтор неверен</span>
                    <input class="content_input_long" id="irepeatpass" type="password" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" autocomplete="off"/>
                </div>
        </div>
        <div class="form_row">
                <input class="button save_pass_btn" name="save" type="submit" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE_PASS") : GetMessage("MAIN_ADD_PASS"))?>">
        </div>
    </div>
<?endif?>
</form>
<?
if($arResult["SOCSERV_ENABLED"])
{
	$APPLICATION->IncludeComponent("bitrix:socserv.auth.split", ".default", array(
			"SHOW_PROFILES" => "Y",
			"ALLOW_DELETE" => "Y"
		),
		false
	);
}
?>