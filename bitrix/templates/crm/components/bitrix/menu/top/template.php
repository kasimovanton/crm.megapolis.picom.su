<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
    <?php
        $userStatus = getUserStatus(CUser::GetID());
    ?>
    <ul class="filter_menu">

    <?
    foreach($arResult as $arItem):
        if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
            continue;
    ?>
        <?if (($arItem['PARAMS']['access'] == 'all' && !in_array($arItem['PARAMS']['disable'], $userStatus)) || in_array($arItem['PARAMS']['access'], $userStatus)):?>
            <?if($arItem["SELECTED"]):?>
                <li class="filter_menu_item filter_menu_item--active"><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?><?if ($arItem['PARAMS']['access'] == "reassign"):?>(<?=tempClientCount(CUser::GetID());?>)<?endif;?></a></li>
            <?else:?>
                <li class="filter_menu_item"><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?><?if ($arItem['PARAMS']['access'] == "reassign"):?>(<?=tempClientCount(CUser::GetID());?>)<?endif;?></a></li>
            <?endif?>
        <?else:?>
            
        <?endif;?>
        
    <?endforeach?>

    </ul>
<?endif?>