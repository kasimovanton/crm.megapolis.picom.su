<?
$_SERVER["DOCUMENT_ROOT"] = "/home/bitrix/ext_www/crm.megapolis18.ru";
$_SERVER['SERVER_NAME'] = "crm.megapolis18.ru";
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(CModule::IncludeModule('iblock')){

    $IBLOCK_ID = 16; 
    $arOrder = Array("SORT"=>"ASC");
    $arSelect = Array("ID", "IBLOCK_ID", 'NAME', "PROPERTY_Client");
    $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "PROPERTY_ALERT"=>false, "PROPERTY_STATUS" =>8962,
                    ">PROPERTY_CURRENT_DATE_OPTION"=> date("Y-m-d H:i:s", time()-150+10800),
                    "<=PROPERTY_CURRENT_DATE_OPTION"=> date("Y-m-d H:i:s", time()+150+10800),);
    $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
    while($ob = $res->GetNextElement()){
        $arFields = $ob->GetFields();
        
        $arFields['PROPERTIES'] = $ob->GetProperties();
        
        CIBlockElement::SetPropertyValues($arFields['ID'], $IBLOCK_ID, 551, "ALERT");
        
        $rsUser = CUser::GetByID($arFields['PROPERTIES']['AUTHOR']['VALUE']);
        $currentManager = $rsUser->Fetch();
        
        if (!empty($currentManager['EMAIL'])){
            $address = $arFields['PROPERTIES']['Street']['VALUE'].' улица, дом '.
                       $arFields['PROPERTIES']['HOUSE']['VALUE'];
            $address .= $arFields['PROPERTIES']['APARTMENT']['VALUE'] ? ', кв. '.$arFields['PROPERTIES']['APARTMENT']['VALUE'] : "";
            $rsRieltorUser = CUser::GetByID($arFields['PROPERTIES']['WATCHER']['VALUE']);
            $currentRieltor = $rsRieltorUser->Fetch();
            $currentRieltorFullName = $currentRieltor['LAST_NAME'].' '.$currentRieltor['NAME'].' '.$currentRieltor['SECOND_NAME'];
            $arMail = array(
                "object_address" => $address,
                "manager_email" => $currentManager['EMAIL'],
                "object_rieltor_id" => $arFields['PROPERTIES']['WATCHER']['VALUE'],
                "object_rieltor_name" => $currentRieltorFullName,
                "object_client_id" => $arFields['PROPERTIES']['Client']['VALUE'],
                "object_client_name" => getElemName($arFields['PROPERTIES']['Client']['VALUE']),
            );
            CEvent::Send("PRESENTATION_SOON", array("s1"), $arMail, "N", 31);
        }
    }
}
?>
