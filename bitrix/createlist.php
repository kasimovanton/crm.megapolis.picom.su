<?
$_SERVER["DOCUMENT_ROOT"] = "/home/bitrix/ext_www/crm.megapolis18.ru";
$_SERVER['SERVER_NAME'] = "crm.megapolis18.ru";
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(CModule::IncludeModule('iblock')){

        $IBLOCK_ID = 5; 
        
        $arOrder = Array("SORT"=>"ASC");
        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_CREATE", "IBLOCK_SECTION_ID", "DATE_ACTIVE_FROM", "DATE_ACTIVE_TO", "DETAIL_TEXT", "DETAIL_PICTURE", "PROPERTY_*");
        $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "!PROPERTY_MAIN_SITE" => false, "CHECK_PERMISSIONS" => "N");
        $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
        
        if (file_exists($_SERVER["DOCUMENT_ROOT"].'/advertisment.json')){
            $arExportAds = array();
            while($ob = $res->GetNextElement()){
                $arFields = $ob->GetFields();
                foreach ($arFields as $keyField => $valueField){
                    if (preg_match("/~/", $keyField)){
                        unset($arFields[$keyField]);
                    }
                }
                $arSection = CIBlockSection::GetByID($arFields['IBLOCK_SECTION_ID']);
                $parentSection = $arSection->GetNext();
                $arFields['IBLOCK_SECTION_ID'] = $parentSection['ID'];
                $arFields['SECTION'] = $parentSection['NAME'];

                $arSection = CIBlockSection::GetByID($parentSection['IBLOCK_SECTION_ID']);
                $mainSection = $arSection->GetNext();
                $arFields['PARENT_SECTION_ID'] = $mainSection['ID'];
                $arFields['PARENT_SECTION'] = $mainSection['NAME'];
                
                if (!empty($arFields['DETAIL_PICTURE'])){
                    $arFields['DETAIL_PICTURE'] = 'http://'.$_SERVER['SERVER_NAME'].CFile::GetPath($arFields['DETAIL_PICTURE']);
                }
                $arProperties = $ob->GetProperties();
                $arFields['PROPERTIES'] = array();
                foreach ($arProperties as $key=>$value){
                    switch ($value['PROPERTY_TYPE']){
                        case "F":
                            foreach ($value['VALUE'] as $fileID){
                                $arFields['PROPERTIES'][$key][] = 'http://'.$_SERVER['SERVER_NAME'].CFile::GetPath($fileID);
                            }
                            break;
                        case "E":
                            if ($value['MULTIPLE'] == 'Y'){
                                foreach ($value['VALUE'] as $element){
                                    $arFields['PROPERTIES'][$key][] = getElemName($element);
                                }
                            } else {
                                $arFields['PROPERTIES'][$key] = getElemName($value['VALUE']);
                            }
                            break;
                        default:
                            $arFields['PROPERTIES'][$key] = $value['VALUE'];
                    }
                    
                    
                }
                
                $rsUser = CUser::GetByID($arFields['PROPERTIES']['Author']);
                $arUser = $rsUser->Fetch();
                $arFields['PROPERTIES']['Author'] = $arUser['LAST_NAME'].' '.$arUser['NAME'].' '.$arUser['SECOND_NAME'];
                unset($arFields['PROPERTIES']['Client']);
                
                $arExportAds[] = $arFields;
            }
            $fileSocket = fopen($_SERVER["DOCUMENT_ROOT"].'/advertisment.json', 'w+');
            if ($fileSocket){
                fwrite($fileSocket, json_encode($arExportAds, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
            }
            fclose($fileSocket);
        }
    }