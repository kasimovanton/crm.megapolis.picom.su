<?php
/**
 * Created by PhpStorm.
 * User: picom
 * Date: 25.11.14
 * Time: 13:23
 */
$APPLICATION->IncludeComponent("picom:users.list.pag", "", array(
																	'SEF_FOLDER' => $arParams['SEF_FOLDER'],
																	'DETAIL_PAGER_TEMPLATE' => $arParams['DETAIL_PAGER_TEMPLATE'],
																	'PAGER_SHOW_ALWAYS' => $arParams['PAGER_SHOW_ALWAYS'],
																	'BOSS_CHANGE' => $arParams['BOSS_CHANGE'],
																	'ALF_FILTER' => $arParams['ALF_FILTER'],
																	'USERS_COUNT'=>$arParams['USERS_COUNT'],
																	));
?>