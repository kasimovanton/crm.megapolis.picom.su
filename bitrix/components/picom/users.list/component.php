<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


$total_ads = getArrayOfCntByProp(5, "PROPERTY_AUTHOR");
$total_client = getArrayOfCntByProp(10, "PROPERTY_RIELTOR");


global $USER;

$userID=$USER->GetID();
//echo $userID;
$users = getUsers(array("UF_HEAD"),array("ID",'LAST_NAME','SECOND_NAME','NAME','PERSONAL_NOTES','DATE_REGISTER','PERSONAL_PHOTO','PERSONAL_PHONE','PERSONAL_MOBILE'),array('ACTIVE'=>'Y'));
//pre($users);
$childUsersId = getChildsId($userID,$users);
//pre($childUsersId);
foreach ($childUsersId as $childId) {
    $user = $users[$childId];
    $user['HEAD'] = $users[$user['UF_HEAD']];
    $arResult['ITEMS'][$childId] = $user;
    $arResult['ITEMS'][$childId]['TOTAL_ADS'] = $total_ads[$childId] ? $total_ads[$childId] : 0;
    $arResult['ITEMS'][$childId]['TOTAL_CLIENT'] = $total_client[$childId] ? $total_client[$childId] : 0;
}
// $arResult['USERS'] = $users;


$this->IncludeComponentTemplate();

?>