<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("USERS_NAME"),
	"DESCRIPTION" => GetMessage("USERS_DESCRIPTION"),
	"ICON" => "/images/users.gif",
	"PATH" => array(
		"ID" => "utility",
		"CHILD" => array(
			"ID" => "user",
			"NAME" => GetMessage("T_DESC_USERS"),				
		),
	),
);

?>