<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (CModule::IncludeModule("iblock"))
{
	
	//добавление картинок------
	

	foreach($_REQUEST['addedPhoto'] as $key=>$val){
        $onloadFile = CFile::GetFileArray($val);
        if (preg_match("/image/", $onloadFile['CONTENT_TYPE'])){
            $arResult['ELEMENT_PROPERTIES']['preAddImages'][] = array('VALUE'=>$val);
        } else {
            $arResult['ELEMENT_PROPERTIES']['forDelFiles'][] = array('VALUE'=>$val);
        }
	}	
    
	if($_REQUEST['CODE']){	
		/*foreach($_FILES['newimages']['name'] as $key=>$val){
			$arFileTemp = array(
					'name' => $_FILES['newimages']['name'][$key],
					'type' => $_FILES['newimages']['type'][$key],
					'tmp_name' => $_FILES['newimages']['tmp_name'][$key],													
			);		
			$arfid  = CFile::SaveFile($arFileTemp,'adsform');
			$arFiles[] = array('VALUE' => $arfid, 'DESCRIPTION' => $arFileTemp['name']); 

		}

		CIBlockElement::SetPropertyValueCode($_REQUEST['CODE'], 'Images', $arFiles);*/
		$newFiles = array();
		foreach($arResult['ELEMENT_PROPERTIES']['preAddImages'] as $img){
            $idfilenew = CFile::SaveFile(CFile::MakeFileArray($img['VALUE']), "testdir");
            $newFiles[] = array('VALUE' => $idfilenew);
			CFile::Delete($img['VALUE']);									
		}
        foreach($arResult['ELEMENT_PROPERTIES']['forDelFiles'] as $del){
			CFile::Delete($del['VALUE']);									
		}
        
		$arResult['ELEMENT_PROPERTIES']['preAddImages'] = $newFiles;
	
		CIBlockElement::SetPropertyValueCode($_REQUEST['CODE'], 'Images', $arResult['ELEMENT_PROPERTIES']['preAddImages']);		
		
	}	
	//-------------------------
    $arFilter  = array('IBLOCK_ID' => $arParams['IBLOCK_ID']);
    $arSelect  = array('ID', 'IBLOCK_SECTION_ID', 'UF_PROPERTY');
    $resSection = CIBlockSection::GetList(false, $arFilter, false, $arSelect);
    while ($arSection = $resSection->Fetch()) {
        //pre($arSection);
        foreach ($arSection['UF_PROPERTY'] as $key=>$prop) {
            if (preg_match("/^PROPERTY_(.+)$/",$prop,$matches)) {
                $arSection['UF_PROPERTY'][$key] = $matches[1];
            }
        }
        $arProps[$arSection['ID']] = $arSection;
    }
    $arResult['PROPERTY_SECTION_LIST'] = $arProps;
    //pre($arProps);
    /* [7] => Array
            (
                [ID] => 7
                [IBLOCK_SECTION_ID] => 6
                [UF_PROPERTY] => Array
                    (
                        [0] => DETAIL_PICTURE
                        [1] => DETAIL_TEXT
                        [2] => SECTION_NAME
                        [3] => Author
                        [4] => AdStatus
                        [5] => Region
                        [6] => City
                        [7] => District
                        [8] => Street
                        [9] => HOUSE
                        [10] => APARTMENT
                        [11] => Price
                        [12] => MarketType
                        [13] => Floor
                        [14] => Floors
                        [15] => Rooms
                        [16] => Square
                        [17] => LIVING_SQUARE
                        [18] => KITCHEN_SQUARE
                        [19] => Images
                        [20] => HouseType
                        [21] => Client
                        [22] => Avito
                    )

            )*/
    
    $arIblockFields = array(
        "NAME",
        "TAGS",
        "DATE_ACTIVE_FROM",
        "DATE_ACTIVE_TO",
        "IBLOCK_SECTION",
        "PREVIEW_TEXT",
        "PREVIEW_PICTURE",
        "DETAIL_TEXT",
        "DETAIL_PICTURE",
    );
    
    $rsProp = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arParams["IBLOCK_ID"]));
    while ($arr=$rsProp->Fetch())
    {       
        $arProperty[] = $arr["CODE"]; 
    }
    $arParams["PROPERTY_CODES"] = array_merge($arIblockFields,$arProperty);
    $arResult['IBLOCK_FIELDS'] = $arIblockFields;
	$arParams["EVENT_NAME"] = trim($arParams["EVENT_NAME"]);
	if(strlen($arParams["EVENT_NAME"]) <= 0)
	$arParams["EVENT_NAME"] = "INFOPORTAL_ADD_ELEMENT";
    
    $arParams["USE_CAPTCHA"] = (($arParams["USE_CAPTCHA"] != "N" && !$USER->IsAuthorized()) ? "Y" : "N");
	
	if($arParams["IBLOCK_ID"] > 0)
		$bWorkflowIncluded = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "WORKFLOW") == "Y" && CModule::IncludeModule("workflow");
	else
		$bWorkflowIncluded = CModule::IncludeModule("workflow");

	$arParams["ID"] = intval($_REQUEST["CODE"]);
	$arParams["MAX_FILE_SIZE"] = intval($arParams["MAX_FILE_SIZE"]);
	$arParams["PREVIEW_TEXT_USE_HTML_EDITOR"] = $arParams["PREVIEW_TEXT_USE_HTML_EDITOR"] === "Y" && CModule::IncludeModule("fileman");
	$arParams["DETAIL_TEXT_USE_HTML_EDITOR"] = $arParams["DETAIL_TEXT_USE_HTML_EDITOR"] === "Y" && CModule::IncludeModule("fileman");
	$arParams["RESIZE_IMAGES"] = $arParams["RESIZE_IMAGES"]==="Y";

	if(!is_array($arParams["PROPERTY_CODES"]))
	{
		$arParams["PROPERTY_CODES"] = array();
	}
	else
	{
		foreach($arParams["PROPERTY_CODES"] as $i=>$k)
			if(strlen($k) <= 0)
				unset($arParams["PROPERTY_CODES"][$i]);
	}
	$arParams["PROPERTY_CODES_REQUIRED"] = is_array($arParams["PROPERTY_CODES_REQUIRED"]) ? $arParams["PROPERTY_CODES_REQUIRED"] : array();
	foreach($arParams["PROPERTY_CODES_REQUIRED"] as $key => $value)
		if(strlen(trim($value)) <= 0)
			unset($arParams["PROPERTY_CODES_REQUIRED"][$key]);

	$arParams["USER_MESSAGE_ADD"] = trim($arParams["USER_MESSAGE_ADD"]);
	if(strlen($arParams["USER_MESSAGE_ADD"]) <= 0)
		$arParams["USER_MESSAGE_ADD"] = GetMessage("IBLOCK_USER_MESSAGE_ADD_DEFAULT");

	$arParams["USER_MESSAGE_EDIT"] = trim($arParams["USER_MESSAGE_EDIT"]);
	if(strlen($arParams["USER_MESSAGE_EDIT"]) <= 0)
		$arParams["USER_MESSAGE_EDIT"] = GetMessage("IBLOCK_USER_MESSAGE_EDIT_DEFAULT");

	if (!$bWorkflowIncluded)
	{
		if ($arParams["STATUS_NEW"] != "N" && $arParams["STATUS_NEW"] != "NEW") $arParams["STATUS_NEW"] = "ANY";
	}

	if(!is_array($arParams["STATUS"]))
	{
		if($arParams["STATUS"] === "INACTIVE")
			$arParams["STATUS"] = array("INACTIVE");
		else
			$arParams["STATUS"] = array("ANY");
	}

	if(!is_array($arParams["GROUPS"]))
		$arParams["GROUPS"] = array();

	$arGroups = $USER->GetUserGroupArray();

	if ($arParams["ID"] == 0)
	{
		$bAllowAccess = count(array_intersect($arGroups, $arParams["GROUPS"])) > 0 || $USER->IsAdmin();
	}
	else
	{
		$bAllowAccess = $USER->GetID() > 0;
	}

	$arResult["ERRORS"] = array();

	if ($bAllowAccess)
	{
		$rsIBlockSectionList = CIBlockSection::GetList(
			array("left_margin"=>"asc"),
			array(
				"ACTIVE"=>"Y",
				"IBLOCK_ID"=>$arParams["IBLOCK_ID"],
			),
			false,
			array("ID", "NAME", "DEPTH_LEVEL","IBLOCK_SECTION_ID")
		);
		$arResult["SECTION_LIST"] = array();
		while ($arSection = $rsIBlockSectionList->GetNext())
		{
			//pre($arSection);
            if ($arSection["DEPTH_LEVEL"]==1) {
                $sections[0]['SECTIONS'][] = $arSection['ID'];
            } else {
                $sections[$arSection['IBLOCK_SECTION_ID']]['SECTIONS'][] = $arSection['ID'];
            }
			/*if ($arSection["DEPTH_LEVEL"]==1){
                $arSection["SECTION_LIST_FIRST"][$arSection["ID"]] = array(
                    "VALUE" => $arSection["NAME"]
                );
            } else {*/
                $arSection["NAME"] = /*str_repeat(" . ", $arSection["DEPTH_LEVEL"]-1).*/$arSection["NAME"];
                $arResult["SECTION_LIST"][$arSection["ID"]] = array(
                    "VALUE" => $arSection["NAME"]
                );
            /*}*/
		}
        $arResult["SECTION_TREE"] = $sections;

		$COL_COUNT = intval($arParams["DEFAULT_INPUT_SIZE"]);
		if($COL_COUNT < 1)
			$COL_COUNT = 30;
		// customize "virtual" properties
		$arResult["PROPERTY_LIST"] = array();
		$arResult["PROPERTY_LIST_FULL"] = array(
			"NAME" => array(
				"PROPERTY_TYPE" => "S",
				"MULTIPLE" => "N",
				"COL_COUNT" => $COL_COUNT,
                "SORT" => "9000"
			),

			/*"TAGS" => array(
				"PROPERTY_TYPE" => "S",
				"MULTIPLE" => "N",
				"COL_COUNT" => $COL_COUNT,
			),

			"DATE_ACTIVE_FROM" => array(
				"PROPERTY_TYPE" => "S",
				"MULTIPLE" => "N",
				"USER_TYPE" => "DateTime",
			),

			"DATE_ACTIVE_TO" => array(
				"PROPERTY_TYPE" => "S",
				"MULTIPLE" => "N",
				"USER_TYPE" => "DateTime",
			),*/

			"IBLOCK_SECTION" => array(
				"PROPERTY_TYPE" => "L",
				"ROW_COUNT" => "80",
				"MULTIPLE" => $arParams["MAX_LEVELS"] == 1 ? "N" : "Y",
				"ENUM" => $arResult["SECTION_LIST"],
                "SORT" => "1",
			),

			/*"PREVIEW_TEXT" => array(
				"PROPERTY_TYPE" => ($arParams["PREVIEW_TEXT_USE_HTML_EDITOR"]? "HTML": "T"),
				"MULTIPLE" => "N",
				"ROW_COUNT" => "5",
				"COL_COUNT" => $COL_COUNT,
			),
			"PREVIEW_PICTURE" => array(
				"PROPERTY_TYPE" => "F",
				"FILE_TYPE" => "jpg, gif, bmp, png, jpeg",
				"MULTIPLE" => "N",
			),*/
			"DETAIL_TEXT" => array(
				"PROPERTY_TYPE" => ($arParams["DETAIL_TEXT_USE_HTML_EDITOR"]? "HTML": "T"),
				"MULTIPLE" => "N",
				"ROW_COUNT" => "5",
				"COL_COUNT" => $COL_COUNT,
                "SORT" => "9100"
			),
			"DETAIL_PICTURE" => array(
				"PROPERTY_TYPE" => "F",
				"FILE_TYPE" => "jpg, gif, bmp, png, jpeg",
				"MULTIPLE" => "N",
                "SORT" => "35",
			),
		);

		// add them to edit-list
		foreach ($arResult["PROPERTY_LIST_FULL"] as $key => $arr)
		{
			if (in_array($key, $arParams["PROPERTY_CODES"])) $arResult["PROPERTY_LIST"][] = $key;
		}

		// get iblock property list
		$rsIBLockPropertyList = CIBlockProperty::GetList(array("sort"=>"asc", "name"=>"asc"), array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arParams["IBLOCK_ID"]));
		while ($arProperty = $rsIBLockPropertyList->GetNext())
		{
			// get list of property enum values
			if ($arProperty["PROPERTY_TYPE"] == "L")
			{
				$rsPropertyEnum = CIBlockProperty::GetPropertyEnum($arProperty["ID"]);
				$arProperty["ENUM"] = array();
				while ($arPropertyEnum = $rsPropertyEnum->GetNext())
				{
					$arProperty["ENUM"][$arPropertyEnum["ID"]] = $arPropertyEnum;
				}
			}

			if ($arProperty["PROPERTY_TYPE"] == "T")
			{
				if (empty($arProperty["COL_COUNT"])) $arProperty["COL_COUNT"] = "30";
				if (empty($arProperty["ROW_COUNT"])) $arProperty["ROW_COUNT"] = "5";
			}

			if(strlen($arProperty["USER_TYPE"]) > 0 )
			{
				$arUserType = CIBlockProperty::GetUserType($arProperty["USER_TYPE"]);
				if(array_key_exists("GetPublicEditHTML", $arUserType))
					$arProperty["GetPublicEditHTML"] = $arUserType["GetPublicEditHTML"];
				else
					$arProperty["GetPublicEditHTML"] = false;
			}
			else
			{
				$arProperty["GetPublicEditHTML"] = false;
			}

			// add property to edit-list
			if (in_array($arProperty["CODE"], $arParams["PROPERTY_CODES"]))
				$arResult["PROPERTY_LIST"][] = $arProperty["CODE"];

			$arResult["PROPERTY_LIST_FULL"][$arProperty["CODE"]] = $arProperty;
		}

		// set starting filter value
		$arFilter = array("IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"], "IBLOCK_ID" => $arParams["IBLOCK_ID"], "SHOW_NEW" => "Y");

		// check type of user association to iblock elements and add user association to filter
		if ($arParams["ELEMENT_ASSOC"] == "PROPERTY_ID" && strlen($arParams["ELEMENT_ASSOC_PROPERTY"]) && is_array($arResult["PROPERTY_LIST_FULL"][$arParams["ELEMENT_ASSOC_PROPERTY"]]))
		{
			if ($USER->GetID()){
            //////////////////////////////////////////////////////////////////////////////////
                $allChildsTree = getChildsId($USER->GetID(), false, true);
                $allChildsTree[] = $USER->GetID();
				$arFilter["PROPERTY_".$arParams["ELEMENT_ASSOC_PROPERTY"]] = $allChildsTree;
            //////////////////////////////////////////////////////////////////////////////////
				// $arFilter["PROPERTY_".$arParams["ELEMENT_ASSOC_PROPERTY"]] = $USER->GetID();
            } else {
				$arFilter["ID"] = -1;
            }
		}
		elseif ($USER->GetID())
		{
			$arFilter["CREATED_BY"] = $USER->GetID();
		}
		// additional bugcheck. situation can be found when property ELEMENT_ASSOC_PROPERTY does not exists and user is not registered
		else
		{
			$arFilter["ID"] = -1;
		}

		//check for access to current element
		if ($arParams["ID"] > 0)
		{
			if (empty($arFilter["ID"])) $arFilter["ID"] = $arParams["ID"];

			// get current iblock element

			// echo "<pre>"; print_r($arFilter); echo "</pre>";

			$rsIBlockElements = CIBlockElement::GetList(array("SORT" => "ASC"), $arFilter);

			if ($arElement = $rsIBlockElements->Fetch())
			{
				$bAllowAccess = true;

				if ($bWorkflowIncluded)
				{
					$LAST_ID = CIBlockElement::WF_GetLast($arElement['ID']);
					if ($LAST_ID != $arElement["ID"])
					{
						$rsElement = CIBlockElement::GetByID($LAST_ID);
						$arElement = $rsElement->Fetch();
					}

					if (!in_array($arElement["WF_STATUS_ID"], $arParams["STATUS"]))
					{
						echo ShowError(GetMessage("IBLOCK_ADD_ACCESS_DENIED"));
						$bAllowAccess = false;
					}
				}
				else
				{
					if (in_array("INACTIVE", $arParams["STATUS"]) === true && $arElement["ACTIVE"] !== "N")
					{
						echo ShowError(GetMessage("IBLOCK_ADD_ACCESS_DENIED"));
						$bAllowAccess = false;
					}
				}
			}
			else
			{
				echo ShowError(GetMessage("IBLOCK_ADD_ELEMENT_NOT_FOUND"));
				$bAllowAccess = false;
			}
		}
		elseif ($arParams["MAX_USER_ENTRIES"] > 0 && $USER->GetID())
		{
			$rsIBlockElements = CIBlockElement::GetList(array("SORT" => "ASC"), $arFilter);
			$elements_count = $rsIBlockElements->SelectedRowsCount();
			if ($elements_count >= $arParams["MAX_USER_ENTRIES"])
			{
				echo ShowError(GetMessage("IBLOCK_ADD_MAX_ENTRIES_EXCEEDED"));
				$bHideAuth = true;
				$bAllowAccess = false;
			}
		}
	}

	if ($bAllowAccess)
	{
		// process POST data
		if (check_bitrix_sessid() && (!empty($_REQUEST["iblock_submit"]) || !empty($_REQUEST["iblock_apply"])))
		{
			$SEF_URL = $_REQUEST["SEF_APPLICATION_CUR_PAGE_URL"];
			$arResult["SEF_URL"] = $SEF_URL;

			$arProperties = $_REQUEST["PROPERTY"];

			$arUpdateValues = array();
			$arUpdatePropertyValues = array();

			// process properties list
			foreach ($arParams["PROPERTY_CODES"]  as $i => $propertyCode)
			{
				$arPropertyValue = $arProperties[$propertyCode];
				// check if property is a real property, or element field
				if (!in_array($propertyCode,$arIblockFields))//intval($propertyID) > 0
				{
					// for non-file properties
					if ($arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] != "F")
					{
						// for multiple properties
						if ($arResult["PROPERTY_LIST_FULL"][$propertyCode]["MULTIPLE"] == "Y")
						{
							$arUpdatePropertyValues[$propertyCode] = array();

							if (!is_array($arPropertyValue))
							{
								$arUpdatePropertyValues[$propertyCode][] = $arPropertyValue;
							}
							else
							{
								foreach ($arPropertyValue as $key => $value)
								{
									if (
										$arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] == "L" && intval($value) > 0
										||
										$arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] != "L" && !empty($value)
									)
									{
										$arUpdatePropertyValues[$propertyCode][] = $value;
									}
								}
							}
						}
						// for single properties
						else
						{
							if ($arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] != "L")
								$arUpdatePropertyValues[$propertyCode] = $arPropertyValue[0];
							else
								$arUpdatePropertyValues[$propertyCode] = $arPropertyValue;
						}
					}
					// for file properties
					else
					{
						$arUpdatePropertyValues[$propertyCode] = array();
						foreach ($arPropertyValue as $key => $value)
						{
							$arFile = $_FILES["PROPERTY_FILE_".$propertyCode."_".$key];
							$arFile["del"] = $_REQUEST["DELETE_FILE"][$propertyCode][$key] == "Y" ? "Y" : "";
							$arUpdatePropertyValues[$propertyCode][$key] = $arFile;

							if(($arParams["MAX_FILE_SIZE"] > 0) && ($arFile["size"] > $arParams["MAX_FILE_SIZE"]))
								$arResult["ERRORS"][] = GetMessage("IBLOCK_ERROR_FILE_TOO_LARGE");
						}

						if (count($arUpdatePropertyValues[$propertyCode]) == 0)
							unset($arUpdatePropertyValues[$propertyCode]);
					}
				}
				else
				{
					// for "virtual" properties
					if ($propertyCode == "IBLOCK_SECTION")
					{
						if (!is_array($arProperties[$propertyCode]))
							$arProperties[$propertyCode] = array($arProperties[$propertyCode]);
						$arUpdateValues[$propertyCode] = $arProperties[$propertyCode];

						if ($arParams["LEVEL_LAST"] == "Y" && is_array($arUpdateValues[$propertyCode]))
						{
							foreach ($arUpdateValues[$propertyCode] as $section_id)
							{
								$rsChildren = CIBlockSection::GetList(
									array("SORT" => "ASC"),
									array(
										"IBLOCK_ID" => $arParams["IBLOCK_ID"],
										"SECTION_ID" => $section_id,
									),
									false,
									array("ID")
								);
								if ($rsChildren->SelectedRowsCount() > 0)
								{
									$arResult["ERRORS"][] = GetMessage("IBLOCK_ADD_LEVEL_LAST_ERROR");
									break;
								}
							}
						}

						if ($arParams["MAX_LEVELS"] > 0 && count($arUpdateValues[$propertyCode]) > $arParams["MAX_LEVELS"])
						{
							$arResult["ERRORS"][] = str_replace("#MAX_LEVELS#", $arParams["MAX_LEVELS"], GetMessage("IBLOCK_ADD_MAX_LEVELS_EXCEEDED"));
						}
					}
					else
					{
						if($arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] == "F")
						{
							$arFile = $_FILES["PROPERTY_FILE_".$propertyCode."_0"];
							$arFile["del"] = $_REQUEST["DELETE_FILE"][$propertyCode][0] == "Y" ? "Y" : "";
							$arUpdateValues[$propertyCode] = $arFile;
							if ($arParams["MAX_FILE_SIZE"] > 0 && $arFile["size"] > $arParams["MAX_FILE_SIZE"])
								$arResult["ERRORS"][] = GetMessage("IBLOCK_ERROR_FILE_TOO_LARGE");
						}
						elseif($arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] == "HTML")
						{
							if($propertyCode == "DETAIL_TEXT")
								$arUpdateValues["DETAIL_TEXT_TYPE"] = "html";
							if($propertyCode == "PREVIEW_TEXT")
								$arUpdateValues["PREVIEW_TEXT_TYPE"] = "html";
							$arUpdateValues[$propertyCode] = $arProperties[$propertyCode][0];
						}
						else
						{
							if($propertyCode == "DETAIL_TEXT")
								$arUpdateValues["DETAIL_TEXT_TYPE"] = "html";
							if($propertyCode == "PREVIEW_TEXT")
								$arUpdateValues["PREVIEW_TEXT_TYPE"] = "html";
							$arUpdateValues[$propertyCode] = $arProperties[$propertyCode][0];
						}
					}
				}
			}

			// check required properties
			foreach ($arParams["PROPERTY_CODES_REQUIRED"] as $key => $propertyCode)
			{
				$bError = false;
				$propertyValue = !in_array($propertyCode,$arIblockFields) ? $arUpdatePropertyValues[$propertyCode] : $arUpdateValues[$propertyCode];

				//Files check
				if ($arResult["PROPERTY_LIST_FULL"][$propertyCode]['PROPERTY_TYPE'] == 'F')
				{
					//New element
					if ($arParams["ID"] <= 0)
					{
						$bError = true;
						if(is_array($propertyValue))
						{
							if(array_key_exists("tmp_name", $propertyValue) && array_key_exists("size", $propertyValue))
							{
								if($propertyValue['size'] > 0)
								{
									$bError = false;
								}
							}
							else
							{
								foreach ($propertyValue as $arFile)
								{
									if ($arFile['size'] > 0)
									{
										$bError = false;
										break;
									}
								}
							}
						}
					}
					//Element field
					elseif (!in_array($propertyCode,$arIblockFields))
					{
						if ($propertyValue['size'] <= 0)
						{
							if (intval($arElement[$propertyCode]) <= 0 || $propertyValue['del'] == 'Y')
								$bError = true;
						}
					}
					//Element property
					else
					{
						$dbProperty = CIBlockElement::GetProperty(
							$arElement["IBLOCK_ID"],
							$arParams["ID"],
							"sort", "asc",
							array("CODE"=>$propertyCode)
						);

						$bCount = 0;
						while ($arProperty = $dbProperty->Fetch())
							$bCount++;

						foreach ($propertyValue as $arFile)
						{
							if ($arFile['size'] > 0)
							{
								$bCount++;
								break;
							}
							elseif ($arFile['del'] == 'Y')
							{
								$bCount--;
							}
						}

						$bError = $bCount <= 0;
					}
				}
				//multiple property
				elseif ($arResult["PROPERTY_LIST_FULL"][$propertyCode]["MULTIPLE"] == "Y" || $arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] == "L")
				{
					if(is_array($propertyValue))
					{
						$bError = true;
						foreach($propertyValue as $value)
						{
							if(strlen($value) > 0)
							{
								$bError = false;
								break;
							}
						}
					}
					elseif(strlen($propertyValue) <= 0)
					{
						$bError = true;
					}
				}
				//single
				elseif (is_array($propertyValue) && array_key_exists("VALUE", $propertyValue))
				{
					if(strlen($propertyValue["VALUE"]) <= 0)
						$bError = true;
				}
				elseif (!is_array($propertyValue))
				{
					if(strlen($propertyValue) <= 0)
						$bError = true;
				}

				if ($bError)
				{
					$arResult["ERRORS"][] = str_replace("#PROPERTY_NAME#", !in_array($propertyCode,$arIblockFields) ? $arResult["PROPERTY_LIST_FULL"][$propertyCode]["NAME"] : (!empty($arParams["CUSTOM_TITLE_".$propertyCode]) ? $arParams["CUSTOM_TITLE_".$propertyCode] : GetMessage("IBLOCK_FIELD_".$propertyCode)), GetMessage("IBLOCK_ADD_ERROR_REQUIRED"));
				}
			}

			// check captcha
			if ($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0)
			{
				if (!$APPLICATION->CaptchaCheckCode($_REQUEST["captcha_word"], $_REQUEST["captcha_sid"]))
				{
					$arResult["ERRORS"][] = GetMessage("IBLOCK_FORM_WRONG_CAPTCHA");
				}
			}

			if (count($arResult["ERRORS"]) == 0)
			{
			
		
			//////////////////////////////////////////////////////////////////////////////////////////////
				if ($arParams["ELEMENT_ASSOC"] == "PROPERTY_ID" && !isset($_REQUEST['CODE']))
					$arUpdatePropertyValues[$arParams["ELEMENT_ASSOC_PROPERTY"]] = $USER->GetID();
				$arUpdateValues["MODIFIED_BY"] = $USER->GetID();

				$arUpdateValues["PROPERTY_VALUES"] = $arUpdatePropertyValues;

				if ($bWorkflowIncluded && strlen($arParams["STATUS_NEW"]) > 0)
				{
					$arUpdateValues["WF_STATUS_ID"] = $arParams["STATUS_NEW"];
					$arUpdateValues["ACTIVE"] = "Y";
				}
				else
				{
					if ($arParams["STATUS_NEW"] == "ANY")
					{
						$arUpdateValues["ACTIVE"] = "N";
					}
					elseif ($arParams["STATUS_NEW"] == "N")
					{
						$arUpdateValues["ACTIVE"] = "Y";
					}
					else
					{
						if ($arParams["ID"] <= 0 ) $arUpdateValues["ACTIVE"] = "N";
						//$arUpdateValues["ACTIVE"] = $arParams["ID"] > 0 ? "Y" : "N";
					}
				}

				// update existing element
				$oElement = new CIBlockElement();
				if ($arParams["ID"] > 0)
				{
					$sAction = "EDIT";

					$bFieldProps = array();
					foreach($arUpdateValues["PROPERTY_VALUES"] as $prop_code=>$v)
					{
						$bFieldProps[$prop_code]=true;
					}
					$dbPropV = CIBlockElement::GetProperty($arParams["IBLOCK_ID"], $arParams["ID"], "sort", "asc", Array("ACTIVE"=>"Y"));
					while($arPropV = $dbPropV->Fetch())
					{
						if(!array_key_exists($arPropV["CODE"], $bFieldProps) && $arPropV["PROPERTY_TYPE"] != "F")
						{
							if($arPropV["MULTIPLE"] == "Y")
							{
								if(!array_key_exists($arPropV["CODE"], $arUpdateValues["PROPERTY_VALUES"]))
									$arUpdateValues["PROPERTY_VALUES"][$arPropV["CODE"]] = array();
								$arUpdateValues["PROPERTY_VALUES"][$arPropV["CODE"]][$arPropV["PROPERTY_VALUE_ID"]] = array(
									"VALUE" => $arPropV["VALUE"],
									"DESCRIPTION" => $arPropV["DESCRIPTION"],
								);
							}
							else
							{
								$arUpdateValues["PROPERTY_VALUES"][$arPropV["CODE"]] = array(
									"VALUE" => $arPropV["VALUE"],
									"DESCRIPTION" => $arPropV["DESCRIPTION"],
								);
							}
						}
					}

					if (!$res = $oElement->Update($arParams["ID"], $arUpdateValues, $bWorkflowIncluded, true, $arParams["RESIZE_IMAGES"]))
					{
						$arResult["ERRORS"][] = $oElement->LAST_ERROR;
					}
				}
				// add new element
				else
				{
					$sectionProps = getSectionProps(5,$_REQUEST['PROPERTY']['IBLOCK_SECTION']);
					
					$arUpdateValues["IBLOCK_ID"] = $arParams["IBLOCK_ID"];

					// set activity start date for new element to current date. Change it, if ya want ;-)
					if (strlen($arUpdateValues["DATE_ACTIVE_FROM"]) <= 0)
					{
						$arUpdateValues["DATE_ACTIVE_FROM"] = ConvertTimeStamp(false, "FULL");
					}

					
					if(in_array('PROPERTY_Images',$sectionProps['UF_PROPERTY'])){					
					//	$arUpdateValues['DETAIL_PICTURE'] = '';
					}

					
					/*if($_REQUEST['mainPhotoId']){
					
						$idfilenew = CFile::SaveFile(CFile::MakeFileArray($_REQUEST['mainPhotoId']), "testdir");
						CFile::Delete($_REQUEST['mainPhotoId']);					
						$arUpdateValues['DETAIL_PICTURE'] = CFile::MakeFileArray($idfilenew);
					}*/

					$arUpdateValues['DETAIL_PICTURE'] = CFile::MakeFileArray($_REQUEST['mainPhotoId']);
					$sAction = "ADD";
					if (!$arParams["ID"] = $oElement->Add($arUpdateValues, $bWorkflowIncluded, true, $arParams["RESIZE_IMAGES"]))
					{
						$arResult["ERRORS"][] = $oElement->LAST_ERROR;
					}
					else
					{
					
						if($arParams["ID"]){

						
							//if(in_array('PROPERTY_Images',$sectionProps['UF_PROPERTY'])){
								//перезапись файла из временной дериктории
								$newFiles = array();
								foreach($arResult['ELEMENT_PROPERTIES']['preAddImages'] as $img){
								
											
									$idfilenew = CFile::SaveFile(CFile::MakeFileArray($img['VALUE']), "testdir");
									$newFiles[] = array('VALUE' => $idfilenew);
									CFile::Delete($img['VALUE']);									
								}
								$arResult['ELEMENT_PROPERTIES']['preAddImages'] = $newFiles;
							
								CIBlockElement::SetPropertyValueCode($arParams["ID"], 'Images', $arResult['ELEMENT_PROPERTIES']['preAddImages']);
							//}				
						}	
					
						if($arParams["SEND_EMAIL"] == "Y")
						{
							if(!empty($arUpdateValues["IBLOCK_SECTION"]["0"]))
							{
								$SECTION_ID = $arUpdateValues["IBLOCK_SECTION"]["0"];
							}
							else 
							{
								$SECTION_ID = 0;
							}
						
   					   		$arFields = array(
								"NAME" => $arUpdateValues["NAME"],
								"SUBJECT" => $arParams["SUBJECT"],
								"EMAIL_TO"    => $arParams["EMAIL_TO"],
								"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
								"IBLOCK_ID" => $arParams["IBLOCK_ID"],
								"ID" => $arParams["ID"],
								"SECTION_ID" => $SECTION_ID,
                         	);
	                        if(!empty($arParams["EVENT_MESSAGE_ID"]))
							{
								foreach($arParams["EVENT_MESSAGE_ID"] as $v)
									if(IntVal($v) > 0)
										CEvent::Send($arParams["EVENT_NAME"], SITE_ID, $arFields, "N", IntVal($v));
							}
							else
								CEvent::Send($arParams["EVENT_NAME"], SITE_ID, $arFields);

							
						}
					}

					if (!empty($_REQUEST["iblock_apply"]) && strlen($SEF_URL) > 0)
					{
						if (strpos($SEF_URL, "?") === false) $SEF_URL .= "?edit=Y";
						elseif (strpos($SEF_URL, "edit=") === false) $SEF_URL .= "&edit=Y";
						$SEF_URL .= "&CODE=".$arParams["ID"];
					}
				}
			}

			// redirect to element edit form or to elements list
			if (count($arResult["ERRORS"]) == 0)
			{
				if (!empty($_REQUEST["iblock_submit"]))
				{
					if (strlen($arParams["LIST_URL"]) > 0)
					{
						$sRedirectUrl = $arParams["LIST_URL"];
					}
					else
					{
						if (strlen($SEF_URL) > 0)
						{
							$SEF_URL = str_replace("edit=Y", "", $SEF_URL);
							$SEF_URL = str_replace("?&", "?", $SEF_URL);
							$SEF_URL = str_replace("&&", "&", $SEF_URL);
							$sRedirectUrl = $SEF_URL;
						}
						else
						{
							$sRedirectUrl = $APPLICATION->GetCurPageParam("", array("edit", "CODE"), $get_index_page=false);
						}

					}
				}
				else
				{
					if (strlen($SEF_URL) > 0)
						$sRedirectUrl = $SEF_URL;
					else
						$sRedirectUrl = $APPLICATION->GetCurPageParam("edit=Y&CODE=".$arParams["ID"], array("edit", "CODE"), $get_index_page=false);
				}

				$sAction = $sAction == "ADD" ? "ADD" : "EDIT";
				$sRedirectUrl .= (strpos($sRedirectUrl, "?") === false ? "?" : "&")."strIMessage=";
				$sRedirectUrl .= urlencode($arParams["USER_MESSAGE_".$sAction]);
				$sRedirectUrl .= '&CODE='.$arParams["ID"];
				//echo $sRedirectUrl;
				LocalRedirect($sRedirectUrl);
				exit();
			}
		}

		//prepare data for form
		$arResult["PROPERTY_REQUIRED"] = is_array($arParams["PROPERTY_CODES_REQUIRED"]) ? $arParams["PROPERTY_CODES_REQUIRED"] : array();
        $arResult["PROPERTY_REQUIRED"] = $arParams["PROPERTY_REQUIRED"];
		if ($arParams["ID"] > 0)
		{
			// $arElement is defined before in elements rights check
			$rsElementSections = CIBlockElement::GetElementGroups($arElement["ID"]);
			$arElement["IBLOCK_SECTION"] = array();
			while ($arSection = $rsElementSections->GetNext())
			{
				$arElement["IBLOCK_SECTION"][] = array("VALUE" => $arSection["ID"]);
			}

			$arResult["ELEMENT"] = array();
			foreach($arElement as $key => $value)
			{
				$arResult["ELEMENT"]["~".$key] = $value;
				if(!is_array($value) && !is_object($value))
					$arResult["ELEMENT"][$key] = htmlspecialcharsbx($value);
				else
					$arResult["ELEMENT"][$key] = $value;
			}

			//Restore HTML if needed
			if(
				$arParams["DETAIL_TEXT_USE_HTML_EDITOR"]
				&& array_key_exists("DETAIL_TEXT", $arResult["ELEMENT"])
				&& strtolower($arResult["ELEMENT"]["DETAIL_TEXT_TYPE"]) == "html"
			)
				$arResult["ELEMENT"]["DETAIL_TEXT"] = $arResult["ELEMENT"]["~DETAIL_TEXT"];

			if(
				$arParams["PREVIEW_TEXT_USE_HTML_EDITOR"]
				&& array_key_exists("PREVIEW_TEXT", $arResult["ELEMENT"])
				&& strtolower($arResult["ELEMENT"]["PREVIEW_TEXT_TYPE"]) == "html"
			)
				$arResult["ELEMENT"]["PREVIEW_TEXT"] = $arResult["ELEMENT"]["~PREVIEW_TEXT"];


			// $arResult["ELEMENT"] = $arElement;

			// load element properties
			$rsElementProperties = CIBlockElement::GetProperty($arParams["IBLOCK_ID"], $arElement["ID"], $by="sort", $order="asc");
			$arResult["ELEMENT_PROPERTIES"] = array();
			while ($arElementProperty = $rsElementProperties->Fetch())
			{
				if(!array_key_exists($arElementProperty["CODE"], $arResult["ELEMENT_PROPERTIES"]))
					$arResult["ELEMENT_PROPERTIES"][$arElementProperty["CODE"]] = array();

				if(is_array($arElementProperty["VALUE"]))
				{
					$htmlvalue = array();
					foreach($arElementProperty["VALUE"] as $k => $v)
					{
						if(is_array($v))
						{
							$htmlvalue[$k] = array();
							foreach($v as $k1 => $v1)
								$htmlvalue[$k][$k1] = htmlspecialcharsbx($v1);
						}
						else
						{
							$htmlvalue[$k] = htmlspecialcharsbx($v);
						}
					}
				}
				else
				{
					$htmlvalue = htmlspecialcharsbx($arElementProperty["VALUE"]);
				}

				$arResult["ELEMENT_PROPERTIES"][$arElementProperty["CODE"]][] = array(
					"ID" => htmlspecialcharsbx($arElementProperty["ID"]),
					"CODE" => htmlspecialcharsbx($arElementProperty["CODE"]),
					"VALUE" => $htmlvalue,
					"~VALUE" => $arElementProperty["VALUE"],
					"VALUE_ID" => htmlspecialcharsbx($arElementProperty["PROPERTY_VALUE_ID"]),
					"VALUE_ENUM" => htmlspecialcharsbx($arElementProperty["VALUE_ENUM"]),
				);
			}

			// process element property files
			$arResult["ELEMENT_FILES"] = array();
			foreach ($arResult["PROPERTY_LIST"] as $propertyCode)
			{
				$arProperty = $arResult["PROPERTY_LIST_FULL"][$propertyCode];
				if ($arProperty["PROPERTY_TYPE"] == "F")
				{
					$arValues = array();
					if (!in_array($propertyCode,$arIblockFields))
					{
						foreach ($arResult["ELEMENT_PROPERTIES"][$propertyCode] as $arProperty)
						{
							$arValues[] = $arProperty["VALUE"];
						}
					}
					else
					{
						$arValues[] = $arResult["ELEMENT"][$propertyCode];
					}

					foreach ($arValues as $value)
					{
						if ($arFile = CFile::GetFileArray($value))
						{
							$arFile["IS_IMAGE"] = CFile::IsImage($arFile["FILE_NAME"], $arFile["CONTENT_TYPE"]);
							$arResult["ELEMENT_FILES"][$value] = $arFile;
						}
					}
				}
			}

			$bShowForm = true;
		}
		else
		{
			$bShowForm = true;
		}

		if ($bShowForm)
		{
			// prepare form data if some errors occured
			if (count($arResult["ERRORS"]) > 0)
			{
				//echo "<pre>",htmlspecialcharsbx(print_r($arUpdateValues, true)),"</pre>";
				foreach ($arUpdateValues as $key => $value)
				{
					if ($key == "IBLOCK_SECTION")
					{
						$arResult["ELEMENT"][$key] = array();
						if(!is_array($value))
						{
							$arResult["ELEMENT"][$key][] = array("VALUE" => htmlspecialcharsbx($value));
						}
						else
						{
							foreach ($value as $vkey => $vvalue)
							{
								$arResult["ELEMENT"][$key][$vkey] = array("VALUE" => htmlspecialcharsbx($vvalue));
							}
						}
					}
					elseif ($key == "PROPERTY_VALUES")
					{
						//Skip
					}
					elseif ($arResult["PROPERTY_LIST_FULL"][$key]["PROPERTY_TYPE"] == "F")
					{
						//Skip
					}
					elseif ($arResult["PROPERTY_LIST_FULL"][$key]["PROPERTY_TYPE"] == "HTML")
					{
						$arResult["ELEMENT"][$key] = $value;
					}
					else
					{
						$arResult["ELEMENT"][$key] = htmlspecialcharsbx($value);
					}
				}

				foreach ($arUpdatePropertyValues as $key => $value)
				{
					if ($arResult["PROPERTY_LIST_FULL"][$key]["PROPERTY_TYPE"] != "F")
					{
						$arResult["ELEMENT_PROPERTIES"][$key] = array();
						if(!is_array($value))
						{
							$value = array(
								array("VALUE" => $value),
							);
						}
						foreach($value as $vv)
						{
							if(is_array($vv))
							{
								if(array_key_exists("VALUE", $vv))
									$arResult["ELEMENT_PROPERTIES"][$key][] = array(
										"~VALUE" => $vv["VALUE"],
										"VALUE" => htmlspecialcharsbx($vv["VALUE"]),
									);
								else
									$arResult["ELEMENT_PROPERTIES"][$key][] = array(
										"~VALUE" => $vv,
										"VALUE" => $vv,
									);
							}
							else
							{
								$arResult["ELEMENT_PROPERTIES"][$key][] = array(
									"~VALUE" => $vv,
									"VALUE" => htmlspecialcharsbx($vv),
								);
							}
						}
					}
				}
			}

			// prepare captcha
			if ($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0)
			{
				$arResult["CAPTCHA_CODE"] = htmlspecialcharsbx($APPLICATION->CaptchaGetCode());
			}

			$arResult["MESSAGE"] = htmlspecialcharsex($_REQUEST["strIMessage"]);

			if(empty($arResult['ELEMENT']['DETAIL_PICTURE']) && $_REQUEST['mainPhotoId']){
				$arResult['ELEMENT']['DETAIL_PICTURE'] = $_REQUEST['mainPhotoId'];
			}
		
			$this->IncludeComponentTemplate();
		}
	}
	if (!$bAllowAccess && !$bHideAuth)
	{
		//echo ShowError(GetMessage("IBLOCK_ADD_ACCESS_DENIED"));
		$APPLICATION->AuthForm("");
	}
}

?>