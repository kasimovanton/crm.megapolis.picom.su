<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
//pre($arResult["SECTION_LIST"]);

?>


<?if (count($arResult["ERRORS"])):?>
	<?=ShowError(implode("<br />", $arResult["ERRORS"]))?>
<?endif?>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
	<?=ShowNote($arResult["MESSAGE"])?>
<?endif?>

<form class="<?=$arParams["FORM_CLASS"]?>" name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">

	<?=bitrix_sessid_post()?>

	<?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>
	    <div class="<?=$arParams["ROW_CLASS"]?>">
            <label for="first_section">Категория</label>
            <select name="first_section" id="first_section">
                <option><?echo GetMessage("CT_BIEAF_PROPERTY_VALUE_NA")?></option>
                <?foreach ($arResult["SECTION_TREE"][0]['SECTIONS'] as $val):?>
                    <?
                    $key = $val;
                    $value = $arResult["SECTION_LIST"][$val]['VALUE'];
                    ?>
                    <?if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                        if (in_array($arResult["ELEMENT"]['IBLOCK_SECTION'][0],$arResult["SECTION_TREE"][0]['SECTIONS'])) {
                            $checked = true;
                        }
                       /* if (in_array($arResult["ELEMENT"]['IBLOCK_SECTION'][0],$arResult["SECTION_TREE"][0]['SECTIONS'])) {
                            $checked = true;
                        }вставить проверку второго*/
                    }?>
                    <option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$value?></option>
                <?endforeach?>
            </select>
        </div>
    <?/*<div class="special special_IBLOCK_SECTION form_row" style="display: block;">
        <label for="iIBLOCK_SECTION">
            Тип сделки:

        </label>
    </div>
        <div class="<?=$arParams["ROW_CLASS"]?>">
            <label for="iIBLOCK_SECTION">Тип сделки</label>
            <select class="offer_type" id="iIBLOCK_SECTION" name="PROPERTY[IBLOCK_SECTION]">
                <option><?echo GetMessage("CT_BIEAF_PROPERTY_VALUE_NA")?></option>
            </select>
        </div>*/?>

		<?if (is_array($arResult["PROPERTY_LIST"]) && count($arResult["PROPERTY_LIST"] > 0)):?>		
			<?foreach ($arResult["PROPERTY_LIST"] as $propertyCode):?>
				<div class="special special_<?=$propertyCode?> <?=$arParams["ROW_CLASS"]?><?if ($arParams["CUSTOM_CLASS_".$propertyCode]=='hidden'):?> hidden<?endif?>" >
                    <label for="i<?=$propertyCode?>">
                        <?if (!in_array($propertyCode,$arResult['IBLOCK_FIELDS'])):?>
                            <?=$arResult["PROPERTY_LIST_FULL"][$propertyCode]["NAME"]?>:
                        <?else:?>
                            <?=!empty($arParams["CUSTOM_TITLE_".$propertyCode]) ? $arParams["CUSTOM_TITLE_".$propertyCode] : GetMessage("IBLOCK_FIELD_".$propertyCode)?>:
                        <?endif?>                        					
                    </label>
                    <?
                    //echo "<pre>"; print_r($arResult["PROPERTY_LIST_FULL"]); echo "</pre>";
                    if (!in_array($propertyCode,$arResult['IBLOCK_FIELDS']))
                    {
                        if (
                            $arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] == "T"
                            &&
                            $arResult["PROPERTY_LIST_FULL"][$propertyCode]["ROW_COUNT"] == "1"
                        )
                            $arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] = "S";
                        elseif (
                            (
                                $arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] == "S"
                                ||
                                $arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] == "N"
                            )
                            &&
                            $arResult["PROPERTY_LIST_FULL"][$propertyCode]["ROW_COUNT"] > "1"
                        )
                            $arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] = "T";
                    }
                    elseif (($propertyCode == "TAGS") && CModule::IncludeModule('search'))
                        $arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"] = "TAGS";

                    if ($arResult["PROPERTY_LIST_FULL"][$propertyCode]["MULTIPLE"] == "Y")
                    {
                        $inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyCode]) : 0;
                        $inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyCode]["MULTIPLE_CNT"];
                    }
                    else
                    {
                        $inputNum = 1;
                    }

                    if($arResult["PROPERTY_LIST_FULL"][$propertyCode]["GetPublicEditHTML"])
                        $INPUT_TYPE = "USER_TYPE";
                    else
                        $INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyCode]["PROPERTY_TYPE"];

                    switch ($INPUT_TYPE):
                        case "USER_TYPE":
                            for ($i = 0; $i<$inputNum; $i++)
                            {
                                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                {
                                    $value = !in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyCode];
                                    $description = !in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["DESCRIPTION"] : "";
                                }
                                elseif ($i == 0)
                                {
                                    $value = in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyCode]["DEFAULT_VALUE"];
                                    $description = "";
                                }
                                else
                                {
                                    $value = "";
                                    $description = "";
                                }
                               // CIBlockPropertyElementList::GetPropertyFieldHtml()
                                echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][$propertyCode]["GetPublicEditHTML"],
                                    array(
                                        $arResult["PROPERTY_LIST_FULL"][$propertyCode],
                                        array(
                                            "VALUE" => $value,
                                            "DESCRIPTION" => $description,
                                        ),
                                        array(
                                            "VALUE" => "PROPERTY[".$propertyCode."][".$i."][VALUE]",
                                            "DESCRIPTION" => "PROPERTY[".$propertyCode."][".$i."][DESCRIPTION]",
                                            "FORM_NAME"=>"iblock_add",
                                        ),
                                    ));
                            ?><?
                            }
                        break;
                        case "TAGS":
                            $APPLICATION->IncludeComponent(
                                "bitrix:search.tags.input",
                                "",
                                array(
                                    "VALUE" => $arResult["ELEMENT"][$propertyCode],
                                    "NAME" => "PROPERTY[".$propertyCode."][0]",
                                    "TEXT" => "size=\"{$arResult["PROPERTY_LIST_FULL"][$propertyCode]["COL_COUNT"]}\" id=\"i{$propertyCode}\" class=\"{$arParams["CUSTOM_CLASS_".$propertyCode]}\"",
                                ), null, array("HIDE_ICONS"=>"Y")
                            );
                            break;
                        case "HTML":
                            $LHE = new CLightHTMLEditor;
                            $LHE->Show(array(
                                'id' => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[".$propertyCode."][0]"),
                                'width' => '100%',
                                'height' => '200px',
                                'inputName' => "PROPERTY[".$propertyCode."][0]",
                                'content' => $arResult["ELEMENT"][$propertyCode],
                                'bUseFileDialogs' => false,
                                'bFloatingToolbar' => false,
                                'bArisingToolbar' => false,
                                'toolbarConfig' => array(
                                    'Bold', 'Italic', 'Underline', 'RemoveFormat',
                                    'CreateLink', 'DeleteLink', 'Image', 'Video',
                                    'BackColor', 'ForeColor',
                                    'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull',
                                    'InsertOrderedList', 'InsertUnorderedList', 'Outdent', 'Indent',
                                    'StyleList', 'HeaderList',
                                    'FontList', 'FontSizeList',
                                ),
                            ));
                            break;
                        case "T":
                            for ($i = 0; $i<$inputNum; $i++)
                            {

                                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                {
                                    $value = !in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE"] : $arResult["ELEMENT"][$propertyCode];
                                }
                                elseif ($i == 0)
                                {
                                    $value = !in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyCode]["DEFAULT_VALUE"];
                                }
                                else
                                {
                                    $value = "";
                                }
                            ?>
                                <textarea class="<?=$arParams["CUSTOM_CLASS_".$propertyCode]?>" id="i<?=$propertyCode?>" cols="<?=$arResult["PROPERTY_LIST_FULL"][$propertyCode]["COL_COUNT"]?>" rows="<?=$arResult["PROPERTY_LIST_FULL"][$propertyCode]["ROW_COUNT"]?>" name="PROPERTY[<?=$propertyCode?>][<?=$i?>]"><?=$value?></textarea>
                            <?
                            }
                        break;

                        case "S":
                        case "N":
                            for ($i = 0; $i<$inputNum; $i++)
                            {
                                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                {
                                    $value = !in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE"] : $arResult["ELEMENT"][$propertyCode];
                                }
                                elseif ($i == 0)
                                {
                                    $value = in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyCode]["DEFAULT_VALUE"];

                                }
                                else
                                {
                                    $value = "";
                                }
                            ?>
                                <input class="<?=$arParams["CUSTOM_CLASS_".$propertyCode]?>" id="i<?=$propertyCode?>" type="text" name="PROPERTY[<?=$propertyCode?>][<?=$i?>]" size="25" value="<?=$value?>" /><?
                                if($arResult["PROPERTY_LIST_FULL"][$propertyCode]["USER_TYPE"] == "DateTime"):?><?
                                    $APPLICATION->IncludeComponent(
                                        'bitrix:main.calendar',
                                        '',
                                        array(
                                            'FORM_NAME' => 'iblock_add',
                                            'INPUT_NAME' => "PROPERTY[".$propertyCode."][".$i."]",
                                            'INPUT_VALUE' => $value,
                                        ),
                                        null,
                                        array('HIDE_ICONS' => 'Y')
                                    );
                                    ?><br /><small><?=GetMessage("IBLOCK_FORM_DATE_FORMAT")?><?=FORMAT_DATETIME?></small><?
                                endif
                                ?><?
                            }
                        break;

                        case "F":
                            for ($i = 0; $i<$inputNum; $i++)
                            {
                                $value = !in_array($propertyCode,$arResult['IBLOCK_FIELDS']) ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE"] : $arResult["ELEMENT"][$propertyCode];
                                ?>
                                <input type="hidden" name="PROPERTY[<?=$propertyCode?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE_ID"] : $i?>]" value="<?=$value?>" />
                                <input type="file" size="<?=$arResult["PROPERTY_LIST_FULL"][$propertyCode]["COL_COUNT"]?>"  name="PROPERTY_FILE_<?=$propertyCode?>_<?=$arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE_ID"] : $i?>" /><br />
                                <?

                                if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value]))
                                {
                                    ?>
                                    <input type="checkbox" name="DELETE_FILE[<?=$propertyCode?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyCode][$i]["VALUE_ID"] : $i?>]" id="file_delete_<?=$propertyCode?>_<?=$i?>" value="Y" /><label for="file_delete_<?=$propertyCode?>_<?=$i?>"><?=GetMessage("IBLOCK_FORM_FILE_DELETE")?></label><br />
                                    <?

                                    if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"])
                                    {
                                        ?>
                                        <img src="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>" height="<?=$arResult["ELEMENT_FILES"][$value]["HEIGHT"]?>" width="<?=$arResult["ELEMENT_FILES"][$value]["WIDTH"]?>" border="0" /><br />
                                        <?
                                    }
                                    else
                                    {
                                        ?>
                                        <?=GetMessage("IBLOCK_FORM_FILE_NAME")?>: <?=$arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"]?><br />
                                        <?=GetMessage("IBLOCK_FORM_FILE_SIZE")?>: <?=$arResult["ELEMENT_FILES"][$value]["FILE_SIZE"]?> b<br />
                                        [<a href="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>"><?=GetMessage("IBLOCK_FORM_FILE_DOWNLOAD")?></a>]<br />
                                        <?
                                    }
                                }
                            }

                        break;
                        case "L":

                            if ($arResult["PROPERTY_LIST_FULL"][$propertyCode]["LIST_TYPE"] == "C")
                                $type = $arResult["PROPERTY_LIST_FULL"][$propertyCode]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
                            else
                                $type = $arResult["PROPERTY_LIST_FULL"][$propertyCode]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";

                            switch ($type):
                                case "checkbox":
                                case "radio":

                                    //echo "<pre>"; print_r($arResult["PROPERTY_LIST_FULL"][$propertyCode]); echo "</pre>";

                                    foreach ($arResult["PROPERTY_LIST_FULL"][$propertyCode]["ENUM"] as $key => $arEnum)
                                    {
                                        $checked = false;
                                        if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                        {
                                            if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyCode]))
                                            {
                                                foreach ($arResult["ELEMENT_PROPERTIES"][$propertyCode] as $arElEnum)
                                                {
                                                    if ($arElEnum["VALUE"] == $key) {$checked = true; break;}
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if ($arEnum["DEF"] == "Y") $checked = true;
                                        }

                                        ?>
                                        <input class="<?=$arParams["CUSTOM_CLASS_".$propertyCode]?>" type="<?=$type?>" name="PROPERTY[<?=$propertyCode?>]<?=$type == "checkbox" ? "[".$key."]" : ""?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> /><label for="property_<?=$key?>"><?=$arEnum["VALUE"]?></label><br />
                                        <?
                                    }
                                break;

                                case "dropdown":
                                case "multiselect":
                                ?>
                                    <select class="<?=$arParams["CUSTOM_CLASS_".$propertyCode]?>" id="i<?=$propertyCode?>" name="PROPERTY[<?=$propertyCode?>]<?=$type=="multiselect" ? "[]\" size=\"".$arResult["PROPERTY_LIST_FULL"][$propertyCode]["ROW_COUNT"]."\" multiple=\"multiple" : ""?>">
                                        <option value="0"><?echo GetMessage("CT_BIEAF_PROPERTY_VALUE_NA")?></option>
                                        <?
                                        if (!in_array($propertyCode,$arResult['IBLOCK_FIELDS'])) $sKey = "ELEMENT_PROPERTIES";
                                        else $sKey = "ELEMENT";

                                        foreach ($arResult["PROPERTY_LIST_FULL"][$propertyCode]["ENUM"] as $key => $arEnum)
                                        {
                                            $checked = false;
                                            if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                            {
                                                foreach ($arResult[$sKey][$propertyCode] as $elKey => $arElEnum)
                                                {
                                                    if ($key == $arElEnum["VALUE"]) {$checked = true; break;}
                                                }
                                            }
                                            else
                                            {
                                                if ($arEnum["DEF"] == "Y") $checked = true;
                                            }
                                            ?>
                                            <option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option>
                                            <?
                                        }
                                        ?>
                                    </select>
                                <?
                                break;

                            endswitch;
                        break;
                    endswitch;?>
                    <?if(in_array($propertyCode, $arResult["PROPERTY_REQUIRED"])):?>
                        <span class="starrequired">*</span>:
                    <?endif?>
                </div>
			<?endforeach;?>
			<?if($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0):?>
				<div class="<?=$arParams["ROW_CLASS"]?>">
                    <?//=GetMessage("IBLOCK_FORM_CAPTCHA_TITLE")?>
					<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
					<label for="captcha"><?=GetMessage("IBLOCK_FORM_CAPTCHA_PROMPT")?><span class="starrequired">*</span>:</label>
					<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
					<input id="captcha" type="text" name="captcha_word" maxlength="50" value=""/>
				</div>
			<?endif?>		
		<?endif?>
		<div class="<?=$arParams["ROW_CLASS"]?>">
			<input class="<?=$arParams["SUBMIT_CLASS"]?>" type="submit" name="iblock_submit" value="<?=GetMessage("IBLOCK_FORM_SUBMIT")?>" />
			<?if (strlen($arParams["LIST_URL"]) > 0 && $arParams["ID"] > 0):?>
                <input class="<?=$arParams["SUBMIT_CLASS"]?>" type="submit" name="iblock_apply" value="<?=GetMessage("IBLOCK_FORM_APPLY")?>" />
            <?endif?>
			<?/*<input type="reset" value="<?=GetMessage("IBLOCK_FORM_RESET")?>" />*/?>
        </div>	
	<?if (strlen($arParams["LIST_URL"]) > 0):?>
        <a href="<?=$arParams["LIST_URL"]?>"><?=GetMessage("IBLOCK_FORM_BACK")?></a>
    <?endif?>
</form>
<script>
//$(function(){
    var props = JSON.parse('<?=json_encode($arResult['PROPERTY_SECTION_LIST'])?>');
    var section_tree = JSON.parse('<?=json_encode($arResult["SECTION_TREE"])?>');
    var sections = JSON.parse('<?=json_encode($arResult["SECTION_LIST"])?>');
    $('.special').hide();

    //console.log(sections);
    //$('.special.special_IBLOCK_SECTION').show();
    $('#iIBLOCK_SECTION').change(function(){
        //console.log($(this).val());
        $('.special').hide();
        $('.special.special_IBLOCK_SECTION').show();
        //console.log(props[$(this).val()]['UF_PROPERTY']);
        var sectionProps = props[$(this).val()]['UF_PROPERTY'];
        for(var i=0; i<sectionProps.length; i++) {
            $('.special_'+sectionProps[i]).show();
        }

        if ($('#iObjectTypeGaraj option:selected').text()=='Гараж') {
            $('.special_ObjectSubtype1').show();
            $('.special_ObjectSubtype2').hide();
        } else if ($('#iObjectTypeGaraj option:selected').text()=='Машиноместо') {
            $('.special_ObjectSubtype2').show();
            $('.special_ObjectSubtype1').hide();
        } else {
            $('.special_ObjectSubtype2').hide();
            $('.special_ObjectSubtype1').hide();
        }
    })

    $('#first_section').change(function(){
        $('.special').hide();
        $('#iIBLOCK_SECTION option[value!=0]').remove();
        if ($(this).val()>0) {
            //console.log(section_tree[$(this).val()]['SECTIONS']);
            $.each(section_tree[$(this).val()]['SECTIONS'], function(i){
                //console.log(this);
                //$('#iIBLOCK_SECTION option[value='+this+']').show();
                $('#iIBLOCK_SECTION').append('<option value="' + this + '">' + sections[this]['VALUE'] + '</option>');
            });
        }
        $('.special.special_IBLOCK_SECTION').show();
    })

$('#iObjectTypeGaraj').change(function(){
    if ($('#iObjectTypeGaraj option:selected').text()=='Гараж') {
        $('.special_ObjectSubtype1').show();
        $('.special_ObjectSubtype2').hide();
    }
    if ($('#iObjectTypeGaraj option:selected').text()=='Машиноместо') {
        $('.special_ObjectSubtype2').show();
        $('.special_ObjectSubtype1').hide();
    }
})



//})


</script>
<?
//echo "<pre>Template arParams: "; print_r($arParams); echo "</pre>";
//echo "<pre>Template arResult: "; print_r($arResult); echo "</pre>";
//echo "<pre>Template Request: "; print_r($_REQUEST); echo "</pre>";
//exit();
?>



