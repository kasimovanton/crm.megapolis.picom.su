<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

function getUsersAgentDb(
	$arParams,
    $select = array('UF_HEAD'),
    $fields = array('ID'),
    $filter = array('GROUPS_ID'=>6,'ACTIVE'=>'Y'),
    $vBy = 'id',
    $vOrder = 'asc')
{

    $rsUsers = CUser::GetList(($by=$vBy), ($order=$vOrder),$filter,array('SELECT'=>$select,'FIELDS' => $fields,
	
	'NAV_PARAMS' => array(		"bDescPageNumbering" => false,
								"nPageSize"=>$arParams["USERS_PER_PAGE"],
								"bShowAll" => false,
								"sNameTemplate" => $arParams["NAME_TEMPLATE"])	
	
	));
    return $rsUsers;
}

$total_ads = getArrayOfCntByProp(5, "PROPERTY_AUTHOR");
$total_client = getArrayOfCntByProp(10, "PROPERTY_RIELTOR");


global $USER;

$userID=$USER->GetID();
//echo $userID;
$users = getUsers(array("UF_HEAD"),array("ID",'LAST_NAME','SECOND_NAME','NAME','PERSONAL_NOTES','DATE_REGISTER','PERSONAL_PHOTO','PERSONAL_PHONE','PERSONAL_MOBILE'),array('ACTIVE'=>'Y'));
//pre($users);
$childUsersId = getChildsId($userID,$users);
//pre($childUsersId);


$usersCount = 0;
$page = 0;
$usersID = '';
foreach ($childUsersId as $childId) {

	if($arParams["USERS_PER_PAGE"] == $usersCount){
		$page++;
		$usersCount = 0;
	}    
	$usersID .= $childId.'|';
	$user = $users[$childId];
    $user['HEAD'] = $users[$user['UF_HEAD']];
    $arResult['ITEMS'][$page][$childId] = $user;
    $arResult['ITEMS'][$page][$childId]['TOTAL_ADS'] = $total_ads[$childId] ? $total_ads[$childId] : 0;
    $arResult['ITEMS'][$page][$childId]['TOTAL_CLIENT'] = $total_client[$childId] ? $total_client[$childId] : 0;
	
	
	$usersCount++;
}

/******************************************************************/
$arResult["ERROR_MESSAGE"] = $strError;
CPageOption::SetOptionString("main", "nav_page_in_session", "N");


$db_res = getUsersAgentDb($arParams,array("UF_HEAD"),array("ID",'LAST_NAME','SECOND_NAME','NAME','PERSONAL_NOTES','DATE_REGISTER','PERSONAL_PHOTO','PERSONAL_PHONE','PERSONAL_MOBILE'),array('ID'=>$usersID,'ACTIVE'=>'Y'));


if($db_res)
{
	$db_res->NavStart($arParams["USERS_PER_PAGE"], false);
	$arResult["NAV_STRING"] = $db_res->GetPageNavStringEx($navComponentObject, GetMessage("LU_TITLE_USER"), $arParams["PAGE_NAVIGATION_TEMPLATE"]);
	$arResult["NAV_RESULT"] = $db_res;
	$arResult["SHOW_RESULT"] = "Y";
	$arResult["SortingEx"]["SHOW_ABC"] = SortingEx("SHOW_ABC", $APPLICATION->GetCurPageParam());
	$arResult["SortingEx"]["NUM_POSTS"] = SortingEx("NUM_POSTS", $APPLICATION->GetCurPageParam());
	$arResult["SortingEx"]["POINTS"] = SortingEx("POINTS", $APPLICATION->GetCurPageParam());
	$arResult["SortingEx"]["DATE_REGISTER"] = SortingEx("DATE_REGISTER", $APPLICATION->GetCurPageParam());
	$arResult["SortingEx"]["LAST_VISIT"] = SortingEx("LAST_VISIT", $APPLICATION->GetCurPageParam());

}
$arResult['ITEMS'] = $arResult['ITEMS'][($arResult['NAV_RESULT']->PAGEN-1)];

/********************************************************************
				/Data
********************************************************************/
$this->IncludeComponentTemplate();
if ($arParams["SET_NAVIGATION"] != "N")
	$APPLICATION->AddChainItem(GetMessage("LU_TITLE_USER"));
if ($arParams["SET_TITLE"] != "N")
	$APPLICATION->SetTitle(GetMessage("LU_TITLE_USER"));
/******************************************************************/
?>