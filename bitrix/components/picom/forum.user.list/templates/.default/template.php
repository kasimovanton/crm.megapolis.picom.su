<?php
/**
 * Created by PhpStorm.
 * User: picom
 * Date: 25.11.14
 * Time: 14:06
 */
?>
<?foreach ($arResult['ITEMS'] as $user) {?>

    <div class="agent_search_card">
        <?if ($user['PERSONAL_PHOTO'])
    {
            $file = CFile::ResizeImageGet($user['PERSONAL_PHOTO'], array('width'=>62, 'height'=>62), BX_RESIZE_IMAGE_PROPORTIONAL, true,array());
            $img = '<img class="agent_img" src="'.$file['src'].'" />';
            echo $img;
        }?>
        <div class="agent_contacts">
            <div class="agent_name">
                <a href="<?=$arParams['SEF_FOLDER'].$user['ID']?>/">
                    <?=$user['LAST_NAME'].' '. $user['NAME'].' '.$user['SECOND_NAME']?>
                </a>
            </div>
            <?if ($user['PERSONAL_PHONE']):?>
                <div class="agent_phone"><?=$user['PERSONAL_PHONE']?></div>
            <?endif?>
            <?if ($user['PERSONAL_MOBILE']):?>
                <div class="agent_phone"><?=$user['PERSONAL_MOBILE']?></div>
            <?endif?>
            <a class="agent_email" href="mailto:<?=$user['EMAIL']?>"><?=$user['EMAIL']?></a>
        </div>
        <div class="agent_links">
            <div class="agent_clients">
                <div class="clients_ico"></div>
                <a href="/clients/?filter=<?=$user['ID']?>" class="agent_ads_link">
                    Клиенты
                    (<?=$user['TOTAL_CLIENT'];?>)
                </a>
            </div>
            <div class="agent_ads">
                <div class="ads_ico"></div>
                <a href="/ads/?Author=<?=$user['ID']?>" class="agent_ads_link">
                    Объявления
                    (<?=$user['TOTAL_ADS'];?>)
                </a>
            </div>
            <?//Раздел для объектов
            /*<div class="agent_objects">
                <div class="objects_ico"></div>
                <a href="#" class="agent_objects_link">Объекты</a>
            </div>*/?>
        </div>
        <div class="agent_description">
            <?if ($user['UF_HEAD']):?>
            <p>Начальник:
                <a href="<?=$arParams['SEF_FOLDER'].$user['HEAD']['ID']?>/">
                    <?=$user['HEAD']['LAST_NAME'].' '. $user['HEAD']['NAME'].' '.$user['HEAD']['SECOND_NAME']?>
                </a>
            </p>
            <?endif?>
            <p><?=$user['PERSONAL_NOTES']?></p>
        </div>
        <div class="agent_join_date">
            <span>Добавлен</span>
            <span>
                <?php $time = strtotime($user['DATE_REGISTER']);
                echo date('d.m.Y',$time);
                ?>
            </span>
            <div class="calendar_ico"></div>
        </div>
    </div>

<?}?>
<?

if ($arResult["NAV_RESULT"]->NavPageCount > 0):
?>
<div class="forum-navigation-box forum-navigation-bottom">
	<div class="forum-page-navigation">
		<?=$arResult["NAV_STRING"]?>
	</div>
	<div class="forum-clear-float"></div>
</div>
<?
endif;
?>