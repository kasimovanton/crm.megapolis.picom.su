<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

// pre($arParams,false);

function getUsersAgentDb(
	$arParams,
    $select = array('UF_HEAD'),
    $fields = array('ID'),
    $filter = array('GROUPS_ID'=>6,'ACTIVE'=>'Y'),
    $vBy = 'id',
    $vOrder = 'asc')
{
    if (isset($arParams['ALF_FILTER']) && $arParams['ALF_FILTER'] != 'no'){
        $filter['LAST_NAME'] = $arParams['ALF_FILTER'].'%';
        $filter['LAST_NAME_EXACT_MATCH'] = 'Y';
        // unset($filter['ID']);
    }
    
    $rsUsers = CUser::GetList(($by=$vBy), ($order=$vOrder),$filter,array('SELECT'=>$select,'FIELDS' => $fields,
	
	'NAV_PARAMS' => array(		"bDescPageNumbering" => false,
								"nPageSize"=>$arParams["USERS_COUNT"],
								"bShowAll" => false,
								"sNameTemplate" => $arParams["NAME_TEMPLATE"])	
	
	));
    return $rsUsers;
}

$total_ads = getArrayOfCntByProp(5, "PROPERTY_AUTHOR");
$total_client = getArrayOfCntByProp(10, "PROPERTY_RIELTOR");


global $USER;

$userID=$USER->GetID();


//� ������ ������ �� ����������� ����������, ��� ������, � ��� ������������ ��������
if (isset($arParams['ALF_FILTER']) && $arParams['ALF_FILTER'] != 'no'){
    $arFilter = array('ACTIVE'=>'Y', 'LAST_NAME' => $arParams['ALF_FILTER'].'%', 'LAST_NAME_EXACT_MATCH' => 'Y',);
    $arResult['usersForMainUsers'] = getUsers(array("UF_HEAD"),array("ID",'LAST_NAME','LAST_LOGIN','SECOND_NAME','NAME'),array('ACTIVE'=>'Y')); //��� ������ ����������� � ������
    
} else {
    $arFilter = array('ACTIVE'=>'Y');
}

$users = getUsers(array("UF_HEAD"),array("ID",'LAST_NAME','LAST_LOGIN','SECOND_NAME','NAME','PERSONAL_NOTES','DATE_REGISTER','PERSONAL_PHOTO','PERSONAL_PHONE','PERSONAL_MOBILE'),$arFilter, array('LAST_NAME'=>'asc', 'NAME' => 'asc', 'SECOND_NAME' => 'asc'));

/*
��� ����, ����� ������ ���������������� ������� BOSS_CHANGE � ALF_FILTER ���������� ���������� ������� getChildsId
��-�� ��� ��� ������������� ������������� ������� � ��������� ���������� ������������ ������
� ����� ������������ ������ ������ ��� �� ������ ���������� (��������, "�" � "��� �����������")
*/


if ($arParams['BOSS_CHANGE'] == 'all_mine'){
    $childUsersId = getChildsId($userID,$users);//������ ����������� ���� ������, ������ arrOfUsers,
} else {
    $childUsersId = getChildsId($userID,$users, true);
}

$arResult['arrOfchildUsersId'] = $childUsersId;


$allUsers=array();
foreach ($users as $badID){
    if ($badID['ID'] != 1){
        $allUsers[] = $badID['ID'];
    }
}


if($arParams['BOSS_CHANGE'] == 'all'){
    $arrOfUsers = $allUsers;
} else {
    $arrOfUsers = $childUsersId;
}

$usersCount = 0;
$page = 0;
$usersID = '';
foreach ($arrOfUsers as $childId) {

	if($arParams["USERS_COUNT"] == $usersCount){
		$page++;
		$usersCount = 0;
	}    
	$usersID .= $childId.'|';
	$user = $users[$childId];
    $user['HEAD'] = $users[$user['UF_HEAD']];
	if($arParams["USERS_COUNT"] > 0){
		$arResult['ITEMS'][$page][$childId] = $user;
		$arResult['ITEMS'][$page][$childId]['TOTAL_ADS'] = $total_ads[$childId] ? $total_ads[$childId] : 0;
		$arResult['ITEMS'][$page][$childId]['TOTAL_CLIENT'] = $total_client[$childId] ? $total_client[$childId] : 0;
	}else{
		$arResult['ITEMS'][$childId] = $user;
		$arResult['ITEMS'][$childId]['TOTAL_ADS'] = $total_ads[$childId] ? $total_ads[$childId] : 0;
		$arResult['ITEMS'][$childId]['TOTAL_CLIENT'] = $total_client[$childId] ? $total_client[$childId] : 0;	
	}
	
	$usersCount++;
}

/******************************************************************/
$arResult["ERROR_MESSAGE"] = $strError;
CPageOption::SetOptionString("main", "nav_page_in_session", "N");

$db_res = getUsersAgentDb($arParams,array("UF_HEAD"),array("ID",'LAST_NAME','SECOND_NAME','NAME','PERSONAL_NOTES','DATE_REGISTER','PERSONAL_PHOTO','PERSONAL_PHONE','PERSONAL_MOBILE'),array('ID'=>$usersID,'ACTIVE'=>'Y'));

if($db_res)
{
	$db_res->NavStart($arParams["USERS_COUNT"], true);
	$arResult["NAV_STRING"] = $db_res->GetPageNavStringEx($navComponentObject, GetMessage("LU_TITLE_USER"), $arParams["DETAIL_PAGER_TEMPLATE"]);
	$arResult["NAV_RESULT"] = $db_res;
	$arResult["SHOW_RESULT"] = "Y";
	$arResult["SortingEx"]["SHOW_ABC"] = SortingEx("SHOW_ABC", $APPLICATION->GetCurPageParam());
	$arResult["SortingEx"]["NUM_POSTS"] = SortingEx("NUM_POSTS", $APPLICATION->GetCurPageParam());
	$arResult["SortingEx"]["POINTS"] = SortingEx("POINTS", $APPLICATION->GetCurPageParam());
	$arResult["SortingEx"]["DATE_REGISTER"] = SortingEx("DATE_REGISTER", $APPLICATION->GetCurPageParam());
	$arResult["SortingEx"]["LAST_VISIT"] = SortingEx("LAST_VISIT", $APPLICATION->GetCurPageParam());

}

if($arParams["USERS_COUNT"] > 0){
$arResult['ITEMS'] = $arResult['ITEMS'][($arResult['NAV_RESULT']->PAGEN-1)];
}
/********************************************************************
				/Data
********************************************************************/
$this->IncludeComponentTemplate();
/*if ($arParams["SET_NAVIGATION"] != "N")
	$APPLICATION->AddChainItem(GetMessage("LU_TITLE_USER"));
if ($arParams["SET_TITLE"] != "N")
	$APPLICATION->SetTitle(GetMessage("LU_TITLE_USER"));*/
/******************************************************************/
?>