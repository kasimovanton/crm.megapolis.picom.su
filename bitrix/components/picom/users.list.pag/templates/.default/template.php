<?php
/**
 * Created by PhpStorm.
 * User: picom
 * Date: 25.11.14
 * Time: 14:06
 */
?>

<div class="search_menu">
    <div class="sorting">
        <span>Выбрать:</span>
        <div class="form_select boss">
            <select>
                <option value="all" <?if (isset($_SESSION['boss']) && $_SESSION['boss'] == 'all'):?>selected<?endif?>>Все агенты</option>
                <option value="mine" <?if (isset($_SESSION['boss']) && $_SESSION['boss'] == 'mine'):?>selected<?endif?>>Мои подчиненные</option>
                <option value="all_mine" <?if (isset($_SESSION['boss']) && $_SESSION['boss'] == 'all_mine'):?>selected<?endif?>>Все подчиненные</option>
            </select>
        </div>
        <div class="form_select alf_filter">
            <select class="short">
                <option value="no">Все</option>
                <?for ($i=176; $i<=207; $i++):?>
                    <?$symb = iconv('ISO-8859-5', 'UTF-8', chr($i));?>
                    <option value="<?=$symb;?>" <?if (isset($_SESSION['alf_filter']) && $_SESSION['alf_filter'] == $symb):?>selected<?endif?>><?=$symb;?></option>
                <?endfor;?>
            </select>
        </div>
        <span>Агентов на странице:</span>
        <div class="form_select agent_page">
            <select class="short">
                <option <?if($_SESSION['agent_page'] == 5):?>selected<?endif;?> numb="5">5</option>
                <option <?if($_SESSION['agent_page'] == 15 || !isset($_SESSION['agent_page'])):?>selected<?endif;?> numb="15">15</option>
                <option <?if($_SESSION['agent_page'] == 30):?>selected<?endif;?> numb="30">30</option>
                <option <?if($_SESSION['agent_page'] == 50):?>selected<?endif;?> numb="50">50</option>
                <option <?if($_SESSION['agent_page'] == 100):?>selected<?endif;?> numb="100">100</option>
                <?/*<option <?if($_SESSION['paging'] == 'all'):?>selected<?endif;?> numb="all">Все</option>*/?>
            </select>
        </div>
        <?/*<span>По первой букве фамилии:</span>
        <div class="form_select alf_filter">
            <select class="short">
                <option value="no">Все</option>
                <?for ($i=176; $i<=207; $i++):?>
                    <?$symb = iconv('ISO-8859-5', 'UTF-8', chr($i));?>
                    <option value="<?=$symb;?>" <?if (isset($_SESSION['alf_filter']) && $_SESSION['alf_filter'] == $symb):?>selected<?endif?>><?=$symb;?></option>
                <?endfor;?>
            </select>
        </div>*/?>
    </div>
    
    

</div>
<?foreach ($arResult['ITEMS'] as $user) {?>
    <div class="agent_search_card">
        <?if ($user['PERSONAL_PHOTO']):?>
            <?$file = CFile::ResizeImageGet($user['PERSONAL_PHOTO'], array('width'=>62, 'height'=>62), BX_RESIZE_IMAGE_PROPORTIONAL, true,array());
            $img = '<img class="agent_img" src="'.$file['src'].'" />';?>
            <a href="<?=$arParams['SEF_FOLDER'].$user['ID']?>/">
                <?= $img;?>
            </a>
        <?endif;?>

        <div class="agent_contacts">
            <div class="agent_name">
                <a href="<?=$arParams['SEF_FOLDER'].$user['ID']?>/">
                    <?=$user['LAST_NAME'].' '. $user['NAME'].' '.$user['SECOND_NAME']?>
                </a>
            </div>
            <?if ($user['PERSONAL_PHONE']):?>
                <div class="agent_phone"><?=$user['PERSONAL_PHONE']?></div>
            <?endif?>
            <?if ($user['PERSONAL_MOBILE']):?>
                <div class="agent_phone"><?=$user['PERSONAL_MOBILE']?></div>
            <?endif?>
            <a class="agent_email" href="mailto:<?=$user['EMAIL']?>"><?=$user['EMAIL']?></a>
        </div>
        <div class="agent_links">
            <?if (in_array($user['ID'],$arResult['arrOfchildUsersId'])):?>
                <div class="agent_clients">
                    <div class="clients_ico"></div>
                    <a href="/clients/?filter=<?=$user['ID']?>" class="agent_ads_link">
                        Клиенты
                        (<?=$user['TOTAL_CLIENT'];?>)
                    </a>
                </div>
            <?endif;?>
            <div class="agent_ads">
                <div class="ads_ico"></div>
                <a href="/ads/?Author=<?=$user['ID']?>" class="agent_ads_link">
                    Объявления
                    (<?=$user['TOTAL_ADS'];?>)
                </a>
            </div>
            <?//Раздел для объектов
            /*<div class="agent_objects">
                <div class="objects_ico"></div>
                <a href="#" class="agent_objects_link">Объекты</a>
            </div>*/?>
        </div>
        <div class="agent_description">
            <?if ($user['UF_HEAD']):?>
            <p>Начальник:
                <?if (isset($_SESSION['alf_filter']) && $_SESSION['alf_filter'] != 'no'):?>
                    <a href="<?=$arParams['SEF_FOLDER'].$user['UF_HEAD']?>/">
                        <?=$arResult['usersForMainUsers'][$user['UF_HEAD']]['LAST_NAME'].' '.$arResult['usersForMainUsers'][$user['UF_HEAD']]['NAME'].' '.$arResult['usersForMainUsers'][$user['UF_HEAD']]['SECOND_NAME']?>
                    </a>
                <?else:?>
                    <a href="<?=$arParams['SEF_FOLDER'].$user['HEAD']['ID']?>/">
                        <?=$user['HEAD']['LAST_NAME'].' '. $user['HEAD']['NAME'].' '.$user['HEAD']['SECOND_NAME']?>
                    </a>
                <?endif?>
            </p>
            <?endif?>
            <p><?=$user['PERSONAL_NOTES']?></p>
        </div>
        <div class="agent_join_date">
            <span>Добавлен</span>
            <span>
                <?php $time = strtotime($user['DATE_REGISTER']);
                echo date('d.m.Y',$time);
                ?>
            </span>
            <div class="calendar_ico"></div>
            <?if (!empty($user['LAST_LOGIN'])):?>
                <?if (strtotime("now") - strtotime($user['LAST_LOGIN']) > 1500):?>
                    <span>Последний вход:</span>
                    <span>
                        <?php  
                            echo $user['LAST_LOGIN'];
                        ?>
                    </span>
                <?else:?>
                    <span>Статус:Online</span>
                <?endif;?>
            <?else:?>
                <span>Не был авторизован</span>
            <?endif;?>
        </div>
    </div>

<?}?>
<?

if ($arResult["NAV_RESULT"]->NavPageCount > 0):
?>

		<?=$arResult["NAV_STRING"]?>

<?
endif;
?>

<?//pre($arResult,false);?>