<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$total_ads = getArrayOfCntByProp(5, "PROPERTY_AUTHOR");
$total_client = getArrayOfCntByProp(10, "PROPERTY_RIELTOR");


global $USER;
global $APPLICATION;

$userID=$USER->GetID();
$users = getUsers(array("UF_HEAD"),array("ID",'LAST_NAME','SECOND_NAME','NAME','PERSONAL_NOTES','DATE_REGISTER','PERSONAL_PHOTO','PERSONAL_PHONE','PERSONAL_MOBILE'),array('ACTIVE'=>'Y'));
$childUsersId = getChildsId($userID,$users);
$arResult['arrOfchildUsersId'] = $childUsersId;
if (!in_array($arParams['ID'],$childUsersId) && $arParams['ID']<>$userID && false) {        //������� ���� - false
    $arResult['MESSAGE'] = GetMessage("T_USERS_MESSAGE");
} else {
    $user = $users[$arParams['ID']];
    $user['HEAD'] = $users[$user['UF_HEAD']];
    $arResult = $user;
    $APPLICATION->SetTitle($user['LAST_NAME'].' '. $user['NAME'].' '.$user['SECOND_NAME']);
    $APPLICATION->AddChainItem($user['LAST_NAME'].' '. $user['NAME'].' '.$user['SECOND_NAME']);
}

$arResult['TOTAL_ADS'] = $total_ads[$arParams['ID']] ? $total_ads[$arParams['ID']] : 0;
$arResult['TOTAL_CLIENT'] = $total_client[$arParams['ID']] ? $total_client[$arParams['ID']] : 0;

$this->IncludeComponentTemplate();

?>