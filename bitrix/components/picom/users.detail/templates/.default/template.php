<?php
/**
 * Created by PhpStorm.
 * User: picom
 * Date: 25.11.14
 * Time: 14:06
 */
?>
<?//pre($arParams)?>
<?if ($arResult['MESSAGE']) {
    echo $arResult['MESSAGE'];
    return;

}?>
<?$user = $arResult;?>

    <div class="agent_search_card">
        
        <?if ($user['PERSONAL_PHOTO']):?>
            <?$file = CFile::ResizeImageGet($user['PERSONAL_PHOTO'], array('width'=>62, 'height'=>62), BX_RESIZE_IMAGE_PROPORTIONAL, true,array());
            $popupImg = CFile::ResizeImageGet($user['PERSONAL_PHOTO'], array('width'=>700, 'height'=>525));
            $img = '<img class="agent_img" src="'.$file['src'].'" />';?>
            <a class="cboxElement" href="<?=$popupImg['src'];?>">
                <?= $img;?>
            </a>
        <?endif;?>
    

        <div class="agent_contacts">
            <div class="agent_name"><?=$user['LAST_NAME'].' '. $user['NAME'].' '.$user['SECOND_NAME']?></div>
            <?if ($user['PERSONAL_PHONE']):?>
                <div class="agent_phone"><?=$user['PERSONAL_PHONE']?></div>
            <?endif?>
            <?if ($user['PERSONAL_MOBILE']):?>
                <div class="agent_phone"><?=$user['PERSONAL_MOBILE']?></div>
            <?endif?>
            <a class="agent_email" href="mailto:<?=$user['EMAIL']?>"><?=$user['EMAIL']?></a>
        </div>
        <div class="agent_links">
            <?if (in_array($user['ID'],$arResult['arrOfchildUsersId'])):?>
                <div class="agent_clients">
                    <div class="clients_ico"></div>
                    <a href="/clients/?filter=<?=$user['ID']?>" class="agent_ads_link">Клиенты (<?=$arResult['TOTAL_CLIENT'];?>)</a>
                </div>
            <?endif;?>
            <div class="agent_ads">
                <div class="ads_ico"></div>
                <a href="/ads/?Author=<?=$user['ID']?>" class="agent_ads_link">Объявления (<?=$arResult['TOTAL_ADS'];?>)</a>
            </div>
        </div>
        <div class="agent_description">
            <?if ($user['HEAD']):?>
            <p>Начальник:
                <a href="<?=$arParams['SEF_FOLDER'].$user['HEAD']['ID']?>/">
                    <?=$user['HEAD']['LAST_NAME'].' '. $user['HEAD']['NAME'].' '.$user['HEAD']['SECOND_NAME']?>
                </a>
            </p>
            <?endif?>
            <p><?=$user['PERSONAL_NOTES']?></p>
        </div>
        <div class="agent_join_date">
            <span>Добавлен</span>
            <span>
                <?php $time = strtotime($user['DATE_REGISTER']);
                echo date('d.m.Y',$time);
                ?>
            </span>
            <div class="calendar_ico"></div>
        </div>
    </div>
    <?if (preg_match("/crm\.megapolis18\.ru\/ads\//", $_SERVER['HTTP_REFERER'])):?>
    <p>
        <a href="<?=$_SERVER['HTTP_REFERER'];?>">Назад</a>
        &nbsp;
        <a href="/agents/">Перейти к списку агентов</a>
    </p>
    <?else:?>
        <p><a href="/agents/">Возврат к списку</a></p>
    <?endif;?>