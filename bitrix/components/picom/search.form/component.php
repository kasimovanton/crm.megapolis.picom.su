<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentName */
/** @var string $componentPath */
/** @var string $componentTemplate */
/** @var string $parentComponentName */
/** @var string $parentComponentPath */
/** @var string $parentComponentTemplate */

if(!IsModuleInstalled("search"))
{
	ShowError(GetMessage("BSF_C_MODULE_NOT_INSTALLED"));
	return;
}

$arResult["FORM_ACTION"] = '/ads';
if (!empty($_REQUEST['subject'])){
	$arResult["FORM_ACTION"] .= '/';
    $arResult["FORM_ACTION"] .= $_REQUEST['subject'];
}
if (!empty($_REQUEST['type'])){
    $arResult["FORM_ACTION"] .= '_'.$_REQUEST['type'];
}
$arResult["FORM_ACTION"] .= '/';


//��� ��� ��� ��� �������� ������ ������������ �� default
// if (!empty($_REQUEST['type'])){
    // $arResult["FORM_ACTION"] .= '_'.$_REQUEST['type'].'/';
// } else {

// }




$iblock = 5;
$arFilter = array(
    "city",
);

$arResult["VALUE_FORM_FIELDS"] = getEnumValues($iblock,$arFilter);
$arResult["VALUE_FORM_FIELDS_XML"] = getEnumValues($iblock,array("OperationType"),true);


    $arOrder = Array("SORT"=>"ASC");
    $arFilter = Array("IBLOCK_ID"=>$iblock, "ACTIVE"=>"Y", "DEPTH_LEVEL"=>"1");
    $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
    $res_sec = CIBlockSection::GetList($arOrder, $arFilter, true);
    
    while($ob = $res_sec->GetNextElement()) 
    {
        $arFields = $ob->GetFields();
        $arResult["VALUE_FORM_FIELDS"]["Category"][$arFields['CODE']] = $arFields['NAME'];
    }
    
$this->IncludeComponentTemplate();
?>
