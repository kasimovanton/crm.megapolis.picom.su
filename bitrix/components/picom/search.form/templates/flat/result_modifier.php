<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();


    if (isset($_REQUEST['path_begin']) && !empty($_REQUEST['path_begin'])) {
        $_SESSION['search_begin'] = $_REQUEST['path_begin'];
    }    
    if (isset($_REQUEST['back_name']) && !empty($_REQUEST['back_name'])) {
        $_SESSION['back_name'] = $_REQUEST['back_name'];
    }
    
    
    //Сохранение фильтрации итерационного поиска при возврате к списку объявлений с помощью механизма сессий
    

    //переменная from_detail хранит true только в случае перехода с детальной страницы объявления после итерационного поиска
    if ($_SESSION['from_detail'] && !isset($_REQUEST['new_search']) && isset($_REQUEST['back'])){
        $_REQUEST['iteration'] = $_SESSION['arrIter'];
    }
    $_SESSION['from_detail'] = false;
    
    //Если был произведен итерационный поиск, то при переходе на страницу детального просмотра, массив фильтра будет сохранен
    // и при возврате назад к списку объявлений параметры фильтра будут восстановлены
    if (isset($_REQUEST['iteration'])){
        $_SESSION['search'] = true;
        $_SESSION['arrIter'] = $_REQUEST['iteration'];
    } else {
        $_SESSION['search'] = false;
        unset($_SESSION['arrIter']); 
    }
    
    
    // ----------------------------

    
    if (!isset($_REQUEST['iteration']) && !isset($_REQUEST['iter_prop'])){
        foreach ($_REQUEST as $key => $value){
            if ($key == 'with_photo'){
                $GLOBALS['filter_ex']['!=PROPERTY_IMAGES'] = false;
                $GLOBALS['filter_ex']['!=DETAIL_PICTURE'] = false;
                continue;
            }    
            // if ($key == 'District'){
                // $GLOBALS['filter_ex']['PROPERTY_'.$key] = $value;
                // continue;
            // }                   
            if ($key == 'search'){
                $keywords = preg_split("/[\s,]+/", $value);
                $flag = false;
                foreach ($keywords as $word){
                    if ($flag){
                        $filter .= '%&&%'.$word;
                    } else {
                        $filter .= '%'.$word;
                        $flag = true;
                    }
                    
                }
                if (!empty($value)){
                    if (!empty($_REQUEST['only_topic'])){
                        $GLOBALS['filter_ex']['?NAME'] = $filter.'%';
                    } else {
                        $new_array = array();
                        $new_array["LOGIC"] = "OR";
                        $new_array[] = array('?PROPERTY_Street'=>$filter.'%');
                        $new_array[] = array('?SEARCHABLE_CONTENT'=>$filter.'%');
                        $GLOBALS['filter_ex'][] = $new_array;
                        // $GLOBALS['filter_ex']['?SEARCHABLE_CONTENT'] = $filter.'%';
                    }
                }
                continue;
            }           
            if ($key == 'start_date'){
                $start_date = $value;
                continue;
            }        
            if ($key == 'end_date'){
                $end_date = $value;
                continue;
            }        
            if ($key == 'min_price'){
                $min_price = $value;
                continue;
            }        
            if ($key == 'max_price'){
                $max_price = $value;
                continue;
            }     
            if ($key == 'min_price_dem'){
                $min_price_dem = $value;
                continue;
            }        
            if ($key == 'max_price_dem'){
                $max_price_dem = $value;
                continue;
            }                    
            if ($key == 'LandAreaListFrom'){
                $LandAreaListFrom = getValueOfEnum(5, "LandAreaListFrom" ,$value);
                continue;
            }        
            if ($key == 'LandAreaListTo'){
                $LandAreaListTo = getValueOfEnum(5, "LandAreaListTo" ,$value);
                continue;
            }        
            if ($key == 'SquareListFrom'){
                $SquareListFrom = getValueOfEnum(5, "SquareListFrom" ,$value);
                continue;
            }        
            if ($key == 'SquareListTo'){
                $SquareListTo = getValueOfEnum(5, "SquareListTo" ,$value);
                continue;
            }
            if ($key == 'FloorList'){
            
                $arOrder = Array("SORT"=>"ASC");
                $arFilter = Array("IBLOCK_ID"=>5, "ACTIVE"=>"Y");
                $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, array('ID', 'NAME', 'PROPERTY_Floor', 'PROPERTY_Floors'));
                
                switch (getValueOfEnum(5, "FloorList" ,$value)){
                    case 'Любой':
                        break;
                    case 'Не первый':
                        $GLOBALS['filter_ex']['!PROPERTY_Floor'] = 1;
                        break;
                    case 'Не последний':
                        //Для того, чтобы выбрать элементы не последних этажей, мы находим все ID, которых значения этажа
                        //не совпадает со значением этажности дома и передает массив ID в фильтр
                        while ($ob = $res->GetNextElement()){
                            $arFields = $ob->GetFields();
                            if ($arFields['PROPERTY_FLOOR_VALUE'] != $arFields['PROPERTY_FLOORS_VALUE']){
                                $arrID[] = $arFields['ID'];
                            }
                        }
                        $GLOBALS['filter_ex']['ID'] = $arrID;
                        break;
                    default:
                        //Здесь по сравнению с предыдущим case в условие добавленого ограничение "не первый этаж"
                        while ($ob = $res->GetNextElement()){
                            $arFields = $ob->GetFields();
                            if ($arFields['PROPERTY_FLOOR_VALUE'] != $arFields['PROPERTY_FLOORS_VALUE'] && $arFields['PROPERTY_FLOOR_VALUE'] != 1){
                                $arrID[] = $arFields['ID'];
                            }
                        }
                        $GLOBALS['filter_ex']['ID'] = $arrID;
                        break;
                }
                continue;
            }
            $GLOBALS['filter_ex']['PROPERTY_'.$key] = $value;
        }
        
        
        if (!empty($start_date)){
            $GLOBALS['filter_ex']['>=DATE_ACTIVE_FROM'] = $start_date;
        }    
        if (!empty($end_date)){
            $GLOBALS['filter_ex']['<=DATE_ACTIVE_FROM'] = date("d.m.Y", MakeTimeStamp($end_date)+86400);
        }    
        if (!empty($min_price)){
            $GLOBALS['filter_ex']['>=PROPERTY_PRICE'] = $min_price;
        }    
        if (!empty($max_price)){
            $GLOBALS['filter_ex']['<=PROPERTY_PRICE'] = $max_price;
        }    
        
        // if (!empty($min_price_dem)){
        // $GLOBALS['filter_ex']['>=PROPERTY_PRICEFROM'] = $min_price_dem;
        // }    
        // if (!empty($max_price_dem)){
            // $GLOBALS['filter_ex']['<=PROPERTY_PRICETO'] = $max_price_dem;
        // }    
        if (!empty($min_price_dem) || !empty($max_price_dem)){
            $intra_array = array();
            $intra_array["LOGIC"] = "OR";
            $intra_array[] = array('=PROPERTY_'.$prop.'List_VALUE'=>"Любая");
            $intra_array[] = array("LOGIC"=>"AND",
                                    array("LOGIC"=>"OR", 
                                        array('>=PROPERTY_PRICEFROM'=>$min_price_dem), 
                                        array('=PROPERTY_PRICEFROM'=>false)
                                    ),
                                    array("LOGIC"=>"OR", 
                                        array("LOGIC"=>"AND", 
                                            array('<=PROPERTY_PRICETO'=>$max_price_dem), 
                                            array('>=PROPERTY_PRICETO'=>$min_price_dem)
                                        ),
                                        array('=PROPERTY_PRICETO'=>false)
                                    ),
                            );
            $GLOBALS['filter_ex'][] = $intra_array;
        }
        
        if (!empty($LandAreaListFrom)){
            $GLOBALS['filter_ex']['>=PROPERTY_LandArea'] = $LandAreaListFrom;
        }    
        if (!empty($LandAreaListTo) && is_numeric($LandAreaListTo)){
            $GLOBALS['filter_ex']['<=PROPERTY_LandArea'] = $LandAreaListTo;
        }    
        if (!empty($SquareListFrom)){
            $GLOBALS['filter_ex']['>=PROPERTY_Square'] = $SquareListFrom;
        }    
        if (!empty($SquareListTo) && is_numeric($SquareListTo)){
            $GLOBALS['filter_ex']['<=PROPERTY_Square'] = $SquareListTo;
        }
    } else {//Работа итерационного фильтра        вернуться в фильтр объявлений
   
        if (!isset($_SESSION['last_url']) && empty($_SESSION['last_url'])){
            $_SESSION['last_url'] = $_SERVER['HTTP_REFERER'];
        }
        $ext_filter["LOGIC"] = "AND";
        foreach ($_REQUEST['iteration'] as $prop => $value){
            if (!is_array($value)){
                if ($prop == 'Rooms'){
                    $intra_array = array();
                    $intra_array["LOGIC"] = "OR";
                    $intra_array[] = array('=PROPERTY_'.$prop.'List_VALUE'=>$value);
                    $intra_array[] = array('=PROPERTY_'.$prop.'List_VALUE'=>false);
                    $ext_filter[] = $intra_array;
                    continue;
                }
                $intra_array = array();
                $intra_array["LOGIC"] = "OR";
                $intra_array[] = array('=PROPERTY_'.$prop.'List_VALUE'=>"Любая");
                $intra_array[] = array("LOGIC"=>"AND",
                                        array("LOGIC"=>"OR", 
                                            array('<=PROPERTY_'.$prop.'From'=>$value), 
                                            array('=PROPERTY_'.$prop.'From'=>false)
                                        ),
                                        array("LOGIC"=>"OR", 
                                            array('>=PROPERTY_'.$prop.'To'=>$value), 
                                            array('=PROPERTY_'.$prop.'To'=>false)
                                        ),
                                );
                $ext_filter[] = $intra_array;
            } else {
                if ($prop == 'RoomsList'){
                    $property_room_list = getEnumValues(5, array('Rooms'));
                    $filtEnum['LOGIC'] = 'OR';
                    foreach ($value as $sing_room){
                        foreach ($property_room_list['Rooms'] as $idroom => $valroom){
                            if ($valroom == $sing_room){
                                $filtEnum[] = array('=PROPERTY_Rooms'=>$idroom);
                            }
                        }
                    }
                    $GLOBALS['filter_ex'][] = $filtEnum;
                    continue;
                }
                $GLOBALS['filter_ex']['>=PROPERTY_'.$prop] = $value[0];
                $GLOBALS['filter_ex']['<=PROPERTY_'.$prop] = $value[1];
            }

        }
        //iter_prop - массив для одиночных свойств, для простого фильтрования
        foreach ($_REQUEST['iter_prop'] as $single_prop => $prop_value){
            if (!empty($prop_value)){
                $GLOBALS['filter_ex']['PROPERTY_'.$single_prop] = $prop_value;
            }
        }
        if (count($ext_filter)>1){
            $GLOBALS['filter_ex']['IBLOCK_ID'] = '5';
            $GLOBALS['filter_ex'][] = $ext_filter;
        }     
        // if (count($ext_filter)>1){
            // $GLOBALS['filter_ex'] = array('IBLOCK_ID'=>5, $ext_filter);
        // }
 
    }

   // pre($GLOBALS['filter_ex']);
   // pre($_REQUEST);
   
   //Находим максимальную цену для фильтра
    CModule::IncludeModule("iblock");
 
    $IBLOCK_ID = 5; 
 
    $arOrder = Array("SORT"=>"ASC");
    $arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_Price");
    $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y");
    // $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
    
    // $max_price = 0;
    // while ($ob = $res->GetNextElement()){
        // $arFields = $ob->GetFields();
        // if ($max_price < $arFields['PROPERTY_PRICE_VALUE']){
            // $max_price = $arFields['PROPERTY_PRICE_VALUE'];
        // }
    // }
    
    
   // $_REQUEST['for_script'] = $max_price;
   if(!empty($_REQUEST['subject']) && !empty($_REQUEST['type'])){
        $maxPrice = getMaxSectPrice($_REQUEST['subject'], $_REQUEST['type']);
        $_REQUEST['for_script'] = $maxPrice[0];
        $_REQUEST['demand'] = $maxPrice[1];
    } else {
        $_REQUEST['for_script'] = 15000000;
    }
   
   
   $arResult['filter_prop'] = filterProp();
   
   /*echo '<pre>';
   print_r($GLOBALS['filter_ex']);
   
   echo '</pre>';*/