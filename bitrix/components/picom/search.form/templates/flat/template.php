 <?php
   $this->SetViewTarget('filter'); //Вывод в header.php в теге <main>-><div class="filter_wrapper">
?> 

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="filter">
    <script>
        cat = '<?=$_REQUEST['subject']?>';
        cat_type = '<?=$_REQUEST['type']?>';
        maxprice = '<?=$_REQUEST['for_script']?>';
        firstName = 'min_price';
        secondName = 'max_price';
        <?/*demand = <?if($_REQUEST['demand']):?>1<?else:?>0<?endif;?>;*/?>
        demand = '<?=$_REQUEST['demand'];?>';
        if (demand){
            firstName = 'min_price_dem';
            secondName = 'max_price_dem';
        }
      
        <?if (isset($_REQUEST['min_price']) && !empty($_REQUEST['min_price'])):?>
            nowFirPrice = '<?=$_REQUEST['min_price']?>';
        <?else:?>
            nowFirPrice = 0;
        <?endif;?>
        <?if (isset($_REQUEST['min_price_dem']) && !empty($_REQUEST['min_price_dem'])):?>
            nowFirPrice = '<?=$_REQUEST['min_price_dem']?>';
        <?endif;?>

        
        <?if (isset($_REQUEST['max_price']) && !empty($_REQUEST['max_price'])):?>
            nowSecPrice = '<?=$_REQUEST['max_price']?>';
        <?else:?>
            nowSecPrice = maxprice;
        <?endif;?>
        <?if (isset($_REQUEST['max_price_dem']) && !empty($_REQUEST['max_price_dem'])):?>
            nowSecPrice = '<?=$_REQUEST['max_price_dem']?>';
        <?endif;?>

        if (maxprice<100000){
            step = maxprice/2;
        } else {
            if (maxprice < 1000000){
                step = 1e4;
            } else {
                step = 1e5;
            }
        }
        
        <?if (isset($_REQUEST['subject']) && !empty($_REQUEST['subject']) & isset($_REQUEST['type']) && !empty($_REQUEST['type'])):?>
            $(document).ready(function() {
                if(maxprice!=0 && maxprice !== null){
                    $(".price_dragger").slider({
                        range:!0,
                        min:0,
                        max: maxprice,
                        step: step,
                        values: [ nowFirPrice, nowSecPrice ],
                        slide:function(a,b){
                            $(".min_price").val(b.values[0]).attr('name', firstName);
                            $(".max_price").val(b.values[1]).attr('name', secondName);
                        }
                    });
                    $(".min_price").val($(".price_dragger").slider("values",0));
                    $(".max_price").val($(".price_dragger").slider("values",1));

                    $('.price_row').show();
                }
            });
        <?endif;?>
    </script>
    <form action="<?=$arResult["FORM_ACTION"]?>" method="GET" class="form_filter">
        <div class="filter_left_block">
            <div class="filter_row">
                <input class="keywords" name="search" type="text" placeholder="Поиск по ключевым словам" 
                    <?if(!empty($_REQUEST['search'])):?>value="<?=$_REQUEST['search']?>"<?endif;?>>
               
                <input id="ctopic" type="checkbox" name="only_topic" <?if(!empty($_REQUEST['only_topic'])):?>checked<?endif;?>>
                <label for="ctopic">Искать только в названиях</label>
                <input id="cphoto" class="with_photo" type="checkbox" name="with_photo" <?if(!empty($_REQUEST['with_photo'])):?>checked<?endif;?>>
                <label for="cphoto">С фото</label>
            </div>
            <!-- -->
            <div class="filter_bottom">
                <div class="filter_row price_row">
                    <span class="price_text">Цена:</span>
                    <div class="price_dragger"></div>
                    <input class="price min_price" type="text" <?if(!empty($_REQUEST['min_price_dem'])):?>name="min_price_dem" value="<?=$_REQUEST['min_price_dem']?>"<?endif;?><?if(!empty($_REQUEST['min_price'])):?>name="min_price" value="<?=$_REQUEST['min_price']?>"<?endif;?>>
                    <span class="price_separator">&mdash;</span>
                    <input class="price max_price" type="text" <?if(!empty($_REQUEST['max_price_dem'])):?>name="max_price_dem" value="<?=$_REQUEST['max_price_dem']?>"<?endif;?><?if(!empty($_REQUEST['max_price'])):?>name="max_price" value="<?=$_REQUEST['max_price']?>"<?endif;?>>
                    <?/*
                    <div class="hidden">
                        <input class="min_price_real" type="hidden" <?if(!empty($_REQUEST['min_price'])):?>name="min_price" value="<?=$_REQUEST['min_price']?>"<?endif;?>>
                        <input class="max_price_real" type="hidden" <?if(!empty($_REQUEST['max_price'])):?>name="max_price" value="<?=$_REQUEST['max_price']?>"<?endif;?>>
                    </div>
                    */?>
                </div>
                <div class="filter_row">
                    <span class="date_text">Дата подачи объявления</span>
                    <input class="date" id="datefrom" type="text" name="start_date" <?if(!empty($_REQUEST['start_date'])):?>value="<?=$_REQUEST['start_date']?>"<?endif;?>>
                    <input type="button" class="calendar_btn" value="">
                    <span>до</span>
                    <input class="date" id="dateto" type="text" name="end_date" <?if(!empty($_REQUEST['end_date'])):?>value="<?=$_REQUEST['end_date']?>"<?endif;?>>
                    <input type="button" class="calendar_btn" value="">
                </div>
            </div>
        </div><!-- /filter_left_block -->
        <div class="filter_right_block">
            <div class="filter_row">
                <div class="form_select subject">
                <select class="subject" name="subject">
                    <option class="hidden" selected disabled>Любая категория</option>
                    <?foreach($arResult["VALUE_FORM_FIELDS"]['Category'] as $key => $option):?>
                        <option value="<?=$key?>" <?if($_REQUEST['subject'] == $key):?>selected<?endif;?>><?=$option?></option>
                    <?endforeach;?>
                </select>
                </div>
                <div class="form_select city">
                    <select class="city" name="City">
                        <option class="hidden" selected disabled>Любой город</option>
                        <?foreach($arResult["VALUE_FORM_FIELDS"]['City'] as $key => $option):?>
                            <option value="<?=$key?>" <?if($_REQUEST['City'] == $key):?>selected<?endif;?>><?=$option?></option>
                        <?endforeach;?>
                    </select>
                </div>
               <div class="form_select district<?if ((isset($_REQUEST['District'])&&!empty($_REQUEST['District'])) || (isset($_REQUEST['City'])&& $_REQUEST['City'] == 49)):?><?else:?> hidden<?endif;?>">
                    <select id="district" <?/*class="district"*/?> name="District[]" multiple="multiple" placeholder="Район">
                        <?php
                            if ((isset($_REQUEST['District'])&&!empty($_REQUEST['District'])) || (isset($_REQUEST['City'])&& $_REQUEST['City'] == 49)){
                                $dist_list = getEnumValues(5, array("District"));
                                if (!empty($dist_list)){
                                    echo setHtmlMultiSelectOption($dist_list);
                                }
                            }
                        ?>
                     </select>
                </div>
                <div class="form_select type <?if(empty($_REQUEST['subject']) && empty($_REQUEST['type'])):?>hidden<?endif;?>">
                    <select class="type" name="type">
                        <option value="">Тип объявления</option>
                        <?foreach($arResult["VALUE_FORM_FIELDS_XML"]['OperationType'] as $key => $option):?>
                            <option value="<?=$key?>" <?if($_REQUEST['type'] == $key):?>selected<?endif;?>><?=$option?></option>
                        <?endforeach;?>
                            <?/*$ar_enum = CIBlockProperty::GetPropertyEnum(9,Array("SORT"=>"asc"),Array());
                            while ($ar_enum_list = $ar_enum->GetNext()):?>
                                <option value="<?=$ar_enum_list['XML_ID']?>" <?if($_REQUEST['type'] == $ar_enum_list['XML_ID']):?>selected<?endif;?>><?=$ar_enum_list['VALUE']?></option>
                            <?endwhile; */?>
                    </select>
                </div>
            </div>
                                
                <div class="filter_bottom">
                    
                        <?php
                            $property_array = filterPropForRooms();
                            $prop_list = getEnumValues(5,$property_array[$_REQUEST['subject']][$_REQUEST['type']]);
                            if (!empty($prop_list)){
                                $flag_rooms = true;
                                $rooms_type = $property_array[$_REQUEST['subject']][$_REQUEST['type']][0];
                            } else {
                                $flag_rooms = false;
                            }
                        ?>
                        <div class="form_select rooms<?if(!$flag_rooms || $rooms_type != 'Rooms'):?> hidden<?endif;?>">
                            <select name="Rooms[]" id="rooms" multiple="multiple" placeholder="Количество комнат">
                                <?php
                                    $dist_list = getEnumValues(5, array("Rooms"));
                                    if (!empty($dist_list)){
                                        echo setHtmlMultiSelectOption($dist_list);
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form_select roomslist<?if(!$flag_rooms || $rooms_type != 'RoomsList'):?> hidden<?endif;?>">
                            <select name="RoomsList[]" id="roomslist" multiple="multiple" placeholder="Количество комнат">
                                <?php
                                    $dist_list = getEnumValues(5, array("RoomsList"));
                                    if (!empty($dist_list)){
                                        echo setHtmlMultiSelectOption($dist_list);
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="filter_row prop_list<?if ($flag_rooms):?> list_with_empty_block<?endif;?>">
                            <?if((isset($_REQUEST['subject'])&&!empty($_REQUEST['subject'])) && (isset($_REQUEST['type'])&&!empty($_REQUEST['type']))):?>
                                <?$prop_list = getEnumValues(5,$arResult['filter_prop'][$_REQUEST['subject']][$_REQUEST['type']]);
                                if (!empty($prop_list)){
                                    echo setHtmlSelectOption($prop_list);
                                }?>
                                <div class="extra_form_row"></div>
                            <?endif;?>
                        </div>
                    
                </div>
        </div>
        <div class="filter_footer clearfix">
            <input id="cmyads" type="checkbox" name="Author" value="<?=CUser::GetID();?>" <?if(!empty($_REQUEST['Author']) && $_REQUEST['Author'] == CUser::GetID()):?>checked<?endif;?>>
            <label for="cmyads">Только мои объявления</label>
            <input class="button find_btn" type="submit" value="<?=GetMessage("BSF_T_SEARCH_BUTTON");?>" />
            <?if (!isset($_SESSION['filter']) || $_SESSION['filter'] == 'opened'):?>
                <span class="toggle_btn toggle_up" data-status="opened" status="opened">свернуть</span>
            <?else:?>
                <span class="toggle_btn toggle_up" data-status="closed" status="closed">развернуть</span>
            <?endif;?>
        </div>
    </form>
</div>

<?php
    $this->EndViewTarget();
?>

 <?php
    $this->SetViewTarget('params'); //Вывод в header.php в теге <content>
?> 
<?if (!empty($_REQUEST)): //Вывод фильтра?>
    <?//<div class="active_filters">?>
        <?php
        foreach ($_REQUEST['iteration'] as $valProp => $value):?>
            <?
                $res = CIBlockProperty::GetByID($valProp, 5, false);
                $ar_res = $res->GetNext();
            ?>
            <div class="active_filters_row">
                <span class="active_filters_prop"><?=$ar_res['NAME'];?>: </span>
                <?if (is_array($value)):?>
                    <span class="active_filters_value">
                        <?if (!empty($value['0'])):?>
                           от <?=number_format($value['0'], 0, ',', ' ');?>
                        <?endif;?>
                        <?if (!empty($value['1'])):?>
                           до <?=number_format($value['1'], 0, ',', ' ');?>
                        <?endif;?>
                    </span>
                <?else:?>
                    <span class="active_filters_value"><?=number_format($value, 0, ',', ' ');?></span>
                <?endif;?>
                <span class="active_filters_cancel">
					<?$formAction = explode("?", $_SERVER['REQUEST_URI']);?>
                    <form action="<?=$formAction[0];?>" method="POST">
                        <?foreach ($_REQUEST['iteration'] as $keyProp => $val):?>
                            <?if ($keyProp == $valProp):?>
                                <?continue;?>
                            <?endif;?>
                            <?if (is_array($value)):?>
                                <?if (!empty($val[0])):?>
                                    <input type="hidden" name="iteration[<?=$keyProp;?>][0]" value="<?=$val[0];?>">
                                <?endif;?>
                                <?if (!empty($val[1])):?>
                                    <input type="hidden" name="iteration[<?=$keyProp;?>][1]" value="<?=$val[1];?>">
                                <?endif;?>
                            <?else:?>
                                <input type="hidden" name="iteration[<?=$keyProp;?>]" value="<?=$val;?>">
                            <?endif;?>
                        <?endforeach;?>
                        <?foreach ($_REQUEST['iter_prop'] as $prop => $value):?>
                            <input type="hidden" value="<?=$value;?>" name="iter_prop[<?=$prop;?>]">
                        <?endforeach;?>
                        <input class="cross_button" value="[X]" type="submit">
                    </form>
                </span>
            </div>
        <?endforeach;?>
        <?/*foreach ($_REQUEST['iter_prop'] as $prop => $value):?>
            <?
                $res = CIBlockProperty::GetByID($valProp, 5, false);
                $ar_res = $res->GetNext();
            ?>
            <div class="active_filters_row">
                <span class="active_filters_prop"><?=$ar_res['NAME'];?>: </span>
                <input type="hidden" value="<?=$value;?>" name="iter_prop[<?=$prop;?>]">
            </div>
        <?endforeach;*/?>
        <?if (!empty($_REQUEST['iteration'])):?>
            <?if (isset($_SESSION['search_begin']) && !empty($_SESSION['search_begin'])):?>
                <div class="active_filters_row">
                    <span class="active_filters_cancel end_search">
                        <a href="<?=$_SESSION['search_begin'];?>">
                            <?if (isset($_SESSION['back_name']) && !empty($_SESSION['back_name'])):?>
                                Вернутся в <?=$_SESSION['back_name'];?>
                            <?else:?>
                                Вернуться в список объявлений
                            <?endif;?>
                        </a>
                    </span>
                </div>
            <?endif;?>
        <?else:?>
            <?unset($_SESSION['search_begin']);?>
            <?unset($_SESSION['back_name']);?>
        <?endif;?>
        <?foreach ($_REQUEST as $prop => $value):?>
                <?if (!empty($value)):?>
                    <?if (is_array($value) && (!isset($_REQUEST['iteration']) && !isset($_REQUEST['iter_prop']))):?>
                        <?$prop_list = CIBlockElement::GetProperty(5, $value, array("sort" => "asc"), array("CODE" => $prop));?>
                        <?$ar_prop = $prop_list->GetNext();?>
                        <?$prop_value= getEnumValues(5,array($prop));?>
                        <div class="active_filters_row">
                            <span class="active_filters_prop"><?=$ar_prop['NAME'];?>:</span>
                            <span class="active_filters_value">
                                <?foreach($value as $val):?>
                                    <?=$prop_value[$prop][$val];?>
                                <?endforeach;?>
                            </span>
                            <?php
                                $mask = '/&'.$prop.'\[\]=[A-Za-z0-9]*/';
                                $link = preg_replace($mask, '', $_SERVER['REQUEST_URI']);
                                if ($link == $_SERVER['REQUEST_URI']){
                                    $mask = '/&'.$prop.'%5B%5D=[A-Za-z0-9]*/';
                                    $link = preg_replace($mask, '', $_SERVER['REQUEST_URI']);
                                }
                            ?>
                            <span class="active_filters_cancel"><a href="<?=$link?>">[X]</a></span>
                        </div>
                    <?else:?>
                        <?$prop_list = CIBlockElement::GetProperty(5, $value, array("sort" => "asc"), array("CODE" => $prop));?>
                        <?if($ar_prop = $prop_list->GetNext()):?>
                            <?$prop_value= getEnumValues(5,array($prop));?>
                                <div class="active_filters_row">
                                    <span class="active_filters_prop"><?=$ar_prop['NAME'];?>:</span>
                                    <span class="active_filters_value"><?=$prop_value[$prop][$value]?></span>
                                    <?php
                                        $mask = '/&'.$prop.'=[A-Za-z0-9]*/';
                                        $link = preg_replace($mask, '', $_SERVER['REQUEST_URI']);
                                        if ($link == $_SERVER['REQUEST_URI']){
                                            $mask = '/\?'.$prop.'=[A-Za-z0-9]*/';
                                            $link = preg_replace($mask, '', $_SERVER['REQUEST_URI']);
                                        }
                                    ?>
                                    <span class="active_filters_cancel"><a href="<?=$link?>">[X]</a></span>
                                </div>
                        <?else:?>
                            <div class="active_filters_row">
                                <?$flag = false;?>
                                <?if ($prop == 'with_photo'):?>
                                    <span class="active_filters_prop">Только с фото: </span>
                                    <span class="active_filters_value">Да</span>
                                    <?$flag = true;?>
                                <?endif?>
                                <?if ($prop == 'only_topic'):?>
                                    <span class="active_filters_prop">Искать только в названиях: </span>
                                    <span class="active_filters_value">Да</span>
                                    <?$flag = true;?>
                                <?endif?>				
                                <?if ($prop == 'District'):?>
                                    <span class="active_filters_prop">Поиск по запросу: </span>
                                    <span class="active_filters_value">2132</span>
                                    <?$flag = true;?>
                                <?endif?>						
                                <?if ($prop == 'search'):?>
                                    <span class="active_filters_prop">Поиск по запросу: </span>
                                    <span class="active_filters_value"><?=$_REQUEST['search'];?></span>
                                    <?$flag = true;?>
                                <?endif?>
                                <?if ($prop == 'min_price'):?>
                                    <span class="active_filters_prop">Цена от: </span>
                                    <span class="active_filters_value">
                                        <?=number_format($_REQUEST['min_price'], 0, ',', ' ');?>
                                    </span>
                                    <?$flag = true;?>
                                <?endif?>
                                <?if ($prop == 'max_price'):?>
                                    <span class="active_filters_prop">Цена до: </span>
                                    <span class="active_filters_value">
                                        <?=number_format($_REQUEST['max_price'], 0, ',', ' ');?>
                                    </span>
                                    <?$flag = true;?>
                                <?endif?>
                                <?if ($prop == 'start_date'):?>
                                    <span class="active_filters_prop">С  </span>
                                    <span class="active_filters_value"><?=$_REQUEST['start_date'];?></span>
                                    <?$flag = true;?>
                                <?endif?>
                                <?if ($prop == 'end_date'):?>
                                    <span class="active_filters_prop">По </span>
                                    <span class="active_filters_value"><?=$_REQUEST['end_date'];?></span>
                                    <?$flag = true;?>
                                <?endif?>
                                <?if ($flag):?>
                                    <?php
                                        if ($prop == 'search'){
                                             // удаление кирилических символов от search до вхождения первого амперсанта
                                            $link = substr_replace($_SERVER['REQUEST_URI'], '', strpos($_SERVER['REQUEST_URI'], 'search'), strpos($_SERVER['REQUEST_URI'], '&')-strpos($_SERVER['REQUEST_URI'], 'search')+1/*6*strlen($_REQUEST['search']) + 8*/); 
                                        } else {
                                            $mask = '/&'.$prop.'=[A-Za-z0-9.]*/';
                                            $link = preg_replace($mask, '', $_SERVER['REQUEST_URI']);
                                            if ($link == $_SERVER['REQUEST_URI']){
                                                $mask = '/\?'.$prop.'=[A-Za-z0-9]*&/';
                                                $link = preg_replace($mask, '?', $_SERVER['REQUEST_URI']);
                                            }
                                        }
                                    ?>
                                    <span class="active_filters_cancel"><a href="<?=$link?>">[X]</a></span>
                                <?endif;?>
                            </div>
                        <?endif?>
                    <?endif;?>
                <?endif;?>
        <?endforeach;?>
   <?// </div>?>
<?endif;?>

<?php
    $this->EndViewTarget();
?>