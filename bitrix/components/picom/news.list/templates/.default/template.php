<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="search_menu">
    <a href="/ads/add.php" class="button button_universal">Добавить объявление</a>
    <div class="sorting">
        <span>Сортировать:</span>
        <div class="form_select">
        <select>
            <option>Дороже</option>
        </select>
        </div>
    </div>
</div>

<div class="search_results">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="entry <?/*recent_entry*/ ?>" <?/*data-color="#cafffc"*/?> id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <?if ($arItem['PROPERTIES']['Avito']['VALUE']):?>
            <span class="avito_ico"></span>
        <?endif;?>
        <div class="entry_date">
            <?$datetime = preg_split("/[\s]+/", $arItem['ACTIVE_FROM']);?>
            <div class="entry_day"><?=date("d.m.Y",  MakeTimeStamp($arItem['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS"));?></div>
            <div class="entry_time"><?=date("H:i",  MakeTimeStamp($arItem['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS"));?></div>
        </div>
        <div class="entry_thumb">
            <div class="entry_img">
            <?if (!empty($arItem['DETAIL_PICTURE'])):?>
                <?$renderImage = CFile::ResizeImageGet($arItem['DETAIL_PICTURE'], Array("width" => '120px', "height" => '90px'),BX_RESIZE_IMAGE_EXACT);?>
                <a href="<?=$arItem['DETAIL_PAGE_URL'];?>"><img src="<?=$renderImage['src'];?>" alt="<?=$renderImage['alt'];?>"></a>
            <?endif;?>
            </div>
            
                <?if (CUser::GetID() == $arItem['PROPERTIES']['Author']['VALUE']):?>
                    <div class="entry_agent">Клиент:</div>
                    <?$client_list = getEnumValues(5, array("Client"));?>
                    <a class="entry_agent_link" href="/clients/<?=$arItem['PROPERTIES']['Client']['VALUE'];?>/">
                        <?=$client_list['Client'][$arItem['PROPERTIES']['Client']['VALUE']]; //result_modifier?>
                    </a>
                <?else:?>
                    <div class="entry_agent">Агент:</div>
                    <a class="entry_agent_link" href="/agents/<?=$arItem['PROPERTIES']['Author']['VALUE'];?>/">
                         <?=$arResult['agents_list']["Author"][$arItem['PROPERTIES']['Author']['VALUE']] //result_modifier?>
                    </a>
                <?endif;?>
        </div>
        <div class="entry_description">
            <a class="entry_header" href="<?=$arItem['DETAIL_PAGE_URL'];?>" ><?=$arItem['NAME'];?></a>
            <p class="entry_text">
                <span class="street">
                    <?if (!empty($arItem['PROPERTIES']['Street']['VALUE'])):?>
                        <?=$arItem['PROPERTIES']['Street']['VALUE'];?>
                    <?endif;?>
                    <?if (!empty($arItem['PROPERTIES']['HOUSE']['VALUE'])):?>
                        <?=$arItem['PROPERTIES']['HOUSE']['VALUE'];?>
                    <?endif;?>
                    <?if (!empty($arItem['PROPERTIES']['APARTMENT']['VALUE'])):?>
                        , <?=$arItem['PROPERTIES']['APARTMENT']['VALUE'];?>
                    <?endif;?>
                </span>
                <br/>
                <?=$arItem['DETAIL_TEXT'];?>
            </p>
            <span class="entry_price">
                <?if (!empty($arItem['PROPERTIES']['Price']['VALUE'])):?>
                    <?=number_format($arItem['PROPERTIES']['Price']['VALUE'], 0, ',', ' ');?> руб.
                <?elseif (!empty($arItem['PROPERTIES']['PriceFrom']['VALUE']) || !empty($arItem['PROPERTIES']['PriceTo']['VALUE'])):?>
                    <?if (!empty($arItem['PROPERTIES']['PriceFrom']['VALUE'])):?>
                        от <?=number_format($arItem['PROPERTIES']['PriceFrom']['VALUE'], 0, ',', ' ');?> руб.
                    <?endif;?>
                    <?if (!empty($arItem['PROPERTIES']['PriceTo']['VALUE'])):?>
                        до <?=number_format($arItem['PROPERTIES']['PriceTo']['VALUE'], 0, ',', ' ');?> руб.
                    <?endif;?>
                <?else:?>
                    <?if (!empty($arItem['PROPERTIES']['PriceList']['VALUE'][0])):?>
                        Любая цена
                    <?else:?>
                        Цена не указана
                    <?endif;?>
                <?endif;?>
            </span>
            <div class="entry_buttons">
                <?$checkAuthor = CUser::GetID()==$arItem['PROPERTIES']['Author']['VALUE'];?>
                <?if ($checkAuthor):?>
                    <a class="btn_edit" href="/ads/add.php?CODE=<?=$arItem['ID'];?>"></a>
                <?endif;?>
                <?if (isset($arResult['reverse'][$arItem['ID']]) && !empty($arResult['reverse'][$arItem['ID']])):?>
                    <form action="/ads/<?=$arResult['reverse'][$arItem['ID']]?>/" method="POST">
                        <?foreach ($arResult['iteration'][$arItem['ID']] as $prop => $value):?>
                            <?if (!is_array($value)):?>
                                <input type="hidden" value="<?=$value;?>" name="iteration[<?=$prop;?>]">
                            <?else:?>
                                <?foreach ($value as $filt_key => $filt_val):?>
                                    <input type="hidden" value="<?=$filt_val;?>" name="iteration[<?=$prop;?>][<?=$filt_key;?>]">
                                <?endforeach;?>
                            <?endif;?>
                        <?endforeach;?>
                        <?foreach ($arResult['iter_prop'][$arItem['ID']] as $prop => $value):?>
                            <input type="hidden" value="<?=$value;?>" name="iter_prop[<?=$prop;?>]">
                        <?endforeach;?>
                        <input class="btn_connections" type="submit">
                    </form>
                <?endif;?>
            </div>
        </div>
	</div>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>


<?pre($arResult, false);?>