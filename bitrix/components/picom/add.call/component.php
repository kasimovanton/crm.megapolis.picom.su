<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
    
    if (isset($_REQUEST["result_id"]) && !empty($_REQUEST["result_id"])){
        
        $success = false;
        
        $clientName = strip_data($_REQUEST['name']);
        $el = new CIBlockElement;

        $PROP = array();
        $PROP["DATE"] = $_REQUEST['date_call'];
        $PROP["CLIENT_ID"] = $_REQUEST['result_id'];
        $PROP["INIT_STATUS"] = 555; //Входящий
        $PROP["CALL_STATUS"] = 557; //Принят

        $arLoadProductArray = Array(
          "MODIFIED_BY"    => $USER->GetID(),
          "IBLOCK_SECTION_ID" => false,
          "IBLOCK_ID"      => 18,
          "PROPERTY_VALUES"=> $PROP,
          "NAME"           => "Входящий звонок",
          "ACTIVE"         => "Y",
          "DETAIL_TEXT" => $_REQUEST['comment_call'] ? $_REQUEST['comment_call'] : "",
          "DETAIL_TEXT_TYPE" => 'html',
        );

        if($PRODUCT_ID = $el->Add($arLoadProductArray)){
            $success = true;
            $arResult['MESSAGE'] = "Вызов успешно добавлен";
        } else {
            $arResult['ERROR_MESSAGE'] = "Error: ".$el->LAST_ERROR;
        }
        
        if (isset($_REQUEST['new_call_submit']) && $success){
            LocalRedirect("/operator/?type=call");
        }
        
    }
    
    $this->IncludeComponentTemplate();