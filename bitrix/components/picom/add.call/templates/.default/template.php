<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="green">
    <?if (isset($arResult['ERROR_MESSAGE'])):?>
        <span class="error">
            <?=$arResult['ERROR_MESSAGE'];?>
        </span>
    <?endif;?>
    <br>
    <?if (isset($arResult['MESSAGE'])):?>
        <span class="success">
            <?=$arResult['MESSAGE'];?>
        </span>
    <?endif;?>
</div>
    <form action="" method="POST" class="new_call_form">
        <div class="top_form_section">
            <div class="add_form_row date_row">
                <label for="date_call">Дата, время:</label>
                <input value="<?=date('d.m.Y H:i:s');?>" type="text" placeholder="Нажмите для выбора даты и времени" id="date_call" name="date_call" onclick="BX.calendar({node: this, field: this, bTime: true, bHideTime:false});">
            </div>
            <div class="add_form_row client_row">
                <a href="#" class="easy_add_client">Выбрать абонента</a>
                <div class="client_search_ajax">
                    <div class="row">
                        <input type="text" placeholder="Поиск" id="global_search" name="global_search" autocomplete="off">
                        <input type="hidden" id="result_id" name="result_id">
                    </div>
                </div>
                <div class="changed_result"></div>
            </div>
        </div>
        <div class="bot_form_section">
            <div class="add_form_row comment_row">
                <label for="comment_call">Комментарий:</label>
                <textarea id="comment_call" name="comment_call"></textarea>
            </div>
            <div class="info_block">
                <div class="time_error">Заполните время звонка</div>
                <br>
                <div class="client_error">Выберите клиента</div>
            </div>
            <div class="add_form_row submit_row">
                <input class="button new_call_submit" type="submit" name="new_call_submit" value="Сохранить вызов">
                <input class="button new_call_ajax_submit" type="submit" name="new_call_ajax_submit" value="Сохранить вызов и добавить еще">
            </div>
        </div>
        
        <div class="search_row help_search_area">
            <div class="ajax_search_result">
            </div>
            <div class="extra_result_info">
            </div>
            <div class="fast_client_add">
                <?/*<span class="close_add_form">[X]</span>*/?>
                <h3>Добавление нового клиента</h3>
                <div class="form_row name_row">
                    <label for="">Имя:</label>
                    <input type="text" name="client_name">
                    <span>Не менее 3-х символов</span>
                </div>
                <div class="form_row phone_row">
                    <label for="">Телефон:</label>
                    <input type="text" name="client_phone">
                    <span>Номер уже существует</span>
                </div>
                <input id="extra_client" type="checkbox" name="extra_client" value="577">
                <label for="extra_client">Риэлтор</label>
                <button disabled="disabled" class="button button_universal gray fast_add_button">Добавить</button>
                <div class="preload">
                    <div class="text-loader">
                        Загрузка
                    </div>
                </div>
            </div>
            <div class="preload call_form_preloader">
                <div class="text-loader">
                    Загрузка
                </div>
            </div>
        </div>
    </form>

