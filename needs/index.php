<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

?>
    <?$userStatus = getUserStatus($USER->GetID());?>
    <?if (!in_array(8, $userStatus) && !($USER->IsAdmin())):?>
        У вас нет прав доступа к этому разделу
    <?else:?>
        <div class="needs_detail">
            <?$APPLICATION->AddChainItem("Колл-центр", "/operator/");?>
            <?$APPLICATION->AddChainItem("Потребность");?>
            <?$APPLICATION->IncludeComponent(
                "bitrix:news.detail",
                "needs_detail",
                Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "IBLOCK_TYPE" => "objects",
                    "IBLOCK_ID" => "5",
                    "ELEMENT_ID" => $_REQUEST["ELEMENT_ID"],
                    "ELEMENT_CODE" => "",
                    "CHECK_DATES" => "Y",
                    "FIELD_CODE" => Array("NAME","PREVIEW_TEXT","PREVIEW_PICTURE","DETAIL_TEXT",),
                    "PROPERTY_CODE" => array("PROPERTY_*",),
                    "IBLOCK_URL" => "",
                    "DETAIL_URL" => "",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_GROUPS" => "Y",
                    "SET_TITLE" => "N",
                    "SET_CANONICAL_URL" => "N",
                    "SET_BROWSER_TITLE" => "Y",
                    "BROWSER_TITLE" => "-",
                    "SET_META_KEYWORDS" => "Y",
                    "META_KEYWORDS" => "-",
                    "SET_META_DESCRIPTION" => "Y",
                    "META_DESCRIPTION" => "-",
                    "SET_LAST_MODIFIED" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "ADD_ELEMENT_CHAIN" => "N",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "USE_PERMISSIONS" => "N",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "USE_SHARE" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Страница",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "SET_STATUS_404" => "N",
                    "SHOW_404" => "N",
                    "MESSAGE_404" => ""
                )
            );?>
           <h2>Комментарии</h2>
           <div class="needs_comments">
                <?$APPLICATION->IncludeComponent("bitrix:forum.topic.reviews","",Array(
                    "SHOW_LINK_TO_FORUM" => "N",
                    "FILES_COUNT" => "1",
                    "FORUM_ID" => "1",
                    "IBLOCK_TYPE" => "objects",
                    "IBLOCK_ID" => "5",
                    "ELEMENT_ID" => $_REQUEST["ELEMENT_ID"],
                    "AJAX_POST" => "N", 
                    "POST_FIRST_MESSAGE" => "Y",
                    "POST_FIRST_MESSAGE_TEMPLATE" => "#IMAGE#[url=#LINK#]#TITLE#[/url]#BODY#",
                    "URL_TEMPLATES_READ" => "read.php?FID=#FID#&TID=#TID#",
                    "URL_TEMPLATES_DETAIL" => "photo_detail.php?ID=#ELEMENT_ID#",
                    "URL_TEMPLATES_PROFILE_VIEW" => "profile_view.php?UID=#UID#",
                    "MESSAGES_PER_PAGE" => "10",
                    "PAGE_NAVIGATION_TEMPLATE" => "",
                    "DATE_TIME_FORMAT" => "d.m.Y H:i:s",
                    "PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
                    "EDITOR_CODE_DEFAULT" => "Y",
                    "SHOW_AVATAR" => "Y",
                    "SHOW_RATING" => "Y",
                    "RATING_TYPE" => "like",
                    "SHOW_MINIMIZED" => "Y",    
                    "USE_CAPTCHA" => "Y",
                    "PREORDER" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
                    )
                );?>
            </div>
        </div>
    <?endif;?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>