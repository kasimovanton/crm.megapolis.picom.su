<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>
<?$userStatus = getUserStatus($USER->GetID());?>
<?if (!in_array(8, $userStatus) && !($USER->IsAdmin())):?>
    <span class="error_text">
        У вас нет прав доступа к этому разделу
    </span>
<?else:?>
    <?
        if (!isset($_REQUEST['CODE'])){
            $title = "Добавить потребность";
        } else {
            $title = "Редактировать потребность";
        }

        $APPLICATION->SetTitle($title);
        ?><?$APPLICATION->IncludeComponent("picom:ads.form", "needs", array(
            "SEND_EMAIL" => "Y",
            "EVENT_NAME" => "FEEDBACK_FORM",
            "EVENT_MESSAGE_ID" => array(
                0 => "7",
            ),
            "IBLOCK_TYPE" => "objects",
            "IBLOCK_ID" => "5",
            "STATUS_NEW" => "N",
            "LIST_URL" => "",
            "USE_CAPTCHA" => "N",
            "USER_MESSAGE_EDIT" => "Объявление сохранено",
            "USER_MESSAGE_ADD" => "Объявление добавлено",
            "DEFAULT_INPUT_SIZE" => "30",
            "RESIZE_IMAGES" => "N",
            "GROUPS" => array(
                0 => "6",
            ),
            "PROPERTY_REQUIRED"=> array(
                0 => "Client",
                1 => "MarketType",
                2 => "Square",
                3 => "SaleRooms",
                4 => "LandArea",
                5 => "Street",
                6 => "Rooms",
                7 => "RoomsList",
                8 => "Floor",
                9 => "Floors",
                10 => "City",
                11 => "DistanceToCity",
                12 => "Country",
                13 => "ObjectTypeZemlya",
                14 => "LeaseType",
                15 => "DETAIL_TEXT",
                16 => "HouseType",
            ),
            "STATUS" => "ANY",
            "ELEMENT_ASSOC" => "PROPERTY_ID",
            "ELEMENT_ASSOC_PROPERTY" => "Author",
            "MAX_USER_ENTRIES" => "",
            "MAX_LEVELS" => "1",
            "LEVEL_LAST" => "Y",
            "MAX_FILE_SIZE" => "0",
            "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
            "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
            "SEF_MODE" => "N",
            "SEF_FOLDER" => "/ads/",
            "CUSTOM_TITLE_NAME" => "",
            "CUSTOM_TITLE_TAGS" => "",
            "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
            "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
            "CUSTOM_TITLE_IBLOCK_SECTION" => "Тип сделки",
            "CUSTOM_TITLE_PREVIEW_TEXT" => "",
            "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
            "CUSTOM_TITLE_DETAIL_TEXT" => "Описание",
            "CUSTOM_TITLE_DETAIL_PICTURE" => "Основное фото",
            "FORM_CLASS" => "content",
            "ROW_CLASS" => "form_row",
            "SUBMIT_CLASS" => "button add_object_btn",
            "CUSTOM_CLASS_NAME" => "content_input_long",
            "CUSTOM_CLASS_TAGS" => "hidden",
            "CUSTOM_CLASS_DATE_ACTIVE_FROM" => "hidden",
            "CUSTOM_CLASS_DATE_ACTIVE_TO" => "hidden",
            "CUSTOM_CLASS_IBLOCK_SECTION" => "offer_type",
            "CUSTOM_CLASS_PREVIEW_TEXT" => "hidden",
            "CUSTOM_CLASS_PREVIEW_PICTURE" => "hidden",
            "CUSTOM_CLASS_DETAIL_TEXT" => "content_input_long",
            "CUSTOM_CLASS_DETAIL_PICTURE" => "",
            "CUSTOM_CLASS_Author" => "hidden",
            "CUSTOM_CLASS_Client" => "input_client",
            "CUSTOM_CLASS_Avito" => "",
            "CUSTOM_CLASS_AdStatus" => "",
            "CUSTOM_CLASS_Images" => "",
            "CUSTOM_CLASS_LeaseType" => "",
            "CUSTOM_CLASS_MarketType" => "",
            "CUSTOM_CLASS_HouseType" => "",
            "CUSTOM_CLASS_ObjectTypeGaraj" => "",
            "CUSTOM_CLASS_ObjectTypeDoma" => "",
            "CUSTOM_CLASS_ObjectTypeCommerce" => "",
            "CUSTOM_CLASS_ObjectTypeZagran" => "",
            "CUSTOM_CLASS_ObjectTypeZemlya" => "",
            "CUSTOM_CLASS_BuildingClass" => "",
            "CUSTOM_CLASS_WallsType" => "",
            "CUSTOM_CLASS_ObjectSubtype1" => "",
            "CUSTOM_CLASS_ObjectSubtype2" => "",
            "CUSTOM_CLASS_Region" => "hidden",
            "CUSTOM_CLASS_City" => "",
            "CUSTOM_CLASS_DistanceToCity" => "content_input_small",
            "CUSTOM_CLASS_District" => "",
            "CUSTOM_CLASS_Street" => "content_input_long",
            "CUSTOM_CLASS_HOUSE" => "content_input_small",
            "CUSTOM_CLASS_APARTMENT" => "content_input_small",
            "CUSTOM_CLASS_Floor" => "content_input_small",
            "CUSTOM_CLASS_Floors" => "content_input_small",
            "CUSTOM_CLASS_SaleRooms" => "content_input_small",
            "CUSTOM_CLASS_Rooms" => "content_input_small",
            "CUSTOM_CLASS_Secured" => "",
            "CUSTOM_CLASS_Square" => "content_input_small",
            "CUSTOM_CLASS_LIVING_SQUARE" => "content_input_small",
            "CUSTOM_CLASS_LandArea" => "content_input_small",
            "CUSTOM_CLASS_KITCHEN_SQUARE" => "content_input_small",
            "CUSTOM_CLASS_DistanceToCityList" => "",
            "CUSTOM_CLASS_RoomsList" => "",
            "CUSTOM_CLASS_FloorList" => "",
            "CUSTOM_CLASS_FloorFrom" => "content_input_small",
            "CUSTOM_CLASS_FloorTo" => "content_input_small",
            "CUSTOM_CLASS_FloorsList" => "",
            "CUSTOM_CLASS_FloorsFrom" => "content_input_small",
            "CUSTOM_CLASS_FloorsTo" => "content_input_small",
            "CUSTOM_CLASS_SquareList" => "",
            "CUSTOM_CLASS_SquareFrom" => "content_input_small",
            "CUSTOM_CLASS_SquareTo" => "content_input_small",
            "CUSTOM_CLASS_LandAreaList" => "",
            "CUSTOM_CLASS_LandAreaFrom" => "content_input_small",
            "CUSTOM_CLASS_LandAreaTo" => "content_input_small",
            "CUSTOM_CLASS_Country" => "",
            "CUSTOM_CLASS_PriceList" => "",
            "CUSTOM_CLASS_Price" => "content_input_small",
            "CUSTOM_CLASS_PriceFrom" => "content_input_small",
            "CUSTOM_CLASS_PriceTo" => "content_input_small"
            ),
            false
        );?>
<?endif;?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>