<?php
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');
    CModule::IncludeModule("iblock");

    
    //Выгружаемые свойства для фильтра
    $property_array = filterPropForRooms();
    

    $prop_list = getEnumValues(5,$property_array[$_REQUEST['category']][$_REQUEST['type']]);

    if (!empty($prop_list)){
        $response = setHtmlMultiSelectOption($prop_list);
        if (!empty($response)){
            $result['success'] = true;
        } else {
            $result['success'] = false;
        }
        
    } else {
        $result['success'] = false;

    }
    $result['roomstype'] = $property_array[$_REQUEST['category']][$_REQUEST['type']][0];
    
        echo json_encode($result);

require($bxRoot.'modules/main/include/epilog_after.php');
?>
