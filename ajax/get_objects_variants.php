<?
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');


    CModule::IncludeModule("iblock");
    $IBLOCK_ID = 19; 
    global $USER;
    

    $arOrder = Array("ID"=>"DESC");
    $arSelect = Array("ID", "NAME", "IBLOCK_ID", "ACTIVE_FROM", "DETAIL_TEXT", "CREATED_BY", "PROPERTY_*");
    $arFilter = Array("IBLOCK_ID"=>16, "ACTIVE"=>"Y", "ID"=>CIBlockElement::SubQuery("PROPERTY_OBJECT", Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "PROPERTY_NEEDS"=>$_REQUEST['needs_id'], "!PROPERTY_OBJECT"=>false)));
    
    
    $list = CIBlockElement::GetList($arOrder, $arFilter, false, $navArParams, $arSelect);
    $rowCount = $list->SelectedRowsCount();
    ?>
    <?if($rowCount):?>
        <?while ($ob = $list->GetNextElement()) :?>
            <?php
                $arFields = $ob->GetFields();
                $arFields['prop'] = $ob->GetProperties();
            ?>
            <li>
                <div class="object_info-var">
                    <span>Название объекта:</span><?=$arFields['NAME'];?>
                </div>
                <div class="object_info-var">
                    <span>Статус объекта:</span><?=getElemName($arFields['prop']['STATUS']['VALUE']);?>
                </div>
                <div class="object_info-var">
                    <?$rsUser = CUser::GetByID($arFields['prop']['AUTHOR']['VALUE']);
                    $arUser = $rsUser->Fetch();?>
                    <span>Офис-менеджер:</span><?=$arUser['LAST_NAME'].' '.$arUser['NAME'].' '.$arUser['SECOND_NAME'];?> 
                    <a href="#" onclick="if (BX.IM){BXIM.openMessenger(<?=$arFields['prop']['AUTHOR']['VALUE'];?>); return false;}">Чат</a>
                </div>
            </li>
        <?endwhile;?>
    <?else:?>
        <li>Предложенные объекты отсутствуют</li>
    <?endif;?>
<?
require($bxRoot.'modules/main/include/epilog_after.php');
