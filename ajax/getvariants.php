<?
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');


    CModule::IncludeModule("iblock");
    $IBLOCK_ID = 19; 
    global $USER;
    
    $maxResultCount = $_REQUEST['page_counter'];
    if ($maxResultCount !== 'all'){
        $currentPage = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
        $navArParams = array('nPageSize'=>$maxResultCount, 'iNumPage'=>$currentPage);
    } else {
        $navArParams = false;
    }
    $arResponse = Array();
    $arOrder = Array("PROPERTY_NEED_BY"=>"ASC, NULLS","ID"=>"DESC");
    $arSelect = Array("ID", "NAME", "IBLOCK_ID", "ACTIVE_FROM", "DETAIL_TEXT", "CREATED_BY", "PROPERTY_*");
    $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "PROPERTY_NEEDS"=>$_REQUEST['needs_id'], "PROPERTY_OBJECT"=>false);
    
    if($_REQUEST['block_type'] == 'current'){
        $arFilter["PROPERTY_STATUS"] = array(561, 562);
    } elseif($_REQUEST['block_type'] == 'close'){
        $arFilter["PROPERTY_STATUS"] = 560;
    }
    
    $list = CIBlockElement::GetList($arOrder, $arFilter, false, $navArParams, $arSelect);
    $rowCount = $list->NavRecordCount;
    while ($ob = $list->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arFields['prop'] = $ob->GetProperties();
        $arResponse[$arFields['ID']] = $arFields;
    }
    
    //проверка выбора закрепленного варианта
    $needHasChoice = checkNeedChoice($_REQUEST['needs_id']);
?>
<?if (count($arResponse) >0):?>
    <?if($rowCount>$_REQUEST['page_counter'] && $_REQUEST['page_counter'] != 'all'):?>
        <div class="page_navigation_block">
            <ul>
                <?php
                    $pageCounter = 0;
                ?>
                <?while($pageCounter <= floor($rowCount/$_REQUEST['page_counter'])):?>
                    <?php
                        $pageCounter++;
                    ?>
                    <li data-page="<?=$pageCounter;?>" <?if($pageCounter==$currentPage):?>class="active"<?endif;?>><?=$pageCounter;?></li>
                <?endwhile;?>
            </ul>
        </div>
    <?endif;?>
    <div class="variant_element_list">
        <?foreach($arResponse as $activeVariantKey => $activeVariantValue):?>
            <?php
                $res = CIBlockElement::GetByID($activeVariantValue['prop']["ADVERT"]['VALUE']);
                if ($ob = $res->GetNextElement()) {
                    $arAdv = $ob->GetFields();
                    $arAdv['prop'] = $ob->GetProperties();
                }
            ?>
            <div class="variant_element" data-id="<?=$activeVariantKey;?>">
                <div class="date_column">
                    <div class="ads_time" title="Время добавления варианта">
                        <?php
                        $datetime = preg_split("/[\s]+/", $activeVariantValue['ACTIVE_FROM']);
                            if (date("d.m.Y",  MakeTimeStamp($activeVariantValue['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS")) == date("d.m.Y")){
                                $entry_day = 'Сегодня';
                            }else{
                                $entry_day = date("d.m.Y",  MakeTimeStamp($activeVariantValue['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS"));
                            }
                        ?>
                        <span><?=date("H:i",  MakeTimeStamp($activeVariantValue['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS"));?></span>
                        <span><?=$entry_day?></span>
                    </div>
                    <?php
                        $arSectionType = array(
                                            7 => "Квартира",
                                            17 => "Комната",
                                            21 => "Дом",
                                            23 => "Земельный участок",
                                            24 => "Гараж",
                                            35 => "Коммерческая недвижимость",
                                            40 => "Недвижимость за рубежом",
                                        );
                        $variantHeader = $arSectionType[$arAdv['IBLOCK_SECTION_ID']];
                        if (!empty($arAdv['prop']['MarketType']['VALUE'])){
                            $variantHeader .= ', '.$arAdv['prop']['MarketType']['VALUE'];
                        }
                        if (!empty($arAdv['prop']['Square']['VALUE'])){
                            $variantHeader .= ', '.$arAdv['prop']['Square']['VALUE'].'м²';
                        }
                        if (!empty($arAdv['prop']['Rooms']['VALUE'])){
                            $variantHeader .= ', '.$arAdv['prop']['Rooms']['VALUE'].'-квартира';
                        }
                        if (!empty($arAdv['prop']['Floor']['VALUE'])){
                            $variantHeader .= ', '.$arAdv['prop']['Floor']['VALUE'];
                            if (!empty($arAdv['prop']['Floors']['VALUE'])){
                                $variantHeader .= '/'.$arAdv['prop']['Floors']['VALUE'];
                            }
                            $variantHeader .= ' эт.';
                        }
                    ?>
                    <div class="variant_name"><a target="_blank" href="<?=$arAdv['DETAIL_PAGE_URL'];?>"><?=$variantHeader;?></a></div>
                    <?if ($activeVariantValue['CREATED_BY'] == $USER->GetID()):?>
                        <?if($_REQUEST['block_type'] == 'current'):?>
                            <a href="#" class="variant_to_close button button_universal">Переместить в обработанные</a>
                            <div class="acception_block">
                                <label>Подтвердите действие:</label>
                                <a href="#" class="cancel_action button button_universal">Отмена</a>
                                <a href="#" class="accept_action button button_universal">Переместить</a>
                                <div class="preload" style="display:block">
                                    <div class="text-loader">
                                        Загрузка
                                    </div>
                                </div>
                            </div>
                        <?endif;?>
                    <?endif;?>
                    <?if ($activeVariantValue['CREATED_BY'] == $USER->GetID()):?>
                        <a href="#" data-id="<?=$activeVariantValue['ID'];?>" class="history_event button button_universal">Комментарии</a>
                    <?endif;?>
                </div>
                <div class="price_column">
                    <div class="variant_price"><?=number_format($arAdv['prop']['Price']['VALUE'], 0, ',', ' ').' руб.';;?></div>
                </div>
                <div class="agent_column">
                    <?php
                        $rsUser = CUser::GetByID($arAdv['prop']['Author']['VALUE']);
                        $arUser = $rsUser->Fetch();
                    ?>
                    <div class="agent_name"><a target="_blank" href="/agents/<?=$arUser['ID'];?>/"><?=$arUser['LAST_NAME'].' '.$arUser['NAME'];?></a></div>
                    <div class="agent_position">риелтор</div>
                    <div class="agent_phone"><?=$arUser['PERSONAL_PHONE'];?></div>
                    <div class="agent_contact"><a href="#" onclick="if (BX.IM){BXIM.openMessenger(<?=$arAdv['prop']['Author']['VALUE'];?>); return false;}">Чат</a></div>
                </div>
                <div class="status_column">
                    <?if ($activeVariantValue['CREATED_BY'] == $USER->GetID()):?>
                        <div class="status_name">
                            <div class="change_time_block">
                                <label for="show_time">Время показа: до </label>
                                <?php
                                    if (!empty($activeVariantValue['prop']["SHOW_TIME"]['VALUE'])){
                                        $dateValue = $activeVariantValue['prop']["SHOW_TIME"]['VALUE'];
                                    } else {
                                        if (date("l") != 'Friday'){
                                            $dateValue = date("d.m.Y H:i:s", MakeTimeStamp(date("d.m.Y H:i:s"))+86400);
                                        } else {
                                            $dateValue = date("d.m.Y H:i:s", MakeTimeStamp(date("d.m.Y H:i:s"))+86400*3);
                                        }
                                    }
                                ?>
                                <input value="<?=$dateValue;?>" onclick="BX.calendar({node: this, field: this, bTime: true, bHideTime:false});" type="text" name="show_time">
                                <button class="decline_new_time">X</button>
                                <button class="accept_new_time">Ок</button>
                            </div>
                            <?if ($activeVariantValue['prop']["SHOW_STATUS"]['VALUE_ENUM_ID'] != 563)://Без заявки на показ?>
                                <div class="status_var_name <?=$activeVariantValue['prop']["SHOW_STATUS"]['VALUE_XML_ID'];?>"><?=$activeVariantValue['prop']["SHOW_STATUS"]['VALUE'];?></div>
                                <?if ($activeVariantValue['prop']["SHOW_STATUS"]['VALUE_ENUM_ID'] == 565)://Добавлен?>
                                    <div class="show_time">До <?=$activeVariantValue['prop']["SHOW_TIME"]['VALUE'];?></div>
                                    <div class="change_time"><a href="#">Изменить время показа</a></div>
                                <?endif;?>
                            <?else:?>
                                <?if ($activeVariantValue['prop']["STATUS"]['VALUE_ENUM_ID'] == 562)://Добавлен?>
                                    <a href="#" class="button button_universal show_variant">Показ варианта</a>
                                <?else:?>
                                    Вариант не был показан
                                <?endif;?>
                            <?endif;?>
                        </div>
                        <?if (!$needHasChoice):?>
                            <?if ($activeVariantValue['prop']["STATUS"]['VALUE_ENUM_ID'] == 562)://Добавлен?>
                                <div class="status_name">
                                    <a href="#" class="button button_universal choice_variant">Выбрать этот вариант</a>
                                </div>
                            <?endif;?>
                        <?elseif($needHasChoice==$activeVariantValue['ID']):?>
                            <span class="choise_success">Вариант выбран для сделки</span>
                        <?endif;?>
                        <?if (!empty($activeVariantValue['DETAIL_TEXT'])):?>
                            <div class="status_name">
                                <div class="comment"><?=$activeVariantValue['DETAIL_TEXT'];?></div>
                            </div>
                        <?endif;?>
                    <?endif;?>

                </div>
            </div>
        <?endforeach;?>
            <div class="preload">
                <div class="text-loader">
                    Загрузка
                </div>
            </div>
    </div>
   <script src="<?=SITE_TEMPLATE_PATH?>/scripts/variants.js"></script>
<?endif;?>
<?
require($bxRoot.'modules/main/include/epilog_after.php');
