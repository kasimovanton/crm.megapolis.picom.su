<?
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');

    
    CModule::IncludeModule("iblock");
    global $USER;
    if (!empty($_REQUEST['need_id']) && !empty($_REQUEST['obj_id'])){
        $arOrder = Array("NAME"=>"ASC");
        $arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_*");
        $arFilter = Array("IBLOCK_ID"=>19, "ACTIVE"=>"Y", "PROPERTY_OBJECT" => $_REQUEST['obj_id'], "PROPERTY_NEEDS"=>$_REQUEST['need_id']);
        $list = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
        if (!($ob = $list->GetNextElement())) {
            $el = new CIBlockElement;

            $PROP = array();
            $PROP["OBJECT"] = $_REQUEST['obj_id'];
            $PROP["NEEDS"] = $_REQUEST['need_id'];
            $PROP["STATUS"] = 562;
            $PROP["SHOW_STATUS"] = 563;

            $arLoadProductArray = Array(
              "MODIFIED_BY"    => $USER->GetID(),
              "IBLOCK_SECTION_ID" => false,
              "IBLOCK_ID"      => 19,
              "PROPERTY_VALUES"=> $PROP,
              "NAME"           => "Вариант",
              "ACTIVE_FROM" => date("d.m.Y H:i:s"),
              "ACTIVE"         => "Y",
              );

            if($PRODUCT_ID = $el->Add($arLoadProductArray)){
                echo $PRODUCT_ID;
            } else {
                 echo "Error: ".$el->LAST_ERROR;
            }
        }
    }
