<?php
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');
    CModule::IncludeModule("iblock");

    
    //Выгружаемые свойства для фильтра
    $property_array = filterProp();
    

    $prop_list = getEnumValues(5,$property_array[$_REQUEST['category']][$_REQUEST['type']]);
    // if (in_array('Rooms', $property_array[$_REQUEST['category']][$_REQUEST['type']])){
        // echo 111111111;
    // }
    if (!empty($prop_list)){
    
        $response['success'] = true;
        $response['data'] = setHtmlSelectOption($prop_list); //echo setHtmlSelectOption($prop_list);
        $maxPrice = getMaxSectPrice($_REQUEST['category'], $_REQUEST['type']);
        $response['maxprice'] = $maxPrice[0];
        $response['demand'] = $maxPrice[1];
    }
    
    echo json_encode($response, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

    
require($bxRoot.'modules/main/include/epilog_after.php');
?>
