<?
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    
    if (empty($_REQUEST['element_history_id'])) die('no element'); 
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');


    CModule::IncludeModule("iblock");
    $IBLOCK_ID = 21; 
    $ELEMENT_ID = $_REQUEST['element_history_id'];
    global $USER;
    
    $arr = ParseDateTime(date("d.m.Y"), FORMAT_DATETIME);
    $dateString = $arr["DD"]." ".ToLower(GetMessage("MONTH_".intval($arr["MM"])."_S"))." ".$arr["YYYY"].' г.';
    $timeString = date("H:i");
    $newComment = '['.$dateString.' '.$timeString.'] '.$_REQUEST['comment'];
    
    $res = getHistoryObject($ELEMENT_ID);
    if ($ob = $res->GetNextElement()){
        $arFields = $ob->GetFields();
        $arFields['prop'] = $ob->GetProperties();

        $arValues = $arFields['prop']['COMMENT']['VALUE'];
        $arValues[] = array("TEXT"=> $newComment, "TYPE" => "HTML");
        CIBlockElement::SetPropertyValueCode($arFields['ID'], 'COMMENT', $arValues); 
    } else {
        $el = new CIBlockElement;

        $PROP = array();
        $PROP["COMMENT"] = $newComment;
        $PROP["ELEMENT_ID"] = $ELEMENT_ID;
        
        
        $arLoadProductArray = Array(
          "MODIFIED_BY"    => $USER->GetID(),
          "IBLOCK_SECTION_ID" => false,
          "IBLOCK_ID"      => 21,
          "PROPERTY_VALUES"=> $PROP,
          "NAME"           => "История событий",
          "ACTIVE_FROM" => date("d.m.Y H:i:s"),
          "ACTIVE"         => "Y",
          );

        if($PRODUCT_ID = $el->Add($arLoadProductArray)){
            // echo $PRODUCT_ID;
        } else {
             // echo "Error: ".$el->LAST_ERROR;
        }
    }
    
    echo $newComment;
    
    require($bxRoot.'modules/main/include/epilog_after.php');
?>