<?
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');

    
    CModule::IncludeModule("iblock");
    global $USER;
    
    if (checkClientUniqNumber($_REQUEST['phone']) && !empty($_REQUEST['name'])){
    
        $clientName = strip_data($_REQUEST['name']);
        $el = new CIBlockElement;

        $PROP = array();
        $PROP[47] = $clientName;
        $PROP[49] = $_REQUEST['phone'];
        $PROP["RIELTOR"] = $USER->GetID();
        
        if(!empty($_REQUEST['extra_client'])){
            $PROP["EXTRA_CLIENT"] = 577;
        }
        
        $arLoadProductArray = Array(
          "MODIFIED_BY"    => $USER->GetID(),
          "IBLOCK_SECTION_ID" => false,
          "IBLOCK_ID"      => 10,
          "PROPERTY_VALUES"=> $PROP,
          "NAME"           => $clientName,
          "ACTIVE_FROM" => date("d.m.Y H:i:s"),
          "ACTIVE"         => "Y",
          );

        if($PRODUCT_ID = $el->Add($arLoadProductArray)){
            echo $PRODUCT_ID;
        } else {
             echo "Error: ".$el->LAST_ERROR;
        }
    }
