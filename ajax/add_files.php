<?
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');

    
    CModule::IncludeModule("iblock");
    $ELEMENT_ID = $_REQUEST['object'];
    $IBLOCK_ID = 16; 
    $IBLOCK_ID_ADV = 5;
    
    $newFiles = array();
    foreach ($_FILES as $keyFile => $arTempFile){
        if (!isset($_REQUEST[$keyFile])){
            $newFiles[] = array("VALUE"=>$arTempFile);
        }
    }
    //���������� �����
    if (!empty($newFiles)){
        CIBlockElement::SetPropertyValueCode($ELEMENT_ID, 'FILES_OBJECTS', $newFiles);
    }

    
    //�������� �������
    $res = CIBlockElement::GetProperty($IBLOCK_ID, $ELEMENT_ID, array("sort" => "asc"), Array("CODE"=>"FILES_OBJECTS"));
    while ($object = $res->GetNext()){
        if (isset($_REQUEST[$object['VALUE']])){
            $arFile["MODULE_ID"] = "iblock";
            $arFile["del"] = "Y";
            CIBlockElement::SetPropertyValueCode($ELEMENT_ID, "FILES_OBJECTS", Array ($object['PROPERTY_VALUE_ID'] => Array("VALUE"=>$arFile)));
        }
    }
