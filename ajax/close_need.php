<?
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');
    
    CModule::IncludeModule("iblock");
    
    $IBLOCK_ID = 5;
    $PRODUCT_ID = intval($_REQUEST['need_id']);
    if ($PRODUCT_ID)
    {
        $allChildsTree = getChildsId(CUser::GetID(), false, true);
        $allChildsTree[] = CUser::GetID();
        $arSelect = Array("ID", "NAME");
        $arFilter = Array("IBLOCK_ID"=>5, "ACTIVE"=>"Y", "ID" =>$_REQUEST['need_id'], "PROPERTY_Author" =>$allChildsTree);
        $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
        if($ob = $res->GetNextElement()){
            $el = new CIBlockElement;
            $res = $el->Update($PRODUCT_ID, Array("ACTIVE" => "N"));
        }
    }
