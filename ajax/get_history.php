<?
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    if (empty($_REQUEST['element_id'])) die('no element');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');

    $list = getHistoryObject($_REQUEST['element_id']);

    $rowCount = $list->SelectedRowsCount();
    ?>
    <?if($rowCount):?>
        <?if ($ob = $list->GetNextElement()) :?>
            <?php
                $arFields = $ob->GetFields();
                $arFields['prop'] = $ob->GetProperties();
            ?>
            <?foreach($arFields['prop']['COMMENT']['VALUE'] as $arComment):?>
                <li>
                    <?=$arComment['TEXT'];?>
                </li>
            <?endforeach;?>
        <?endif;?>
    <?else:?>
        Комментарии отсутствуют
    <?endif;?>
<?
require($bxRoot.'modules/main/include/epilog_after.php');
