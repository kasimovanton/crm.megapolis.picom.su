<?
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');
?>
 <?php
    CModule::IncludeModule("iblock");
    $IBLOCK_ID = 16; 
    $VALUES = array();
    $res = CIBlockElement::GetProperty($IBLOCK_ID, $_REQUEST['object'], array("sort" => "asc"), Array("CODE"=>"FILES_OBJECTS"));
?>

<?if (intval($res->SelectedRowsCount())==1):?>
    <?$ob = $res->GetNext();?>
    <?if (!empty($ob['VALUE'])):?>
        <div class="flexslider">
            <ul class="slides">
                <?
                    $rsFile = CFile::GetByID($ob['VALUE']);
                    $arFile = $rsFile->Fetch();
                ?>
                <li>
                    <span class="detail_image"><img src="<?=CFile::GetPath($ob['VALUE']);?>"></span>
                    <span class="flex-caption"><?=$arFile['ORIGINAL_NAME'];?></span>
                </li>
            </ul>
        </div>
    <?else:?>
        Изображения отсутствуют
    <?endif;?>
<?else:?>
   
    <div class="flexslider">
        <ul class="slides">
            <?while ($ob = $res->GetNext()):?>
                <?
                    $rsFile = CFile::GetByID($ob['VALUE']);
                    $arFile = $rsFile->Fetch();
                ?>
                <li>
                     <span class="detail_image"><img src="<?=CFile::GetPath($ob['VALUE']);?>"></span>
                    <span class="flex-caption"><?=$arFile['ORIGINAL_NAME'];?></span>
                </li>
            <?endwhile;?>
        </ul>
    </div>
    <script>
        $('.flexslider').flexslider({
            animation: "fade",
            controlNav: false   
        });
    </script>
<?endif;?>