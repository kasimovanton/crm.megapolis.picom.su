<?
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');

    pre($_REQUEST);
    
    CModule::IncludeModule("iblock");
    $IBLOCK_ID = 19; 
    $ELEMENT_ID = $_REQUEST['variant_id'];
    if (!empty($ELEMENT_ID)){
        $arLoadProperties = array();
        if($_REQUEST['new_show_status'] == 'Y'){
            $arLoadProperties = array("SHOW_STATUS" => 566);
        } elseif($_REQUEST['new_show_status'] == 'N') {
            $arLoadProperties = array("SHOW_STATUS" => 567);
        }
        if(!empty($arLoadProperties)){
            CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, $IBLOCK_ID, $arLoadProperties);
            
            $elementObject = new CIBlockElement;
            $arLoadProductArray = Array(
                "IBLOCK_SECTION_ID" => false,
                "IBLOCK_ID"   => $IBLOCK_ID,
                "ACTIVE"  => "Y",
                "DETAIL_TEXT" => $_REQUEST['show_comment'],
            );
            $elementObject->Update($ELEMENT_ID, $arLoadProductArray);
        }
        echo true;
    } else {
        echo false;
    }
require($bxRoot.'modules/main/include/epilog_after.php');
