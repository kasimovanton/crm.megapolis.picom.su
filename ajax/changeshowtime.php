<?
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');


    CModule::IncludeModule("iblock");
    $IBLOCK_ID = 19; 
    $ELEMENT_ID = $_REQUEST['variant_id'];
    if (!empty($ELEMENT_ID)){
        $arLoadProperties = array('SHOW_TIME' => $_REQUEST['new_time'], "SHOW_STATUS" => 565);
        CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, $IBLOCK_ID, $arLoadProperties);
        echo true;
    } else {
        echo false;
    }
require($bxRoot.'modules/main/include/epilog_after.php');
