<?php
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');


    CModule::IncludeModule("iblock");
    $prop = $_REQUEST['obj'] == '375' ? "ObjectSubtype1" : "ObjectSubtype2";
    $prop_list = getEnumValues(5,array($prop));
    if (!empty($prop_list)){
        echo setHtmlSelectOption($prop_list);
    }

require($bxRoot.'modules/main/include/epilog_after.php');
