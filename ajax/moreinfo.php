<?php
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';

    if (!$isAjax) die('no access');
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');

    CModule::IncludeModule("iblock");
    $clientID = trim($_REQUEST['client']);
    $IBLOCK_ID = 10; 
    $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "ID"=>$clientID);
    $arOrder = Array("NAME"=>"ASC");
    $arSelect = Array("ID", "NAME", "IBLOCK_ID", "PREVIEW_TEXT", "PROPERTY_*");

    $list = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
?>
<?if ($ob = $list->GetNextElement()):?>
    <?
        $arFields = $ob->GetFields();
        $arFields['prop'] = $ob->GetProperties();
    ?>
    <?if (!empty($arFields['prop']['RIELTOR']['VALUE'])):?>
        <?php
            $rsUser = CUser::GetByID($arFields['prop']['RIELTOR']['VALUE']);
            $arRieltor = $rsUser->Fetch();
        ?>
        <span><b>Риэлтор: </b><?=$arRieltor['LAST_NAME'].' '.$arRieltor['NAME'].' '.$arRieltor['SECOND_NAME'];?></span>
    <?endif;?>
    <?if (!empty($arFields['prop']['MANAGER']['VALUE'])):?>
        <?php
            $rsUser = CUser::GetByID($arFields['prop']['MANAGER']['VALUE']);
            $arManager = $rsUser->Fetch();
        ?>
        <span><b>Менеджер: </b><?=$arManager['LAST_NAME'].' '.$arManager['NAME'].' '.$arManager['SECOND_NAME'];?></span>
    <?endif;?>
    <?if (!empty($arFields['prop']['DOP_PHONE']['VALUE'])):?>
        <span><b>Дополнительный телефон: </b><?=$arFields['prop']['DOP_PHONE']['VALUE'];?></span>
    <?endif;?>
    <?if (!empty($arFields['prop']['DOP_FACE']['VALUE'])):?>
         <span><b>Контакное лицо: </b><?=$arFields['prop']['DOP_FACE']['VALUE'];?></span>
    <?endif;?>
    <?if (!empty($arFields['PREVIEW_TEXT'])):?>
        <span><b>Подробно: </b><?=$arFields['PREVIEW_TEXT'];?></span>
    <?endif;?>
<?else:?>
    <?return false;?>
<?endif;?>
