<?
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');

    CModule::IncludeModule("iblock");
    $IBLOCK_ID = 16; 
    $VALUES = array();
    $res = CIBlockElement::GetProperty($IBLOCK_ID, $_REQUEST['object'], array("sort" => "asc"), Array("CODE"=>"FILES_OBJECTS"));
?>

<?$index=0;?>


<?if (intval($res->SelectedRowsCount())==1):?>
    <?$ob = $res->GetNext();?>
    <?if (!empty($ob['VALUE'])):?>
        <?php
            $rsFile = CFile::GetByID($ob['VALUE']);
            $arFile = $rsFile->Fetch();
        ?>
        <div class="preview_canvas canvas_img">
            <div class="img">
                <img data-file="<?=$arFile['ID'];?>" src="<?=CFile::GetPath($ob['VALUE']);?>"   id="imageCanvas<?=$index;?>">
            </div>
            <a class="photo_remove"></a>
            <span class="new_file_name"><?=$arFile['ORIGINAL_NAME'];?></span>
            <input id="Shown<?=$index;?>" type="checkbox" value="Y" name="Shown[]">
            <label for="Shown<?=$index;?>">Показывать</label>
        </div>
        <?$index++;?>
    <?endif;?>
<?else:?>
    <?while ($ob = $res->GetNext()):?>
        <?php
            $rsFile = CFile::GetByID($ob['VALUE']);
            $arFile = $rsFile->Fetch();
        ?>
        <div class="preview_canvas canvas_img">
            <div class="img">
                <img data-file="<?=$arFile['ID'];?>" src="<?=CFile::GetPath($ob['VALUE']);?>"   id="imageCanvas<?=$index;?>">
            </div>
            <a class="photo_remove"></a>
            <span class="new_file_name"><?=$arFile['ORIGINAL_NAME'];?></span>
            <input id="Shown<?=$index;?>" type="checkbox" value="Y" name="Shown[]">
            <label for="Shown<?=$index;?>">Показывать</label>
        </div>
        <?$index++;?>
    <?endwhile;?>
<?endif;?>
    <input type="hidden" value="<?=$_REQUEST['object'];?>" name="current_object">
    <script>
        imageCounter = <?=$index;?>;
        realFileArrayCounter = <?=$index;?>;
    </script>