<?
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');

    CModule::IncludeModule("iblock");
    $ELEMENT_ID = $_REQUEST['object'];
    $IBLOCK_ID = 16; 
    
    if (!empty($ELEMENT_ID)){
        $arLoadProperties = array(
                                'IS_WATCH' => 549,
                            );
        if (isset($_REQUEST['agree']) && $_REQUEST['agree']=='yes'){
            $arLoadProperties['WATCH_AGREE'] = 553;
        }
        if (isset($_REQUEST['comment']) && !empty($_REQUEST['comment'])){
            $arLoadProperties['WATCHER_COMMENT'] = array('VALUE'=>array('TYPE'=>'HTML', 'TEXT'=>preg_replace("/\n/", "<br>", $_REQUEST['comment'])));
        }
        CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, $IBLOCK_ID, $arLoadProperties);
        echo $ELEMENT_ID;
    }
