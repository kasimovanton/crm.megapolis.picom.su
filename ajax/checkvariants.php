<?
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');


    CModule::IncludeModule("iblock");
    $IBLOCK_ID = 19; 
 
    $activeResponse = array();
    $arActiveOrder = Array("NAME"=>"ASC");
    $arActiveSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_*");
    $arActiveFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "PROPERTY_STATUS" => 562, "PROPERTY_NEEDS"=>$_REQUEST['needs_id']);
    $list = CIBlockElement::GetList($arActiveOrder, $arActiveFilter, false, false, $arActiveSelect);
    $activeCount = $list->SelectedRowsCount();
    while ($ob = $list->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arFields['prop'] = $ob->GetProperties();
        $activeResponse[$arFields['ID']] = $arFields;
    } 
    
    $closeResponse = array();
    $arCloseOrder = Array("NAME"=>"ASC");
    $arCloseSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_*");
    $arCloseFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "PROPERTY_STATUS" => 560, "PROPERTY_NEEDS"=>$_REQUEST['needs_id']);
    $list = CIBlockElement::GetList($arCloseOrder, $arCloseFilter, false, false, $arCloseSelect);
    $closeCount = $list->SelectedRowsCount();
    while ($ob = $list->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arFields['prop'] = $ob->GetProperties();
        $closeResponse[$arFields['ID']] = $arFields;
    }
    
?>

<?if ($closeCount+$activeCount >0):?>
(<?=$activeCount;?>/<?=$closeCount+$activeCount;?>)
<?endif;?>
<?
require($bxRoot.'modules/main/include/epilog_after.php');
