<?php
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');
    
    if(isset($_REQUEST['status']) && !empty($_REQUEST['status'])){
        $_SESSION['filter'] = $_REQUEST['status'];
    }
    if(isset($_REQUEST['warning']) && !empty($_REQUEST['warning'])){
        $_SESSION['warning'] = $_REQUEST['warning'];
    }
    if(isset($_REQUEST['paging']) && !empty($_REQUEST['paging'])){
        $_SESSION['paging'] = $_REQUEST['paging'];
    }
    if(isset($_REQUEST['sort']) && !empty($_REQUEST['sort'])){
        $_SESSION['sort'] = $_REQUEST['sort'];
    }
    if(isset($_REQUEST['agent_page']) && !empty($_REQUEST['agent_page'])){
        $_SESSION['agent_page'] = $_REQUEST['agent_page'];
    }
    if(isset($_REQUEST['order']) && !empty($_REQUEST['order'])){
        $_SESSION['order'] = $_REQUEST['order'];
    }
    if(isset($_REQUEST['tab']) && !empty($_REQUEST['tab'])){
        $_SESSION['tab'] = $_REQUEST['tab'];
    }
    if(isset($_REQUEST['tab_watch']) && !empty($_REQUEST['tab_watch'])){
        $_SESSION['tab_watch'] = $_REQUEST['tab_watch'];
    }
    if(isset($_REQUEST['tab_show']) && !empty($_REQUEST['tab_show'])){
        $_SESSION['tab_show'] = $_REQUEST['tab_show'];
    }
    
    //�������� boss - ����� ����������� � �������� alf_filter - ������ �� ������ ����� �������
    //����������������� ����� ����� ��������
    if(isset($_REQUEST['boss']) && !empty($_REQUEST['boss'])){
        $_SESSION['boss'] = $_REQUEST['boss'];
        $_SESSION['alf_filter'] = 'no';
    }
    if(isset($_REQUEST['alf_filter']) && !empty($_REQUEST['alf_filter'])){
        $_SESSION['alf_filter'] = $_REQUEST['alf_filter'];
        $_SESSION['boss'] = 'all';
    }
    
    if(isset($_REQUEST['end_search']) && !empty($_REQUEST['end_search'])){
        unset($_SESSION['search_begin']);
    }

    
    require($bxRoot.'modules/main/include/epilog_after.php');
