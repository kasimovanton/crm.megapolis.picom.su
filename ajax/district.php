<?
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');


    CModule::IncludeModule("iblock");
    $IBLOCK_ID = 9; 
 
    $arOrder = Array("NAME"=>"ASC");
    $arSelect = Array("ID", "NAME", "IBLOCK_ID");
    $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "PROPERTY_CITY" => $_REQUEST['district']);
    $list = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

    while ($ob = $list->GetNextElement()) {
        $arFields = $ob->GetFields();
        $response[$arFields['ID']] = $arFields['NAME'];
        
    }
    
    if (!empty($response)){
        $result['error'] = '';
        foreach($response as $key => $value){
            $result['response'][$key] = $value;
        }    
        echo json_encode($result);
    } else {
        $result['error'] = 'empty';
        echo json_encode($result);
    }

require($bxRoot.'modules/main/include/epilog_after.php');
