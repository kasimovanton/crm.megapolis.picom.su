<?php
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';

    if (!$isAjax) die('no access');
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');

    $maxResultCount = 20;
    
    CModule::IncludeModule("iblock");
    $requestString = trim($_REQUEST['request']);
    $IBLOCK_ID = 10; 
    $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y");
    $arOrder = Array("PROPERTY_DOP_PHONE"=>"nulls,asc", "NAME"=>"ASC", "PROPERTY_PHONE"=>"ASC");
    $arSelect = Array("ID", "NAME", "IBLOCK_ID", "PREVIEW_TEXT", "PROPERTY_*");
    $new_array = array();
    $new_array["LOGIC"] = "AND";
    $arRequest = preg_split('/ /', $requestString);
    foreach ($arRequest as $filterString){
        if(!empty($filterString)){
            $newsub_array = array();
            $newsub_array["LOGIC"] = "OR";
            $newsub_array[] = array('PROPERTY_PHONE'=>'%'.$filterString.'%');
            $newsub_array[] = array('PROPERTY_DOP_PHONE'=>'%'.$filterString.'%');
            $newsub_array[] = array('NAME'=>'%'.$filterString.'%');
            $new_array[] = $newsub_array;
        }
    }
    $arFilter[] = $new_array;

    $list = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
    
    $arResponse = array();
    while ($ob = $list->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arFields['prop'] = $ob->GetProperties();
        // print_r($arFields);
        $arResponse[$arFields['ID']] = array(
                                               'NAME' => $arFields['prop']['PHONE']['VALUE'].' '.$arFields['NAME'],
                                               'PHONE' => $arFields['prop']['DOP_PHONE']['VALUE'],
                                               'FACE' => $arFields['prop']['DOP_FACE']['VALUE'],
                                               'TEXT' => $arFields['PREVIEW_TEXT'],
                                            );
        
    }
?>
<?if (count($arResponse)>0):?>
    <div class="number_result">
        <i>Результатов: </i>
        <span><?if (count($arResponse)<=$maxResultCount):?>
            <?=count($arResponse);?>
        <?else:?>
            <?=$maxResultCount;?> из <?=count($arResponse);?><?endif;?>
        </span>
    </div>
    <?$counter=0;?>
    <?foreach($arResponse as $idResponse => $response):?>
        <?php
            $primaryName = false;
            foreach ($arRequest as $filterString){
                if(!empty($filterString) && preg_match('/'.$filterString.'/', $response['NAME'])){
                   $primaryName = true;
                    break;
                }
            }
        ?>
        <div class="search_result<?if(!$primaryName):?> secondary<?endif;?>" data-value="<?=$idResponse;?>">
            <span><?=$response['NAME'];?></span>
            <div class="icon_block">
                <?if (!empty($response['PHONE'])):?>
                    <span class="icon">t</span>
                <?endif;?>
                <?if (!empty($response['FACE'])):?>
                    <span class="icon">f</span>
                <?endif;?>
                <?if (!empty($response['TEXT'])):?>
                    <span class="icon">i</span>
                <?endif;?>
            </div>
        </div>
        <?php
            $counter++;
            if ($counter>=$maxResultCount) break;
        ?>
    <?endforeach?>
<?endif?>