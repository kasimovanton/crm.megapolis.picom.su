<?
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');

    CModule::IncludeModule("iblock");
    $IBLOCK_ID = 16; 
    
    $arOrder = Array("NAME"=>"ASC");
    $arSelect = Array("ID", "NAME", "IBLOCK_ID");
    $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "ID" => $_REQUEST['object_id']);
    $list = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

    if ($ob = $list->GetNextElement()) {
        $arFields = $ob->GetProperties();
        echo htmlspecialcharsBack($arFields['WATCHER_COMMENT']['VALUE']['TEXT']);
    } else {
        echo "Комментарий риелтора отсутствует.";
    }
   

