<?php
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');
    
    $userStatus = getUserStatus($USER->GetID());
    if (!in_array(9, $userStatus) && !($USER->IsAdmin())){
        echo 'У вас нет прав доступа к этому разделу';
        exit();
    }
    
    function strip_data($text) {
        $text = strip_tags($text);
        $text = htmlspecialchars($text);
        $text = mysql_escape_string($text);
                
        return $text;
    }
    
    CModule::IncludeModule("iblock");


    
    $IBLOCK_ID = 10; 
    $clientID = strip_data($_REQUEST['client']);
    $managerID = strip_data($_REQUEST['manager']);
    
    $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "ID" =>$clientID, "PROPERTY_MANAGER" => false);
    $arOrder = Array("NAME"=>"ASC");
    $arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_RIELTOR", "PROPERTY_MANAGER");

    $list = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
    if($ob = $list->GetNextElement()){
        CIBlockElement::SetPropertyValues($clientID, $IBLOCK_ID, $managerID, "MANAGER");
        
        $arFields = $ob->GetFields();

        if (!empty($arFields['PROPERTY_RIELTOR_VALUE'])){
            $rsUser = CUser::GetByID($arFields['PROPERTY_RIELTOR_VALUE']);
            $currentUser = $rsUser->Fetch();
            $arMail = array(
                "rieltor_email" => $currentUser['EMAIL'],
                "client_id59" => $clientID,
                "manager_id" => $managerID,
            );
            CEvent::Send("ADD_CLIENT_MANAGER", array("s1"), $arMail, "N", 29);
        }
        echo "Вы успешно закреплены за клиентом, как его офис-менеджер";
    } else {
        echo "Ошибка: Не найдено запрошенного клиента без прикрепленного офис-менеджера";
    }

    