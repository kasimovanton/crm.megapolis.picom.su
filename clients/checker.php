<?php
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');
    
    $userStatus = getUserStatus($USER->GetID());
    if (!in_array(9, $userStatus) && !($USER->IsAdmin())){
        echo 'У вас нет прав доступа к этому разделу';
        exit();
    }
    
    function strip_data($text) {
        $text = strip_tags($text);
        $text = htmlspecialchars($text);
        $text = mysql_escape_string($text);
                
        return $text;
    }
    
    CModule::IncludeModule("iblock");

    $IBLOCK_ID = 10; 
    $number = strip_data($_REQUEST['search_number']);
    $user_id = strip_data($_REQUEST['user']);
    
    $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y");
    $arOrder = Array("NAME"=>"ASC");
    $arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_RIELTOR", "PROPERTY_MANAGER");
    $new_array = array();
    $new_array["LOGIC"] = "OR";
    $new_array[] = array('PROPERTY_PHONE'=>$number);
    $new_array[] = array('PROPERTY_DOP_PHONE'=>$number);
    $arFilter[] = $new_array;
    $list = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
?>
    <?if($ob = $list->GetNextElement()):?>
       <?php
            $arFields = $ob->GetFields();
            $userId = $arFields['PROPERTY_MANAGER_VALUE'] ? $arFields['PROPERTY_MANAGER_VALUE'] : $arFields['PROPERTY_RIELTOR_VALUE'];
            $rsUser = CUser::GetByID($userId);
            if ($currentUser = $rsUser->Fetch()){
                $fullName = $currentUser['LAST_NAME'].' '.$currentUser['NAME'].' '.$currentUser['SECOND_NAME'];
            } else {
                $fullName = "Неизвестный пользователь";
            }
        ?>
        <?if(!empty($arFields['PROPERTY_MANAGER_VALUE'])):?>
            <?if ($user_id != $arFields['PROPERTY_MANAGER_VALUE']):?>
                <span class="status">Номер найден. Офис менеджер уже задан:</span>
                <span class="owner"><a href="/agents/<?=$arFields['PROPERTY_MANAGER_VALUE'];?>/"><?=$fullName;?></a></span>
            <?else:?>
                <span class="status">Ваш клиент</span>
                <span class="name"><a href="/clients/<?=$arFields['ID'];?>/"><?=$arFields['NAME'];?></a></span>
            <?endif;?>
            
        <?else:?>
            <span class="status">Номер найден. Офис менеджер не задан. Риэлтор:</span>
            <span class="owner"><a href="/agents/<?=$arFields['PROPERTY_RIELTOR_VALUE'];?>/"><?=$fullName;?></a></span>
            <input type="hidden" value="<?=$arFields['ID'];?>" name="client_id" />
            <input id="adder" type="button" value="Присвоить" />
            <script>
                $('#adder').click(function(){
                    if($('#search_number').val()){
                        $.ajax({
                            cache: false,
                            data: {'ajax':'y', 'manager':$('input[name=user_id]').val(),'client': $('input[name=client_id]').val()},
                            url: '/clients/add_manager.php',
                            error: function(){
                                console.log('Connecting error');
                            },
                            success: function(data){
                                $('.block_result').show().html(data);
                            }
                        });
                    } else {
                        $('.info_status').addClass('red');

                    }
                });
            </script>
        <?endif;?>
       
        
    <?else:?>
        <span class="status">Клиент с таким номером телефона отсутствует в системе</span>
        <?php
            $deleteArray = array("(", ")", "-", "+", " ");
            $clearNumber = str_replace($deleteArray, "", $number);
        ?>
        <a class="button button_universal" target="_blank" href="/clients/add.php?pnone_number=<?=$clearNumber;?>">Добавить клиента</a>
    <?endif;?>
    
    