<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Проверка существования клиента");
?>
<?$userStatus = getUserStatus($USER->GetID());
if (!in_array(9, $userStatus) && !($USER->IsAdmin())){
    echo 'У вас нет прав доступа к этому разделу';
    exit();
}?>

    <span class="info_status">Введите номер телефона</span>
    <input id="search_number" type="text" name="search"/>
    <input id="check" type="button" value="Проверить" />
    <input type="hidden" value="<?=$USER->GetID();?>" name="user_id" />
    <div class="block_result hidden">

    </div>
    
    <script>
        $('#check').click(function(){
            if($('#search_number').val()){
                $.ajax({
                    cache: false,
                    data: {'ajax':'y', 'user':$('input[name=user_id]').val(),'search_number': $('#search_number').val()},
                    url: '/clients/checker.php',
                    error: function(){
                        console.log('Connecting error');
                    },
                    success: function(data){
                        $('.block_result').show().html(data);
                        $('.info_status').removeClass('red');
                        
                    }
                });
            } else {
                $('.info_status').addClass('red');

            }
        });
    </script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>