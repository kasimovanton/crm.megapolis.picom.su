<?
    if(!isset($_REQUEST['ajax'])){
        exit;
    }
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
    
    CModule::IncludeModule("iblock");
    $rsUser = CUser::GetByID($USER->GetID());
    $currentUser = $rsUser->Fetch();
    
    
    $arOrder = Array("SORT"=>"ASC");
    $arSelect = Array("ID", "NAME");
    $arFilter = Array("IBLOCK_ID"=>5, "ACTIVE"=>"Y", "PROPERTY_Client" =>$_REQUEST['CODE']);
    $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

    if($ob = $res->GetNextElement()){
        $errorMsg = 'У клиента еще остались объявления';
        header("Location: http://crm.megapolis18.ru/clients/".$_REQUEST['CODE'].'/?errorMsg='.$errorMsg);
        exit;
    } else {
        
        $allChildsTree = getChildsId(CUser::GetID(), false, true);
        $allChildsTree[] = CUser::GetID();
        $arSelect = Array("ID", "NAME", "PROPERTY_MANAGER");
        $arFilter = Array("IBLOCK_ID"=>10, "ACTIVE"=>"Y", "ID" =>$_REQUEST['CODE'], "PROPERTY_RIELTOR" =>$allChildsTree);
        $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
        if($ob = $res->GetNextElement()){
            $arFields = $ob->GetFields();
            $IBLOCK_ID = 10;
            $CLIENT_ID = intval($_REQUEST['CODE']);
            if (!empty($arFields['PROPERTY_MANAGER_VALUE'])){
                if ($CLIENT_ID){
                    CIBlockElement::SetPropertyValues($CLIENT_ID, $IBLOCK_ID, mysql_escape_string($arFields['PROPERTY_MANAGER_VALUE']),'RIELTOR');
                } 
            } else {
                if ($CLIENT_ID){
                    CIBlockElement::SetPropertyValues($CLIENT_ID, $IBLOCK_ID, mysql_escape_string($_REQUEST['reason']),'REASON');
                    $el = new CIBlockElement;
                    $res = $el->Update($CLIENT_ID, Array("ACTIVE" => "N"));
                    if($res){
                        $reason = mysql_escape_string($_REQUEST['reason']);
                        addLog("Удаление", "Пользователь {$currentUser['LAST_NAME']} {$currentUser['NAME']} произвел деактивацию клиента по причине: \"{$reason}\" ", $currentUser['ID'], "Клиент", $CLIENT_ID);
                    }
                } 
            }

        } else {
            $errorMsg = 'У Вас недостаточно прав для выполнения этого действия';
            header("Location: http://".$_SERVER['SERVER_NAME']."/clients/".$_REQUEST['CODE'].'/?errorMsg='.$errorMsg);
            exit;
        }

        // header("Location: http://crm.megapolis18.ru/clients/"); 
        // exit;
    }
    



