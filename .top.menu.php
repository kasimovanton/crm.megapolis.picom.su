<?
$aMenuLinks = Array(
	Array(
		"Поиск", 
		"/search/", 
		Array(), 
        Array(
            "access" => 8,
        ), 
		"" 
	),
	Array(
		"Объявления", 
		"/ads/", 
		Array(), 
        Array(
            "access" => "all",
            "disable" => 8,
        ), 
		"" 
	),
	Array(
		"Агенты", 
		"/agents/", 
		Array(), 
        Array(
            "access" => "all",
            "disable" => "",
        ), 
		"" 
	),
	Array(
		"Клиенты", 
		"/clients/", 
		Array(), 
		Array(
            "access" => "all",
            "disable" => 8,
        ),  
		"" 
	),
	Array(
		"Переопределение", 
		"/temp_clients/", 
		Array(), 
        Array(
            "access" => "reassign",
            "disable" => "",
        ),  
		"" 
	),
	Array(
		"Объекты", 
		"/objects/", 
		Array(), 
        Array(
            "access" => 9,
            "disable" => "",
        ), 
		"" 
	),
	Array(
		"Колл-центр", 
		"/operator/", 
		Array(), 
        Array(
            "access" => 8,
            "disable" => "",
        ), 
		"" 
	),
    Array(
		"Осмотр", 
		"/watch/", 
		Array(), 
        Array(
            "access" => "watch",
            "disable" => "",
        ),  
		"" 
	),
	Array(
		"Показ", 
		"/show/", 
		Array(), 
        Array(
            "access" => "show",
            "disable" => "",
        ),  
		"" 
	),
);
?>