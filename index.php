<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Megapolis CRM");
?>
<?$userStatus = getUserStatus($USER->GetID());?>
    <?if (!in_array(8, $userStatus)):?>
        <?$APPLICATION->IncludeComponent("picom:news.list", ".default", array(
            "IBLOCK_TYPE" => "objects",
            "IBLOCK_ID" => "5",
            "NEWS_COUNT" => "10",
            "SORT_BY1" => "ACTIVE_FROM",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "PRICE",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "filter_ex",
            "FIELD_CODE" => array(
                0 => "NAME",
                1 => "DETAIL_TEXT",
                2 => "DETAIL_PICTURE",
                3 => $arParams["LIST_FIELD_CODE"],
                4 => "",
            ),
            "PROPERTY_CODE" => array(
                0 => "",
                1 => "PRICE",
                2 => "",
            ),
            "CHECK_DATES" => "N",
            "DETAIL_URL" => "/ads/#SECTION_CODE#/#ID#/",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => $arParams["CACHE_TIME"],
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "N",
            "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
            "ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
            "SET_TITLE" => "N",
            "SET_BROWSER_TITLE" => "N",
            "SET_META_KEYWORDS" => "N",
            "SET_META_DESCRIPTION" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "PAGER_TEMPLATE" => ".default",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "Объявления",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "",
            "PAGER_SHOW_ALL" => "N",
            "DISPLAY_DATE" => "N",
            "USER_ID" => CUser::GetID(),
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "N",
            "DISPLAY_PREVIEW_TEXT" => "N",
            "AJAX_OPTION_ADDITIONAL" => ""
            ),
            $component
        );?>

        <?
        $APPLICATION->IncludeComponent(
            "picom:search.form",
            "flat",
            Array(
                "PAGE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["search"]
            ),
            $component
        );
        ?>
    <?else:?>
        <?LocalRedirect("/search/");?>
    <?endif;?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>