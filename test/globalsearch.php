<?php
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';

    if (!$isAjax) die('no access');
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');

    $maxResultCount = 20;
    
    CModule::IncludeModule("iblock");
    $requestString = trim($_REQUEST['request']);
    $IBLOCK_ID = 10; 
    $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y");
    $arOrder = Array("NAME"=>"ASC");
    $arSelect = Array("ID", "NAME", "IBLOCK_ID", "DETAIL_TEXT", "PROPERTY_*");
    $new_array = array();
    $new_array["LOGIC"] = "AND";
    $arRequest = preg_split('/ /', $requestString);
    foreach ($arRequest as $filterString){
        if(!empty($filterString)){
            $newsub_array = array();
            $newsub_array["LOGIC"] = "OR";
            $newsub_array[] = array('PROPERTY_PHONE'=>'%'.$filterString.'%');
            $newsub_array[] = array('PROPERTY_DOP_PHONE'=>'%'.$filterString.'%');
            $newsub_array[] = array('NAME'=>'%'.$filterString.'%');
            $new_array[] = $newsub_array;
        }
    }
    $arFilter[] = $new_array;

    $list = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
    
    $arResponse = array();
    while ($ob = $list->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arFields['prop'] = $ob->GetProperties();
        // print_r($arFields);
        $arResponse[$arFields['ID']] = array(
                                               'NAME' => $arFields['prop']['PHONE']['VALUE'].' '.$arFields['NAME'],
                                               'PHONE' => $arFields['prop']['DOP_PHONE']['VALUE'],
                                               'TEXT' => $arFields['DETAIL_TEXT'],
                                            );
        
    }
?>
<?if (count($arResponse)>0):?>
    <div class="number_result">
        <i>Результатов: </i>
        <span><?if (count($arResponse)<=$maxResultCount):?>
            <?=count($arResponse);?>
        <?else:?>
            <?=$maxResultCount;?> из <?=count($arResponse);?><?endif;?>
        </span>
    </div>
    <?$counter=0;?>
    <?foreach($arResponse as $idResponse => $response):?>
        <div class="search_result" data-value="<?=$idResponse;?>"><?=$response['NAME'];?></div>
        <?php
            $counter++;
            if ($counter>=$maxResultCount) break;
        ?>
    <?endforeach?>
<?else:?>
    <h3>Добавление нового клиента</h3>
    <div class="row">
        <label for="">Имя</label>
        <input type="text" name="client_name">
    </div>
    <div class="row">
        <label for="">Телефон</label>
        <input type="text" name="client_phone">
    </div>
    <button class="button button_universal">Добавить</button>
<?endif?>