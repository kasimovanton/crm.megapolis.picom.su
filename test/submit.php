<?php
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';

    if (!$isAjax) die('no access');
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');
    // require('/test/Excel/reader.php');
    // echo $_SERVER['DOCUMENT_ROOT'].'/test/Excel/reader.php';
    CModule::IncludeModule("iblock");
    
    $newPrice = $_FILES[0];
    
    $response = array(
                    'error' => false,
                    'success' => "",
                    'text_response' => "1",
                );
    if ($newPrice['type'] != 'text/xml'){
        $response['error'] = true;
        $response['text_response'] = 'Загруженный файл не является форматом xml';
    } elseif ($newPrice['error'] > 0){
        $response['error'] = true;
        $response['text_response'] = 'Во время загрузки файла произошли ошибки. Попробуйте обновить страницу.';
    } else {
        // $arrXmlFile =  simplexml_load_file($_FILES[0]['tmp_name']);
        $response['success'] = true;
        // $response['text_response'] = $arrXmlFile;
        // foreach($arrXmlFile->Product as $arProduct){
            // $response['text_response'] .= $arProduct->Id;
        // }
    }
    
    // if(!$response['error']){
        // $response['success'] = true;
        // $response['text_response'] = 'Проверка xml-файла прошла успешно';
    // }
    
    echo json_encode($response, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

