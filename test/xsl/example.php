<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');


/** Include PHPExcel_IOFactory */
require_once dirname(__FILE__) . '/Classes/PHPExcel/IOFactory.php';


if (!file_exists("example.xlsx")) {
	exit("File not exist." . EOL);
}

$objPHPExcel = PHPExcel_IOFactory::load("example.xlsx");

$objPHPExcel->setActiveSheetIndex(0);
$aSheet = $objPHPExcel->getActiveSheet();

$allData = array();
foreach($aSheet->getRowIterator() as $row){
    $cellIterator = $row->getCellIterator();
    $item = array();
    foreach($cellIterator as $cell){
        $currentCell = $cell->getCalculatedValue();
        if(!empty($currentCell))
        array_push($item, $currentCell);
    }
    if(!empty($item)){
        array_push($allData, $item);
    }
}
echo '<pre>';
print_r($allData);
echo '</pre>';

$errorCounter = 0;
$textError = array();
$arHeader = $allData[0];
if ($arHeader[0] != 'Идентификатор' ||
    $arHeader[1] != 'Тип' ||
    $arHeader[2] != 'Наименование' ||
    $arHeader[3] != 'Обозначение' ||
    $arHeader[4] != 'Количество' ||
    $arHeader[5] != 'Цена'
    ){
    $errorCounter++;
    $textError[] =  'Ошибка в заголовке файла';
}

echo 'error: '.$errorCounter;
echo '<pre>';
print_r($textError);
echo '</pre>';

// $objReader = PHPExcel_IOFactory::createReader('Excel2007');
// $testObject = $objReader->load("05featuredemo.xlsx");
// echo '<pre>';
// print_r($objReader->getReadDataOnly());
// print_r($objReader->listWorksheetInfo("05featuredemo.xlsx"));
// echo '</pre>';




/*$objWriter->save(str_replace('.php', '.xlsx', __FILE__));

$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;

echo date('H:i:s') , " File written to " , str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;
echo 'Call time to write Workbook was ' , sprintf('%.4f',$callTime) , " seconds" , EOL;
// Echo memory usage
echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;


// Echo memory peak usage
echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

// Echo done
echo date('H:i:s') , " Done writing file" , EOL;
echo 'File has been created in ' , getcwd() , EOL;*/
