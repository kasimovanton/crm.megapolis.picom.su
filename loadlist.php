<?
    require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
    $APPLICATION->SetTitle('Megapolis CRM');
?>

<?CModule::IncludeModule("iblock");?>

<?php
    function timeExplode($strDate)
    {
        $arDateTime = explode(" ", $strDate);
        $arDate = explode(".", $arDateTime[0]);
        $arTime = explode(":", $arDateTime[1]);
                    
        return mktime($arTime[0], $arTime[1], $arTime[2], $arDate[1], $arDate[0], $arDate[2]);
    }
    
    $IBLOCK_ID = 5; 
    $arOrder = Array("SORT"=>"ASC");
    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_CREATE", "IBLOCK_SECTION_ID", "DATE_ACTIVE_FROM", "DATE_ACTIVE_TO", "DETAIL_TEXT", "DETAIL_PICTURE", "PROPERTY_*");
    $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "!PROPERTY_Avito" => false);
    $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
    
    //Телефон call-center
    $callPhone = getCallcenterNumber();

    if (file_exists('test.xml')){
        $data='<?xml version="1.0" encoding="UTF-8"?>
            <Ads target="Avito.ru" formatVersion="2">
            </Ads>';
        $xml=new SimpleXMLElement($data);
        
        while($ob = $res->GetNextElement()){
            $arFields = $ob->GetFields();
            
            $arFields['PROPERTIES'] = $ob->GetProperties();




            $ar_res = getSectionProps($IBLOCK_ID,$arFields['IBLOCK_SECTION_ID']);

            //Avito->xml

            //Данные автора
            $rsUser = CUser::GetByID($arFields['PROPERTIES']['Author']['VALUE']);
            $arUser = $rsUser->Fetch();
            //Родительский раздел
            $sect = CIBlockSection::GetByID($ar_res['IBLOCK_SECTION_ID']);
            $parent_sect = $sect->GetNext();

            $adv = $xml->addChild('Ad');
            
            $adsId = "";
            $checkPointA = timeExplode("10.08.2015 00:00:00");
            if (($checkPointA - timeExplode($arFields['DATE_CREATE']))<0){
                $adsId .= "n";
            }
            if ($arFields['PROPERTIES']['CORRECTIVE_AVITO']['VALUE']){
                $adsId .= "i";
            }
            $adv->addChild('Id', $adsId.$arFields['ID']);
            $adv->addChild('Category',$parent_sect['NAME']);
            $adv->addChild('OperationType',$ar_res['NAME']);
            
            $adv->addChild('DateBegin', date("Y-m-d"));
            
            if (!empty($arFields['DATE_ACTIVE_TO'])){
                $adv->addChild('DateEnd', date("Y-m-d", MakeTimeStamp($arFields['DATE_ACTIVE_TO'], "DD.MM.YYYY HH:MI:SS")));
            }
            if (!empty($arFields['DETAIL_TEXT'])){
                $adv->addChild('Description ', $arFields['DETAIL_TEXT']);
            }            
            
            if (!empty($arFields['DETAIL_PICTURE']) || (count($arFields['PROPERTIES']['Images']['VALUE'])>0 && !empty($arFields['PROPERTIES']['Images']['VALUE']))){
                $struct_img = $adv->addChild('Images');
            }
            if (!empty($arFields['DETAIL_PICTURE'])){
                $src = $struct_img->addChild('Image');
                $src -> addAttribute('url', 'http://'.$_SERVER['SERVER_NAME'].CFile::GetPath($arFields['DETAIL_PICTURE']));
            }
            if (count($arFields['PROPERTIES']['Images']['VALUE'])>0 && !empty($arFields['PROPERTIES']['Images']['VALUE'])){
                foreach ($arFields['PROPERTIES']['Images']['VALUE'] as $img){
                    $src = $struct_img->addChild('Image');
                    $src -> addAttribute('url', 'http://'.$_SERVER['SERVER_NAME'].CFile::GetPath($img));
                }
            }
                
            if ($arFields['PROPERTIES']['CALLC']['VALUE'] && $callPhone != false){
                $adv->addChild('ContactPhone', $callPhone);
            } else {
                $adv->addChild('EMail',trim($arUser['EMAIL']));
                $adv->addChild('ManagerName',trim($arUser['NAME']));
                $adv->addChild('ContactPhone',trim($arUser['PERSONAL_PHONE']));
            }
            
            

            foreach ($ar_res['UF_AVITO'] as $value){
                $property = preg_replace("/PROPERTY_/", "", $value);
                switch (getPropType(5, $property)){
                    case 'E':
                        if (!empty($arFields['PROPERTIES'][$property]['VALUE'])){
                            $adv->addChild($property, getElemName($arFields['PROPERTIES'][$property]['VALUE']));
                        }
                        break;
                     default:
                        if (!empty($arFields['PROPERTIES'][$property]['VALUE'])){  
                            if (preg_match("/ObjectType/", $property)){
                                $adv->addChild('ObjectType', $arFields['PROPERTIES'][$property]['VALUE']);
                            } else {
                                if (preg_match("/ObjectSubtype/", $property)){
                                    $adv->addChild('ObjectSubtype', $arFields['PROPERTIES'][$property]['VALUE']);
                                } else {
                                    if ($property == 'Street'){
                                        $fullStreet = trim($arFields['PROPERTIES']['Street']['VALUE'].' '.$arFields['PROPERTIES']['HOUSE']['VALUE']);
                                        $adv->addChild('Street', $fullStreet); //street + HOUSE
                                    } else {
                                        if ($property == 'DistanceToCity' && $arFields['PROPERTIES'][$property]['VALUE']=="В черте города"){
                                            $adv->addChild($property, 0);
                                        } else {
                                            $adv->addChild($property, $arFields['PROPERTIES'][$property]['VALUE']);
                                        }
                                    }
                                    
                                }
                            }
                        }
                        break;
                }
                
            }
        }
    } else {
            exit('Failed to open test.xml.');
    }
    $xml->asXML('test.xml');
    
?>
<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');?>