<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>
    <?$userStatus = getUserStatus($USER->GetID());?>
    <?if (!in_array(8, $userStatus) && !($USER->IsAdmin())):?>
        У вас нет прав доступа к этому разделу
    <?else:?>
        <?
        $APPLICATION->SetTitle("Добавить вызов");
        ?>
        <?$APPLICATION->IncludeComponent(
            "picom:add.call",
            ".default",
            Array(
            ),
            false
        );?>
    <?endif;?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>