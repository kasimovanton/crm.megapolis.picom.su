<?php
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
    $APPLICATION->SetTitle("Колл-центр");
?>

    <?$userStatus = getUserStatus($USER->GetID());?>
    <?if (!in_array(8, $userStatus) && !($USER->IsAdmin())):?>
        У вас нет прав доступа к этому разделу
    <?else:?>
        <?php
            $iBlock = 5;
            $arFilter = array(
                "city",
            );
            $arResult["ADDITIONAL_PROPERTY"] = getEnumValues($iBlock,$arFilter);
            
            $filter = array('GROUPS_ID'=>6,'ACTIVE'=>'Y');
            $rsUsers = CUser::GetList(
                ($by='id'), ($order='asc'),
                $filter,
                array('SELECT'=>array('UF_HEAD'),'FIELDS' => array())
            );
            $arUserList = array();
            while ($user = $rsUsers->GetNext()) {
                $arUserList[$user['ID']] = $user;
                $arUserList[$user['ID']]['GROUPS'] = CUser::GetUserGroup($user['ID']);
                $arUserList[$user['ID']]['FULL_NAME'] = $user['LAST_NAME'].' '.$user['NAME'].' '.$user['SECOND_NAME'];
            }
        ?>
        <div class="filter_tabs_block">
            <div class="tabs_head_block">
                <h3 for="need_tab"<?if (!isset($_REQUEST['type']) || $_REQUEST['type']=='need'):?> class="active"<?endif;?>> Объявления (Потребности)</h3>
                <h3 for="call_tab"<?if ($_REQUEST['type']=='call'):?> class="active"<?endif;?>>Журнал вызовов (входящие)</h3>
            </div>
            <div class="tabs call_tab<?if ($_REQUEST['type']=='call'):?> visible<?endif;?>" data-type="calls">
                <div class="ajax_filter">
                    <form class="calls_form" method="POST" action="">
                        <div class="filter_row_wrap">
                            <div class="filter_row">
                                <select class=" selection dropdown" name="select_contact_type" id="select_contact_type">
                                    <option></option>
                                    <option value="client">Звонки от клиентов</option>
                                    <option value="agent">Звонки от агентов</option>
                                </select>
                            </div>
                            <div class="filter_row">
                                <div class="input-wrap">
                                    <input placeholder="Номер телефона" type="text" name="call_phone" autocomplete="off">
                                    <button type="button" class="del-input select2-search-choice-close"></button>
                                </div>
                            </div>
                            <div class="filter_row">
                                <div class="input-wrap">
                                    <input placeholder="Дата/время (От)" type="text" name="call_date_from" onclick="BX.calendar({node: this, field: this, bTime: true, bHideTime:false});">
                                    <button type="button" class="del-input select2-search-choice-close"></button>
                                </div>
                            </div>
                            <div class="filter_row">
                                <div class="input-wrap">
                                    <input placeholder="Дата/время (До)" type="text" name="call_date_to" onclick="BX.calendar({node: this, field: this, bTime: true, bHideTime:false});">
                                    <button type="button" class="del-input select2-search-choice-close"></button>
                                </div>
                            </div>
                        </div>
                        <div class="filter_row_wrap">
                            <!--<span class="alert phone_type">Выберите необходимый тип контакта для фильтрации по номеру телефона</span>-->
                            <!--class="multi_agent"//Для multiple="multiple"-->
                            <div class="filter_row">
                                <select class=" selection dropdown" name="select_rieltor" id="select_rieltor">
                                    <option></option>
                                    <?foreach($arUserList as $userKey => $arUser):?>
                                        <?if(!in_array(9, $arUser['GROUPS']) && !in_array(8, $arUser['GROUPS'])):?>
                                            <option value="<?=$userKey;?>"><?=$arUser['FULL_NAME'];?></option>
                                        <?endif;?>
                                    <?endforeach;?>
                                </select>
                            </div>
                            <div class="filter_row">
                                <select class=" selection dropdown" name="select_manager" id="select_manager">
                                    <option></option>
                                    <?foreach($arUserList as $userKey => $arUser):?>
                                        <?if(in_array(9, $arUser['GROUPS'])):?>
                                            <option value="<?=$userKey;?>"><?=$arUser['FULL_NAME'];?></option>
                                        <?endif;?>
                                    <?endforeach;?>
                                </select>
                            </div>
                            <div class="filter_row">
                                <select class=" selection dropdown" name="select_operator" id="select_operator">
                                    <option></option>
                                    <?foreach($arUserList as $userKey => $arUser):?>
                                        <?if(in_array(8, $arUser['GROUPS'])):?>
                                            <option value="<?=$userKey;?>"><?=$arUser['FULL_NAME'];?></option>
                                        <?endif;?>
                                    <?endforeach;?>
                                </select>
                            </div>
                        </div>
                        <div class="filter_row_wrap filter_footer">
                            <input id="only_mine" class="only_mine" type="checkbox" name="only_mine">
                            <label for="only_mine">Только мои вызовы</label>
                        </div>
                    </form>
                </div>
                <div class="form_select ajax_sort">
                    <span>Сортировать:</span>
                    <select>
                            <option value="by_date">по последнему входящему</option>
                            <option value="by_duration">по длительности звонка</option>
                    </select>
                </div>
            </div>
            <div class="tabs need_tab<?if (!isset($_REQUEST['type']) || $_REQUEST['type']=='need'):?> visible<?endif;?>" data-type="needs">
                <div class="ajax_filter">
                    <form class="needs_form" method="POST" action="">
                        <div class="filter_row_wrap">
                            <div class="filter_row category_select">
                                <span class="price_text">Категория:</span>
                                <div class="input-wrap">
                                    <select name="subject">
                                        <option value=""<?if(!isset($_REQUEST['subject'])):?> selected=""<?endif;?>>Любая категория</option>
                                        <?php
                                        $arOrder = Array("SORT"=>"ASC");
                                        $arFilter = Array("IBLOCK_ID"=>5, "ACTIVE"=>"Y", "DEPTH_LEVEL"=>"1");
                                        $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
                                        $res_sec = CIBlockSection::GetList($arOrder, $arFilter, true);
                                        while($ob = $res_sec->GetNextElement()):?>
                                              <?php
                                                    $arFields = $ob->GetFields();
                                                ?>
                                                <option <?if($_REQUEST['subject']==$arFields['CODE']):?>selected<?endif;?> value="<?=$arFields['CODE'];?>"><?=$arFields['NAME'];?></option>
                                        <?endwhile;?>
                                    </select>
                                    <button type="button" class="del-input select2-search-choice-close"></button>
                                </div>
                            </div>
                            <div class="filter_row city_select">
                                <span class="price_text">Город:</span>
                                <div class="input-wrap">
                                    <select name="city">
                                        <option value="" selected="">Любой город</option>
                                        <?foreach ($arResult["ADDITIONAL_PROPERTY"]['City'] as $keyId => $strCity):?>
                                            <option value="<?=$keyId;?>"><?=$strCity;?></option>
                                        <?endforeach;?>
                                    </select>
                                    <button type="button" class="del-input select2-search-choice-close"></button>
                                </div>
                            </div>
                            <div class="filter_row district_select">
                                <span class="price_text">Район:</span>
                                <div class="input-wrap">
                                    <select placeholder="Любой район" name="district" multiple="multiple">
                                        <option value="237">Устиновский</option>
                                        <option value="225">Первомайский</option>
                                        <option value="224">Октябрьский</option>
                                        <option value="217">Ленинский</option>
                                        <option value="215">Индустриальный</option>
                                    </select>
                                    <button type="button" class="del-input select2-search-choice-close"></button>
                                </div>
                            </div>
                        </div>
                        <div class="filter_row_wrap filter_footer">
                            <div class="filter_row filter_row_button">
                                <input id="mine_need" class="mine_need" type="checkbox" name="mine_need">
                                <label for="mine_need">Только мои потребности</label>
                            </div>
                            <div class="filter_row filter_row_button">
                                <button class="button button_universal filter_reset">Сброс</button>
                            </div>
                            <div class="filter_row filter_row_button">
                                <button class="button button_universal filter_submit">Поиск</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="form_select ajax_sort">
                    <span>Сортировка:</span>
                    <select>
                        <option value="by_date">По дате добавления</option>
                    </select>
                </div>
            </div>
        </div>
        <h4>Результаты</h4>
        <div class="ajax_result_block">
            <div class="current_ajax_list">
            </div>
            <span class="no_result_block">По запросу ничего не найдено</span>
            <span class="ajax_next_block" data-page="2" data-get="1">Показать еще</span>
            <div class="preload" style="display:none;">
                <div class="text-loader">
                    Загрузка
                </div>
            </div>
        </div>
    <?endif;?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>