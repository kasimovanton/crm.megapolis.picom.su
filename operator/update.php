<?php
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';

    if (!$isAjax) die('no access');
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');
    CModule::IncludeModule("iblock");
    global $USER;
    $maxResultCount = 10;
    $arRequestFilter = $_REQUEST['filter'];
    $infoType = $_REQUEST['type'];
    $sortValue = $_REQUEST['sort'];
    $currentPage = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
    $navArParams = array('nPageSize'=>$maxResultCount, 'iNumPage'=>$currentPage);
    switch($infoType){
        case "calls":
            $IBLOCK_ID = 18; 
            $CLIENT_IBLOCK_ID = 10; 
            
            $arClientWithNeeds = getClientIDListWithNeeds();
            
            $arClientFilter = Array("IBLOCK_ID"=>$CLIENT_IBLOCK_ID, "ACTIVE"=>"Y", "ID"=>CIBlockElement::SubQuery("PROPERTY_CLIENT_ID", array(
                                                                                    "IBLOCK_ID" => $IBLOCK_ID,
                                                                                  )));
            $arClientOrder = Array("ID"=>"ASC");
            $arClientSelect = Array("ID", "NAME", "IBLOCK_ID", "DETAIL_TEXT", "PROPERTY_*");
            $clientList = CIBlockElement::GetList($arClientOrder, $arClientFilter, false, false, $arClientSelect);
            $arClientList = array();
            while ($ob = $clientList->GetNextElement()){
                $arFields = $ob->GetFields();
                $arFields['prop'] = $ob->GetProperties();
                $arClientList[$arFields['ID']] = $arFields;
            }
            
            $filter = array('GROUPS_ID'=>6,'ACTIVE'=>'Y');
            $rsUsers = CUser::GetList(
                ($by='id'), ($order='asc'),
                $filter,
                array('SELECT'=>array('UF_HEAD'),'FIELDS' => array())
            );
            $arUserList = array();
            while ($user = $rsUsers->GetNext()) {
                $arUserList[$user['ID']] = $user;
                $arUserList[$user['ID']]['FULL_NAME'] = $user['LAST_NAME'].' '.$user['NAME'].' '.$user['SECOND_NAME'];
            }
            $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y");
            $arSelect = Array("ID", "NAME", "IBLOCK_ID", "DETAIL_TEXT", "PROPERTY_*");

            switch($sortValue){
                case "by_date":
                    $arOrder = Array("PROPERTY_DATE"=>"DESC");  
                    break;
                case "by_duration":
                    $arOrder = Array("PROPERTY_DURATION"=>"ASC, NULLS");
                    break;
                default:
                    $arOrder = Array("PROPERTY_DATE"=>"DESC");  
            }
            
            $additionalFilter = array();
            $additionalFilter["LOGIC"] = "AND";
            
            if (isset($arRequestFilter['only_mine']) && !empty($arRequestFilter['only_mine'])){
                $additionalFilter[] = array('CREATED_BY' => $USER->GetID());
            }
            
            if (isset($arRequestFilter['call_date_from']) && !empty($arRequestFilter['call_date_from'])){
                $additionalFilter[] = array('>=PROPERTY_DATE' => date('Y-m-d H:i:s', timeExplode($arRequestFilter['call_date_from'])));
            }    
            if (isset($arRequestFilter['call_date_to']) && !empty($arRequestFilter['call_date_to'])){
                $additionalFilter[] = array('<=PROPERTY_DATE' => date('Y-m-d H:i:s', timeExplode($arRequestFilter['call_date_to'])));
            } 
            
            if (isset($arRequestFilter['select_contact_type']) && !empty($arRequestFilter['select_contact_type'])){
                switch($arRequestFilter['select_contact_type']){
                    case "client":
                         $additionalFilter[] = array('!=PROPERTY_CLIENT_ID' => false);
                        break;
                    case "agent":
                        $additionalFilter[] = array('!=PROPERTY_USER_ID' => false);
                        break;
                }
            }

            if (isset($arRequestFilter['call_phone']) && !empty($arRequestFilter['call_phone'])){
                $extraPhoneClientFilter = array();
                $extraPhoneUserFilter = array();
                foreach($arClientList as $keyClient =>$arClient){
                    if ($arClient['prop']['PHONE']['VALUE'] == $arRequestFilter['call_phone']){
                        $extraPhoneClientFilter[] = $keyClient;
                    }
                }
                if(empty($additionalFilter['PROPERTY_CLIENT_ID'])){
                    foreach($arUserList as $keyUser =>$arUser){
                        if ($arUser['PERSONAL_PHONE'] == $arRequestFilter['call_phone']){
                            $extraPhoneUserFilter[] = $keyUser;
                        }
                    }
                }
                if (empty($extraPhoneClientFilter) && empty($extraPhoneUserFilter)){
                    $additionalFilter[] = array('ID'=>false);
                } else {
                    $additionalFilter[] = array(
                                'LOGIC'=>'OR',
                                array('PROPERTY_CLIENT_ID' =>$extraPhoneClientFilter),
                                array('PROPERTY_USER_ID' =>$extraPhoneUserFilter),
                    );
                }
            }
            
            if( isset($arRequestFilter['select_rieltor']) && !empty($arRequestFilter['select_rieltor']) ||
                isset($arRequestFilter['select_manager']) && !empty($arRequestFilter['select_manager']) ||
                isset($arRequestFilter['select_operator']) && !empty($arRequestFilter['select_operator']))
            {
                
                $extraUserFilter = array();
                foreach($arClientList as $keyClient =>$arClient){
                    $addRieltorKey = false;
                    $addManagerKey = false;
                    $addOperatorKey = false;
                    if (isset($arRequestFilter['select_rieltor']) && !empty($arRequestFilter['select_rieltor'])){
                        if ($arClient['prop']['AGENT']['VALUE'] == $arRequestFilter['select_rieltor']){
                            $addRieltorKey = true;
                        }
                    } else {
                        $addRieltorKey = true;
                    }
                    if (isset($arRequestFilter['select_manager']) && !empty($arRequestFilter['select_manager'])){
                        if ($arClient['prop']['MANAGER']['VALUE'] == $arRequestFilter['select_manager']){
                            $addManagerKey = true;
                        }
                    } else {
                        $addManagerKey = true;
                    }
                    if (isset($arRequestFilter['select_operator']) && !empty($arRequestFilter['select_operator'])){
                        if ($arClient['prop']['OPERATOR']['VALUE'] == $arRequestFilter['select_operator']){
                            $addOperatorKey = true;
                        }
                    } else {
                        $addOperatorKey = true;
                    }
                    if ($addRieltorKey&&$addManagerKey&&$addOperatorKey){
                        $extraUserFilter[] = $keyClient;
                    }
                }

                if (empty($extraUserFilter)){
                    $additionalFilter[] = array('ID' =>false);
                } else {
                    $additionalFilter[] = array('PROPERTY_CLIENT_ID' =>$extraUserFilter);
                }
                
            }
            /*
            //Работа фильтра без логики AND и OR
            $additionalFilter = array();
            if (isset($arRequestFilter['call_date_from']) && !empty($arRequestFilter['call_date_from'])){
                $additionalFilter['>=PROPERTY_DATE'] = date('Y-m-d H:i:s', timeExplode($arRequestFilter['call_date_from']));
            }    
            if (isset($arRequestFilter['call_date_to']) && !empty($arRequestFilter['call_date_to'])){
                $additionalFilter['<=PROPERTY_DATE'] = date('Y-m-d H:i:s', timeExplode($arRequestFilter['call_date_to']));
            } 
            
            $havePhoneFilter = isset($arRequestFilter['call_phone']) && !empty($arRequestFilter['call_phone']);
            if ($havePhoneFilter){
                $additionalFilter['PROPERTY_CLIENT_ID'] = array();
                foreach($arClientList as $keyClient =>$arClient){
                    if ($arClient['prop']['PHONE']['VALUE'] == $arRequestFilter['call_phone']){
                        $additionalFilter['PROPERTY_CLIENT_ID'][] = $keyClient;
                    }
                }
                if(empty($additionalFilter['PROPERTY_CLIENT_ID'])){
                    $additionalFilter['PROPERTY_USER_ID'] = array();
                    foreach($arUserList as $keyUser =>$arUser){
                        if ($arUser['PERSONAL_PHONE'] == $arRequestFilter['call_phone']){
                            $additionalFilter['PROPERTY_USER_ID'][] = $keyUser;
                        }
                    }
                }
                if (empty($additionalFilter['PROPERTY_CLIENT_ID']) && empty($additionalFilter['PROPERTY_USER_ID'])){
                    $additionalFilter['ID'] = false;
                }
            } elseif (isset($arRequestFilter['select_contact_type']) && !empty($arRequestFilter['select_contact_type'])){
                if ($arRequestFilter['select_contact_type'] == 'client'){
                    $additionalFilter['!=PROPERTY_CLIENT_ID'] = false;
                } elseif($arRequestFilter['select_contact_type'] == 'agent'){
                    $additionalFilter['!=PROPERTY_USER_ID'] = false;
                }
            }
            if (isset($arRequestFilter['select_rieltor']) && !empty($arRequestFilter['select_rieltor'])){
                foreach($arClientList as $keyClient =>$arClient){
                    if ($arClient['prop']['AGENT']['VALUE'] == $arRequestFilter['select_rieltor'] &&
                        !in_array($arRequestFilter['select_rieltor'], $additionalFilter['PROPERTY_CLIENT_ID'])){
                        $additionalFilter['PROPERTY_CLIENT_ID'][] = $keyClient;
                    }
                }
                if (!isset($additionalFilter['ID']) && empty($additionalFilter['PROPERTY_CLIENT_ID'])){
                    $additionalFilter['ID'] = false;
                }
            }
            if (isset($arRequestFilter['select_manager']) && !empty($arRequestFilter['select_manager'])){
                foreach($arClientList as $keyClient =>$arClient){
                    if ($arClient['prop']['MANAGER']['VALUE'] == $arRequestFilter['select_manager'] &&
                        !in_array($arRequestFilter['select_manager'], $additionalFilter['PROPERTY_CLIENT_ID'])){
                        $additionalFilter['PROPERTY_CLIENT_ID'][] = $keyClient;
                    }
                }
                if (!isset($additionalFilter['ID']) && empty($additionalFilter['PROPERTY_CLIENT_ID'])){
                    $additionalFilter['ID'] = false;
                }
            }*/
           
            $arFilter += $additionalFilter;
            $list = CIBlockElement::GetList($arOrder, $arFilter, false, $navArParams, $arSelect);
            $allResult = $list->NavRecordCount;
            if ($currentPage>1&&$allResult<$maxResultCount*($currentPage-1)){
                return false;
            }
            ?>
                <?if($currentPage == 1):?>
                    <i>Найдено результов: <?=$allResult;?></i>
                <?endif;?>
            <?while ($ob = $list->GetNextElement()):?>
                <?php
                    $arFields = $ob->GetFields();
                    $arFields['prop'] = $ob->GetProperties();
                ?>
                <?//pre($arFields['prop']);?>
                <div class="result_row" data-value="<?=$arFields['ID'];?>" data-result-type="<?if (!empty($arFields['prop']['USER_ID']['VALUE'])):?>agent<?else:?>client<?endif;?>">
                    <div class="call_result block_status">
                        <div class="call_result-info">
                            <?php
                                if (date("d.m.Y",  MakeTimeStamp($arFields['prop']['DATE']['VALUE'], "DD.MM.YYYY HH:MI:SS")) == date("d.m.Y")){
                                    $entry_day = 'Сегодня';
                                }else{
                                    $entry_day = date("d.m.Y",  MakeTimeStamp($arFields['prop']['DATE']['VALUE'], "DD.MM.YYYY HH:MI:SS"));
                                }
                            ?>
                            <span class="status-date <?=$arFields['prop']['CALL_STATUS']['VALUE_XML_ID'];?>_call">
                                <?=$entry_day;?>
                            </span>
                            <span class="status-time"><?=date("H:i",  MakeTimeStamp($arFields['prop']['DATE']['VALUE'], "DD.MM.YYYY HH:MI:SS"));?></span>
                        </div>
                        <span class="status-duration">Длительность звонка:
                            <?if (!empty($arFields['prop']['DURATION']['VALUE'])):?>
                                <div><?=$arFields['prop']['DURATION']['VALUE'];?> сек.</div>
                            <?else:?>
                                <div>Вызов добавлен вручную</div>
                            <?endif;?>
                        </span>
                    </div>
                    <div class="call_info block_status">
                        <?if (!empty($arFields['prop']['CLIENT_ID']['VALUE'])):?>
                            <?php
                                $arCaller =  $arClientList[$arFields['prop']['CLIENT_ID']['VALUE']];
                            ?>
                        <?else:?>
                            <?if (!empty($arFields['prop']['USER_ID']['VALUE'])):?>
                            <?php
                                $arCaller['NAME'] = $arUserList[$arFields['prop']['USER_ID']['VALUE']]['LAST_NAME'].' '.$arUserList[$arFields['prop']['USER_ID']['VALUE']]['NAME'].' '.$arUserList[$arFields['prop']['USER_ID']['VALUE']]['SECOND_NAME']; 
                                $arCaller['prop']['PHONE']['VALUE'] = $arUserList[$arFields['prop']['USER_ID']['VALUE']]['PERSONAL_PHONE'];
                                ?>
                            <?else:?>
                                <?continue;?>
                            <?endif;?>
                        <?endif;?>
                        <div class="phone"><span><?=$arCaller['prop']['PHONE']['VALUE'];?></span></div>
                        <div class="caller">
                            <?=$arCaller['NAME'];?>
                            <i><?if(!empty($arFields['prop']['CLIENT_ID']['VALUE'])):?>Клиент<?else:?>Агент<?endif;?></i>
                        </div>
                        <div class="info review_info">
                            <?=nl2br($arFields['DETAIL_TEXT']);?>
                        </div>
                        <div class="info edit_info">
                           <div class="preload" style="display:none;">
                                <div class="text-loader">Загрузка</div>
                            </div>
                            <textarea placeholder="Комментарий"><?=$arFields['DETAIL_TEXT'];?><?//=preg_replace('/\<br(\s*)?\/?\>/i', "\n", $arFields['DETAIL_TEXT']);?></textarea>
                            <button type="button" class="info_update">Сохранить</button>
                            <button type="button" class="info_cancel">Отмена</button>
                        </div>
                    </div>
                    <div class="call_panel block_status">
                        <div class="agent-wrap">
                            <?if(!empty($arCaller['prop']['AGENT']['VALUE'])):?>
                                <div class="agent">
                                    <?if (isset($arUserList[$arCaller['prop']['AGENT']['VALUE']])):?>
                                        <?if(!empty($arUserList[$arCaller['prop']['AGENT']['VALUE']]['PERSONAL_PHOTO'])):?>
                                            <?$renderImageAgent = CFile::ResizeImageGet($arUserList[$arCaller['prop']['AGENT']['VALUE']]['PERSONAL_PHOTO'], Array("width" => '25px', "height" => '25px'), BX_RESIZE_IMAGE_EXACT);?>
                                            <div class="image"><img src="<?=$renderImageAgent['src'];?>"></div>
                                        <?else:?>
                                            <div class="image"><img src="<?=SITE_TEMPLATE_PATH?>/img/nophoto.jpg"></div>
                                        <?endif;?>
                                        <div class="agent-info">
                                            <div class="title_name"><?=$arUserList[$arCaller['prop']['AGENT']['VALUE']]['FULL_NAME'];?></div>
                                            <div class="position"><?=$arUserList[$arCaller['prop']['AGENT']['VALUE']]['WORK_POSITION'];?></div>
                                        </div>
                                    <?endif;?>
                                </div>
                            <?endif;?>
                            <?if(!empty($arCaller['prop']['MANAGER']['VALUE'])):?>
                                <div class="agent">
                                    <?if(!empty($arUserList[$arCaller['prop']['MANAGER']['VALUE']]['PERSONAL_PHOTO'])):?>
                                        <?$renderImageManager = CFile::ResizeImageGet($arUserList[$arCaller['prop']['MANAGER']['VALUE']]['PERSONAL_PHOTO'], Array("width" => '25px', "height" => '25px'), BX_RESIZE_IMAGE_EXACT);?>
                                        <div class="image"><img src="<?=$renderImageManager['src'];?>"></div>
                                    <?else:?>
                                        <div class="image"><img src="<?=SITE_TEMPLATE_PATH?>/img/nophoto.jpg"></div>
                                    <?endif;?>
                                    <div class="agent-info">
                                        <div class="title_name"><?=$arUserList[$arCaller['prop']['MANAGER']['VALUE']]['FULL_NAME'];?></div>
                                        <div class="position"><?=$arUserList[$arCaller['prop']['MANAGER']['VALUE']]['WORK_POSITION'];?></div>
                                    </div>
                                </div>
                            <?endif;?>
                            <?if(!empty($arCaller['prop']['OPERATOR']['VALUE'])):?>
                                <div class="agent">
                                    <div class="image"><img src="<?=SITE_TEMPLATE_PATH?>/img/nophoto.jpg"></div>
                                    <div class="agent-info">
                                        <div class="title_name"><?=$arUserList[$arCaller['prop']['OPERATOR']['VALUE']]['FULL_NAME'];?></div>
                                        <div class="position"><?=$arUserList[$arCaller['prop']['OPERATOR']['VALUE']]['WORK_POSITION'];?></div>
                                    </div>
                                </div>
                            <?endif;?>
                        </div>
                        <div class="needs_info-wrap">
                            <div class="needs_info-l">
                                <a href="#" class="button btn_edit" title="Редактировать комментарий"></a>
                            </div>
                                <div class="needs_info-r">
                                    <div class="needs_info">
                                        Активных потребностей: <span><?=getActiveNeeds($arFields['prop']['CLIENT_ID']['VALUE']);?></span>
                                    </div>
                                    <?if($USER->GetID() == $arCaller['prop']['RIELTOR']['VALUE'] || !in_array($arFields['prop']['CLIENT_ID']['VALUE'], $arClientWithNeeds)):?>
                                        <a target="_blank" href="/needs/add.php?client_id=<?=$arFields['prop']['CLIENT_ID']['VALUE'];?>" class="button button_universal">+ Добавить потребность</a>
                                    <?endif;?>
                                </div>
                        </div>
                    </div>
                    
                </div>
            <?endwhile;?>
            <script>
                $('.result_row').each(function(index){
                    $(this).find('textarea').val($.trim($(this).find('.review_info').html().replace(/<br>/g,"")));
                });
            </script>
            <?break;
        case "needs":
            $IBLOCK_ID = 5; 
            
            switch($sortValue){
                case "by_date":
                    $arOrder = Array("ACTIVE_FROM"=>"DESC");
                    break;
                default:
                    $arOrder = Array("ACTIVE_FROM"=>"DESC");
            }
            
            $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "PROPERTY_IS_NEED"=>564);
            $arSelect = Array("ID", "NAME", "ACTIVE_FROM", "IBLOCK_ID", "DETAIL_TEXT", "PROPERTY_*");
            
            $additionalFilter = array();
            if(!empty($arRequestFilter['subject'])){
                $additionalFilter['SECTION_CODE'] = $arRequestFilter['subject'];
                $additionalFilter['INCLUDE_SUBSECTIONS'] = "Y";
            }
            if(!empty($arRequestFilter['city'])){
                $additionalFilter['PROPERTY_City'] = $arRequestFilter['city'];
            }
            if(!empty($arRequestFilter['district'])){
                $additionalFilter['PROPERTY_District'] = $arRequestFilter['district'];
            }
            if(!empty($arRequestFilter['mine_need'])){
                $additionalFilter['PROPERTY_Author'] = CUser::GetID();
            }
            $arFilter += $additionalFilter;
            
            $list = CIBlockElement::GetList($arOrder, $arFilter, false, $navArParams, $arSelect);
            
            $allResult = $list->NavRecordCount;
            if ($currentPage>1&&$allResult<$maxResultCount*($currentPage-1)){
                return false;
            }
            ?>
                <i>Найдено результов: <?=$allResult;?></i>
            <?while ($ob = $list->GetNextElement()):?>
                <?php
                    $arFields = $ob->GetFields();
                    $arFields['prop'] = $ob->GetProperties();
                ?> 
                <div class="entry result_row" data-id="<?=$arFields['ID'];?>">
                    <?$datetime = preg_split("/[\s]+/", $arFields['ACTIVE_FROM']);
                        if (date("d.m.Y",  MakeTimeStamp($arFields['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS")) == date("d.m.Y")){
                            $entry_day = 'Сегодня';
                        }else{
                            $entry_day = date("d.m.Y",  MakeTimeStamp($arFields['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS"));
                        }
                    ?>
                    <div class="entry_date">
                        <div class="entry_day"><?=$entry_day?></div>
                        <div class="entry_time"><?=date("H:i",  MakeTimeStamp($arFields['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS"));?></div>
                    </div>
                    <div class="entry_thumb">
                        <div class="entry_agent">Город: </div><?=getElemName($arFields['prop']['City']['VALUE']);?>
                        <?if(!empty($arFields['prop']['District']['VALUE'])):?>
                            <div class="entry_agent">Район: </div><?=getElemName($arFields['prop']['District']['VALUE']);?>
                        <?endif;?>
                        <div class="entry_agent">Клиент:</div>
                        <a target="_blank" data-id="<?=$arFields['ID'];?>" class="entry_search_client_link" href="/search/?search_input=<?=getElemName($arFields['prop']['Client']['VALUE']);?>&search_type=mans"><?=getElemName($arFields['prop']['Client']['VALUE']);?></a>
                    </div>
                    <div class="entry_description">
                        <a target="_blank" class="entry_header" href="/needs/<?=$arFields['ID'];?>/"><?=$arFields['NAME'];?></a>
                        <p class="entry_text">
                            <?=$arFields['DETAIL_TEXT'];?>
                        </p>
                        <?if($USER->GetID() == $arFields['prop']['Author']['VALUE']):?>
                            <div class="propose_button check">
                                <?if(!empty($_COOKIE['search_element_id'])):?>
                                    <?php
                                        $arCurrentNeedVariants = getVariatsList($arFields['ID']);
                                    ?>
                                    <?if(!in_array($_COOKIE['search_element_id'], $arCurrentNeedVariants)):?>
                                        <span class="propose">Добавить вариант к этой потребности</span>
                                        <a target="_blank" href="/needs/<?=$arFields['ID'];?>/" class="propose_check">Перейти в потребность</a>
                                    <?else:?>
                                        <a target="_blank" href="/needs/<?=$arFields['ID'];?>/" class="propose_check shown">Перейти в потребность</a>
                                    <?endif;?>
                                <?else:?>    
                                    <a target="_blank" class="btn_edit" href="/needs/add.php?CODE=<?=$arFields['ID'];?>"></a>
                                    <a href="/search/" class="add_propose button button_universal">Подобрать варианты</a>
                                <?endif;?>
                            </div>
                        <?endif;?>
                    </div>
                        <?php
                        $priceString = "";
                        if (!empty($arFields['prop']['PriceFrom']['VALUE']) || !empty($arFields['prop']['PriceTo']['VALUE'])){
                            if (!empty($arFields['prop']['PriceFrom']['VALUE'])){
                                $priceString = 'От '.number_format($arFields['prop']['PriceFrom']['VALUE'], 0, ',', ' ').' руб.';
                            }
                            if (!empty($arFields['prop']['PriceTo']['VALUE'])){
                                $priceString .= ' до '.number_format($arFields['prop']['PriceTo']['VALUE'], 0, ',', ' ').' руб.';
                            }
                        } else {
                            $priceString = 'Цена не указана';
                        }?>
                    <span class="entry_price"><?=$priceString;?></span>
                </div>
            <?endwhile;?>
            <?
            break;
        default:
            return false;
    }
?>