<?php
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';

    if (!$isAjax) die('no access');
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');
    CModule::IncludeModule("iblock");
    global $USER;
    $IBLOCK_ID = 18; 

    if (!empty($_REQUEST['call_id']) && is_numeric($_REQUEST['call_id']) && !empty($_REQUEST['comment_value'])){
        $elementObject = new CIBlockElement;
        $arLoadProductArray = Array(
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID"   => $IBLOCK_ID,
            "ACTIVE"  => "Y",
            "DETAIL_TEXT" => $_REQUEST['comment_value']
        );
        if($elementObject->Update($_REQUEST['call_id'], $arLoadProductArray)){
            echo true;
        } else {
            echo false;
        }
    }