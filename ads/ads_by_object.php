<?
    $haveObject = isset($_REQUEST['object_id']) && !empty($_REQUEST['object_id']);
    
    if (!$haveObject) die('no access');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');

    if (CModule::IncludeModule("iblock")){
        $IBLOCK_ID_ADV = 5; 
        $IBLOCK_ID_OBJECT = 16; 
        
        $objectElement = $_REQUEST['object_id'];
        $arOrder = Array("NAME"=>"ASC");
        $arSelect = Array("ID", "NAME", "IBLOCK_ID");
        $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID_OBJECT, "ACTIVE"=>"Y", "ID" => $objectElement);
        $list = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

        if ($ob = $list->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arFields['properties'] = $ob->GetProperties();
            if (!empty($arFields['properties']['IS_WATCH']['VALUE']) && !empty($arFields['properties']['WATCH_AGREE']['VALUE'])){
                $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID_ADV, "ACTIVE"=>"Y", "PROPERTY_OBJECT_ELEMENT" => $objectElement);
                $list = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
                if ($ob = $list->GetNextElement()) {
                    echo "Попытка повторного создания объявления";
                    return;
                } else {
                    $el = new CIBlockElement;

                    $arProperty = array();
                    $arPropertyKeys = array('District', 'City', 'Floor', 'Floors', 'Square', 'Street',
                                            'Rooms', 'MarketType', 'HOUSE', 'APARTMENT',
                                            );
                    foreach ($arPropertyKeys as $propKey){
                        $arProperty[$propKey] = $arFields['properties'][$propKey]['VALUE'];
                    }
                    
                    $arHardPropertyKeys = array('MarketType', 'HouseType', 'Rooms');   
                    foreach ($arHardPropertyKeys as $propKey){
                        $propertyEnum = getEnumValues(5, array($propKey));
                        $valueEnum = array_flip($propertyEnum[$propKey]);
                        $arProperty[$propKey] = $valueEnum[$arFields['properties'][$propKey]['VALUE']];
                    }            

                    $arProperty['AdStatus']['VALUE'] = 351;
                    $arProperty['CALLC']['VALUE'] = 513;
                    $arProperty['OBJECT_ELEMENT'] = $objectElement;
                    $arProperty['Price'] = $arFields['properties']['PRICE']['VALUE'];
                    $arProperty['Author'] = $arFields['properties']['WATCHER']['VALUE'];

                    $arLoadProductArray = Array(
                      "MODIFIED_BY"    => $USER->GetID(),
                      "IBLOCK_SECTION_ID" => 7,
                      "IBLOCK_ID"      => $IBLOCK_ID_ADV,
                      "PROPERTY_VALUES"=> $arProperty,
                      "NAME"           => "Элемент",
                      "ACTIVE"         => "Y",
                      "DATE_ACTIVE_FROM" => date("d.m.Y H:i:s"),
                      );

                    if($PRODUCT_ID = $el->Add($arLoadProductArray)){
                        echo "New ID: ".$PRODUCT_ID;
                        header("Location: http://".$_SERVER['SERVER_NAME'].'/ads/add.php?CODE='.$PRODUCT_ID);
                    } else {
                        header("Location: http://".$_SERVER['SERVER_NAME'].'/ads/add.php?OBJECT_ERROR='.$el->LAST_ERROR);
                    }
                }
            } else {
                echo "Работы по объекту не подтверждены";
            }
        } else {
            echo "Запрошенный объект отсутствует";
        }
    }