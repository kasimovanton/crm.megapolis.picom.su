<?php
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    
    if (!$isAjax) die('no access');
    
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');


    CModule::IncludeModule("iblock");
 


 
    
    if (!empty($response)){
        $result = '<select class="district" name="district">'.
                        '<option class="hidden" selected disabled>Район</option>';
        foreach($response as $key => $value){
            $result .= '<option value="'.$key.'">'.$value.'</option>';
        }    
        $result .= '</select>';


        echo $result;
    }

require($bxRoot.'modules/main/include/epilog_after.php');
