<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle('Переназначение');
?>


<?php
global $APPLICATION;
global $USER;
CModule::IncludeModule("iblock");


    if (empty($_REQUEST['agent_id']) || $_REQUEST['agent_id'] == "default"){
        exit("Не выбран агент!");
    }
    if (empty($_REQUEST['user_id'])){
        exit("Не указан текущий владелец!");
    }
    if (empty($_REQUEST['client_id']) || !is_array($_REQUEST['client_id']) ||(count($_REQUEST['client_id']) == 1 &&  $_REQUEST['client_id'][0] == "default")){
        exit("Не выбран клиент для перенаправления!");
    }
    

    $PROPERTY_REASSIGN = "REASSIGN";  // код свойства "переопределение"
    $PROPERTY_RIELTOR = "RIELTOR";  // код свойства "Автор"
    $IBLOCK_ID = 10; //инфоблок клиенты
    $ADS_IBLOCK_ID = 5; //инфоблок объявления
 
    $arOrder = Array("SORT"=>"ASC");
    $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "ID" => $_REQUEST['client_id'],
                    "PROPERTY_RIELTOR"=>$_REQUEST['user_id'], "!PROPERTY_REASSIGN"=>false
                );
    $res = CIBlockElement::GetList($arOrder, $arFilter, false, false);
    $clientLinks = "";
    while ($ob = $res->GetNextElement()){
        $arFields = $ob->GetFields();
        $clientLinks .= "<a href='http://".$_SERVER['SERVER_NAME']."/clients/".$arFields['ID']."/'>".$arFields['NAME']."</a><br/>";
        CIBlockElement::SetPropertyValuesEx($arFields['ID'], false, array($PROPERTY_REASSIGN => ""));
        CIBlockElement::SetPropertyValuesEx($arFields['ID'], false, array($PROPERTY_RIELTOR => $_REQUEST['agent_id']));
        //Получаем список объявлений удаляемого юзера
        $arSelectAds = Array("ID", "IBLOCK_ID", "ACTIVE", "NAME");
        $arFilterAds = Array("IBLOCK_ID"=>$ADS_IBLOCK_ID,"ACTIVE"=>"Y",
                            "PROPERTY_Client"=>$_REQUEST['client_id'], "PROPERTY_Author"=>$_REQUEST['user_id'],
                        );
        $resads = CIBlockElement::GetList($arOrder, $arFilterAds, false, false, $arSelectAds);
        while ($ob = $resads->GetNextElement()){
            $userAds = $ob->GetFields();
            pre($userAds);
            CIBlockElement::SetPropertyValuesEx($userAds['ID'], false, array("Author" => $_REQUEST['agent_id']));//Начальник удаляемого агента или админ
        }
    }
    
    if (intval($res->SelectedRowsCount())<1){
        echo "Введены некорректные данные переопределения клиентов!";
    } else {
    
        $newUserRs = CUser::GetByID($_REQUEST['agent_id']);
        $newUser = $newUserRs->Fetch();

        $userBoss = CUser::GetByID($_REQUEST['user_id']);
        $userBossData = $userBoss->Fetch();
        if (!empty($newUser['EMAIL'])){
            $arMail = array(
                // "EMAIL_REC" => 'kasimovar@picom.ru',
                // "EMAIL_REC" => 'kasimovanton@bk.ru',
                "EMAIL_REC" => $newUser['EMAIL'],
                "CURRENT_OWNER"=> $newUser['NAME'].' '.$newUser['SECOND_NAME'],
                "BOSS_FIO" => $userBossData['LAST_NAME'].' '.$userBossData['NAME'].' '.$userBossData['SECOND_NAME'],
                "CLIENT_LINKS" => $clientLinks,
                // "OLD_USER" => $oldUser['LAST_NAME'].' '.$oldUser['NAME'].' '.$oldUser['SECOND_NAME'],
            );
            CEvent::Send("NEW_CLIENTS", array("s1"), $arMail, "N", 28);
        }
        if (tempClientCount(CUser::GetID())){
            header("Location:http://".$_SERVER['SERVER_NAME']."/temp_clients/");
        } else {
            header("Location:http://".$_SERVER['SERVER_NAME']."/clients/");
        }
    }
    
?>
<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');?>