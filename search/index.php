<?php
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
    $APPLICATION->SetTitle("Поиск");
?>

    <?$userStatus = getUserStatus($USER->GetID());?>
    <?if (!in_array(8, $userStatus) && !($USER->IsAdmin())):?>
        У вас нет прав доступа к этому разделу
    <?else:?>
        <?php
            $iBlock = 5;
            $arFilter = array(
                "city",
            );
            $arResult["ADDITIONAL_PROPERTY"] = getEnumValues($iBlock,$arFilter);
        ?>
        <div class="filter_tabs_block">
            <div class="tabs_head_block">
                <h3 for="elements_tab"<?if (!isset($_REQUEST['search_type']) || $_REQUEST['search_type']=='elements'):?> class="active"<?endif;?>>Объявления и объекты</h3>
                <h3 for="mans_tab"<?if ($_REQUEST['search_type']=='mans'):?> class="active"<?endif;?>>Сотрудники и клиенты</h3>
            </div>
            <div class="tabs elements_tab<?if (!isset($_REQUEST['search_type']) || $_REQUEST['search_type']=='elements'):?> visible<?endif;?>" data-type="elements">
                <div class="ajax_filter">
                    <form class="elements_form" method="POST" action="">
                        <ul class="filter_row_wrap">
                            <li class="search_block subjects-el search_block_select">
                                <select name="subject">
                                    <option value=""<?if(!isset($_REQUEST['subject'])):?> selected=""<?endif;?>>Любая категория</option>
                                    <?php
                                        $arOrder = Array("SORT"=>"ASC");
                                        $arFilter = Array("IBLOCK_ID"=>5, "ACTIVE"=>"Y", "DEPTH_LEVEL"=>"1");
                                        $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
                                        $res_sec = CIBlockSection::GetList($arOrder, $arFilter, true);
                                          while($ob = $res_sec->GetNextElement()):?>
                                          <?php
                                                $arFields = $ob->GetFields();
                                            ?>
                                            <option <?if($_REQUEST['subject']==$arFields['CODE']):?>selected<?endif;?> value="<?=$arFields['CODE'];?>"><?=$arFields['NAME'];?></option>
                                        <?endwhile;?>
                                </select>
                            </li>
                            <li class="search_block type_subjects-el search_block_select">
                                <select name="type_subjects">
                                    <option value="" selected="">Любой тип</option>
                                    <?$allTypes = getEnumValues(5,array("OperationType"),true);?>
                                    <?foreach($allTypes['OperationType'] as $key => $option):?>
                                        <option <?if($_REQUEST['type_subjects']==$key):?>selected<?endif;?> value="<?=$key;?>"><?=$option;?></option>
                                    <?endforeach;?>
                                </select>
                            </li>
                            <li class="search_block search-el">
                                <div class="input-wrap">
                                    <input type="text"<?if (!empty($_REQUEST['search_input'])):?> value="<?=$_REQUEST['search_input'];?>"<?endif;?> placeholder="Поиск" name="search_input" autocomplete="off">
                                    <button type="button" class="del-input select2-search-choice-close"></button>
                                </div>
                            </li>
                            <li class="search_block city-el search_block_select">
                                <div class="input-wrap">
                                    <select name="city">
                                        <option value="" selected="">Любой город</option>
                                        <?foreach ($arResult["ADDITIONAL_PROPERTY"]['City'] as $keyId => $strCity):?>
                                            <option value="<?=$keyId;?>"><?=$strCity;?></option>
                                        <?endforeach;?>
                                    </select>
                                    <button type="button" class="del-input select2-search-choice-close"></button>
                                </div>
                            </li>
                            <li class="search_block district-el search_block_select">
                                <div class="input-wrap">
                                    <select placeholder="Любой район" name="district" multiple="multiple">
                                        <option value="237">Устиновский</option>
                                        <option value="225">Первомайский</option>
                                        <option value="224">Октябрьский</option>
                                        <option value="217">Ленинский</option>
                                        <option value="215">Индустриальный</option>
                                    </select>
                                    <button type="button" class="del-input select2-search-choice-close"></button>
                                </div>
                            </li>
                        </ul>
                        <div class="extra_filter search_extra_filter">
                        </div>
                        <div class="filter_row_wrap ajax_search_date">
                            <div class="filter_row">
                                <span class="price_text">Дата:</span>
                                <div class="input-wrap">
                                    <input class="date" id="datefrom_obj" onclick="BX.calendar({node: this, field: this, bTime: false, bHideTime:false});" type="text" name="start_date" readonly="readonly">
                                    <button type="button" class="del-input select2-search-choice-close"></button>
                                </div>
                                <input type="button" onclick="BX.calendar({node: this, field: datefrom_obj, bTime: false, bHideTime:false});" class="calendar_btn">
                                <span>&mdash;</span>
                                <div class="input-wrap">
                                    <input class="date dd_locked" id="dateto_obj" onclick="BX.calendar({node: this, field: this, bTime: false, bHideTime:false});" type="text" name="end_date" readonly="readonly">
                                    <button type="button" class="del-input select2-search-choice-close"></button>
                                </div>
                                <input type="button" onclick="BX.calendar({node: this, field: dateto_obj, bTime: false, bHideTime:false});" class="calendar_btn">
                            </div>
                        </div>
                        <div class="filter_row_wrap footer_search_row">
                            <div class="filter_row">
                                <input id="cavito" type="checkbox" name="Avito" value="441">
                                <label for="cavito">Выгружено на авито</label>
                            </div>
                            <div class="filter_row">
                                <input id="cMAIN_SITE" type="checkbox" name="MAIN_SITE" value="552">
                                <label for="cMAIN_SITE">Выгружено на сайт</label>
                            </div>
                            <div class="filter_row">
                                <input id="cphoto" class="with_photo" type="checkbox" name="with_photo">
                                <label for="cphoto">Только с фото</label>
                            </div>
                            <div class="filter_row filter_row_button">
                                <button class="button button_universal filter_reset">Сброс</button>
                            </div>
                            <div class="filter_row filter_row_button">
                                <button class="button button_universal filter_submit">Поиск</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="form_select ajax_sort">
                    <span>Сортировка:</span>
                    <select name="sort">
                        <option value="by_date">По дате добавления</option>
                        <option value="by_price_min">По цене (с дешевых)</option>
                        <option value="by_price_max">По цене (с дорогих)</option>
                    </select>
                </div>
                <div class="preload">
                    <div class="text-loader">
                        Загрузка
                    </div>
                </div>
            </div>
            
            <div class="tabs no_additional_row mans_tab<?if ($_REQUEST['search_type']=='mans'):?> visible<?endif;?>" data-type="mans">
                <div class="ajax_filter">
                    <form class="mans_form" method="POST" action="">
                        <div class="filter_row_wrap">
                            <div class="filter_row search_mans_row">
                                <div class="input-wrap">
                                    <input type="text"<?if (!empty($_REQUEST['search_input'])):?> value="<?=$_REQUEST['search_input'];?>"<?endif;?> placeholder="Поиск" name="search_input" autocomplete="off">
                                    <button type="button" class="del-input select2-search-choice-close"></button>
                                    <button class="button button_universal filter_submit">Поиск</button>
                                </div>
                            </div>
                        </div>
                        <div class="filter_row_wrap">
                            <div class="filter_row">
                                <input id="only_my" class="only_my" type="checkbox" name="only_my" value="<?=$USER->GetID();?>">
                                <label for="only_my">Только мои клиенты</label>
                            </div>
                        </div>
                        <div class="filter_row_wrap extra_man_row">
                                <div class="input-wrap">
                                    <span class="man_name"></span>
                                    <input name="extra_man_value" type="hidden">
                                    <button type="button" class="del-input select2-search-choice-close"></button>
                                </div>
                        </div>
                    </form>
                </div>
                <div class="form_select ajax_sort">
                    <span>Сортировка:</span>
                    <select>
                        <option value="abc">По алфавиту (от А до Я)</option>
                        <option value="cba">По алфавиту (от Я до А)</option>
                        <option value="date_end">По дате добавления (по убыванию)</option>
                        <option value="date_begin">По дате добавления (по возврастанию)</option>
                    </select>
                </div>
                <div class="preload">
                    <div class="text-loader">
                        Загрузка
                    </div>
                </div>
            </div>
            <div class="search_results">
                <h4 class="search_head" id="first_block_ajax">
                    <span class="elements<?if ($_REQUEST['search_type']=='elements' || !isset($_REQUEST['search_type'])):?> current<?endif;?>">Объявления</span>
                    <span class="mans<?if ($_REQUEST['search_type']=='mans'):?> current<?endif;?>">Клиенты</span>
                </h4>
                <div class="ajax_search_block_first">
                    <div class="searched">Найдено: <span>0</span></div>
                    <div class="search_ajax_list">
                    </div>
                    <div class="search_next" data-page="2" data-type="first">Показать еще (5)</div>
                    <div class="search_turn" data-type="first">Обновить</div>
                    <div class="preload" style="display:none;">
                        <div class="text-loader">
                            Загрузка
                        </div>
                    </div>
                </div>            
                <h4 class="search_head" id="second_block_ajax">
                    <span class="elements <?if ($_REQUEST['search_type']=='elements'|| !isset($_REQUEST['search_type'])):?> current<?endif;?>">Объекты</span>
                    <span class="mans<?if ($_REQUEST['search_type']=='mans'):?> current<?endif;?>">Сотрудники</span>
                </h4>
                <div class="ajax_search_block_second">
                    <div class="searched">Найдено: <span>0</span></div>
                    <div class="search_ajax_list">
                    </div>
                    <div class="search_next" data-page="2" data-type="second">Показать еще (5)</div>
                    <div class="search_turn" data-type="second">Обновить</div>
                    <div class="preload">
                        <div class="text-loader">
                            Загрузка
                        </div>
                    </div>
                </div>
            </div>
    <?endif;?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>