<?php
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';

    if (!$isAjax) die('no access');
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');


    CModule::IncludeModule("iblock");
?>
<?php
    $arCurrentFilterProperties = filterSearchProp();
    $currentFilterProperties = $arCurrentFilterProperties[$_REQUEST['subject']][$_REQUEST['type']];
?>
<div class="select_block_ajax_filter">
    <?foreach ($currentFilterProperties['selects'] as $propertyKey => $propertyArValue):?>
        <script>
            addExtraSelect('<?=$propertyKey;?>_select', '<?=$propertyArValue['caption'];?>');
        </script>
        <div class="input-wrap input-wrap-select">
            <div class="filter_row select2_row <?=$propertyKey;?>_row">
                <span class="price_text"><?=$propertyArValue['name'];?>:</span>
                <select placeholder="Любой" class="js-example-basic-multiple <?=$propertyKey;?>_select" name="<?=$propertyKey;?>">
                    <?$selectList = getEnumValues(5, array($propertyKey));?>
                    <option value="">Любой</option>
                    <?foreach ($selectList[$propertyKey] as $key => $value):?>
                        <option value="<?=$key;?>"><?=$value;?></option>
                    <?endforeach;?>
                </select>
            </div>
            <button type="button" class="del-input select2-search-choice-close"></button>
        </div>
    <?endforeach;?>
    <?foreach ($currentFilterProperties['multiselects'] as $propertyKey => $propertyArValue):?>
        <script>
            addExtraSelect('<?=$propertyKey;?>_select', '<?=$propertyArValue['caption'];?>');
        </script>
        <div class="input-wrap input-wrap-select multi-select-wrap">
            <div class="filter_row multi_select_row select2_row <?=$propertyKey;?>_row">
                <span class="price_text"><?=$propertyArValue['name'];?>:</span>
                <select placeholder="Любой" class="js-example-basic-multiple <?=$propertyKey;?>_select" multiple="multiple" name="<?=$propertyKey;?>">
                    <?$selectList = getEnumValues(5, array($propertyKey));?>
                    <?foreach ($selectList[$propertyKey] as $key => $value):?>
                        <option value="<?=$key;?>"><?=$value;?></option>
                    <?endforeach;?>
                </select>
                <button type="button" class="del-input select2-search-choice-close"></button>
            </div>
        </div>
    <?endforeach;?>
</div>
<div class="slider_block_ajax_filter">
    <?foreach ($currentFilterProperties['sliders'] as $propertyKey => $propertyArValue):?>
        <?if ($propertyKey!='Price'):?>
            <script>
                addExtraSlider('<?=$propertyKey;?>_dragger', <?=$propertyArValue['min'];?>, <?=$propertyArValue['max'];?>, '<?=$propertyKey;?>', <?=$propertyArValue['step'];?>);
            </script>
            <div class="filter_row">
                <span class="price_text"><?=$propertyArValue['name'];?>:</span>
                <div class="price_dragger extra_dragger <?=$propertyKey;?>_dragger"></div>
                <input class="<?=$propertyKey;?> min_<?=$propertyKey;?>" type="hidden">
                <input class="<?=$propertyKey;?> max_<?=$propertyKey;?>" type="hidden">
            </div>
        <?else:?>
            <div class="filter_row price_ajax_search select2_row">
                <span class="price_text"><?=$propertyArValue['name'];?>:</span>
                <div class="input-wrap">
                    <input autocomplete="off" placeholder="От" class="<?=$propertyKey;?> min_<?=$propertyKey;?>" type="text" name="min_Price">
                    <button type="button" class="del-input select2-search-choice-close"></button>
                </div>
                <span>&mdash;</span>
                <div class="input-wrap">
                    <input autocomplete="off" placeholder="до" class="<?=$propertyKey;?> max_<?=$propertyKey;?>" type="text" name="max_Price">
                    <button type="button" class="del-input select2-search-choice-close"></button>
                </div>
                <script>
                    addExtraSelect('price_type_select', 'Типов');
                </script>
                <select name="price_type" class="price_type_select">
                    <option value="one">руб.</option>
                    <option value="thou">тыс. руб.</option>
                    <option value="mln">млн. руб.</option>
                </select>
            </div>
        <?endif;?>
    <?endforeach;?>
</div>
<?if (isset($currentFilterProperties['objects']) && !empty($currentFilterProperties['objects'])):?>
    <div class="object_properties_filter">
        <div class="filter_row status_row">
            <script>
                addExtraSelect('object_status_select', 'Статусов');
            </script>
            <div class="input-wrap input-wrap-select">
                <span class="price_text">Статус объекта:</span>
                <select placeholder="Любой" name="object_status" class="object_status_select" multiple="multiple">
                <?php
                    $arStatusOrder = Array("SORT"=>"asc", "ID"=>"asc");
                    $arStatusSelect = Array("ID", "NAME", "PROPERTY_COLOR", "PROPERTY_TITLE");
                    $arStatusFilter = Array("IBLOCK_ID"=>17, "ACTIVE"=>"Y");
                    $res = CIBlockElement::GetList($arStatusOrder, $arStatusFilter, false, false, $arStatusSelect);
                    $arStatusObjectsList = array();
                    
                    while ($ob = $res->GetNextElement()):
                ?>
                    <?php
                        $statusFields = $ob->GetFields();
                    ?>
                    <option value="<?=$statusFields['ID'];?>"><?=$statusFields['NAME'];?></option>
                <?endwhile;?>
                   
                </select>
                <button type="button" class="del-input select2-search-choice-close"></button>
            </div>
        </div>
    </div>
<?endif;?>