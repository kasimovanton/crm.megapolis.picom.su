<?php
    $isAjax = isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'y';
    if (!$isAjax) die('no access');
    $bxRoot = $_SERVER['DOCUMENT_ROOT'].'/bitrix/';
    require($bxRoot.'modules/main/include/prolog_before.php');
    CModule::IncludeModule("iblock");
    global $USER;
    $maxResultCount = 5;
    $arRequestFilter = $_REQUEST['filter'];
    $infoType = $_REQUEST['type'];
    $sortValue = $_REQUEST['sort'];
    $currentPage = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
    $navArParams = array('nPageSize'=>$maxResultCount, 'iNumPage'=>$currentPage);
    if(!empty($infoType)){
        $allUsers = getUsers(array("UF_HEAD"),array("ID",'LAST_NAME','LAST_LOGIN','SECOND_NAME','NAME'),array('ACTIVE'=>'Y'));
        
        $allClients = array();
        $arFilter = Array("IBLOCK_ID"=>10, "ACTIVE"=>"Y");
        $arOrder = Array("ID"=>"ASC");
        $arSelect = Array("ID", "NAME");
        $list = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
        while ($ob = $list->GetNextElement()) {
            $arFields = $ob->GetFields();
            $allClients[$arFields['ID']] = $arFields['NAME'];
        }
                switch($arRequestFilter['price_type']){
                    case "one":
                        $multuParam = 1;
                        break;
                    case "thou":
                        $multuParam = 1000;
                        break;
                    case "mln":
                        $multuParam = 1000000;
                        break;
                    default:
                        $multuParam = 1;
                }
                
        if(!empty($_COOKIE['search_element_id'])){
            $arCurrentNeedVariants = getVariatsList($_COOKIE['search_element_id']);
            $arCurrentObjectNeedVariants = getVariatsList($_COOKIE['search_element_id'], true);
            
            $arCheckOrder = Array("ID"=>"DESC");
            $arCheckSelect = Array("ID", "NAME", "IBLOCK_ID", "ACTIVE_FROM", "DETAIL_TEXT", "CREATED_BY", "PROPERTY_*");
            $arCheckFilter = Array("IBLOCK_ID"=>5, "ACTIVE"=>"Y" , "ID" => CIBlockElement::SubQuery("PROPERTY_ADVERT", array(
                                                "IBLOCK_ID" => 19,
                                                "!PROPERTY_NEED_BY" => false,
                                              )),);
            $arCheck = CIBlockElement::GetList($arCheckOrder, $arCheckFilter, false, false, $arCheckSelect);
            $arChoicedAdvert = array();
            if ($obCheck = $arCheck->GetNextElement()) {
                $arFields = $obCheck->GetFields();
                $arChoicedAdvert[] = $arFields['ID'];
            }
        }                
    }

    switch($infoType){
        case "elements":
            if (!isset($_REQUEST['blockType']) || empty($_REQUEST['blockType']) || $_REQUEST['blockType'] == 'first'){
                $IBLOCK_ID_ADS = 5;

                $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID_ADS, "ACTIVE"=>"Y", "INCLUDE_SUBSECTIONS" => "Y");
                if (!empty($arRequestFilter['subject'])){
                    if (!empty($arRequestFilter['type_subjects'])){
                        $arFilter['SECTION_CODE'] = $arRequestFilter['subject'].'_'.$arRequestFilter['type_subjects'];
                    } else {
                        $arFilter['SECTION_CODE'] = $arRequestFilter['subject'];
                    }
                }
                if (!empty($arRequestFilter['city'])){
                    $arFilter['PROPERTY_CITY'] = $arRequestFilter['city'];
                }
                if (!empty($arRequestFilter['district'])){
                    $arFilter['PROPERTY_DISTRICT'] = $arRequestFilter['district'];
                }
                if (!empty($arRequestFilter['start_date'])){
                    $arFilter['>=DATE_ACTIVE_FROM'] = $arRequestFilter['start_date'];
                }    
                if (!empty($arRequestFilter['end_date'])){
                    $arFilter['<=DATE_ACTIVE_FROM'] = date("d.m.Y", MakeTimeStamp($arRequestFilter['end_date'])+86400);
                }   

                /*from extra_filter*/
                foreach($arRequestFilter as $keyFilter => $filterValue){
                    if (is_array($filterValue)){
                        if (isset($filterValue['min'])){
                            $arFilter['>=PROPERTY_'.$keyFilter] = $filterValue['min'];
                        }
                        if (!empty($filterValue['max'])){
                            $arFilter['<=PROPERTY_'.$keyFilter] = $filterValue['max'];
                        }
                    }
                }

                if (!empty($arRequestFilter['min_Price'])){
                    $arFilter['>=PROPERTY_PRICE'] = $arRequestFilter['min_Price']*$multuParam;
                }
                if (!empty($arRequestFilter['max_Price'])){
                    $arFilter['<=PROPERTY_PRICE'] = $arRequestFilter['max_Price']*$multuParam;
                }
                
                if (!empty($arRequestFilter['Rooms'])){
                    $arFilter['PROPERTY_Rooms'] = $arRequestFilter['Rooms'];
                }                      
                if (!empty($arRequestFilter['MarketType'])){
                    $arFilter['PROPERTY_MarketType'] = $arRequestFilter['MarketType'];
                }                
                if (!empty($arRequestFilter['HouseType'])){
                    $arFilter['PROPERTY_HouseType'] = $arRequestFilter['HouseType'];
                }      
                if (!empty($arRequestFilter['Avito'])){  
                    $arFilter['PROPERTY_Avito'] = $arRequestFilter['Avito'];
                }
                if (!empty($arRequestFilter['MAIN_SITE'])){  
                    $arFilter['PROPERTY_MAIN_SITE'] = $arRequestFilter['MAIN_SITE'];
                }
                if (!empty($arRequestFilter['with_photo'])){  
                    $with_image_array = array();
                    $with_image_array["LOGIC"] = "OR";
                    $with_image_array[] = array('!=PROPERTY_IMAGES'=>false);
                    $with_image_array[] = array('!=DETAIL_PICTURE'=>false);
                    $arFilter[] = $with_image_array;
                }    
                
                if(!empty($arRequestFilter['search_input'])){
                    $new_array = array();
                    $new_array["LOGIC"] = "AND";
                    $arRequest = preg_split('/ /', $arRequestFilter['search_input']);
                    foreach ($arRequest as $filterString){
                        if(!empty($filterString)){
                            $newsub_array = array();
                            $newsub_array["LOGIC"] = "OR";
                            $newsub_array[] = array('DETAIL_TEXT'=>'%'.$filterString.'%');
                            $newsub_array[] = array('PROPERTY_Street'=>'%'.$filterString.'%');
                            $newsub_array[] = array('NAME'=>'%'.$filterString.'%');
                            $new_array[] = $newsub_array;
                        }
                    }
                    $arFilter[] = $new_array;
                }
                /*from extra_filter*/
                
                switch($sortValue){
                    case "by_date":
                        $arOrder = Array("ACTIVE_FROM"=>"DESC");
                        break;
                    case "by_price_min":
                        $arOrder = Array("PROPERTY_PRICE"=>"ASC,NULLS");
                        break;
                    case "by_price_max":
                        $arOrder = Array("PROPERTY_PRICE"=>"DESC,NULLS");
                        break;
                    default:
                        $arOrder = Array("ACTIVE_FROM"=>"DESC");  
                }
                
                $arSelect = Array("ID", "NAME", "IBLOCK_ID", "ACTIVE_FROM", "DETAIL_PICTURE", "DETAIL_PAGE_URL", "DETAIL_TEXT", "PROPERTY_*");
                // $arSelect = Array('*');
                
                $list = CIBlockElement::GetList($arOrder, $arFilter, false, $navArParams, $arSelect);

                $arResponse = array('first_length' => $list->NavRecordCount);
                while ($ob = $list->GetNextElement()) {
                    $arFields = $ob->GetFields();
                    $arFields['prop'] = $ob->GetProperties();
                    // pre($arFields);
                    // exit();
                    $responseElement = array();
                    $responseElement['id'] = $arFields['ID'];
                    $responseElement['href'] = $arFields['DETAIL_PAGE_URL'];
                    $responseElement['name'] = $arFields['NAME'];
                    $responseElement['info'] = $arFields['DETAIL_TEXT'];
                    if (preg_match('/prodam/', $arFields['DETAIL_PAGE_URL'])){
                        $responseElement['offer'] = true;
                        if(!empty($_COOKIE['search_element_id'])){
                            $responseElement['offer_with_need'] = $_COOKIE['search_element_id'];
                            if(in_array($arFields['ID'], $arCurrentNeedVariants)){
                                $responseElement['already_in_need'] = true;
                            } else {
                                $responseElement['already_in_need'] = false;
                            }
                            if(in_array($arFields['ID'], $arChoicedAdvert)){
                                $responseElement['already_in_choice'] = true;
                            } else {
                                $responseElement['already_in_choice'] = false;
                            }
                        }
                    } else {
                        $responseElement['offer'] = false;
                    }
                    
                    if (!empty($arFields['DETAIL_PICTURE'])){
                        $mainPhoto = $arFields['DETAIL_PICTURE'];
                    } else {
                        if (!empty($arFields['prop']['Images']['VALUE'][0])){
                            $mainPhoto = $arFields['prop']['Images']['VALUE'][0];
                        } else {
                            //связанные объекты - из шаблона списка объявления
                            $mainPhoto = false;
                        }
                    }
                    
                    if ($mainPhoto){
                        $renderImage = CFile::ResizeImageGet($mainPhoto, Array("width" => '120px', "height" => '90px'),BX_RESIZE_IMAGE_EXACT);
                        $responseElement['photo'] = $renderImage['src'];
                    }

                    $responseElement['rieltor_id'] = $arFields['prop']['Author']['VALUE'];
                    $responseElement['rieltor_name'] = mb_substr(trim($allUsers[$arFields['prop']['Author']['VALUE']]['NAME']), 0, 1, "UTF-8").'. '.$allUsers[$arFields['prop']['Author']['VALUE']]['LAST_NAME'];

                    if (!empty($arFields['prop']['OBJECT_ELEMENT']['VALUE'])){
                        $dbProps = CIBlockElement::GetProperty(16, $arFields['prop']['OBJECT_ELEMENT']['VALUE'], array("sort" => "asc"), Array("CODE"=>"Client"));
                        if($arProps = $dbProps->Fetch()){
                            $responseElement['client_id'] = IntVal($arProps["VALUE"]);
                            $responseElement['client_name'] = $allClients[IntVal($arProps["VALUE"])];
                        }
                    } else {
                        $responseElement['client_id'] = $arFields['prop']['Client']['VALUE'];
                        $responseElement['client_name'] = $allClients[$arFields['prop']['Client']['VALUE']];
                    }
                    $responseElement['address'] = $arFields['prop']['Street']['VALUE'];
                    if(!empty($arFields['prop']['HOUSE']['VALUE'])){$responseElement['address'] .= ' '.$arFields['prop']['HOUSE']['VALUE'];}
                    if(!empty($arFields['prop']['APARTMENT']['VALUE'])){$responseElement['address'] .= ', '.$arFields['prop']['APARTMENT']['VALUE'];}
                    $responseElement['avito'] = $arFields['prop']['Avito']['VALUE'] ? true : false;
                    $responseElement['callc'] = $arFields['prop']['CALLC']['VALUE'] ? true : false;
                    if (!empty($arFields['prop']['Price']['VALUE'])){
                        $responseElement['price'] = number_format($arFields['prop']['Price']['VALUE'], 0, ',', ' ').' руб.';
                    } else {
                        if (!empty($arFields['prop']['PriceFrom']['VALUE']) || !empty($arFields['prop']['PriceTo']['VALUE'])){
                            if (!empty($arFields['prop']['PriceFrom']['VALUE'])){
                                $responseElement['price'] = 'От '.number_format($arFields['prop']['PriceFrom']['VALUE'], 0, ',', ' ').' руб.';
                            }
                            if (!empty($arFields['prop']['PriceTo']['VALUE'])){
                                $responseElement['price'] .= ' до '.number_format($arFields['prop']['PriceTo']['VALUE'], 0, ',', ' ').' руб.';
                            }
                        } else {
                            $responseElement['price'] = 'Цена не указана';
                        }
                    }
                    
                    $datetime = preg_split("/[\s]+/", $arFields['ACTIVE_FROM']);
                    if (date("d.m.Y",  MakeTimeStamp($arFields['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS")) == date("d.m.Y")){
                        $entry_day = 'Сегодня';
                    }else{
                        $entry_day = date("d.m.Y",  MakeTimeStamp($arFields['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS"));
                    }
                    $responseElement['day'] = $entry_day;
                    $responseElement['time'] = date("H:i",  MakeTimeStamp($arFields['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS"));

                    $arResponse['first'][] = $responseElement;
                    
                }
            }
            if (!isset($_REQUEST['blockType']) || empty($_REQUEST['blockType']) || $_REQUEST['blockType'] == 'second'){
                $arStatusOrder = Array("SORT"=>"asc", "ID"=>"asc");
                $arStatusSelect = Array("ID", "NAME", "PROPERTY_COLOR", "PROPERTY_TITLE");
                $arStatusFilter = Array("IBLOCK_ID"=>17, "ACTIVE"=>"Y");
                $res = CIBlockElement::GetList($arStatusOrder, $arStatusFilter, false, false, $arStatusSelect);
                $arStatusObjectsList = array();
                
                while ($ob = $res->GetNextElement()){
                    $statusFields = $ob->GetFields();
                    $arStatusObjectsList[$statusFields['ID']] = $statusFields;
                }

                
                $IBLOCK_ID_OBJ = 16;
                $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID_OBJ, "ACTIVE"=>"Y");
                if ((!empty($arRequestFilter['subject']) && $arRequestFilter['subject']!='kvartiry') 
                    || (!empty($arRequestFilter['type_subjects']) && $arRequestFilter['type_subjects']!='prodam')){
                     $arFilter['ID'] = false;
                }
                
                if (!empty($arRequestFilter['start_date'])){
                    $arFilter['>=PROPERTY_CURRENT_DATE_OPTION'] = date('Y-m-d H:i:s', timeExplode($arRequestFilter['start_date']));
                }    
                if (!empty($arRequestFilter['end_date'])){
                    $arFilter['<=PROPERTY_CURRENT_DATE_OPTION'] = date('Y-m-d H:i:s', timeExplode($arRequestFilter['end_date'])+86400);
                } 
                if (!empty($arRequestFilter['MarketType'])){
                    $arFilter['PROPERTY_MarketType'] = $arRequestFilter['MarketType'];
                }                
                if (!empty($arRequestFilter['HouseType'])){
                    $arFilter['PROPERTY_HouseType'] = $arRequestFilter['HouseType'];
                }              
                if (!empty($arRequestFilter['object_status'])){
                    $arFilter['PROPERTY_STATUS'] = $arRequestFilter['object_status'];
                }      
                if(!empty($arRequestFilter['search_input'])){
                    $new_array = array();
                    $new_array["LOGIC"] = "AND";
                    $arRequest = preg_split('/ /', $arRequestFilter['search_input']);
                    foreach ($arRequest as $filterString){
                        if(!empty($filterString)){
                            $newsub_array = array();
                            $newsub_array["LOGIC"] = "OR";
                            $newsub_array[] = array('DETAIL_TEXT'=>'%'.$filterString.'%');
                            $newsub_array[] = array('NAME'=>'%'.$filterString.'%');
                            $new_array[] = $newsub_array;
                        }
                    }
                    $arFilter[] = $new_array;
                }
                
                if (!empty($arRequestFilter['city'])){
                    $arFilter['PROPERTY_CITY'] = $arRequestFilter['city'];
                }
                if (!empty($arRequestFilter['district'])){
                    $arFilter['PROPERTY_DISTRICT'] = $arRequestFilter['district'];
                }
                
                $newPriceArray = array();
                $newPriceArray["LOGIC"] = "AND";
                $newPriceArray[] = array('!=PROPERTY_PRICE'=>false);
                if (!empty($arRequestFilter['min_Price'])){
                    $newPriceArray[] = array('>=PROPERTY_PRICE'=>$arRequestFilter['min_Price']*$multuParam);
                }
                if (!empty($arRequestFilter['max_Price'])){
                    $newPriceArray[] = array('<=PROPERTY_PRICE'=>$arRequestFilter['max_Price']*$multuParam);
                }
                $arFilter[] = $newPriceArray;
                
                foreach($arRequestFilter as $keyFilter => $filterValue){
                    if (is_array($filterValue) && $keyFilter != 'Rooms'){
                        if (isset($filterValue['min'])){
                            $arFilter['>=PROPERTY_'.$keyFilter] = $filterValue['min'];
                        }
                        if (!empty($filterValue['max'])){
                            $arFilter['<=PROPERTY_'.$keyFilter] = $filterValue['max'];
                        }
                    }
                }
                if (!empty($arRequestFilter['Rooms'])){

                    $roomsAds = getEnumValues(5, array('Rooms'));
                    $roomsObjects = getEnumValues(16, array('Rooms'));
                    $roomsRelation = array();

                    $arFilter['PROPERTY_Rooms'] = array();
                    
                    foreach($arRequestFilter['Rooms'] as $value){
                        $arFilter['PROPERTY_Rooms'][] = array_search($roomsAds['Rooms'][$value], $roomsObjects['Rooms']);
                        
                    }
                }      

                
                switch($sortValue){
                    case "by_date":
                        $arOrder = Array("ACTIVE_FROM"=>"DESC");
                        break;
                    case "by_price_min":
                        $arOrder = Array("PROPERTY_PRICE"=>"ASC, NULLS");
                        break;
                    case "by_price_max":
                        $arOrder = Array("PROPERTY_PRICE"=>"DESC, NULLS");
                        break;
                    default:
                        $arOrder = Array("ACTIVE_FROM"=>"DESC");  
                }

                $arSelect = Array("ID", "NAME", "IBLOCK_ID", "DETAIL_TEXT", "PROPERTY_*");
                
                $list = CIBlockElement::GetList($arOrder, $arFilter, false, $navArParams, $arSelect);
                $arResponse['second_length'] = $list->NavRecordCount;
                while ($ob = $list->GetNextElement()) {
                    $arFields = $ob->GetFields();
                    $arFields['prop'] = $ob->GetProperties();
                    $responseElement = array();
                    $responseElement['id'] = $arFields['ID'];
                    $responseElement['name'] = $arFields['NAME'];
                    if (!empty($arFields['prop']['PRICE']['VALUE'])){
                        $responseElement['price'] = number_format($arFields['prop']['PRICE']['VALUE'], 0, ',', ' ').' руб.';
                    } else {
                        $responseElement['price'] = 'Цена не указана';
                    }
                    $arFirstDate = explode(' ', $arFields['prop']['FIRST_CONTACT']['VALUE']);
                    $responseElement['first_contact'] = $arFirstDate[0];
                    

                    $arr = ParseDateTime($arFields['prop']['CURRENT_DATE_OPTION']['VALUE'], FORMAT_DATETIME);
                    $checkPointA = timeExplode($arFields['prop']['CURRENT_DATE_OPTION']['VALUE']);
                    if ($checkPointA - timeExplode(date("d.m.Y H:i:s"))<0){
                        $responseElement['late'] = true;
                    }
                    
                    if (!in_array($arFields['prop']['STATUS']['VALUE'], array(8977, 8978, 8979, 11106, 11377))){
                        $responseElement['date'] = $arr["DD"]." ".ToLower(GetMessage("MONTH_".intval($arr["MM"])."_S"))." ".$arr["YYYY"];
                        $responseElement['time'] = date("H:i",  MakeTimeStamp($arFields['prop']['CURRENT_DATE_OPTION']['VALUE'], "DD.MM.YYYY HH:MI:SS"));
                    } else {
                        $responseElement['comment'] = $arFields['prop']['CURRENT_COMMENT_OPTION']['VALUE'];
                    }
                    
                    
                    $responseElement['client_id'] = $arFields['prop']['Client']['VALUE'];
                    $responseElement['client_name'] = $allClients[$arFields['prop']['Client']['VALUE']];
                    
                    $responseElement['rieltor'] = $allUsers[$arFields['prop']['WATCHER']['VALUE']]['LAST_NAME'].' '.$allUsers[$arFields['prop']['WATCHER']['VALUE']]['NAME'].' '.$allUsers[$arFields['prop']['WATCHER']['VALUE']]['SECOND_NAME'];
                    $responseElement['rieltor_id'] = $arFields['prop']['WATCHER']['VALUE'];
                    
                    $responseElement['is_watch'] = $arFields['prop']['IS_WATCH']['VALUE']? true : false;
                    
                    $responseElement['district'] = getElemName($arFields['prop']['District']['VALUE']);
                    $responseElement['markettype'] = $arFields['prop']['MarketType']['VALUE'];
                    $responseElement['housetype'] = $arFields['prop']['HouseType']['VALUE'];
                    $responseElement['rooms'] = $arFields['prop']['Rooms']['VALUE'];
                    $responseElement['square'] = $arFields['prop']['Square']['VALUE'];
                    $responseElement['floor'] = $arFields['prop']['Floor']['VALUE'];
                    $responseElement['floors'] = $arFields['prop']['Floors']['VALUE'] ? '/'.$arFields['prop']['Floors']['VALUE'] : 'этаж';

                    $responseElement['status_name'] = $arStatusObjectsList[$arFields['prop']['STATUS']['VALUE']]['NAME'];
                    $responseElement['status_title'] = $arStatusObjectsList[$arFields['prop']['STATUS']['VALUE']]['PROPERTY_TITLE_VALUE'];
                    $responseElement['status_color'] = $arStatusObjectsList[$arFields['prop']['STATUS']['VALUE']]['PROPERTY_COLOR_VALUE'];
                    

                    if(!empty($_COOKIE['search_element_id'])){
                        $responseElement['offer_with_need'] = $_COOKIE['search_element_id'];
                        if(in_array($arFields['ID'], $arCurrentObjectNeedVariants)){
                            $responseElement['already_in_need'] = true;
                        } else {
                            $responseElement['already_in_need'] = false;
                        }
                    }
                    
                    $arResponse['second'][] = $responseElement;
                }
            }
            echo json_encode($arResponse, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_HEX_QUOT);
            break;
        case "mans":
            if (!isset($_REQUEST['blockType']) || empty($_REQUEST['blockType']) || $_REQUEST['blockType'] == 'first'){
            
                $arClientWithNeeds = getClientIDListWithNeeds();
            
                $IBLOCK_ID_CLIENT = 10; 
                $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID_CLIENT, "ACTIVE"=>"Y");
                switch($sortValue){
                    case "date_begin":
                        $arOrder = Array("ACTIVE_FROM"=>"ASC");
                        break;
                    case "date_end":
                        $arOrder = Array("ACTIVE_FROM"=>"DESC");
                        break;
                    case "abc":
                        $arOrder = Array("NAME"=>"ASC"); 
                        break;
                    case "cba":
                        $arOrder = Array("NAME"=>"DESC"); 
                        break;
                    default:
                        $arOrder = Array("NAME"=>"ASC");  
                }
                $arSelect = Array("ID", "NAME", "IBLOCK_ID", "DETAIL_TEXT", "PREVIEW_TEXT", "ACTIVE_FROM", "PROPERTY_*");
                
                if (!empty($arRequestFilter['only_my'])){  
                    $arFilter['PROPERTY_RIELTOR'] = $arRequestFilter['only_my'];
                }
                
                if (!empty($arRequestFilter['extra_man_value'])){
                    $arFilter['ID'] = $arRequestFilter['extra_man_value'];
    
                } else {
                    if(!empty($arRequestFilter['search_input'])){
                        $new_array = array();
                        $new_array["LOGIC"] = "AND";
                        $arRequest = preg_split('/ /', $arRequestFilter['search_input']);
                        foreach ($arRequest as $filterString){
                            if(!empty($filterString)){
                                $newsub_array = array();
                                $newsub_array["LOGIC"] = "OR";
                                $newsub_array[] = array('PROPERTY_PHONE'=>'%'.$filterString.'%');
                                $newsub_array[] = array('PROPERTY_DOP_PHONE'=>'%'.$filterString.'%');
                                $newsub_array[] = array('NAME'=>'%'.$filterString.'%');
                                $new_array[] = $newsub_array;
                            }
                        }
                        $arFilter[] = $new_array;
                    }
                }

                $list = CIBlockElement::GetList($arOrder, $arFilter, false, $navArParams, $arSelect);
                $arResponse = array('first_length' => $list->NavRecordCount);
                while ($ob = $list->GetNextElement()) {
                    $arFields = $ob->GetFields();
                    $arFields['prop'] = $ob->GetProperties();
                    $responseElement = array();
                    $responseElement['id'] = $arFields['ID'];
                    $responseElement['name'] = $arFields['NAME'];
                    $responseElement['description'] = $arFields['PREVIEW_TEXT'];
                    $responseElement['date'] = $arFields['ACTIVE_FROM'];
                    $responseElement['phone'] = $arFields['prop']['PHONE']['VALUE'];
                    $responseElement['dopphone'] = $arFields['prop']['DOP_PHONE']['VALUE'];
                    $responseElement['face'] = $arFields['prop']['DOP_FACE']['VALUE'];
                    $responseElement['need_count'] = getActiveNeeds($arFields['ID']);
                    if($arFields['prop']['RIELTOR']['VALUE'] == $USER->GetID() || !in_array($arFields['ID'], $arClientWithNeeds)){
                        $responseElement['can_add_need'] =  true;
                    }
                    if ($arFields['prop']['RIELTOR']['VALUE'] == $USER->GetID()){
                        $responseElement['owner'] =  true;
                    }
                    $responseElement['rieltor'] = $allUsers[$arFields['prop']['RIELTOR']['VALUE']]['LAST_NAME'].' '.$allUsers[$arFields['prop']['RIELTOR']['VALUE']]['NAME'].' '.$allUsers[$arFields['prop']['RIELTOR']['VALUE']]['SECOND_NAME'];
                    $responseElement['manager'] = $allUsers[$arFields['prop']['MANAGER']['VALUE']]['LAST_NAME'].' '.$allUsers[$arFields['prop']['MANAGER']['VALUE']]['NAME'].' '.$allUsers[$arFields['prop']['MANAGER']['VALUE']]['SECOND_NAME'];
                    $arResponse['first'][] = $responseElement;
                }
            }
            if (!isset($_REQUEST['blockType']) || empty($_REQUEST['blockType']) || $_REQUEST['blockType'] == 'second'){
                $filter = Array();
                if (empty($arRequestFilter['only_my'])){  
                    if (!empty($arRequestFilter['extra_man_value'])){
                        $filter['ID'] = $arRequestFilter['extra_man_value'];
                        $filter['ACTIVE'] = "Y";
                    }
                } else {
                    $filter['ID'] = 0;
                }

                if(!empty($arRequestFilter['search_input']) && empty($arRequestFilter['extra_man_value']) && empty($arRequestFilter['only_my'])){
                    $arRequest = preg_split('/ /', $arRequestFilter['search_input']);
                    $nameFilter = "";
                    foreach ($arRequest as $filterString){
                        if(!empty($filterString) && !preg_match('/\+/', $filterString) && !preg_match('/\(/', $filterString)){
                            $nameFilter .= $filterString.'|';
                        }
                    }
                    $filter = array("NAME" => $arRequestFilter['search_input'], 'ACTIVE'=>'Y');
                    $rsUsers['NameUsers'] = CUser::GetList(($by="LAST_NAME"), ($order="asc"), $filter, array('NAV_PARAMS'=>$navArParams, 'SELECT'=>array("UF_*")));
                    $filter = array("PERSONAL_PHONE" => $nameFilter, 'ACTIVE'=>'Y');
                    $rsUsers['PhoneUsers']  = CUser::GetList(($by="LAST_NAME"), ($order="asc"), $filter, array('NAV_PARAMS'=>$navArParams, 'SELECT'=>array("UF_*")));
                } else {
                    $rsUsers = CUser::GetList(($by="ID"), ($order="desc"), $filter, array('NAV_PARAMS'=>$navArParams, 'SELECT'=>array("UF_*")));
                }
                
                if (is_array($rsUsers)){
                    $arResponse['second_length'] = $rsUsers['NameUsers']->NavRecordCount + $rsUsers['PhoneUsers']->NavRecordCount + $rsUsers['MobileUsers']->NavRecordCount;
                    foreach($rsUsers as $arRsUser){
                        while ($user = $arRsUser->GetNext()) {
                            $arUser = array();
                            $arUser['name'] = $user['LAST_NAME'].' '.$user['NAME'].' '.$user['SECOND_NAME'];
                            $arUser['last_login'] = $user['LAST_LOGIN'];
                            $arUser['date'] = date('d.m.Y', strtotime($user['DATE_REGISTER']));
                            $arImage = CFile::ResizeImageGet($user['PERSONAL_PHOTO'], Array("width" => '100px', "height" => '100px'),BX_RESIZE_IMAGE_EXACT  );
                            $arUser['photo'] = $arImage['src'];
                            $arUser['id'] = $user['ID'];
                            $arUser['phone'] = $user['PERSONAL_PHONE'];
                            $arUser['head'] = $allUsers[$user['UF_HEAD']]['LAST_NAME'].' '.$allUsers[$user['UF_HEAD']]['NAME'].' '.$allUsers[$user['UF_HEAD']]['SECOND_NAME'];
                            $arResponse['second'][] = $arUser;
                        }
                    }
                } else {
                    $arResponse['second_length'] = $rsUsers->NavRecordCount;
                    while ($user = $rsUsers->GetNext()) {
                            $arUser = array();
                            $arUser['name'] = $user['LAST_NAME'].' '.$user['NAME'].' '.$user['SECOND_NAME'];
                            $arUser['last_login'] = $user['LAST_LOGIN'];
                            $arUser['date'] = date('d.m.Y', strtotime($user['DATE_REGISTER']));
                            $arImage = CFile::ResizeImageGet($user['PERSONAL_PHOTO'], Array("width" => '100px', "height" => '100px'),BX_RESIZE_IMAGE_EXACT  );
                            $arUser['photo'] = $arImage['src'];
                            $arUser['id'] = $user['ID'];
                            $arUser['phone'] = $user['PERSONAL_PHONE'];
                            $arUser['head'] = $allUsers[$user['UF_HEAD']]['LAST_NAME'].' '.$allUsers[$user['UF_HEAD']]['NAME'].' '.$allUsers[$user['UF_HEAD']]['SECOND_NAME'];
                            $arResponse['second'][] = $arUser;
                    }
                }
            }
            echo json_encode($arResponse, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_HEX_QUOT);
            break;
        default:
            echo json_encode(false);
    }
?>